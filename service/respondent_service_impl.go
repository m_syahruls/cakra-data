package service

import (
	"log"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/bson"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type RespondentServiceImpl struct {
	RespondentRepository     repository.RespondentRepository
	ProgressSurveyRepository repository.ProgresSurveyRepository
	ProgressSurveySummary    repository.ProgresSurveySumRepository
	ResponseRepository       repository.ResponseRepository
	Validate                 *validator.Validate
}

type RespondentService interface {
	CreateRespondentForSurvey(ctx *fiber.Ctx, request web.CreateRespondent) web.RespondentResponse
	FindAll(page, limit, surveyorId string) []web.UserProgressRespondentDetail
	FindById(respondentId string) web.RespondentResponse
	Update(ctx *fiber.Ctx, userid string, request web.UserUpdateRequest) web.RespondentResponse
	Delete(ctx *fiber.Ctx, respondentId string)
}

func NewRespondentService(respondentRepository repository.RespondentRepository, progresSurveyRepos repository.ProgresSurveyRepository, progresSurveySumRepos repository.ProgresSurveySumRepository, responseRepository repository.ResponseRepository, validate *validator.Validate) RespondentService {
	return &RespondentServiceImpl{
		RespondentRepository:     respondentRepository,
		ProgressSurveyRepository: progresSurveyRepos,
		ProgressSurveySummary:    progresSurveySumRepos,
		ResponseRepository:       responseRepository,
		Validate:                 validate,
	}
}

func (service *RespondentServiceImpl) CreateRespondentForSurvey(ctx *fiber.Ctx, request web.CreateRespondent) web.RespondentResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	if !helper.IsNumeric(request.Phone) {
		panic(exception.NewError(fiber.StatusBadRequest, "phone should numeric"))
	}

	if len([]rune(request.Phone)) < 10 || len([]rune(request.Phone)) > 13 {
		panic(exception.NewError(fiber.StatusBadRequest, "phone should 10-13 digit"))
	}

	var responden domain.Respondent

	if responden, _ = service.RespondentRepository.FindByQuery(bson.M{"phone": request.Phone}); responden.Id == "" {
		//Create respondent
		responden = domain.Respondent{
			Id:        strings.ToLower(randstr.String(10)),
			Name:      request.Name,
			Phone:     request.Phone,
			Email:     request.Email,
			Gender:    request.Gender,
			CreatedAt: time.Now(),
		}
		service.RespondentRepository.Create(responden)
	}

	return helper.ToRespondentResponse(responden)
}

func (service *RespondentServiceImpl) FindById(respondentId string) web.RespondentResponse {
	respondent, err := service.RespondentRepository.FindById(respondentId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "respondent not found"))
	}

	return helper.ToRespondentResponse(respondent)
}

func (service *RespondentServiceImpl) FindAll(page, limit, surveyorId string) []web.UserProgressRespondentDetail {
	respondent := service.RespondentRepository.FindAll(page, limit, surveyorId)
	return helper.ToAllProgressResponses(respondent)
}

func (service *RespondentServiceImpl) Update(ctx *fiber.Ctx, userid string, request web.UserUpdateRequest) web.RespondentResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.RespondentRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "respondent not found"))
	}

	user.Name = request.Name
	if request.Email != "" {
		user.Email = request.Email
	}

	if request.Phone != "" {
		if !helper.IsNumeric(request.Phone) {
			panic(exception.NewError(fiber.StatusBadRequest, "phone should numeric"))
		}

		if len([]rune(request.Phone)) < 10 || len([]rune(request.Phone)) > 13 {
			panic(exception.NewError(fiber.StatusBadRequest, "phone should 10-13 digit"))
		}

		if phone, _ := service.RespondentRepository.FindByQuery(bson.M{"phone": request.Phone}); phone.Id != "" && phone.Id != userid {
			panic(exception.NewError(fiber.StatusBadRequest, "phone has been registered"))
		}

		user.Phone = request.Phone
	}

	if request.Gender != "" {
		user.Gender = request.Gender
	}

	service.RespondentRepository.Update(user, userid)

	return helper.ToRespondentResponse(user)
}

func (service *RespondentServiceImpl) Delete(ctx *fiber.Ctx, respondentId string) {

	user, err := service.RespondentRepository.FindById(respondentId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	err = service.RespondentRepository.Delete(respondentId)
	helper.PanicIfError(err)

	prgresSum, _ := service.ProgressSurveySummary.FindByQuery(bson.M{"respondent_id": respondentId})

	// delete in summary progres
	service.ProgressSurveySummary.Delete(prgresSum.ID)

	//update user progres survey
	userRecruitment, err := service.ProgressSurveyRepository.FindByUserId(prgresSum.SurveyorId)

	if err == nil || userRecruitment.ID != "" {
		userRecruitment.SurveyProgress = userRecruitment.SurveyProgress - 1
		service.ProgressSurveyRepository.Update(userRecruitment)
	}

	// delete response survey by respondent id
	service.ResponseRepository.DeleteManyByRespondentId(respondentId)

	log.Println("LOG", "deleted user", user.Name)
}
