package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"mime/multipart"
	"os"
	"strconv"
	"time"

	"log"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/tealeg/xlsx"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/go-playground/validator/v10"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type IssueServiceImpl struct {
	IssueRepository               repository.IssueRepository
	IssueSubRepository            repository.IssueSubRepository
	Validate                      *validator.Validate
	ProvinceRepository            repository.ProvinceRepository
	RegencyRepository             repository.RegencyRepository
	DistrictRepository            repository.DistrictRepository
	VillageRepository             repository.VillageRepository
	IssueLogRepository            repository.IssueLogRepository
	IssueManagementDataRepository repository.IssueManagementDataRepository
}

type IssueService interface {
	FindAllYear(issueId string) []web.IssueResponse
	FindYearSub(issueId string, year string) web.IssueResponse
	FindYearById(issueId string) web.IssueResponse
	FindAll(page, limit, label string, value string) []web.IssueResponse
	FindAllPinPoints(id string, year string, issue string, province string) []web.IssueResponsePoints
	FindById(IssueId string) web.IssueResponse
	ImportIssue(file *multipart.FileHeader, year string, issue string, sub_issue string) []domain.IssueDataBencana

	CreateManagementData(ctx *fiber.Ctx, date string, sub_issue string, request domain.IssueDataUpdateInsert) domain.IssueManagementData

	Create(request web.CreateIssueRequest) web.IssueResponse
	Update(issueId string, request web.UpdateIssueRequest) web.IssueResponse
	Delete(issueId string)
}

func NewIssueService(IssueRepository repository.IssueRepository, IssueSubRepository repository.IssueSubRepository, validate *validator.Validate, ProvinceRepository repository.ProvinceRepository, RegencyRepository repository.RegencyRepository, DistrictRepository repository.DistrictRepository, VillageRepository repository.VillageRepository, IssueLogRepository repository.IssueLogRepository, IssueManagementDataRepository repository.IssueManagementDataRepository) IssueService {
	return &IssueServiceImpl{
		IssueRepository:               IssueRepository,
		IssueSubRepository:            IssueSubRepository,
		Validate:                      validate,
		ProvinceRepository:            ProvinceRepository,
		RegencyRepository:             RegencyRepository,
		DistrictRepository:            DistrictRepository,
		VillageRepository:             VillageRepository,
		IssueLogRepository:            IssueLogRepository,
		IssueManagementDataRepository: IssueManagementDataRepository,
	}
}

func (service *IssueServiceImpl) FindById(issueId string) web.IssueResponse {
	Issue, err := service.IssueRepository.FindById(issueId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Issue not found"))
	}

	return helper.ToIssueResponse(Issue)
}

func (service *IssueServiceImpl) FindAllPinPoints(id string, year string, issue string, province string) []web.IssueResponsePoints {
	Issue := service.IssueRepository.FindAllPintPoints(id, year, issue, province)
	return helper.ToIssuePinPointsResponses(Issue)
}

func (service *IssueServiceImpl) FindAll(page, limit, label string, value string) []web.IssueResponse {
	Issue := service.IssueRepository.FindAll(page, limit, label, value)
	return helper.ToIssueResponses(Issue)
}

func (service *IssueServiceImpl) ImportIssue(file *multipart.FileHeader, year string, issue string, sub_issue string) []domain.IssueDataBencana {
	xlFile, err := file.Open()
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Failed To Open File"))
	}
	defer xlFile.Close()

	xl, err := xlsx.OpenReaderAt(xlFile, file.Size)
	if err != nil {
		panic(exception.NewError(fiber.ErrBadRequest.Code, "Failed TO Reader File"))
	}

	sheet, ok := xl.Sheet["1"]
	if !ok {
		panic(exception.NewError(fiber.ErrBadRequest.Code, "Get Sheet 1"))
	}
	rows := sheet.Rows
	dataIssues := []domain.IssueDataBencana{}
	var dataLogs []interface{}
	IssueSubCollection := service.IssueSubRepository.FindAll("", "", "", sub_issue, "")
	IssueCollection := service.IssueRepository.FindAll("", "", "", issue)

	for i, row := range rows {
		// Skip the first row (index 0)
		if i == 0 {
			continue
		}

		// Check if the row has non-empty cells
		hasData := false
		for _, cell := range row.Cells {
			if cell.String() != "" {
				hasData = true
				break
			}
		}

		// If the row has no data, skip it
		if !hasData {
			continue
		}

		// Read the values from the columns
		if len(row.Cells) >= 2 {
			regencies := row.Cells[0].String()
			latitude := row.Cells[1].String()
			longitude := row.Cells[2].String()
			desc := row.Cells[3].String()
			total, _ := strconv.Atoi(row.Cells[4].String())

			dataIssue := domain.IssueDataBencana{
				Regencies: regencies,
				Latitude:  latitude,
				Longitude: longitude,
				Total:     total,
			}

			for i := 5; i < 12; i++ {
				totals, _ := strconv.Atoi(row.Cells[i].String())
				columnName := rows[0].Cells[i].String()
				dataIssueDetails := domain.DataMetadataIssueData{
					Name:  columnName,
					Total: totals,
				}
				dataIssue.Data = append(dataIssue.Data, dataIssueDetails)
			}

			dataIssueSubIssue := domain.DataMetadataIssueData{
				Name:  sub_issue,
				Total: total,
			}
			dataIssue.SubIssues = append(dataIssue.SubIssues, dataIssueSubIssue)

			dataIssues = append(dataIssues, dataIssue)

			// might be used for villages data set
			// villagesCollection := service.VillageRepository.FindAll("", "", "", villages)
			// districtCollection, _ := service.DistrictRepository.FindDistrictById(villagesCollection[0].DistrictId)
			// regencyColllection, _ := service.RegencyRepository.FindById(districtCollection.RegencyId)
			// provinceCollection, _ := service.ProvinceRepository.FindById(strconv.Itoa(regencyColllection.ProvinceId))

			regenciesCollection := service.RegencyRepository.FindAll("", "", "", regencies)
			provinceCollection, _ := service.ProvinceRepository.FindById(strconv.Itoa(regenciesCollection[0].ProvinceId))

			// check existing data, and delete it to replace later with new data
			checkIssueLog := service.IssueLogRepository.FindAll("", "", IssueCollection[0].Value, IssueSubCollection[0].Label, year)
			if checkIssueLog != nil {
				service.IssueLogRepository.Delete(strconv.Itoa(provinceCollection.ID), "", IssueCollection[0].Value, IssueSubCollection[0].Label, year)
				service.IssueManagementDataRepository.Delete(IssueCollection[0].Value, IssueSubCollection[0].Label, year)
			}

			issueLogData := domain.IssueLogData{
				TypeIssue: IssueCollection[0].Value,
				SubIssue:  IssueSubCollection[0].Label,
				Latitude:  latitude,
				Longitude: longitude,
				Desc:      desc,
			}

			issueLog := domain.IssueLog{
				ID:           primitive.NewObjectID(),
				ProvinceID:   provinceCollection.ID,
				ProvinceName: provinceCollection.Name,
				RegencyID:    regenciesCollection[0].ID,
				RegencyName:  regenciesCollection[0].Name,
				DistrictID:   0,
				DistrictName: "District Name",
				VillageID:    0,
				VillageName:  "Village Name",
				Year:         year,
				SubIssueId:   IssueSubCollection[0].ID,
				Data:         issueLogData,
				CreatedAt:    time.Now(),
			}

			dataLogs = append(dataLogs, issueLog)
		}
	}
	service.IssueRepository.InsertMany(dataLogs)

	if err != nil {
		panic(exception.NewError(fiber.ErrBadRequest.Code, "Issue not found"))
	}

	// Return the JSON data in the response
	return dataIssues
}

func (service *IssueServiceImpl) FindYearSub(issueId string, year string) web.IssueResponse {
	Issue, err := service.IssueRepository.FindYearSub(issueId, year)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Issue not found"))
	}

	return helper.ToIssueResponse(Issue)
}

func (service *IssueServiceImpl) FindYearById(issueId string) web.IssueResponse {
	Issue, err := service.IssueRepository.FindYearById(issueId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Issue not found"))
	}

	return helper.ToIssueResponse(Issue)
}

func (service *IssueServiceImpl) FindAllYear(issueId string) []web.IssueResponse {
	Issue := service.IssueRepository.FindAllYear(issueId)
	return helper.ToIssueResponses(Issue)
}

func (service *IssueServiceImpl) Create(request web.CreateIssueRequest) web.IssueResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isIssueExist := service.IssueRepository.FindIsExist(request.Label, request.Value)
	if isIssueExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Issue already exists"))
	}

	issue := domain.Issue{
		ID:    strings.ToLower(randstr.String(10)),
		Label: request.Label,
		Value: request.Value,
		Color: request.Color,
	}
	service.IssueRepository.Create(issue)

	return helper.ToIssueResponse(issue)
}

func (service *IssueServiceImpl) Update(issueId string, request web.UpdateIssueRequest) web.IssueResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	// isIssueExist := service.IssueRepository.FindIsExist(request.Label, request.Value)
	// if isIssueExist {
	// 	panic(exception.NewError(fiber.StatusBadRequest, "Issue already exists"))
	// }

	issue, err := service.IssueRepository.FindById(issueId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Issue not found"))
	}

	issue.Label = request.Label
	issue.Value = request.Value
	issue.Color = request.Color

	service.IssueRepository.Update(issue, issueId)

	return helper.ToIssueResponse(issue)
}

func (service *IssueServiceImpl) Delete(issueId string) {
	issue, err := service.IssueRepository.FindById(issueId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Issue not found"))
	}

	err = service.IssueRepository.Delete(issueId)
	helper.PanicIfError(err)
	log.Println("LOG", "deleted issue", issue.Label)
}

func (service *IssueServiceImpl) CreateManagementData(ctx *fiber.Ctx, year string, sub_issue string, request domain.IssueDataUpdateInsert) domain.IssueManagementData {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	IssueManagementData := domain.IssueManagementData{
		ID:        strings.ToLower(randstr.String(10)),
		Year:      year,
		CreatedAt: time.Now(),
		IssueType: request.IssueType,
		SubIssue:  sub_issue,
		Data:      request.Data,
	}

	linkFile := service.UploadFileExcel(ctx)
	IssueManagementData.FileUrl = linkFile

	service.IssueRepository.CreateManagementData(IssueManagementData)

	return IssueManagementData
}

func (service *IssueServiceImpl) UploadFileExcel(c *fiber.Ctx) string {
	ctx := context.Background()

	bucketName := os.Getenv("MINIO_BUCKET_DATA")
	file, err := c.FormFile("file")
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request, files cant be empty"))
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.xlsx", os.Getenv("MINIO_PATH_FILES"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	// if fileSize > 512000 {
	// 	panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	// }

	if fileSize > 2048000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 2MB"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}
