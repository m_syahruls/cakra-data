package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type SurveyResultRepositoryImpl struct {
	Collection         *mongo.Collection
	CollectionResponse *mongo.Collection
}

type SurveyResultRepository interface {
	FindById(surveyId, questionId string) (domain.SurveyResult, error)
	FindCountResponseDateById(surveyId string) []domain.ResponseCountDate
	FindResponseByQuery(surveyId, villageId, districtId, regencyId string) ([]domain.Response, error)
}

func NewSurveyResultRepository(database *mongo.Database) SurveyResultRepository {
	return &SurveyResultRepositoryImpl{
		Collection:         database.Collection("surveys"),
		CollectionResponse: database.Collection("responses"),
	}
}

func (repository *SurveyResultRepositoryImpl) FindById(surveyId, questionId string) (domain.SurveyResult, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: surveyId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "questions"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "survey_id"}, {Key: "as", Value: "questions"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$questions"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "questions.question_number", Value: 1}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "questions._id"}, {Key: "foreignField", Value: "question_id"}, {Key: "as", Value: "questions.options"}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "survey_name", Value: bson.D{{Key: "$first", Value: "$survey_name"}}},
		{Key: "status", Value: bson.D{{Key: "$first", Value: "$status"}}},
		{Key: "total_respondent", Value: bson.D{{Key: "$first", Value: "$total_respondent"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "updated_at", Value: bson.D{{Key: "$first", Value: "$updated_at"}}},
		{Key: "questions", Value: bson.D{{Key: "$push", Value: "$questions"}}},
	}}}

	query := mongo.Pipeline{matchStage, lookupStage, unwindStage, sortStage, lookupStage2}

	helper.QuerySurveyResult(questionId, "", "", "", &query)

	query = append(query, groupStage)

	result, err := repository.Collection.Aggregate(ctx, query)
	helper.PanicIfError(err)

	var survey domain.SurveyResult
	result.Next(ctx)
	result.Decode(&survey)

	return survey, result.Err()
}

func (repository *SurveyResultRepositoryImpl) FindCountResponseDateById(surveyId string) []domain.ResponseCountDate {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "survey_id", Value: surveyId}}}}
	addField := bson.D{{Key: "$addFields", Value: bson.M{
		"date": bson.M{
			"year":  bson.M{"$year": "$created_at"},
			"month": bson.M{"$month": "$created_at"},
			"day":   bson.M{"$dayOfMonth": "$created_at"},
		},
	}}}
	groupStage := bson.D{{Key: "$group", Value: bson.M{
		"_id":           bson.M{"$dayOfYear": "$created_at"},
		"date":          bson.M{"$first": "$date"},
		"last_response": bson.M{"$last": "$created_at"},
		"respondent_id": bson.M{"$push": "$respondent_id"},
		"count":         bson.M{"$sum": 1}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "_id", Value: 1}}}}
	result, err := repository.CollectionResponse.Aggregate(ctx, mongo.Pipeline{matchStage, addField, groupStage, sortStage})
	helper.PanicIfError(err)

	var responsesCount []domain.ResponseCountDate
	for result.Next(ctx) {
		var responseCount domain.ResponseCountDate
		result.Decode(&responseCount)
		responsesCount = append(responsesCount, responseCount)
	}

	return responsesCount
}

func (repository *SurveyResultRepositoryImpl) FindResponseByQuery(surveyId, villageId, districtId, regencyId string) ([]domain.Response, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "survey_id", Value: surveyId}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "location.village_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "village"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "districts"}, {Key: "localField", Value: "village.district_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "district"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$district"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "regencies"}, {Key: "localField", Value: "district.regency_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "regency"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$regency"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	query := mongo.Pipeline{matchStage, lookupStage1, unwindStage1, lookupStage2, unwindStage2, lookupStage3, unwindStage3}

	helper.QuerySurveyResult("", villageId, districtId, regencyId, &query)

	result, err := repository.CollectionResponse.Aggregate(ctx, query)

	helper.PanicIfError(err)

	var responses []domain.Response

	for result.Next(ctx) {
		var response domain.Response
		result.Decode(&response)
		responses = append(responses, response)
	}

	return responses, result.Err()
}
