package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type AnswerRepositoryImpl struct {
	Collection         *mongo.Collection
	CollectionLocation *mongo.Collection
}

type AnswerRepository interface {
	Create(answer domain.Answer)
	FindById(answerId string) (domain.Answer, error)
	FindDetailById(answerId string) (domain.Answer, error)
	FindByQuery(query bson.D) (domain.Answer, error)
	FindAll(page, limit, questionId, responseId string) []domain.Answer
	Update(answer domain.Answer)
	Delete(answerId string) error

	DeleteByResponseId(responseId string) error
	FindCountByQuery(query bson.D) int
	FindAnswerTextByQuestionId(questionId string) ([]domain.SurveyAnswerText, error)
	FindAllQuestion(questionId string) []domain.Answer
	FindAnswerTextByQuestionAndResponseId(questionId string, responseId []string) ([]domain.SurveyAnswerText, error)
	FindCountByQueryUsingAgg(questionId, optionId string, responseId []string) (int, error)

	FindLoc(villageId string) []domain.Answer
}

func NewAnswerRepository(database *mongo.Database) AnswerRepository {
	return &AnswerRepositoryImpl{
		Collection:         database.Collection("answers"),
		CollectionLocation: database.Collection("villages"),
	}
}

func (repository *AnswerRepositoryImpl) Create(answer domain.Answer) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":            answer.ID,
		"response_id":    answer.ResponseId,
		"question_id":    answer.QuestionId,
		"option_id":      answer.OptionsId,
		"answer_text":    answer.AnswerText,
		"answer_numeric": answer.AnswerNumeric,
	})

	helper.PanicIfError(err)
}

func (repository *AnswerRepositoryImpl) FindById(answerId string) (domain.Answer, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var answer domain.Answer
	query := bson.M{"_id": answerId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&answer)

	return answer, result.Err()
}

func (repository *AnswerRepositoryImpl) FindDetailById(answerId string) (domain.Answer, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: answerId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "questions"}, {Key: "localField", Value: "question_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "question"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$question"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "option_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "option"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$option"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage2, unwindStage2})
	helper.PanicIfError(err)

	var answer domain.Answer

	result.Next(ctx)
	result.Decode(&answer)

	return answer, result.Err()
}

func (repository *AnswerRepositoryImpl) FindByQuery(query bson.D) (domain.Answer, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var answer domain.Answer

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&answer)

	return answer, result.Err()
}

func (repository *AnswerRepositoryImpl) FindAll(page, limit, questionId, responseId string) []domain.Answer {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	pipeline := mongo.Pipeline{}

	helper.QueryAnswer(questionId, responseId, &pipeline)

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "questions"}, {Key: "localField", Value: "question_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "question"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$question"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "option_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "option"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$option"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "option_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "village"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage4 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "districts"}, {Key: "localField", Value: "village.district_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "district"}}}}
	unwindStage4 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$district"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage5 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "regencies"}, {Key: "localField", Value: "district.regency_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "regency"}}}}
	unwindStage5 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$regency"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	pipeline = append(pipeline, lookupStage, unwindStage, lookupStage2, unwindStage2, lookupStage3, unwindStage3, lookupStage4, unwindStage4, lookupStage5, unwindStage5)

	// check page & limit
	helper.PaggingAggregat(page, limit, &pipeline)

	cursor, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var answers []domain.Answer

	for cursor.Next(ctx) {
		var answer domain.Answer
		cursor.Decode(&answer)
		answers = append(answers, answer)
	}

	return answers
}

func (repository *AnswerRepositoryImpl) Update(answer domain.Answer) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": answer.ID}
	update := bson.M{
		"$set": bson.M{
			"option_id":      answer.OptionsId,
			"answer_text":    answer.AnswerText,
			"answer_numeric": answer.AnswerNumeric,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *AnswerRepositoryImpl) Delete(answerId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": answerId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *AnswerRepositoryImpl) DeleteByResponseId(responseId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteMany(ctx, bson.M{
		"response_id": responseId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *AnswerRepositoryImpl) FindCountByQuery(query bson.D) int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	result, _ := repository.Collection.CountDocuments(ctx, query)

	return int(result)
}

func (repository *AnswerRepositoryImpl) FindAnswerTextByQuestionId(questionId string) ([]domain.SurveyAnswerText, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var answers []domain.SurveyAnswerText

	result, err := repository.Collection.Find(ctx, bson.M{"question_id": questionId})

	for result.Next(ctx) {
		var answer domain.SurveyAnswerText
		result.Decode(&answer)
		answers = append(answers, answer)
	}

	return answers, err
}

func (repository *AnswerRepositoryImpl) FindAllQuestion(optionId string) []domain.Answer {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	result, _ := repository.Collection.Find(ctx, bson.M{"option_id": optionId})

	var answers []domain.Answer

	for result.Next(ctx) {
		var answer domain.Answer
		result.Decode(&answer)
		answers = append(answers, answer)
	}

	return answers
}

func (repository *AnswerRepositoryImpl) FindAnswerTextByQuestionAndResponseId(questionId string, responseId []string) ([]domain.SurveyAnswerText, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage1 := bson.D{{Key: "$match", Value: bson.D{{Key: "question_id", Value: questionId}}}}
	matchStage2 := bson.D{{Key: "$match", Value: bson.D{{Key: "response_id", Value: bson.D{{Key: "$in", Value: responseId}}}}}}

	cursor, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage1, matchStage2})
	if err != nil {
		return nil, err
	}

	var answers []domain.SurveyAnswerText

	for cursor.Next(ctx) {
		var answer domain.SurveyAnswerText
		cursor.Decode(&answer)
		answers = append(answers, answer)
	}

	return answers, err
}

func (repository *AnswerRepositoryImpl) FindCountByQueryUsingAgg(questionId, optionId string, responseId []string) (int, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage1 := bson.D{{Key: "$match", Value: bson.D{{Key: "question_id", Value: questionId}}}}
	matchStage2 := bson.D{{Key: "$match", Value: bson.D{{Key: "option_id", Value: optionId}}}}
	matchStage3 := bson.D{{Key: "$match", Value: bson.D{{Key: "response_id", Value: bson.D{{Key: "$in", Value: responseId}}}}}}
	//countStage := bson.D{{Key: "$count", Value: "total"}}

	cursor, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage1, matchStage2, matchStage3})
	if err != nil {
		return 0, err
	}

	results := 0
	for cursor.Next(ctx) {
		results = results + 1
	}

	return results, err
}

func (repository *AnswerRepositoryImpl) FindLoc(villageId string) []domain.Answer {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	pipeline := mongo.Pipeline{}

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: villageId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "districts"}, {Key: "localField", Value: "district_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "district"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$district"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "regencies"}, {Key: "localField", Value: "district.regency_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "regency"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$regency"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	pipeline = append(pipeline, matchStage, lookupStage, unwindStage, lookupStage1, unwindStage1)

	cursor, err := repository.CollectionLocation.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var answers []domain.Answer

	for cursor.Next(ctx) {
		var answer domain.Answer
		cursor.Decode(&answer)
		answers = append(answers, answer)
	}

	return answers
}
