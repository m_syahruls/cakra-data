package service

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type ProvinceServiceImpl struct {
	ProvinceRepository repository.ProvinceRepository
}

type ProvinceService interface {
	FindAll(page, limit, name string) []web.ProvinceResponse
	FindById(provinceId string) web.ProvinceResponse
	FindByName(provinceName string) web.ProvinceResponse
}

func NewProvinceService(provinceRepository repository.ProvinceRepository) ProvinceService {
	return &ProvinceServiceImpl{
		ProvinceRepository: provinceRepository,
	}
}

func (service *ProvinceServiceImpl) FindById(provinceId string) web.ProvinceResponse {
	province, err := service.ProvinceRepository.FindById(provinceId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "province not found"))
	}

	return helper.ToProvinceResponse(province)
}

func (service *ProvinceServiceImpl) FindByName(provinceName string) web.ProvinceResponse {
	province, err := service.ProvinceRepository.FindByName(provinceName)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "province not found"))
	}

	return helper.ToProvinceResponse(province)
}

func (service *ProvinceServiceImpl) FindAll(page, limit, name string) []web.ProvinceResponse {
	province := service.ProvinceRepository.FindAll(page, limit, name)
	return helper.ToProvinceResponses(province)
}
