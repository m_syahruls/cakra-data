package web

import (
	"time"

	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type SurveyResult struct {
	ID             string                 `json:"id"`
	SurveyName     string                 `json:"survey_name"`
	Status         int                    `json:"status"`
	TotalResponden int                    `json:"total_respondent"`
	CreatedAt      time.Time              `json:"created_at"`
	UpdatedAt      time.Time              `json:"updated_at"`
	Questions      []SurveyQuestionResult `json:"questions" bson:"questions"`
}

type SurveyQuestionResult struct {
	ID              string               `json:"id"`
	QuestionNumber  int                  `json:"question_number"`
	SurveyId        string               `json:"survey_id"`
	Section         string               `json:"section"`
	InputType       string               `json:"input_type"`
	QuestionName    string               `json:"question_name"`
	QuestionSubject string               `json:"question_subject"`
	Location        []domain.Region      `json:"location" bson:"location"`
	AnswerText      []string             `json:"answer_text"`
	Options         []SurveyOptionAnswer `json:"options"`
}

type SurveyOptionAnswer struct {
	ID          string `json:"id"`
	QuestionId  string `json:"question_id"`
	OptionName  string `json:"option_name"`
	Value       int    `json:"value"`
	TotalAnswer int    `json:"total_answer"`
}

type ResponseCountDate struct {
	Date             interface{} `json:"date" bson:"date"`
	RespondentId     interface{} `json:"respondent_id" bson:"respondent_id"`
	LastResponseDate interface{} `json:"last_response" bson:"last_response"`
	Count            interface{} `json:"count" bson:"count"`
}

type Region struct {
	Regions []string `json:"regions"`
	Number  int      `json:"number"`
}
