package web

import (
	"time"
)

type SurveyResponse struct {
	ID             string              `json:"id"`
	SurveyName     string              `json:"survey_name"`
	Status         int                 `json:"status"`
	TotalResponden int                 `json:"total_respondent"`
	CreatedAt      time.Time           `json:"created_at"`
	UpdatedAt      time.Time           `json:"updated_at"`
	Question       []QuestionForSurvey `json:"questions"`
}

type QuestionForSurvey struct {
	QuestionName string `json:"question_name"`
	InputType    string `json:"input_type"`
	QuestionId   string `json:"question_id"`
}

type SurveyDetailResponse struct {
	ID             string                   `json:"id"`
	SurveyName     string                   `json:"survey_name"`
	Status         int                      `json:"status"`
	TotalResponden int                      `json:"total_respondent"`
	CreatedAt      time.Time                `json:"created_at"`
	UpdatedAt      time.Time                `json:"updated_at"`
	Questions      []SurveyQuestionResponse `json:"questions"`
}

type SurveyQuestionResponse struct {
	ID              string                 `json:"id"`
	QuestionNumber  int                    `json:"question_number"`
	SurveyId        string                 `json:"survey_id"`
	Section         string                 `json:"section"`
	InputType       string                 `json:"input_type"`
	QuestionName    string                 `json:"question_name"`
	QuestionSubject string                 `json:"question_subject"`
	Options         []SurveyOptionResponse `json:"options"`
}

type SurveyOptionResponse struct {
	ID         string `json:"id"`
	QuestionId string `json:"question_id"`
	OptionName string `json:"option_name"`
	Value      int    `json:"value"`
	Color      string `json:"color"`
}

type SurveyResponseByUserId struct {
	ID               string    `json:"id"`
	SurveyName       string    `json:"survey_name"`
	Status           int       `json:"status"`
	TotalQuestion    int       `json:"total_question"`
	TotalResponden   int       `json:"total_respondent"`
	CreatedAt        time.Time `json:"created_at"`
	UpdatedAt        time.Time `json:"updated_at"`
	ResponseIdByUser string    `json:"response_id_by_user"`
}
