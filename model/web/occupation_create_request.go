package web

type OccupationCreateRequest struct {
	Level int    `json:"level" validation:"required"`
	Name  string `json:"name" validation:"required"`
}
