package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
)

func UnauthorizeReturn(ctx *fiber.Ctx, message string) error {
	return ctx.Status(fiber.StatusUnauthorized).JSON(web.WebResponse{
		Code:    fiber.StatusUnauthorized,
		Status:  false,
		Message: message,
	})
}
