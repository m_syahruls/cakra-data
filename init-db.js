use admin
db.createUser(
    {
        user: "patronpolitik",
        pwd: "NoZMK&XTzz",
        roles: [
            {
                role: "readWrite",
                db: "patron_politik"
            }
        ]
    }
)

db = new Mongo().getDB("patron_politik");

db.createCollection('users', { capped: false });
db.createCollection('occupations', { capped: false });

db.users.insertOne(
    {
        "_id": "adminadmin",
        "name": "Admin Device Management",
        "username": "admin",
        "email": "support@synapsis.id",
        "phone": "xxxxxxxxxxxx",
        "status": "active",
        "level": 1,
        "password": "$2a$14$aowUOCFsw2WS7gOZgXXMouHJWhR2lVPabweVuITawF0NIaJb/wzwq",
        "created_at": ISODate("2022-08-24T02:23:06.683Z"),
        "updated_at": ISODate("2022-08-24T02:23:06.683Z")
    }
)

db.users.insertOne(
  {
    "_id": "9ywyaizkbc",
    "nik": "admin",
    "name": "admin",
    "occupation_id": "rd94nxx5o6",
    "gender": "male",
    "password": "$2a$14$MCU9cqUUqrgLXsujFDuvleckDXgSwHuBcOqJ26zsZD33QAYQ8A3V2",
    "referral_code": "tyfbntm",
    "email": "admin@admin.com",
    "phone": "xxxxxxxxx",
    "created_at": ISODate("2022-08-24T02:23:06.683Z"),
    "updated_at": ISODate("2022-08-24T02:23:06.683Z")
  }
)

db.occupations.insertMany([
  {
    "_id": "rd94nxx5o6",
    "name": "super admin",
    "level": 1
  },
  {
    "_id": "f0uquizj2o",
    "level": 2,
    "name": "Koordinator"
  },
  {
    "_id": "jelwt05miq",
    "level": 3,
    "name": "Relawan"
  },
  {
    "_id": "wftyndllit",
    "level": 4,
    "name": "General User"
  }
])

db.permissions.insertMany([
  {
    "_id": "ryhfxtjijs",
    "label": "Create Issue",
    "value": "create-issue"
  },
  {
    "_id": "o4lmw810nj",
    "label": "Read Issue",
    "value": "read-issue"
  },
  {
    "_id": "kveylrmhy9",
    "label": "Update Issue",
    "value": "update-issue"
  },
  {
    "_id": "3bjtfsv2yp",
    "label": "Delete Issue",
    "value": "delete-issue"
  },
  {
    "_id": "4ewi1crddw",
    "label": "Create Issue Sub",
    "value": "create-issueSub"
  }
])

db.roles.insertMany([
  {
    "_id": "kxxedxah57",
    "label": "Admin",
    "value": "admin",
    "permissions": [
      "create-issue",
      "read-issue",
      "update-issue",
      "delete-issue",
      "create-subIssue"
    ]
  },
  {
    "_id": "ptoa4ecar3",
    "label": "Intel",
    "value": "intel",
    "permissions": [
      "create-issue",
      "read-issue",
      "update-issue",
      "delete-issue"
    ]
  }
])

db.roles.insertMany([
  {
    "_id": "0vyu60k4uq",
    "label": "Monitoring",
    "value": "monitoring"
  },
  {
    "_id": "ibzyctbz2o",
    "label": "Survey",
    "value": "survey"
  },
  {
    "_id": "tgus5hmuq3",
    "value": "access-management",
    "label": "Access Management"
  },
  {
    "_id": "stwh8dckmc",
    "label": "User Management",
    "value": "user-management"
  },
  {
    "_id": "p0oysuitzq",
    "value": "data-management",
    "label": "Data Management"
  }
])