package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type AccessRepositoryImpl struct {
	Collection *mongo.Collection
}

type AccessRepository interface {
	Create(access domain.Access)
	FindById(accessId string) (domain.Access, error)
	FindAll(label string, value string) []domain.Access
	FindIsExist(label string, value string) bool
	Update(access domain.Access, accessId string)
	Delete(accessId string) error
}

func NewAccessRepository(database *mongo.Database) AccessRepository {
	return &AccessRepositoryImpl{
		Collection: database.Collection("accesses"),
	}
}

func (repository *AccessRepositoryImpl) Create(access domain.Access) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":   access.ID,
		"label": access.Label,
		"value": access.Value,
	})

	helper.PanicIfError(err)
}

func (repository *AccessRepositoryImpl) FindById(accessId string) (domain.Access, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var access domain.Access
	query := bson.M{"_id": accessId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&access)

	return access, result.Err()
}

func (repository *AccessRepositoryImpl) FindAll(label string, value string) []domain.Access {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssue("", label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	var accesses []domain.Access

	for cursor.Next(ctx) {
		var access domain.Access
		cursor.Decode(&access)
		accesses = append(accesses, access)
	}

	return accesses
}

func (repository *AccessRepositoryImpl) FindIsExist(label string, value string) bool {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssueExist(label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	for cursor.Next(ctx) {
		return true
	}

	return false
}

func (repository *AccessRepositoryImpl) Update(access domain.Access, accessId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": accessId}
	update := bson.M{
		"$set": bson.M{
			"label": access.Label,
			"value": access.Value,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *AccessRepositoryImpl) Delete(accessId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": accessId,
	})

	helper.PanicIfError(err)

	return nil
}
