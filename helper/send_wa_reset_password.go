package helper

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
)

var (
	endpoint = os.Getenv("ENDPOINT")
	schedule = os.Getenv("SCHEDULE")
	tokenWA  = os.Getenv("TOKEN_WA")
)

func WhatsappSender(phone, token string) error {
	var encodedString = base64.StdEncoding.EncodeToString([]byte(token))
	token = encodedString
	urls := urlReset + "?phone=" + phone + "&token=" + token
	message := fmt.Sprintf("Berikut adalah link untuk mereset password anda:\n\n%s", urls)

	data := url.Values{}
	data.Set("target", phone)
	data.Set("message", message)
	data.Set("schedule", schedule)

	client := &http.Client{}
	request, err := http.NewRequest(http.MethodPost, endpoint, strings.NewReader(data.Encode()))
	PanicIfError(err)

	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Add("Authorization", tokenWA)

	result, err := client.Do(request)
	PanicIfError(err)

	log.Println(result.Status)
	defer result.Body.Close()

	body, err := ioutil.ReadAll(result.Body)
	PanicIfError(err)

	m := make(map[string]interface{})
	err = json.Unmarshal(body, &m)
	PanicIfError(err)

	if m["status"] != true {
		reason := m["reason"].(string)
		panic(exception.NewError(500, reason))
	}

	return nil
}
