package repository

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ComplaintRepositoryImpl struct {
	Collection                  *mongo.Collection
	CollectionChat              *mongo.Collection
	CollectionComplaintStatus   *mongo.Collection
	CollectionComplaintCategory *mongo.Collection
}

type ComplaintRepository interface {
	CreateComplaint(complaint domain.Complaint)
	FindAllComplaint(userid, complaintStatusId, categoryId, page, limit string) []domain.Complaint
	FindDetailComplaintById(complaintid string) domain.Complaint
	FindComplaintById(complaintid string) domain.Complaint
	UpdateComplaint(complaint domain.Complaint)
	UpdateReadStatusComplaint(complaint domain.Complaint)
	UpdateComplaintStatus(complaint domain.Complaint)
	DeleteComplaint(complaintId string) error

	CreateComplaintChat(complaintChat domain.ComplaintChat)
	FindComplaintChatById(complaintChatId string) domain.ComplaintChat
	FindChatByComplaintId(complaintId, page, limit string) []domain.ComplaintChat
	UpdateComplaintChat(complaintChat domain.ComplaintChat)
	UpdateReadStatusAllComplaintChat(userid, complaintId string)
	DeleteComplaintChat(complaintChatId string) error

	FindAllComplaintWithLastChat(userid, complaintStatusId, categoryId, page, limit string) []domain.ComplaintResponseWithLastChat

	CreateComplaintStatus(complaint domain.ComplaintStatus)
	FindAllComplaintStatus(page, limit string) []domain.ComplaintStatus
	FindComplaintStatusById(complaintStatusId string) domain.ComplaintStatus
	UpdateStatusNameComplaint(statusComplaint domain.ComplaintStatus)

	CreateComplaintCategory(complaint domain.ComplaintCategory)
	FindAllComplaintCategory(page, limit string) []domain.ComplaintCategory
	FindComplaintCategoryById(complaintCategoryId string) (domain.ComplaintCategory, error)
}

func NewComplaintRepository(database *mongo.Database) ComplaintRepository {
	return &ComplaintRepositoryImpl{
		Collection:                  database.Collection("complaints"),
		CollectionChat:              database.Collection("complaints_chat"),
		CollectionComplaintStatus:   database.Collection("complaints_status"),
		CollectionComplaintCategory: database.Collection("complaints_category"),
	}
}

func (repository *ComplaintRepositoryImpl) CreateComplaint(complaint domain.Complaint) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":                 complaint.ID,
		"sender_id":           complaint.SenderId,
		"title":               complaint.Title,
		"content":             complaint.Content,
		"link_image":          complaint.LinkImage,
		"address":             complaint.Address,
		"longitude":           complaint.Longitude,
		"latitude":            complaint.Latitude,
		"read_status":         complaint.ReadStatus,
		"category_id":         complaint.CategoryId,
		"complaint_status_id": complaint.ComplaintStatusId,
		"created_at":          complaint.CreatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) FindAllComplaint(userid, complaintStatusId, categoryId, page, limit string) []domain.Complaint {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "sender_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "complaints_status"}, {Key: "localField", Value: "complaint_status_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "complaint_status"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$complaint_status"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "complaints_category"}, {Key: "localField", Value: "category_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "category"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$category"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "created_at", Value: -1}}}}
	pipeline := mongo.Pipeline{lookupStage, unwindStage, lookupStage1, unwindStage1, lookupStage2, unwindStage2, sortStage}

	helper.PaggingAggregat(page, limit, &pipeline)
	helper.QueryComplaint(userid, complaintStatusId, categoryId, &pipeline)

	cursor, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var systemComplaints []domain.Complaint

	for cursor.Next(ctx) {
		var systemComplaint domain.Complaint
		cursor.Decode(&systemComplaint)
		systemComplaints = append(systemComplaints, systemComplaint)
	}

	return systemComplaints
}

func (repository *ComplaintRepositoryImpl) FindAllComplaintWithLastChat(userid, complaintStatusId, categoryId, page, limit string) []domain.ComplaintResponseWithLastChat {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "sender_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "complaints_chat"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "complaint_id"}, {Key: "as", Value: "complaint_chat"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$complaint_chat"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "complaints_status"}, {Key: "localField", Value: "complaint_status_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "complaint_status"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$complaint_status"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "created_at", Value: -1}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "sender_id", Value: bson.D{{Key: "$first", Value: "$sender_id"}}},
		{Key: "title", Value: bson.D{{Key: "$first", Value: "$title"}}},
		{Key: "content", Value: bson.D{{Key: "$first", Value: "$content"}}},
		{Key: "link_image", Value: bson.D{{Key: "$first", Value: "$link_image"}}},
		{Key: "read_status", Value: bson.D{{Key: "$first", Value: "$read_status"}}},
		{Key: "longitude", Value: bson.D{{Key: "$first", Value: "$longitude"}}},
		{Key: "latitude", Value: bson.D{{Key: "$first", Value: "$latitude"}}},
		{Key: "address", Value: bson.D{{Key: "$first", Value: "$address"}}},
		{Key: "complaint_status_id", Value: bson.D{{Key: "$first", Value: "$complaint_status_id"}}},
		{Key: "complaint_status_desc", Value: bson.D{{Key: "$first", Value: "$complaint_status_desc"}}},
		{Key: "complaint_status", Value: bson.D{{Key: "$first", Value: "$complaint_status"}}},
		{Key: "user", Value: bson.D{{Key: "$first", Value: "$user"}}},
		{Key: "last_chat", Value: bson.D{{Key: "$last", Value: "$complaint_chat"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "updated_at", Value: bson.D{{Key: "$first", Value: "$updated_at"}}},
	}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, lookupStage1, unwindStage1, lookupStage2, unwindStage2, groupStage, sortStage}

	helper.PaggingAggregat(page, limit, &pipeline)
	helper.QueryComplaint(userid, complaintStatusId, categoryId, &pipeline)

	cursor, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var systemComplaints []domain.ComplaintResponseWithLastChat

	for cursor.Next(ctx) {
		var systemComplaint domain.ComplaintResponseWithLastChat
		cursor.Decode(&systemComplaint)
		systemComplaints = append(systemComplaints, systemComplaint)
	}

	return systemComplaints
}

func (repository *ComplaintRepositoryImpl) FindDetailComplaintById(complaintid string) domain.Complaint {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: complaintid}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "sender_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "complaints_status"}, {Key: "localField", Value: "complaint_status_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "complaint_status"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$complaint_status"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "complaints_category"}, {Key: "localField", Value: "category_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "category"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$category"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage1, unwindStage1, lookupStage2, unwindStage2})
	helper.PanicIfError(err)

	result.Next(ctx)
	var systemComplaint domain.Complaint
	result.Decode(&systemComplaint)

	return systemComplaint
}

func (repository *ComplaintRepositoryImpl) FindComplaintById(complaintid string) domain.Complaint {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor, err := repository.Collection.Find(ctx, bson.M{"_id": complaintid})
	helper.PanicIfError(err)

	cursor.Next(ctx)
	var systemComplaint domain.Complaint
	cursor.Decode(&systemComplaint)

	return systemComplaint
}

func (repository *ComplaintRepositoryImpl) UpdateComplaint(complaint domain.Complaint) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": complaint.ID}
	update := bson.M{
		"$set": bson.M{
			"title":      complaint.Title,
			"content":    complaint.Content,
			"link_image": complaint.LinkImage,
			"longitude":  complaint.Longitude,
			"latitude":   complaint.Latitude,
			"updated_at": complaint.UpdatedAt,
		},
	}
	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) UpdateReadStatusComplaint(complaint domain.Complaint) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": complaint.ID}
	update := bson.M{
		"$set": bson.M{
			"read_status": complaint.ReadStatus,
			"updated_at":  time.Now(),
		},
	}
	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) UpdateComplaintStatus(complaint domain.Complaint) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": complaint.ID}
	update := bson.M{
		"$set": bson.M{
			"complaint_status_id":   complaint.ComplaintStatusId,
			"complaint_status_desc": complaint.ComplaintStatusDesc,
			"updated_at":            time.Now(),
		},
	}
	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) DeleteComplaint(complaintId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": complaintId,
	})

	helper.PanicIfError(err)

	return nil
}

// Chat
func (repository *ComplaintRepositoryImpl) CreateComplaintChat(complaintChat domain.ComplaintChat) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionChat.InsertOne(ctx, bson.M{
		"_id":          complaintChat.ID,
		"complaint_id": complaintChat.ComplaintId,
		"sender_id":    complaintChat.SenderId,
		"link_image":   complaintChat.LinkImage,
		"message":      complaintChat.Message,
		"longitude":    complaintChat.Longitude,
		"latitude":     complaintChat.Latitude,
		"read_status":  complaintChat.ReadStatus,
		"created_at":   complaintChat.CreatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) FindComplaintChatById(complaintChatId string) domain.ComplaintChat {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor, err := repository.CollectionChat.Find(ctx, bson.M{"_id": complaintChatId})
	helper.PanicIfError(err)

	cursor.Next(ctx)
	var complaintChat domain.ComplaintChat
	cursor.Decode(&complaintChat)

	return complaintChat
}

func (repository *ComplaintRepositoryImpl) FindChatByComplaintId(complaintId, page, limit string) []domain.ComplaintChat {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	cursor, err := repository.CollectionChat.Find(ctx, bson.M{"complaint_id": complaintId}, filter)
	helper.PanicIfError(err)

	var systemComplaints []domain.ComplaintChat

	for cursor.Next(ctx) {
		var systemComplaint domain.ComplaintChat
		cursor.Decode(&systemComplaint)
		systemComplaints = append(systemComplaints, systemComplaint)
	}

	return systemComplaints
}

func (repository *ComplaintRepositoryImpl) UpdateComplaintChat(complaintChat domain.ComplaintChat) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": complaintChat.ID}
	update := bson.M{
		"$set": bson.M{
			"message": complaintChat.Message,
		},
	}
	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) UpdateReadStatusAllComplaintChat(userid, complaintId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"complaint_id": complaintId, "sender_id": bson.M{"$ne": userid}}
	update := bson.M{
		"$set": bson.M{
			"read_status": "1",
			"updated_at":  time.Now(),
		},
	}
	_, err := repository.CollectionChat.UpdateMany(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) DeleteComplaintChat(complaintChatId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionChat.DeleteOne(ctx, bson.M{
		"_id": complaintChatId,
	})

	helper.PanicIfError(err)

	return nil
}

// Complaint Status
func (repository *ComplaintRepositoryImpl) CreateComplaintStatus(complaint domain.ComplaintStatus) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionComplaintStatus.InsertOne(ctx, bson.M{
		"_id":         complaint.ID,
		"status_name": complaint.StatusName,
	})

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) UpdateStatusNameComplaint(statusComplaint domain.ComplaintStatus) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": statusComplaint.ID}
	update := bson.M{
		"$set": bson.M{
			"status_name": statusComplaint.StatusName,
		},
	}
	_, err := repository.CollectionComplaintStatus.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) FindAllComplaintStatus(page, limit string) []domain.ComplaintStatus {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	options := helper.Pagging(page, limit, -1)

	cursor, err := repository.CollectionComplaintStatus.Find(ctx, bson.M{}, options)
	helper.PanicIfError(err)

	var complaintStatus []domain.ComplaintStatus

	for cursor.Next(ctx) {
		var complaint domain.ComplaintStatus
		cursor.Decode(&complaint)
		complaintStatus = append(complaintStatus, complaint)
	}

	return complaintStatus
}

func (repository *ComplaintRepositoryImpl) FindComplaintStatusById(complaintStatusId string) domain.ComplaintStatus {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor, err := repository.CollectionComplaintStatus.Find(ctx, bson.M{"_id": complaintStatusId})
	helper.PanicIfError(err)

	cursor.Next(ctx)
	var complaintStatus domain.ComplaintStatus
	cursor.Decode(&complaintStatus)

	return complaintStatus
}

// Complaint Category
func (repository *ComplaintRepositoryImpl) CreateComplaintCategory(complaint domain.ComplaintCategory) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionComplaintCategory.InsertOne(ctx, bson.M{
		"_id":           complaint.ID,
		"category_name": complaint.CategoryName,
	})

	helper.PanicIfError(err)
}

func (repository *ComplaintRepositoryImpl) FindAllComplaintCategory(page, limit string) []domain.ComplaintCategory {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	options := helper.Pagging(page, limit, -1)

	cursor, err := repository.CollectionComplaintCategory.Find(ctx, bson.M{}, options)
	helper.PanicIfError(err)

	var complaintCategory []domain.ComplaintCategory

	for cursor.Next(ctx) {
		var complaint domain.ComplaintCategory
		cursor.Decode(&complaint)
		complaintCategory = append(complaintCategory, complaint)
	}

	return complaintCategory
}

func (repository *ComplaintRepositoryImpl) FindComplaintCategoryById(complaintCategoryId string) (domain.ComplaintCategory, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor := repository.CollectionComplaintCategory.FindOne(ctx, bson.M{"_id": complaintCategoryId})

	var complaint domain.ComplaintCategory
	cursor.Decode(&complaint)

	return complaint, cursor.Err()
}
