package web

type OccupationUpdateRequest struct {
	Name string `json:"name" validation:"required"`
}
