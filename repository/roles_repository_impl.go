package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type RoleRepositoryImpl struct {
	Collection *mongo.Collection
}

type RoleRepository interface {
	Create(role domain.Role)
	FindById(roleId string) (domain.Role, error)
	FindAll(label string, value string) []domain.Role
	FindIsExist(label string, value string) bool
	Update(role domain.Role, roleId string)
	AssignPermission(role domain.Role, roleId string)
	Delete(roleId string) error
}

func NewRoleRepository(database *mongo.Database) RoleRepository {
	return &RoleRepositoryImpl{
		Collection: database.Collection("roles"),
	}
}

func (repository *RoleRepositoryImpl) Create(role domain.Role) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":         role.ID,
		"label":       role.Label,
		"value":       role.Value,
		"permissions": role.Permission,
	})

	helper.PanicIfError(err)
}

func (repository *RoleRepositoryImpl) FindById(roleId string) (domain.Role, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var role domain.Role
	query := bson.M{"_id": roleId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&role)

	return role, result.Err()
}

func (repository *RoleRepositoryImpl) FindAll(label string, value string) []domain.Role {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssue("", label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	var roles []domain.Role

	for cursor.Next(ctx) {
		var role domain.Role
		cursor.Decode(&role)
		roles = append(roles, role)
	}

	return roles
}

func (repository *RoleRepositoryImpl) FindIsExist(label string, value string) bool {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssueExist(label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	for cursor.Next(ctx) {
		return true
	}

	return false
}

func (repository *RoleRepositoryImpl) Update(role domain.Role, roleId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": roleId}
	update := bson.M{
		"$set": bson.M{
			"label": role.Label,
			"value": role.Value,
			// "permissions": role.Permission,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *RoleRepositoryImpl) AssignPermission(role domain.Role, roleId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": roleId}
	update := bson.M{
		"$set": bson.M{
			// "label":       role.Label,
			// "value":       role.Value,
			"permissions": role.Permission,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *RoleRepositoryImpl) Delete(roleId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": roleId,
	})

	helper.PanicIfError(err)

	return nil
}
