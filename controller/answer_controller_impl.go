package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type AnswerControllerImpl struct {
	AnswerService service.AnswerService
	LogService    service.LogService
}

type AnswerController interface {
	NewAnswerRouter(app *fiber.App)
}

func NewAnswerController(answerService service.AnswerService, logService service.LogService) AnswerController {
	return &AnswerControllerImpl{
		AnswerService: answerService,
		LogService:    logService,
	}
}

func (controller *AnswerControllerImpl) NewAnswerRouter(app *fiber.App) {
	answer := app.Group("/answer")
	answer.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})
	answer.Post("/", controller.Create)
	answer.Get("/:answerid", controller.FindById)
	answer.Get("/", controller.FindAll)
	answer.Put("/:answerid", controller.Update)
	answer.Delete("/:answerid", controller.Delete)
}

func (controller *AnswerControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.AnswerCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	answerResponse := controller.AnswerService.Create(request)

	action := fmt.Sprintf("create answer for response %s", request.ResponseId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    answerResponse,
	})
}

func (controller *AnswerControllerImpl) FindById(ctx *fiber.Ctx) error {
	answerId := ctx.Params("answerid")

	answerResponse := controller.AnswerService.FindById(answerId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    answerResponse,
	})
}

func (controller *AnswerControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query page, limit, question id & response id
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	questionId := ctx.Query("questionid")
	responseId := ctx.Query("responseid")
	answerResponse := controller.AnswerService.FindAll(page, limit, questionId, responseId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    answerResponse,
	})
}

func (controller *AnswerControllerImpl) Update(ctx *fiber.Ctx) error {
	answerId := ctx.Params("answerid")
	var request web.AnswerUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	answerUpdated := controller.AnswerService.Update(answerId, request)

	action := fmt.Sprintf("update answer %s", answerId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    answerUpdated,
	})
}

func (controller *AnswerControllerImpl) Delete(ctx *fiber.Ctx) error {
	answerId := ctx.Params("answerid")

	controller.AnswerService.Delete(answerId)

	action := fmt.Sprintf("delete answer %s", answerId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
