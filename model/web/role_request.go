package web

type RoleCreateRequest struct {
	Label      string   `json:"label" validate:"required"`
	Value      string   `json:"value" validate:"required"`
	Permission []string `json:"permissions"`
}

type RoleUpdateRequest struct {
	Label      string   `json:"label" validate:"required"`
	Value      string   `json:"value" validate:"required"`
	Permission []string `json:"permissions"`
}
