package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type RecruitRepositoryImpl struct {
	Collection     *mongo.Collection
	CollectionUser *mongo.Collection
}

type RecruitRepository interface {
	Create(recruit domain.UserRecruit)
	FindByUserId(userId string) (domain.UserRecruit, error)
	FindDetailByUserId(userId string) (domain.RecruitedDetail, error)
	FindAll(page, limit, userId, searchName, level string) []domain.UserRecruitDetail
	Update(recruit domain.UserRecruit)
	FindUserByUserId(userId string) (domain.User, error)
	Delete(userId string) error

	UpdateGoal(recruit domain.UserRecruit)
}

func NewRecruitRepository(database *mongo.Database) RecruitRepository {
	return &RecruitRepositoryImpl{
		Collection:     database.Collection("user_recruits"),
		CollectionUser: database.Collection("users"),
	}
}

func (repository *RecruitRepositoryImpl) Create(recruit domain.UserRecruit) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":              recruit.ID,
		"user_id":          recruit.UserId,
		"recruit_progress": recruit.RecruitProgress,
		"recruit_goal":     recruit.RecruitGoal,
	})

	helper.PanicIfError(err)
}

func (repository *RecruitRepositoryImpl) FindByUserId(userId string) (domain.UserRecruit, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var recruit domain.UserRecruit
	query := bson.M{"user_id": userId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&recruit)

	return recruit, result.Err()
}

func (repository *RecruitRepositoryImpl) FindDetailByUserId(userId string) (domain.RecruitedDetail, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "user_id", Value: userId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "recruited_user"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_recruit_id"}, {Key: "as", Value: "recruited_user"}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "recruited_user.user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "recruited_user"}}}}

	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage1, lookupStage2})
	helper.PanicIfError(err)

	var recruit domain.RecruitedDetail

	result.Next(ctx)
	result.Decode(&recruit)

	return recruit, result.Err()
}

func (repository *RecruitRepositoryImpl) FindAll(page, limit, userId, searchName, level string) []domain.UserRecruitDetail {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	pipeline := mongo.Pipeline{}

	helper.QueryRecruitUserId(userId, &pipeline)

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "user.occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user.occupation"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user.occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}

	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "recruited_user"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_recruit_id"}, {Key: "as", Value: "recruited_user"}}}}

	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "recruited_user.user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "recruited_user"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruited_user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	lookupStage4 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "recruited_user._id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruited_user.recruitment"}}}}
	unwindStage4 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruited_user.recruitment"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "recruited_user.recruitment.recruit_progress", Value: -1}}}}

	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "user", Value: bson.D{{Key: "$first", Value: "$user"}}},
		{Key: "recruit_progress", Value: bson.D{{Key: "$first", Value: "$recruit_progress"}}},
		{Key: "recruit_goal", Value: bson.D{{Key: "$first", Value: "$recruit_goal"}}},
		{Key: "recruited_user", Value: bson.D{{Key: "$push", Value: "$recruited_user"}}},
	}}}

	pipeline = append(pipeline, lookupStage, unwindStage, lookupStage1, unwindStage1, lookupStage2, lookupStage3, unwindStage3, lookupStage4, unwindStage4, sortStage, groupStage)

	helper.QueryRecruit(searchName, level, &pipeline)

	helper.PaggingAggregat(page, limit, &pipeline)

	cursor, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var recruits []domain.UserRecruitDetail

	for cursor.Next(ctx) {
		var recruit domain.UserRecruitDetail
		cursor.Decode(&recruit)
		recruits = append(recruits, recruit)
	}

	return recruits
}

func (repository *RecruitRepositoryImpl) Update(recruit domain.UserRecruit) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": recruit.ID}
	update := bson.M{
		"$set": bson.M{
			"recruit_progress": recruit.RecruitProgress,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *RecruitRepositoryImpl) FindUserByUserId(userId string) (domain.User, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var survey domain.User
	query := bson.M{"_id": userId}

	result := repository.CollectionUser.FindOne(ctx, query)
	result.Decode(&survey)

	return survey, result.Err()
}

func (repository *RecruitRepositoryImpl) Delete(userId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"user_id": userId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *RecruitRepositoryImpl) UpdateGoal(recruit domain.UserRecruit) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": recruit.ID}
	update := bson.M{
		"$set": bson.M{
			"recruit_goal": recruit.RecruitGoal,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}
