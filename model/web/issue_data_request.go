package web

type IssueDataRequest struct {
	ID        string           `json:"id" bson:"_id"`
	Year      string           `json:"year" bson:"year"`
	IssueType string           `json:"issue_type" bson:"issue_type"`
	MetaData  IssueDataBencana `json:"metadata" bson:"-"`
}

type IssueDataBencana struct {
	Meninggal   int `json:"meninggal"`
	Hilang      int `json:"hilang"`
	Terluka     int `json:"terluka"`
	Menderita   int `json:"menderita"`
	Mengungsi   int `json:"mengungsi"`
	Rumah       int `json:"rumah"`
	Pendidikan  int `json:"pendidikan"`
	Kesehatan   int `json:"kesehatan"`
	Peribadatan int `json:"peribadatan"`
	Fasum       int `json:"fasum"`
	Perkantoran int `json:"perkantoran"`
	Jembatan    int `json:"jembatan"`
	Pabrik      int `json:"pabrik"`
	Kios        int `json:"kios"`
}
