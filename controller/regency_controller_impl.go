package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type RegencyControllerImpl struct {
	RegencyService service.RegencyService
}

type RegencyController interface {
	NewRegencyRouter(app *fiber.App)
}

func NewRegencyController(regencyService service.RegencyService) RegencyController {
	return &RegencyControllerImpl{
		RegencyService: regencyService,
	}
}

func (controller *RegencyControllerImpl) NewRegencyRouter(app *fiber.App) {
	regency := app.Group("/regencies")
	regency.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	regency.Get("/:regencyid", controller.FindById)
	regency.Get("/", controller.FindAll)
}

func (controller *RegencyControllerImpl) FindById(ctx *fiber.Ctx) error {
	regencyId := ctx.Params("regencyid")

	regencyResponse := controller.RegencyService.FindById(regencyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    regencyResponse,
	})
}

func (controller *RegencyControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query by question id
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	provinceId := ctx.Query("provinceid")
	name := ctx.Query("name")
	regencyResponse := controller.RegencyService.FindAll(page, limit, provinceId, name)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    regencyResponse,
	})
}
