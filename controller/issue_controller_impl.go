package controller

import (
	"fmt"
	"log"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type IssueControllerImpl struct {
	IssueService     service.IssueService
	LogService       service.LogService
	RegencyService   service.RegencyService
	ProvinceService  service.ProvinceService
	IssueDataService service.IssueDataService
}

type IssueController interface {
	NewIssueRouter(app *fiber.App)
}

func NewIssueController(issueService service.IssueService,
	logService service.LogService, regencyService service.RegencyService,
	provinceService service.ProvinceService, issueDataService service.IssueDataService) IssueController {
	return &IssueControllerImpl{
		IssueService:     issueService,
		LogService:       logService,
		RegencyService:   regencyService,
		ProvinceService:  provinceService,
		IssueDataService: issueDataService,
	}
}

func (controller *IssueControllerImpl) NewIssueRouter(app *fiber.App) {
	app.Get("/years", controller.FindAllYear)

	issue := app.Group("/issues")
	issues := app.Group("/issues-all")
	issue.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	issue.Post("/import", controller.ImportIssue)
	issues.Get("/:issue_type/:year", controller.FindIssueData)
	issue.Get("/rank", controller.FindIssueDataRank)
	issue.Get("/points", controller.FindPoints)
	issue.Get("/density", controller.FindDensityIssue)
	issue.Get("/:issueId/sub-issues", controller.FindYearSub)
	issue.Get("/:issueId/years", controller.FindYearById)
	issue.Get("/:issueId", controller.FindById)
	issue.Get("/", controller.FindAll)

	issue.Post("/", controller.Create)
	issue.Put("/:issueId", controller.Update)
	issue.Delete("/:issueId", controller.Delete)
}

func (controller *IssueControllerImpl) FindById(ctx *fiber.Ctx) error {
	issueId := ctx.Params("issueId")

	issueResponse := controller.IssueService.FindById(issueId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) FindIssueData(ctx *fiber.Ctx) error {
	issue_type := ctx.Params("issue_type")
	year := ctx.Params("year")

	log.Println(issue_type)
	log.Println(year)

	issueResponse := controller.IssueDataService.FindIssueData(year, issue_type, "")
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) FindIssueDataRank(ctx *fiber.Ctx) error {
	issue_type := ctx.Query("issue")
	year := ctx.Query("year")
	issueResponse := controller.IssueDataService.FindIssueDataRank(issue_type, year)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) FindDensityIssue(ctx *fiber.Ctx) error {
	issue_type := ctx.Query("issue")
	year := ctx.Query("year")
	scope := ctx.Query("scope")
	province := ctx.Query("province")
	issueResponse := controller.IssueDataService.FindDensityIssue(issue_type, year, scope, province)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	label := ctx.Query("label")
	value := ctx.Query("value")
	issueResponse := controller.IssueService.FindAll(page, limit, label, value)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) FindPoints(ctx *fiber.Ctx) error {
	id := ctx.Query("id")
	year := ctx.Query("year")
	issue := ctx.Query("issue")
	province := ctx.Query("province")
	issueResponse := controller.IssueService.FindAllPinPoints(id, year, issue, province)

	//Convert Data
	groups := make(map[string][]web.DataPinPoinResult)
	for _, point := range issueResponse {
		dataPinPoint := web.DataPinPoinResult{
			ID: point.ID,
			Data: struct {
				Desc      string `json:"desc"`
				Latitude  string `json:"latitude"`
				Longitude string `json:"longitude"`
			}{
				Desc:      point.Data.Desc,
				Latitude:  point.Data.Latitude,
				Longitude: point.Data.Longitude,
			},
		}
		groups[point.SubIssueId] = append(groups[point.SubIssueId], dataPinPoint)
	}

	var result []web.GroupedDataPintPoint
	for subIssueID, points := range groups {
		groupedData := web.GroupedDataPintPoint{
			SubIssueID: subIssueID,
			Points:     points,
		}
		result = append(result, groupedData)
	}

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}

func (controller *IssueControllerImpl) ImportIssue(ctx *fiber.Ctx) error {
	year := ctx.FormValue("year")
	issue := ctx.FormValue("issue")
	sub_issue := ctx.FormValue("sub_issue")
	file, err := ctx.FormFile("file")
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Failed to get file from request",
			"error":   err.Error(),
		})
	}

	//Get Issue Data
	getIssueData := controller.IssueDataService.FindIssueData(year, issue, sub_issue)
	var issueDataId string
	if len(getIssueData.ID) == 0 {
		// Create Data Issue
		CreateIssueData := controller.IssueDataService.Create(year, issue, sub_issue)
		issueDataId = CreateIssueData.ID
	} else {
		issueDataId = getIssueData.ID
	}

	log.Println("Issue Data ID: ", issueDataId)
	issueResponse := controller.IssueService.ImportIssue(file, year, issue, sub_issue)

	dataWilayah := []domain.DataWilayah{}
	metadataMap := make(map[string]domain.IssueDataBencanaPayload)
	mergedData := make(map[string]int)
	type MergedResult struct {
		Data []domain.DataMetadataIssueData `json:"data"`
	}
	var mergedResult MergedResult

	for _, issueRow := range issueResponse {
		//Get Regency
		getRegency := controller.RegencyService.FindByName(issueRow.Regencies)
		//Get Provinces
		getProvince := controller.ProvinceService.FindById(strconv.Itoa(getRegency.ProvinceId))

		// Check if the wilayah already exists in the map
		if existingMetadata, ok := metadataMap[getProvince.Name]; ok {
			// If it exists, accumulate the metadata values
			// existingMetadata.TotalKejadian += totalRow
			existingMetadata.TotalKejadian += 1
			metadataMap[getProvince.Name] = existingMetadata
		} else {
			// If it doesn't exist, add a new entry to the map
			metadataMap[getProvince.Name] = domain.IssueDataBencanaPayload{
				// TotalKejadian: issueRow.Total,
				TotalKejadian: 1,
				Data:          issueRow.Data,
				SubIssues:     issueRow.SubIssues,
				// Assign other metadata fields as needed
			}
		}

		for _, incident := range issueRow.Data {
			mergedData[incident.Name] += incident.Total
		}
	}

	for name, total := range mergedData {
		mergedResult.Data = append(mergedResult.Data, domain.DataMetadataIssueData{Name: name, Total: total})
	}

	// Convert the map to a list of DataWilayah
	totalMetadata := domain.IssueDataBencanaPayload{}

	for wilayah, metadata := range metadataMap {
		totalMetadata.TotalKejadian = metadata.TotalKejadian
		subIssues := make([]domain.DataMetadataIssueData, 0) // Initialize a new slice
		// metadataSubIssue := getIssueData.MetaData.SubIssues
		// metadataSubIssueMap := make(map[string]domain.DataMetadataIssueData)
		// metadataDataIssue := getIssueData.MetaData.Data
		// metadataDataIssueMap := make(map[string]domain.DataMetadataIssueData)

		for _, subIssue := range metadata.SubIssues {
			// Convert each sub-issue from DataMetadataIssueData to DataSubIssueData
			// metadataSubIssue = append(metadataSubIssue, domain.DataMetadataIssueData{
			subIssues = append(subIssues, domain.DataMetadataIssueData{
				Name:  subIssue.Name,
				Total: totalMetadata.TotalKejadian,
			})
			// // // Check if the wilayah already exists in the map
			// if existingMetadata, ok := metadataSubIssueMap[subIssue.Name]; ok {
			// 	// If it exists, accumulate the metadata values
			// 	existingMetadata.Total += totalMetadata.TotalKejadian
			// 	metadataSubIssueMap[subIssue.Name] = existingMetadata
			// } else {
			// 	// If it doesn't exist, add a new entry to the map
			// 	metadataSubIssue = append(metadataSubIssue, domain.DataMetadataIssueData{
			// 		Name:  subIssue.Name,
			// 		Total: metadata.TotalKejadian,
			// 	})
			// }
		}

		// for _, dataIssue := range metadata.Data {
		// 	// Convert each sub-issue from DataMetadataIssueData to DataSubIssueData
		// 	// Check if the wilayah already exists in the map
		// 	if existingMetadata, ok := metadataDataIssueMap[dataIssue.Name]; ok {
		// 		// If it exists, accumulate the metadata values
		// 		existingMetadata.Total += totalMetadata.TotalKejadian
		// 		metadataDataIssueMap[dataIssue.Name] = existingMetadata
		// 	} else {
		// 		// If it doesn't exist, add a new entry to the map
		// 		metadataDataIssue = append(metadataDataIssue, domain.DataMetadataIssueData{
		// 			Name:  dataIssue.Name,
		// 			Total: metadata.TotalKejadian,
		// 		})
		// 	}
		// }

		ProvinceId := controller.ProvinceService.FindByName(wilayah)
		dataWilayah = append(dataWilayah, domain.DataWilayah{
			ID:      ProvinceId.ID,
			Wilayah: wilayah,
			MetaData: domain.MetadataWilayahIssueData{
				TotalIncidents: metadata.TotalKejadian,
				// TotalIncidents: totalMetadata.TotalKejadian,
				Data: metadata.Data,
			},
			SubIssues: subIssues,
			// SubIssues: metadataSubIssue,
			// Assign other fields as needed
		})
	}

	subIssues := make([]domain.DataMetadataIssueData, 0) // Initialize a new slice
	// subIssues := getIssueData.MetaData.Data
	totalMetadata.SubIssues = append(subIssues, domain.DataMetadataIssueData{
		Name:  sub_issue,
		Total: totalMetadata.TotalKejadian,
	})
	totalMetadata.Data = mergedResult.Data

	IssueDataAppend := domain.IssueDataUpdateInsert{
		Year:      year,
		IssueType: issue,
		Data:      dataWilayah,
		MetaData:  totalMetadata,
	}

	controller.IssueDataService.Update(issue, year, IssueDataAppend)
	controller.IssueService.CreateManagementData(ctx, year, sub_issue, IssueDataAppend)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    IssueDataAppend,
	})
}

func (controller *IssueControllerImpl) FindYearById(ctx *fiber.Ctx) error {
	issueId := ctx.Params("issueId")

	issueResponse := controller.IssueService.FindYearById(issueId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) FindAllYear(ctx *fiber.Ctx) error {
	issueId := ctx.Query("id")
	issueResponse := controller.IssueService.FindAllYear(issueId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) FindYearSub(ctx *fiber.Ctx) error {
	issueId := ctx.Params("issueId")
	year := ctx.Query("year")

	issueResponse := controller.IssueService.FindYearSub(issueId, year)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) Create(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can create issue")
	// }

	var request web.CreateIssueRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	issueResponse := controller.IssueService.Create(request)

	action := fmt.Sprintf("create issue %s", issueResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) Update(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can update issue")
	// }

	var request web.UpdateIssueRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	issueId := ctx.Params("issueId")

	issueResponse := controller.IssueService.Update(issueId, request)

	action := fmt.Sprintf("update issue %s", issueResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueResponse,
	})
}

func (controller *IssueControllerImpl) Delete(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can delete issue")
	// }

	issueId := ctx.Params("issueId")

	controller.IssueService.Delete(issueId)

	action := fmt.Sprintf("delete issue %s", issueId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
