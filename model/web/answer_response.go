package web

type AnswerResponse struct {
	ID            string                 `json:"id"`
	ResponseId    string                 `json:"response_id"`
	QuestionId    string                 `json:"question_id"`
	OptionsId     string                 `json:"option_id"`
	AnswerText    string                 `json:"answer_text"`
	AnswerNumeric int32                  `json:"answer_numeric"`
	Question      AnswerQuestionResponse `json:"question"`
	Options       interface{}            `json:"option"`
}

type AnswerQuestionResponse struct {
	ID              string `json:"id"`
	QuestionNumber  int    `json:"question_number"`
	SurveyId        string `json:"survey_id"`
	Section         string `json:"section"`
	InputType       string `json:"input_type"`
	QuestionName    string `json:"question_name"`
	QuestionSubject string `json:"question_subject"`
}

type AnswerOptionResponse struct {
	ID         string `json:"id"`
	QuestionId string `json:"question_id"`
	OptionName string `json:"option_name"`
	Value      *int   `json:"value"`
	Color      string `json:"color"`
}
