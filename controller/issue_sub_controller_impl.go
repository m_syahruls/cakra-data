package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type IssueSubControllerImpl struct {
	IssueSubService service.IssueSubService
	LogService      service.LogService
}

type IssueSubController interface {
	NewIssueSubRouter(app *fiber.App)
}

func NewIssueSubController(issueSubService service.IssueSubService, logService service.LogService) IssueSubController {
	return &IssueSubControllerImpl{
		IssueSubService: issueSubService,
		LogService:      logService,
	}
}

func (controller *IssueSubControllerImpl) NewIssueSubRouter(app *fiber.App) {
	issueSub := app.Group("/sub-issues")
	issueSub.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	issueSub.Get("/:issueSubId", controller.FindById)
	issueSub.Get("/", controller.FindAll)
	issueSub.Post("/", controller.Create)
	issueSub.Put("/:issueSubId", controller.Update)
	issueSub.Delete("/:issueSubId", controller.Delete)
}

func (controller *IssueSubControllerImpl) FindById(ctx *fiber.Ctx) error {
	issueSubId := ctx.Params("issueSubId")

	issueSubResponse := controller.IssueSubService.FindById(issueSubId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueSubResponse,
	})
}

func (controller *IssueSubControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	issueId := ctx.Query("issue_id")
	label := ctx.Query("label")
	value := ctx.Query("value")

	issueSubResponse := controller.IssueSubService.FindAll(page, limit, issueId, label, value)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueSubResponse,
	})
}

func (controller *IssueSubControllerImpl) Create(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can create sub issue")
	// }

	var request web.CreateIssueSubRequest
	request.IssueID = ctx.FormValue("issue_id")
	request.Label = ctx.FormValue("label")
	request.Value = ctx.FormValue("value")

	// err := ctx.BodyParser(&request)
	// helper.PanicIfError(err)

	issueSubResponse := controller.IssueSubService.Create(ctx, request)

	action := fmt.Sprintf("create sub issue %s", issueSubResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    issueSubResponse,
	})
}

func (controller *IssueSubControllerImpl) Update(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can update sub issue")
	// }

	var request web.UpdateIssueSubRequest
	request.IssueID = ctx.FormValue("issue_id")
	request.Label = ctx.FormValue("label")
	request.Value = ctx.FormValue("value")

	// err := ctx.BodyParser(&request)
	// helper.PanicIfError(err)

	issueSubId := ctx.Params("issueSubId")

	issueSubResponse := controller.IssueSubService.Update(ctx, issueSubId, request)

	action := fmt.Sprintf("update sub issue %s", issueSubResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueSubResponse,
	})
}

func (controller *IssueSubControllerImpl) Delete(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can delete sub issue")
	// }

	issueSubId := ctx.Params("issueSubId")

	controller.IssueSubService.Delete(issueSubId)

	action := fmt.Sprintf("delete sub issue %s", issueSubId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
