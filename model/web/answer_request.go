package web

type AnswerCreateRequest struct {
	ResponseId string      `json:"response_id" validate:"required"`
	QuestionId string      `json:"question_id" validate:"required"`
	Answer     interface{} `json:"answer" validate:"required"`
}

type AnswerUpdateRequest struct {
	Answer interface{} `json:"answer" validate:"required"`
}
