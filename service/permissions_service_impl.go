package service

import (
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type PermissionServiceImpl struct {
	PermissionRepository repository.PermissionRepository
	Validate             *validator.Validate
}

type PermissionService interface {
	Create(request web.PermissionCreateRequest) web.PermissionResponse
	FindById(permissionId string) web.PermissionResponse
	FindAll(label string, value string) []web.PermissionResponse
	Update(permissionId string, request web.PermissionUpdateRequest) web.PermissionResponse
	Delete(permissionId string)
}

func NewPermissionService(permissionRepository repository.PermissionRepository, validate *validator.Validate) PermissionService {
	return &PermissionServiceImpl{
		PermissionRepository: permissionRepository,
		Validate:             validate,
	}
}

func (service *PermissionServiceImpl) Create(request web.PermissionCreateRequest) web.PermissionResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isPermissionExist := service.PermissionRepository.FindIsExist(request.Label, request.Value)
	if isPermissionExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Permission already exists"))
	}

	permission := domain.Permission{
		ID:    strings.ToLower(randstr.String(10)),
		Label: request.Label,
		Value: request.Value,
	}

	service.PermissionRepository.Create(permission)

	return helper.ToPermissionResponse(permission)
}

func (service *PermissionServiceImpl) FindById(permissionId string) web.PermissionResponse {
	permission, err := service.PermissionRepository.FindById(permissionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Permission not found"))
	}

	return helper.ToPermissionResponse(permission)
}

func (service *PermissionServiceImpl) FindAll(label string, value string) []web.PermissionResponse {
	permissions := service.PermissionRepository.FindAll(label, value)
	return helper.ToPermissionResponses(permissions)
}

func (service *PermissionServiceImpl) Update(permissionId string, request web.PermissionUpdateRequest) web.PermissionResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isPermissionExist := service.PermissionRepository.FindIsExist(request.Label, request.Value)
	if isPermissionExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Permission already exists"))
	}

	permission, err := service.PermissionRepository.FindById(permissionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Permission not found"))
	}

	permission.Label = request.Label
	permission.Value = request.Value

	service.PermissionRepository.Update(permission, permissionId)

	return helper.ToPermissionResponse(permission)
}

func (service *PermissionServiceImpl) Delete(permissionId string) {
	permission, err := service.PermissionRepository.FindById(permissionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Permission not found"))
	}

	err = service.PermissionRepository.Delete(permissionId)
	helper.PanicIfError(err)
	log.Println("LOG ", "deleted permission", permission.Label)
}
