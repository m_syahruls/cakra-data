package domain

import "time"

type Complaint struct {
	ID                  string            `json:"id" bson:"_id"`
	SenderId            string            `json:"sender_id" bson:"sender_id"`
	Title               string            `json:"title" bson:"title"`
	Content             string            `json:"content" bson:"content"`
	LinkImage           string            `json:"link_image" bson:"link_image"`
	Address             string            `json:"address" bson:"address"`
	Longitude           string            `json:"longitude" bson:"longitude"`
	Latitude            string            `json:"latitude" bson:"latitude"`
	ReadStatus          string            `json:"read_status" bson:"read_status"`
	ComplaintStatusId   string            `json:"complaint_status_id" bson:"complaint_status_id"`
	ComplaintStatus     ComplaintStatus   `json:"complaint_status" bson:"complaint_status"`
	ComplaintStatusDesc string            `json:"complaint_status_desc" bson:"complaint_status_desc"`
	CategoryId          string            `json:"category_id" bson:"category_id"`
	ComplaintCategory   ComplaintCategory `json:"category" bson:"category"`
	User                User              `json:"user" bson:"user"`
	CreatedAt           time.Time         `json:"created_at" bson:"created_at"`
	UpdatedAt           time.Time         `json:"updated_at" bson:"updated_at"`
}

type ComplaintStatus struct {
	ID         string `json:"id" bson:"_id"`
	StatusName string `json:"status_name" bson:"status_name"`
}

type ComplaintCategory struct {
	ID           string `json:"id" bson:"_id"`
	CategoryName string `json:"category_name" bson:"category_name"`
}

type ComplaintChat struct {
	ID          string    `json:"id" bson:"_id"`
	ComplaintId string    `json:"complaint_id" bson:"complaint_id"`
	SenderId    string    `json:"sender_id" bson:"sender_id"`
	LinkImage   string    `json:"link_image" bson:"link_image"`
	Message     string    `json:"message" bson:"message"`
	ReadStatus  string    `json:"read_status" bson:"read_status"`
	Longitude   string    `json:"longitude" bson:"longitude"`
	Latitude    string    `json:"latitude" bson:"latitude"`
	CreatedAt   time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" bson:"updated_at"`
}

type ComplaintResponseWithLastChat struct {
	ID                  string            `json:"id" bson:"_id"`
	SenderId            string            `json:"sender_id" bson:"sender_id"`
	Title               string            `json:"title" bson:"title"`
	Content             string            `json:"content" bson:"content"`
	LinkImage           string            `json:"link_image" bson:"link_image"`
	Address             string            `json:"address" bson:"address"`
	Longitude           string            `json:"longitude" bson:"longitude"`
	Latitude            string            `json:"latitude" bson:"latitude"`
	ReadStatus          string            `json:"read_status" bson:"read_status"`
	ComplaintStatusId   string            `json:"complaint_status_id" bson:"complaint_status_id"`
	ComplaintStatus     ComplaintStatus   `json:"complaint_status" bson:"complaint_status"`
	ComplaintStatusDesc string            `json:"complaint_status_desc" bson:"complaint_status_desc"`
	ComplaintCategoryId string            `json:"complaint_category_id" bson:"complaint_category_id"`
	ComplaintCategory   ComplaintCategory `json:"complaint_category" bson:"complaint_category"`
	User                User              `json:"user" bson:"user"`
	LastChat            ComplaintChat     `json:"last_chat" bson:"last_chat"`
	CreatedAt           time.Time         `json:"created_at" bson:"created_at"`
	UpdatedAt           time.Time         `json:"updated_at" bson:"updated_at"`
}
