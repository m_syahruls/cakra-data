package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type TalkwalkerControllerImpl struct {
	TalkwalkerService service.TalkwalkerService
	LogService        service.LogService
}

type TalkwalkerController interface {
	NewTalkwalkerRouter(app *fiber.App)
}

func NewTalkwalkerController(issueSubService service.TalkwalkerService, logService service.LogService) TalkwalkerController {
	return &TalkwalkerControllerImpl{
		TalkwalkerService: issueSubService,
		LogService:        logService,
	}
}

func (controller *TalkwalkerControllerImpl) NewTalkwalkerRouter(app *fiber.App) {
	issueSub := app.Group("/talkwalkers")
	issueSub.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	issueSub.Get("/", controller.FindAll)
	issueSub.Post("/", controller.Create)

}

func (controller *TalkwalkerControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	name := ctx.Query("name")

	issueSubResponse := controller.TalkwalkerService.FindAll(page, limit, name)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueSubResponse,
	})
}

func (controller *TalkwalkerControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.CreateTalkwalkerRequest
	request.Name = ctx.FormValue("name")

	issueSubResponse := controller.TalkwalkerService.Create(ctx, request)

	action := fmt.Sprintf("create talkwalker %s", issueSubResponse.Name)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    issueSubResponse,
	})
}
