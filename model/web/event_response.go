package web

import "time"

type EventResponse struct {
	ID            string    `json:"id"`
	EventName     string    `json:"event_name"`
	ImageUrl      string    `json:"image_url"`
	Description   string    `json:"description"`
	Link          string    `json:"link"`
	DateStart     time.Time `json:"date_start"`
	DateEnd       time.Time `json:"date_end"`
	Location      string    `json:"location"`
	ContactPerson string    `json:"contact_person"`
	Status        *bool     `json:"status"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}

type EventResponseDetailUser struct {
	ID            string    `json:"id"`
	EventName     string    `json:"event_name"`
	ImageUrl      string    `json:"image_url"`
	Description   string    `json:"description"`
	Link          string    `json:"link"`
	DateStart     time.Time `json:"date_start"`
	DateEnd       time.Time `json:"date_end"`
	Location      string    `json:"location"`
	ContactPerson string    `json:"contact_person"`
	IsParticipate *bool     `json:"is_participate"`
	Status        *bool     `json:"status"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}

type EventResponseDetailAdmin struct {
	ID            string             `json:"id"`
	EventName     string             `json:"event_name"`
	ImageUrl      string             `json:"image_url"`
	Description   string             `json:"description"`
	Link          string             `json:"link"`
	DateStart     time.Time          `json:"date_start" bson:"date_start"`
	DateEnd       time.Time          `json:"date_end" bson:"date_end"`
	Location      string             `json:"location" bson:"location"`
	ContactPerson string             `json:"contact_person" bson:"contact_person"`
	Status        *bool              `json:"status"`
	CreatedAt     time.Time          `json:"created_at"`
	UpdatedAt     time.Time          `json:"updated_at"`
	Participant   []EventParticipate `json:"participant"`
}

type EventParticipate struct {
	ID   string `json:"_id"`
	Name string `json:"name"`
}
