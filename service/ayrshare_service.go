package service

import (
	"fmt"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/mitchellh/mapstructure"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type AyrshareServiceImpl struct {
	SocialmediaHistoricalRepository repository.SocialmediaHistoricalRepository
	AyrshareUrl                     string
}

type AyrshareService interface {
	PostToSocialMedia(token string, data web.AyrsharePostRequest) web.AyrsharePostResponse
	GetUserDetails(token string) web.AyrshareUserDetail
	GetUserAnalytics(token string, request web.AyrshareUserAnalyticsRequest) web.AyrshareAnalyticsResponse
	CheckEligibility(token string) web.AyrshareEligibility
	consumeAyrshareAPI(token string, endpoint string) (int, interface{})

	CreateHistory(token string, data domain.SocialmediaHistorical)
	FindHistoricalById(token string, query map[string]interface{}) []domain.SocialmediaHistorical
	// FindLastHistoricalByToken(token string) domain.SocialmediaHistorical

	CreateTotal(total domain.SocialmediaLastTotal)
	CreateNewTotal(id string, request web.AyrshareAnalyticsResponse, platform string)
	FindTotalBySocialmediaId(totalId string) domain.SocialmediaLastTotal
	UpdateTotalBySocialmediaId(totalId string, total domain.SocialmediaLastTotal)
}

func NewAyrshareService(socmedRepository repository.SocialmediaHistoricalRepository, ayrshareUrl string) AyrshareService {
	return &AyrshareServiceImpl{
		SocialmediaHistoricalRepository: socmedRepository,
		AyrshareUrl:                     ayrshareUrl,
	}
}

func (service *AyrshareServiceImpl) PostToSocialMedia(token string, request web.AyrsharePostRequest) web.AyrsharePostResponse {
	a := fiber.AcquireAgent()
	req := a.Request()
	a.JSON(request)
	req.Header.SetMethod(fiber.MethodPost)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	req.SetRequestURI(fmt.Sprintf("%s/post", service.AyrshareUrl))

	if err := a.Parse(); err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, err.Error()))
	}

	var data web.AyrsharePostResponse
	var errors []error
	code, _, errors := a.Struct(&data)

	if len(errors) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, errors[0].Error()))
	}
	if code != 200 {
		return data
	}

	return data
}

func (service *AyrshareServiceImpl) GetUserAnalytics(token string, request web.AyrshareUserAnalyticsRequest) web.AyrshareAnalyticsResponse {
	a := fiber.AcquireAgent()
	req := a.Request()
	a.JSON(request)
	req.Header.SetMethod(fiber.MethodPost)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	req.SetRequestURI(fmt.Sprintf("%s/analytics/social", service.AyrshareUrl))

	if err := a.Parse(); err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, err.Error()))
	}

	var data web.AyrshareAnalyticsResponse
	var errors []error
	code, _, errors := a.Struct(&data)

	if len(errors) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, errors[0].Error()))
	}
	if code != 200 {
		return data
	}

	return data
}

func (service *AyrshareServiceImpl) GetUserDetails(token string) web.AyrshareUserDetail {
	statusCode, res := service.consumeAyrshareAPI(token, "/user")

	if statusCode != 200 {
		var badRequest web.AyrshareForbidden
		mapstructure.Decode(res, &badRequest)
		panic(exception.NewError(statusCode, fmt.Sprintf("%s %s,  %s", badRequest.Action, badRequest.Status, badRequest.Message)))
	}

	var data web.AyrshareUserDetail
	mapstructure.Decode(res, &data)

	return data
}

func (service *AyrshareServiceImpl) CheckEligibility(token string) web.AyrshareEligibility {
	statusCode, _ := service.consumeAyrshareAPI(token, "/user")
	var ret = true
	var message = "eligible"

	if statusCode != 200 {
		if statusCode == 403 {
			message = "forbidden token"
		} else {
			message = "unauthorized token"
		}
		ret = false
	}

	return web.AyrshareEligibility{
		Eligibility: ret,
		Message:     message,
		Code:        statusCode,
	}
}

func (service *AyrshareServiceImpl) consumeAyrshareAPI(token string, endpoint string) (int, interface{}) {
	a := fiber.AcquireAgent()
	req := a.Request()
	req.Header.SetMethod(fiber.MethodGet)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	req.SetRequestURI(fmt.Sprintf("%s%s", service.AyrshareUrl, endpoint))

	if err := a.Parse(); err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, err.Error()))
	}

	var data interface{}
	var errors []error
	code, _, errors := a.Struct(&data)

	if len(errors) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, errors[0].Error()))
	}
	// if code != 200 {
	// 	panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d, when calling: %s", code, origin)))
	// }

	return code, data

	// url := fmt.Sprintf("%s/user", service.AyrshareUrl)

	// oauth := &oauth2.Token{
	// 	RefreshToken: service.GoogleRefreshToken,
	// }
	// client := service.GoogleConfig.Client(context.Background(), oauth)
	// req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(query))
	// req.Header.Set("developer-token", service.GoogleDeveloperToken)
	// req.Header.Set("login-customer-id", service.GoogleManagerId)

	// res, err := client.Do(req)
	// if err != nil {
	// 	panic(exception.NewError(res.StatusCode, err.Error()))
	// }

	// bytes, err := io.ReadAll(res.Body)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// err = json.Unmarshal(bytes, &array)
	// if err != nil {
	// 	panic(exception.NewError(http.StatusInternalServerError, fmt.Sprintf("internal server error %v", err)))
	// }

	// return array, res.StatusCode
}

func (service *AyrshareServiceImpl) CreateHistory(token string, data domain.SocialmediaHistorical) {
	// fmt.Println("test")
	service.SocialmediaHistoricalRepository.CreateHistory(data)
}

func (service *AyrshareServiceImpl) FindHistoricalById(token string, query map[string]interface{}) []domain.SocialmediaHistorical {
	// fmt.Println("test")
	// user := service.SocialmediaHistoricalRepository.FindHistoricalById(token, query)
	return service.SocialmediaHistoricalRepository.FindHistoricalById(token, query)
}

// func (service *AyrshareServiceImpl) FindLastHistoricalByToken(token string) domain.SocialmediaHistorical {
// 	// fmt.Println("test")
// 	// user := service.SocialmediaHistoricalRepository.FindHistoricalById(token, query)
// 	return service.SocialmediaHistoricalRepository.FindLastHistoricalByToken(token)
// }

func (service *AyrshareServiceImpl) CreateTotal(total domain.SocialmediaLastTotal) {
	service.SocialmediaHistoricalRepository.CreateTotal(total)
}

func (service *AyrshareServiceImpl) CreateNewTotal(id string, request web.AyrshareAnalyticsResponse, platform string) {
	data := helper.AnalyticWebToTotalDomain(id, request, platform, time.Now(), time.Now())

	service.SocialmediaHistoricalRepository.CreateTotal(data)
}

func (service *AyrshareServiceImpl) FindTotalBySocialmediaId(totalId string) domain.SocialmediaLastTotal {
	return service.SocialmediaHistoricalRepository.FindTotalBySocialmediaId(totalId)
}

func (service *AyrshareServiceImpl) UpdateTotalBySocialmediaId(totalId string, total domain.SocialmediaLastTotal) {
	service.SocialmediaHistoricalRepository.UpdateTotalBySocialmediaId(totalId, total)
}
