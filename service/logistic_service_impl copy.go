package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type LogisticServiceImpl struct {
	LogisticRepository repository.LogisticRepository
	Validate           *validator.Validate
}

type LogisticService interface {
	CreateLogistic(ctx *fiber.Ctx, logisticReq web.LogisticCreate) web.LogisticResponse
	FindAllLogistic(userid, categoryId, page, limit string) []web.LogisticResponse
	FindDetailByLogisticId(ctx *fiber.Ctx, logisticId string) web.LogisticResponse
	UpdateByLogisticId(ctx *fiber.Ctx, logisticId string, request web.LogisticUpdate) web.LogisticResponse
	DeleteLogistic(logisticId string)

	CreateLogisticCategory(ctx *fiber.Ctx, request web.LogisticCategoryRequest) domain.LogisticCategory
	FindAllLogisticCategory(page, limit string) []domain.LogisticCategory
	UpdateCategoryLogistic(categoryid string, request web.UpdateLogisticCategory) domain.LogisticCategory
	DeleteCategoryLogistic(categoryid string)

	FindSumLogistic(userid, categoryid string) int
}

func NewLogisticService(logisticRepository repository.LogisticRepository, validate *validator.Validate) LogisticService {
	return &LogisticServiceImpl{
		LogisticRepository: logisticRepository,
		Validate:           validate,
	}
}

func (service *LogisticServiceImpl) CreateLogistic(ctx *fiber.Ctx, request web.LogisticCreate) web.LogisticResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, err = service.LogisticRepository.FindLogisticCategoryById(request.CategoryId)
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "category not found"))
	}

	linkImage := service.UploadImage(ctx)

	dataLogistic := domain.Logistic{
		Id:            strings.ToLower(randstr.String(10)),
		ImageLogistic: linkImage,
		UserId:        request.UserId,
		Nama:          request.Nama,
		Location:      request.Location,
		CategoryId:    request.CategoryId,
		Longitude:     request.Longitude,
		Latitude:      request.Latitude,
		CreatedAt:     time.Now(),
		UpdatedAt:     time.Now(),
	}
	service.LogisticRepository.CreateLogistic(dataLogistic)

	return helper.ToLogisticResponse(dataLogistic)
}

func (service *LogisticServiceImpl) FindAllLogistic(userid, categoryId, page, limit string) []web.LogisticResponse {

	logistic := service.LogisticRepository.FindAllLogistic(userid, categoryId, page, limit)

	return helper.ToLogisticResponses(logistic)
}

func (service *LogisticServiceImpl) FindDetailByLogisticId(ctx *fiber.Ctx, logisticId string) web.LogisticResponse {
	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	logistic := service.LogisticRepository.FindDetailLogisticById(logisticId)

	// check if not admin and not sender of logistic cant see logistic
	if logistic.UserId != userid && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to logistic"))
	}

	if logistic.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "logistic not found"))
	}

	return helper.ToLogisticResponse(logistic)
}

func (service *LogisticServiceImpl) UpdateByLogisticId(ctx *fiber.Ctx, logisticId string, request web.LogisticUpdate) web.LogisticResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	logistic := service.LogisticRepository.FindLogisticById(logisticId)

	if logistic.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "logistic not found"))
	}

	if logistic.UserId != userid && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "this logistic not yours"))
	}

	if request.Nama != "" {
		logistic.Nama = request.Nama
	}

	if request.CategoryId != "" {
		logistic.CategoryId = request.CategoryId
	}

	logistic.UpdatedAt = time.Now()

	service.LogisticRepository.UpdateLogistic(logistic)

	return helper.ToLogisticResponse(logistic)
}

func (service *LogisticServiceImpl) DeleteLogistic(logisticId string) {
	logistic := service.LogisticRepository.FindLogisticById(logisticId)
	if logistic.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "logistic not found"))
	}
	err := service.LogisticRepository.DeleteLogistic(logisticId)
	helper.PanicIfError(err)
}

func (service *LogisticServiceImpl) UploadImage(c *fiber.Ctx) string {
	ctx := context.Background()

	bucketName := os.Getenv("MINIO_BUCKET_COMPLAINT")
	file, err := c.FormFile("images")
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.jpg", os.Getenv("MINIO_PATH"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	if fileSize > 512000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}

// Logistic Category
func (service *LogisticServiceImpl) CreateLogisticCategory(ctx *fiber.Ctx, request web.LogisticCategoryRequest) domain.LogisticCategory {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	logisticCategory := domain.LogisticCategory{
		Id:   request.Id,
		Name: request.Name,
	}
	service.LogisticRepository.CreateLogisticCategory(logisticCategory)

	category, _ := service.LogisticRepository.FindLogisticCategoryById(logisticCategory.Id)

	return category
}

func (service *LogisticServiceImpl) FindAllLogisticCategory(page, limit string) []domain.LogisticCategory {

	logisticCategory := service.LogisticRepository.FindAllLogisticCategory(page, limit)

	return logisticCategory
}

func (service *LogisticServiceImpl) UpdateCategoryLogistic(categoryid string, request web.UpdateLogisticCategory) domain.LogisticCategory {
	category, _ := service.LogisticRepository.FindLogisticCategoryById(categoryid)
	if category.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "logistic not found"))
	}
	category.Name = request.Name

	err := service.LogisticRepository.UpdateCategoryLogistic(categoryid, category)
	helper.PanicIfError(err)

	return category
}

func (service *LogisticServiceImpl) DeleteCategoryLogistic(categoryid string) {
	category, _ := service.LogisticRepository.FindLogisticCategoryById(categoryid)
	logistic := service.LogisticRepository.FindAllLogistic("", categoryid, "", "")
	if len(logistic) > 0 {
		panic(exception.NewError(fiber.StatusBadRequest, "category already use for logistic"))
	}
	if category.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "logistic not found"))
	}
	err := service.LogisticRepository.DeleteCategoryLogistic(categoryid)
	helper.PanicIfError(err)
}

func (service *LogisticServiceImpl) FindSumLogistic(userid, categoryid string) int {

	return service.LogisticRepository.FindSumLogistic(userid, categoryid)
}
