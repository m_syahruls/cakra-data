package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type CandidateControllerImpl struct {
	CandidateService service.CandidateService
}

type CandidateController interface {
	NewCandidateRouter(app *fiber.App)
}

func NewCandidateController(candidateService service.CandidateService) CandidateController {
	return &CandidateControllerImpl{
		CandidateService: candidateService,
	}
}

func (controller *CandidateControllerImpl) NewCandidateRouter(app *fiber.App) {
	candidate := app.Group("/candidate")
	candidate.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	candidate.Post("/", controller.Create)
	candidate.Get("/", controller.FindAll)
}

func (controller *CandidateControllerImpl) Create(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can create partai")
	}

	var request web.Candidate
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	data := controller.CandidateService.Create(request)

	//action := fmt.Sprintf("create partai %s", eventResponse.EventName)
	//actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	//controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    data,
	})
}

func (controller *CandidateControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	partaiId := ctx.Query("partaiid")
	electionTypeId := ctx.Query("electiontypeid")
	daerahPemiluId := ctx.Query("daerahpemiluid")

	candidate := controller.CandidateService.FindAll(page, limit, partaiId, electionTypeId, daerahPemiluId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    candidate,
	})
}
