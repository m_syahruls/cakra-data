package domain

type Permission struct {
	ID    string `json:"id" bson:"_id"`
	Label string `json:"label" bson:"label"`
	Value string `json:"value" bson:"value"`
}
