package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type SurveyControllerImpl struct {
	SurveyService service.SurveyService
	LogService    service.LogService
}

type SurveyController interface {
	NewSurveyRouter(app *fiber.App)
}

func NewSurveyController(surveyService service.SurveyService, logService service.LogService) SurveyController {
	return &SurveyControllerImpl{
		SurveyService: surveyService,
		LogService:    logService,
	}
}

func (controller *SurveyControllerImpl) NewSurveyRouter(app *fiber.App) {
	survey := app.Group("/survey")
	survey.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	survey.Post("/", controller.Create)
	survey.Get("/:surveyid", controller.FindDetailById)
	survey.Get("/", controller.FindAll)
	survey.Put("/update-status/:surveyid", controller.UpdateStatus)
	survey.Put("/:surveyid", controller.UpdateSurvey)
	survey.Delete("/:surveyid", controller.Delete)

	survey.Get("/user/:userid", controller.FindAllSurveyByUserId)
}

func (controller *SurveyControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.SurveyCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can create survey"))
	}

	surveyResponse := controller.SurveyService.Create(request)

	action := fmt.Sprintf("create survey %s", request.SurveyName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    surveyResponse,
	})
}

func (controller *SurveyControllerImpl) FindDetailById(ctx *fiber.Ctx) error {
	surveyId := ctx.Params("surveyid")

	surveyResponse := controller.SurveyService.FindDetailById(surveyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    surveyResponse,
	})
}

func (controller *SurveyControllerImpl) UpdateStatus(ctx *fiber.Ctx) error {
	surveyId := ctx.Params("surveyid")

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can update survey"))
	}

	controller.SurveyService.UpdateStatus(surveyId)

	action := fmt.Sprintf("update status survey %s", surveyId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

func (controller *SurveyControllerImpl) UpdateSurvey(ctx *fiber.Ctx) error {
	var request web.SurveyUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can update survey"))
	}

	surveyId := ctx.Params("surveyid")

	request.ID = surveyId

	survey := controller.SurveyService.UpdateSurvey(request)

	action := fmt.Sprintf("update survey %s", survey.SurveyName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    survey,
	})
}

func (controller *SurveyControllerImpl) FindAll(ctx *fiber.Ctx) error {
	// query by survey_name, status, page & limit
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	searchName := ctx.Query("search")
	status := ctx.Query("status")

	surveyResponse := controller.SurveyService.FindAll(page, limit, searchName, status)
	alldata := controller.SurveyService.CountAllSurvey(searchName, status)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponseAll{
		Code:         fiber.StatusOK,
		Status:       true,
		Message:      "success",
		TotalAllData: alldata,
		Data:         surveyResponse,
	})
}

func (controller *SurveyControllerImpl) Delete(ctx *fiber.Ctx) error {
	surveyId := ctx.Params("surveyid")

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can delete survey"))
	}

	controller.SurveyService.Delete(surveyId)

	action := fmt.Sprintf("delete survey %s", surveyId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

// by user id get survey + response id
func (controller *SurveyControllerImpl) FindAllSurveyByUserId(ctx *fiber.Ctx) error {
	userId := ctx.Params("userid")
	status := ctx.Query("status")

	survey := controller.SurveyService.FindAllSurveyByUserId(userId, status)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    survey,
	})
}
