package web

import "time"

type ProfileDetailResponse struct {
	Id            string                 `json:"id"`
	OccupationId  string                 `json:"occupation_id"`
	NIK           string                 `json:"nik"`
	Name          string                 `json:"name"`
	Email         string                 `json:"email"`
	Phone         string                 `json:"phone"`
	Gender        string                 `json:"gender"`
	ReferralCode  string                 `json:"referral_code"`
	ProfileImage  string                 `json:"profile_image"`
	GoogleAdsID   string                 `json:"google_ads_id"`
	AyrshareToken string                 `json:"ayrshare_token"`
	DetermID      string                 `json:"determ_id" bson:"determ_id"`
	CreatedAt     time.Time              `json:"created_at"`
	UpdatedAt     time.Time              `json:"updated_at"`
	Occupation    OccupationResponseUser `json:"occupation"`
	Recruitment   UserRecruitResponse    `json:"recruitment"`
	Logistic      UserRecruitResponse    `json:"logistic"`
	Information   UserRecruitResponse    `json:"information"`
	Survey        ProgressSurvey         `json:"survey"`
}

type ProfileResponse struct {
	Id        string    `json:"id"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ProfileUpdateRequest struct {
	Name  string `json:"name" validate:"required"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

type ProfileUpdateGoogleAdsID struct {
	GoogleAdsID string `json:"google_ads_id" validate:"required"`
}

type ProfileUpdateGoogleAdsResponse struct {
	GoogleAdsID   string                    `json:"google_ads_id"`
	GoogleAdsName string                    `json:"google_ads_name"`
	Results       []googleAdsCampaignDetail `json:"results"`
	FieldMask     string                    `json:"fieldMask"`
	RequestId     string                    `json:"requestId"`
}

type ProfileUpdateAyrshareToken struct {
	AyrshareToken string `json:"ayrshare_token" validate:"required"`
}

type ProfileUpdateAyrshareResponse struct {
	ActiveSocialAccounts []string              `json:"activeSocialAccounts"`
	DisplayNames         []ayrshareDisplayName `json:"displayNames"`
	Email                string                `json:"email"`
}

type ProfileUpdateDetermID struct {
	DetermID string `json:"determ_id" validate:"required"`
}
