package domain

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type UserPemilih struct {
	Id           string     `json:"id" bson:"_id"`
	Name         string     `json:"name" bson:"name"`
	Email        string     `json:"email" bson:"email"`
	Gender       string     `json:"gender" bson:"gender"`
	Phone        string     `json:"phone" bson:"phone"`
	Password     string     `json:"password" bson:"password"`
	OccupationId string     `json:"occupation_id" bson:"occupation_id"`
	ReligionId   string     `json:"religion" bson:"religion"`
	NIK          string     `json:"nik" bson:"nik"`
	VillageId    string     `json:"village_id" bson:"village_id"`
	DistrictId   string     `json:"district_id" bson:"district_id"`
	RegencyId    string     `json:"regency_id" bson:"regency_id"`
	Longitude    string     `json:"longitude" bson:"longitude"`
	Latitude     string     `json:"latitude" bson:"latitude"`
	KTP          string     `json:"image_ktp" bson:"image_ktp"`
	Selfie       string     `json:"foto_selfie" bson:"foto_selfie"`
	ProfileImage string     `json:"profile_image" bson:"profile_image"`
	DptId        string     `json:"dpt_id" bson:"dpt_id"`
	CandidateId  string     `json:"candidate_id" bson:"candidate_id"`
	Occupation   Occupation `json:"occupation" bson:"occupation"`
	CreatedAt    time.Time  `json:"created_at" bson:"created_at"`
	UpdatedAt    time.Time  `json:"updated_at" bson:"updated_at"`
}

func (user *UserPemilih) SetPassword(password string) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	user.Password = string(hashedPassword)
}

func (user *UserPemilih) ComparePassword(correctPassword string, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(correctPassword), []byte(password))
}
