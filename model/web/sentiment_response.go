package web

type MTKGetGroupsResponse struct {
	Duration       int    `json:"duration"`
	Code           int    `json:"code"`
	OrganizationID string `json:"organization"`
	Method         string `json:"method"`
	Data           struct {
		Groups []struct {
			Id       int    `json:"id"`
			Name     string `json:"name"`
			Keywords []struct {
				Id   int    `json:"id"`
				Name string `json:"name"`
			} `json:"keywords"`
		} `json:"groups"`
	} `json:"data"`
}

type MTKGetLanguagesReponses struct {
	Duration       int         `json:"duration"`
	Code           int         `json:"code"`
	OrganizationID string      `json:"organization"`
	Method         string      `json:"method"`
	Data           interface{} `json:"data"`
}

type MTKGetLanguagesLocationsResponse struct {
	SourceTypes interface{} `json:"source_types"`
	Languages   interface{} `json:"languages"`
	Locations   interface{} `json:"locations"`
}

type MTKPostCompleteResponse struct {
	MentionsOverTime         MTKPostReportResponse       `json:"mentions_over_time"`
	SumOfMentions            MTKPostReportBriefResponse  `json:"sum_of_mentions"`
	SumOfImpressions         MTKPostReportBriefResponse  `json:"sum_of_impressions"`
	SumOfAllSource           MTKPostReportResponse       `json:"sum_of_all_source"`
	EffectiveSentiment       MTKPostReportResponse       `json:"effective_sentiment"`
	MentionsOverTimeBySource MTKPostReportDetailResponse `json:"mentions_over_time_by_source"`
	SentimentOverTime        MTKPostReportDetailResponse `json:"sentiment_over_time"`
	WordCloud                []DummyWordCloudResponse    `json:"wordcloud"`
}

type MTKPostReportResponse struct {
	Duration int    `json:"duration"`
	Code     int    `json:"code"`
	Method   string `json:"method"`
	Data     struct {
		TotalValue interface{} `json:"total_value"`
		Entries    []struct {
			KeyType string      `json:"key_type"`
			Value   interface{} `json:"value"`
			Key     interface{} `json:"key"`
		} `json:"entries"`
	} `json:"data"`
}

type MTKPostReportBriefResponse struct {
	Duration int    `json:"duration"`
	Code     int    `json:"code"`
	Method   string `json:"method"`
	Data     struct {
		TotalValue interface{} `json:"total_value"`
		Progress   struct {
			Total                 interface{} `json:"total"`
			CalculatingFeed       interface{} `json:"calculating_feed"`
			Done                  bool        `json:"done"`
			FeedDone              bool        `json:"feed_done"`
			CollectingMentions    interface{} `json:"collecting_mentions"`
			AnalysingPartialSlots interface{} `json:"analysing_partial_slots"`
		} `json:"progress"`
	} `json:"data"`
}

type MTKPostReportDetailResponse struct {
	Duration int    `json:"duration"`
	Code     int    `json:"code"`
	Method   string `json:"method"`
	Data     struct {
		TotalValue interface{} `json:"total_value"`
		Entries    []struct {
			Entries []struct {
				KeyType string      `json:"key_type"`
				Value   interface{} `json:"value"`
				Key     interface{} `json:"key"`
			} `json:"entries"`
			KeyType string      `json:"key_type"`
			Value   interface{} `json:"value"`
			Key     interface{} `json:"key"`
		} `json:"entries"`
	} `json:"data"`
}

type DummyWordCloudResponse struct {
	Text  string `json:"text"`
	Value int    `json:"value"`
}

type DetermEligibility struct {
	Eligibility bool   `json:"eligibility"`
	Message     string `json:"message"`
	Code        int    `json:"code"`
}

// type SocialResponse struct {
// 	Summary Summary `json:"summary"`
// 	Data    Data    `json:"data"`
// }

// type SocialSummaryResponse struct {
// 	Impressions    float32   `json:"impressions" bson:"impressions"`
// 	Engagements    float32   `json:"engagements" bson:"engagements"`
// 	EngagementRate float32   `json:"engagement_rate" bson:"engagement_rate"`
// 	PostLinkClicks float32   `json:"post_link_clicks" bson:"post_link_clicks"`
// 	UpdatedAt      time.Time `json:"updated_at"`
// }

// type SocialDatapointResponse struct {
// 	Timestamp      time.Time              `json:"timestamp"`
// 	AudienceGrowth domain.SocialData      `json:"audience_growth"`
// 	Impression     domain.SocialData      `json:"impression"`
// 	Engagement     domain.SocialData      `json:"engagement"`
// 	EngagementRate domain.SocialDataFloat `json:"engagement_rate"`
// 	VideoViews     domain.SocialData      `json:"video_views"`
// }

// type Data struct {
// 	DataId         []string          `json:"data_id"`
// 	AudienceGrowth []SocialData      `json:"audience_growth"`
// 	Impression     []SocialData      `json:"impression"`
// 	Engagement     []SocialData      `json:"engagement"`
// 	EngagementRate []SocialDataFloat `json:"engagement_rate"`
// 	VideoViews     []SocialData      `json:"video_views"`
// }

// type SocialData struct {
// 	Timestamp time.Time         `json:"timestamp"`
// 	Data      domain.SocialData `json:"data"`
// }

// type SocialDataFloat struct {
// 	Timestamp time.Time              `json:"timestamp"`
// 	Data      domain.SocialDataFloat `json:"data"`
// }
