package web

type UserCreateRecruit struct {
	UserId      string `json:"user_id" validate:"required"`
	RecruitGoal int    `json:"recruit_goal" validate:"required"`
}
