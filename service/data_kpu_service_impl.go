package service

import (
	"strings"

	"github.com/go-playground/validator/v10"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type DataKpuServiceImpl struct {
	DataKpuRepository repository.DataKpuRepository
	Validate          *validator.Validate
}

type DataKpuService interface {
	FindAll(regencyId string) web.ResponseQuestionByRegency
}

func NewDataKpuService(dataKpuRepository repository.DataKpuRepository, validate *validator.Validate) DataKpuService {
	return &DataKpuServiceImpl{
		DataKpuRepository: dataKpuRepository,
		Validate:          validate,
	}
}

func (service *DataKpuServiceImpl) FindAll(regencyId string) web.ResponseQuestionByRegency {

	var dataKpus []domain.KpuData
	if regencyId == "" {
		dataKpus = service.DataKpuRepository.FindAll()
	} else {
		regencyarr := strings.SplitAfter(regencyId, ",")
		dataKpus = service.DataKpuRepository.FindAllWithRegency(regencyarr)
	}

	var data web.ResponseQuestionByRegency

	data.QuestionName = "Data KPU 2019"
	data.QuestionID = "dtkpu"
	data.Options = append(data.Options, "pkb", "gerindra", "pdip", "golkar", "nasdem", "garuda", "berkarya", "pks", "perindo", "ppp", "psi", "pan", "hanura", "demokrat", "pbb", "pkpi")
	data.Color = append(data.Color, "#17fc03", "#e87313", "#f50707", "#f5c907", "#04237a", "#eb7373", "#5c5c05", "#ff8000", "#395c80", "#1a700c", "#6b270a", "#03244a", "#612617", "#3d72e3", "#8fdb9f", "#750b04")
	var dataV []web.ResponseQuestionByLocation

	dataD := make(map[string]int)

	for _, village := range dataKpus {
		dataD[village.DistrictId+"pkb"] += int(village.PKB)
		dataD[village.DistrictId+"gerindra"] += int(village.Gerindra)
		dataD[village.DistrictId+"pdip"] += int(village.PDIP)
		dataD[village.DistrictId+"golkar"] += int(village.Golkar)
		dataD[village.DistrictId+"nasdem"] += int(village.NasDem)
		dataD[village.DistrictId+"garuda"] += int(village.Garuda)
		dataD[village.DistrictId+"berkarya"] += int(village.Berkarya)
		dataD[village.DistrictId+"pks"] += int(village.PKS)
		dataD[village.DistrictId+"perindo"] += int(village.Perindo)
		dataD[village.DistrictId+"ppp"] += int(village.PPP)
		dataD[village.DistrictId+"psi"] += int(village.PSI)
		dataD[village.DistrictId+"pan"] += int(village.PAN)
		dataD[village.DistrictId+"hanura"] += int(village.Hanura)
		dataD[village.DistrictId+"demokrat"] += int(village.Demokrat)
		dataD[village.DistrictId+"pbb"] += int(village.PBB)
		dataD[village.DistrictId+"pkpi"] += int(village.PKPI)
	}

	for _, village := range dataKpus {
		var dt web.ResponseQuestionByLocation
		dt.VillageId = village.VillageId
		dt.DistrictId = village.DistrictId

		dt.Count = append(dt.Count, int(village.PKB), int(village.Gerindra), int(village.PDIP), int(village.Golkar), int(village.NasDem), int(village.Garuda), int(village.Berkarya),
			int(village.PKS), int(village.Perindo), int(village.PPP), int(village.PSI), int(village.PAN), int(village.Hanura), int(village.Demokrat), int(village.PBB), int(village.PKPI))

		dt.DistrictCount = append(dt.DistrictCount, dataD[village.DistrictId+"pkb"], dataD[village.DistrictId+"gerindra"], dataD[village.DistrictId+"pdip"], dataD[village.DistrictId+"golkar"],
			dataD[village.DistrictId+"nasdem"], dataD[village.DistrictId+"garuda"], dataD[village.DistrictId+"berkarya"], dataD[village.DistrictId+"pks"], dataD[village.DistrictId+"perindo"],
			dataD[village.DistrictId+"ppp"], dataD[village.DistrictId+"psi"], dataD[village.DistrictId+"pan"], dataD[village.DistrictId+"hanura"], dataD[village.DistrictId+"demokrat"], dataD[village.DistrictId+"pbb"],
			dataD[village.DistrictId+"pkpi"])
		dataV = append(dataV, dt)
	}

	data.Responses = dataV

	return data
}
