package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"mime/multipart"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type UserPemilihServiceImpl struct {
	UserPemilihRepository   repository.UserPemilihRepository
	OccupationRepository    repository.OccupationRepository
	RecruitRepository       repository.RecruitRepository
	RecruitedUserRepository repository.RecruitedUserRepository
	DistrictRepository      repository.DistrictRepository
	UserDptRepositoy        repository.UserDptRepository
	ReligionRepository      repository.ReligionRepository
	CandidateRepository     repository.CandidateRepository
	Validate                *validator.Validate
}

type UserPemilihService interface {
	Create(ctx *fiber.Ctx, request web.UserPemilih) domain.UserPemilih
	//FindById(userPemilihId string) web.UserPemilih
}

func NewUserPemilihService(
	userPemilihRepository repository.UserPemilihRepository,
	occupationRepository repository.OccupationRepository,
	recruitRepository repository.RecruitRepository,
	recruitedUserRepository repository.RecruitedUserRepository,
	districtRepository repository.DistrictRepository,
	userDptRepository repository.UserDptRepository,
	religionRepository repository.ReligionRepository,
	candidateRepository repository.CandidateRepository,
	validate *validator.Validate) UserPemilihService {
	return &UserPemilihServiceImpl{
		UserPemilihRepository:   userPemilihRepository,
		OccupationRepository:    occupationRepository,
		RecruitRepository:       recruitRepository,
		RecruitedUserRepository: recruitedUserRepository,
		DistrictRepository:      districtRepository,
		UserDptRepositoy:        userDptRepository,
		ReligionRepository:      religionRepository,
		CandidateRepository:     candidateRepository,
		Validate:                validate,
	}
}

func (service *UserPemilihServiceImpl) Create(ctx *fiber.Ctx, request web.UserPemilih) domain.UserPemilih {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	userDpt, err := service.UserDptRepositoy.FindById(request.DptId)
	if err != nil || userDpt.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "user not found in dpt"))
	}

	religion := service.ReligionRepository.FindById(request.ReligionId)
	if religion.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "religion id not found"))
	}

	candidate, _ := service.CandidateRepository.FindById(request.CandidateId)
	if candidate.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "candidate id not found"))
	}

	if !helper.IsNumeric(request.Phone) {
		panic(exception.NewError(fiber.StatusBadRequest, "phone should numeric"))
	}

	if len([]rune(request.Phone)) < 10 || len([]rune(request.Phone)) > 13 {
		panic(exception.NewError(fiber.StatusBadRequest, "phone should 10-13 digit"))
	}

	if nik, _ := service.UserPemilihRepository.FindByQuery("nik", userDpt.NIK); nik.Id != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "nik has been registered"))
	}
	if phone, _ := service.UserPemilihRepository.FindByQuery("phone", request.Phone); phone.Id != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "phone has been registered"))
	}

	occupation, err := service.OccupationRepository.FindByLevel("level", 4)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "occupation not found"))
	}

	checkAuth(ctx, occupation)

	linkKtp := service.UploadImage(ctx, "ktp")
	linkSelfie := service.UploadImage(ctx, "selfie")

	village, _ := service.DistrictRepository.FindVillageById(userDpt.VillageId)
	district, _ := service.DistrictRepository.FindDistrictById(village.DistrictId)
	regency, _ := service.DistrictRepository.FindRegencyById(district.RegencyId)

	userPemilih := domain.UserPemilih{
		Id:           strings.ToLower(randstr.String(10)),
		Name:         userDpt.Name,
		NIK:          userDpt.NIK,
		Phone:        request.Phone,
		ReligionId:   request.ReligionId,
		DptId:        request.DptId,
		KTP:          linkKtp,
		Selfie:       linkSelfie,
		VillageId:    userDpt.VillageId,
		DistrictId:   district.ID,
		RegencyId:    strconv.Itoa(regency.ID),
		CandidateId:  request.CandidateId,
		Longitude:    request.Longitude,
		Latitude:     request.Latitude,
		OccupationId: occupation.ID,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}

	if userDpt.JenisKelamin == "P" {
		userPemilih.Gender = "Perempuan"
	} else {
		userPemilih.Gender = "Laki-Laki"
	}

	if occupation.Level == 2 || occupation.Level == 3 || occupation.Level == 4 {
		// update userPemilih recruit
		userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))
		userPemilihRecruitment, err := service.RecruitRepository.FindByUserId(userId)

		if err != nil || userPemilihRecruitment.ID == "" {
			defaultGoal, _ := strconv.Atoi(helper.DefaultGoalRecruitment)

			userPemilihRecruitment = domain.UserRecruit{
				ID:              strings.ToLower(randstr.String(10)),
				UserId:          userId,
				RecruitProgress: 1,
				RecruitGoal:     defaultGoal,
			}

			service.RecruitRepository.Create(userPemilihRecruitment)
		} else {
			userPemilihRecruitment.RecruitProgress = userPemilihRecruitment.RecruitProgress + 1
			service.RecruitRepository.Update(userPemilihRecruitment)
		}

		//add userPemilih to recruited userPemilih
		recrutedUserPemilih := domain.RecruitedUser{
			ID:            strings.ToLower(randstr.String(10)),
			UserId:        userPemilih.Id,
			UserRecruitId: userPemilihRecruitment.ID,
			RecruiterId:   userId,
		}
		service.RecruitedUserRepository.Create(recrutedUserPemilih)
	}

	userPemilih.SetPassword(userDpt.NIK)

	service.UserPemilihRepository.Create(userPemilih)

	userPemilih.Occupation = occupation

	return userPemilih
}

// Upload Image
func (service *UserPemilihServiceImpl) UploadImage(c *fiber.Ctx, filetype string) string {
	ctx := context.Background()

	var bucketName string
	var file *multipart.FileHeader
	var err error
	if filetype == "ktp" {
		bucketName = os.Getenv("MINIO_BUCKET_KTP_PEMILIH")
		file, err = c.FormFile("ktp")
		if err != nil {
			panic(exception.NewError(fiber.StatusBadRequest, "bad request file ktp"))
		}
	} else {
		bucketName = os.Getenv("MINIO_BUCKET_SELFIE_PEMILIH")
		file, err = c.FormFile("selfie")
		if err != nil {
			panic(exception.NewError(fiber.StatusBadRequest, "bad request file selfie"))
		}
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request file"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.jpg", os.Getenv("MINIO_PATH"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	if fileSize > 512000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}
