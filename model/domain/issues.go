package domain

type Issue struct {
	ID    string      `json:"id" bson:"_id"`
	Label string      `json:"label" bson:"label"`
	Value string      `json:"value" bson:"value"`
	Color string      `json:"color" bson:"color"`
	Years []IssueYear `json:"years" bson:"-"`
}

type IssueSubData struct {
	Name string `json:"name" bson:"name"`
}

type IssueYear struct {
	ID            string         `json:"id" bson:"_id"`
	Year          string         `json:"year" bson:"year"`
	IssueSubs     []IssueSub     `json:"sub-issues" bson:"-"`
	IssueSubsData []IssueSubData `json:"sub_issue_data" bson:"sub_issue_data"`
}

type IssueDataBencana struct {
	Regencies string                  `json:"regencies"`
	Latitude  string                  `json:"latitude"`
	Longitude string                  `json:"longitude"`
	Total     int                     `json:"total"`
	Data      []DataMetadataIssueData `json:"data" bson:"data"`
	SubIssues []DataMetadataIssueData `json:"sub_issue" bson:"sub_issue"`
}
