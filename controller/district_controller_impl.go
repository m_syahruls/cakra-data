package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type DistrictControllerImpl struct {
	DistrictService service.DistrictService
}

type DistrictController interface {
	NewDistrictRouter(app *fiber.App)
}

func NewDistrictController(districtService service.DistrictService) DistrictController {
	return &DistrictControllerImpl{
		DistrictService: districtService,
	}
}

func (controller *DistrictControllerImpl) NewDistrictRouter(app *fiber.App) {
	district := app.Group("/districts")
	district.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	district.Get("/:districtid", controller.FindById)
	district.Get("/", controller.FindAll)
}

func (controller *DistrictControllerImpl) FindById(ctx *fiber.Ctx) error {
	districtId := ctx.Params("districtid")

	districtResponse := controller.DistrictService.FindById(districtId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    districtResponse,
	})
}

func (controller *DistrictControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query by question id
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	regencyId := ctx.Query("regencyid")
	name := ctx.Query("name")
	districtResponse := controller.DistrictService.FindAll(page, limit, regencyId, name)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    districtResponse,
	})
}
