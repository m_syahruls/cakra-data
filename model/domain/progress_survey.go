package domain

type ProgressSurvey struct {
	ID             string `json:"id" bson:"_id"`
	UserId         string `json:"user_id" bson:"user_id"`
	SurveyProgress int    `json:"survey_progess" bson:"survey_progress"`
	SurveyGoal     int    `json:"survey_goal" bson:"survey_goal"`
}

type ProgressSurveyDetail struct {
	ID             string       `json:"id" bson:"_id"`
	UserId         string       `json:"user_id" bson:"user_id"`
	SurveyProgress int          `json:"survey_progess" bson:"survey_progress"`
	SurveyGoal     int          `json:"survey_goal" bson:"survey_goal"`
	User           User         `json:"user" bson:"user"`
	RespondentUser []Respondent `json:"respondent_user" bson:"respondent_user"`
}

type ProgressSurveySummary struct {
	ID               string `json:"id" bson:"_id"`
	RespondentId     string `json:"respondent_id" bson:"respondent_id"`
	ProgressSurveyId string `json:"progres_survey_id" bson:"progress_survey_id"`
	SurveyorId       string `json:"surveyor_id" bson:"surveyor_id"`
}
