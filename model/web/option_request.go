package web

type OptionCreateRequest struct {
	QuestionId string `json:"question_id" validate:"required"`
	OptionName string `json:"option_name" validate:"required"`
	Value      *int   `json:"value" validate:"required"`
	Color      string `json:"color"`
}

type OptionUpdateRequest struct {
	OptionName string `json:"option_name" validate:"required"`
	Value      *int   `json:"value" validate:"required"`
	Color      string `json:"color"`
}
