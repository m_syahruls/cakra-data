package web

type IssueDataResponse struct {
	ID        string           `json:"id" bson:"_id"`
	Year      string           `json:"year" bson:"year"`
	IssueType string           `json:"issue_type" bson:"issue_type"`
	MetaData  IssueDataBencana `json:"metadata" bson:"-"`
}

type IssueDataResponseRank struct {
	ID        int               `json:"id" bson:"id"`
	Rank      int               `json:"rank,omitempty"`
	MetaData  IssueMetaDataRank `json:"metadata" bson:"metadata"`
	Wilayah   string            `json:"wilayah" bson:"wilayah"`
	SubIssues []SubIssues       `json:"sub_issue,omitempty" bson:"sub_issue"`
}

type IssueMetaDataRank struct {
	Total int `json:"total_incident" bson:"total_incident"`
}

type SubIssues struct {
	Name  string `json:"name" bson:"name"`
	Total int    `json:"total_incidents" bson:"total_incidents"`
	Rank  int    `json:"rank" bson:"_"`
}

type NewIssueDataResponse struct {
	ID           string            `json:"id" bson:"_id"`
	Year         string            `json:"year" bson:"year"`
	IssueType    string            `json:"issue_type" bson:"issue_type"`
	MetaData     MetadataIssueData `json:"metadata" bson:"metadata"`
	SubIssueData []SubIssueData    `json:"sub_issue_data" bson:"sub_issue_data"`
	Data         []DataWilayah     `json:"data" bson:"data"`
}

type MetadataIssueData struct {
	TotalKejadian int                     `json:"total_incidents" bson:"total_incidents"`
	Data          []DataMetadataIssueData `json:"data" bson:"data"`
}

type DataMetadataIssueData struct {
	Name  string `json:"name" bson:"name"`
	Total int    `json:"total_incidents" bson:"total_incidents"`
}

type SubIssueData struct {
	Name           string             `json:"name" bson:"name"`
	TotalIncidents int                `json:"total_incidents" bson:"total_incidents"`
	Data           []DataSubIssueData `json:"data" bson:"data"`
}

type DataSubIssueData struct {
	Name           string `json:"name" bson:"name"`
	TotalIncidents int    `json:"total_incidents" bson:"total_incidents"`
}

type DataWilayah struct {
	ID        int                      `json:"id" bson:"id"`
	Wilayah   string                   `json:"wilayah" bson:"wilayah"`
	MetaData  MetadataWilayahIssueData `json:"metadata" bson:"metadata"`
	SubIssues []DataSubIssueData       `json:"sub_issue" bson:"sub_issue"`
}

type MetadataWilayahIssueData struct {
	TotalIncidents int                     `json:"total_incidents" bson:"total_incidents"`
	Data           []DataMetadataIssueData `json:"data" bson:"data"`
}
