package service

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type UserServiceImpl struct {
	UserRepository          repository.UserRepository
	OccupationRepository    repository.OccupationRepository
	RecruitRepository       repository.RecruitRepository
	RecruitedUserRepository repository.RecruitedUserRepository
	DistrictRepository      repository.DistrictRepository
	Validate                *validator.Validate
}

type UserService interface {
	Create(ctx *fiber.Ctx, request web.UserCreateRequest) web.UserResponse
	FindById(userId string) web.UserDetailResponse
	FindAll(page, limit, level, districtId, searchName string) []web.UserResponse
	Update(ctx *fiber.Ctx, userid string, request web.UserUpdateRequest) web.UserResponse
	Delete(ctx *fiber.Ctx, userId string)

	IsReferral(userId, recuiterId string) bool

	CreateRespondentForSurvey(ctx *fiber.Ctx, request web.UserResponse)
	FindAllCoor(page, limit, level, districtId, searchName string) []web.UserDetailResponse
	TotalRelawan(districtId string) int
	TotalKoordinator(districtId string) int
	TotalPemilih() int
	TotalPemilihBaru() int
	GetRankByLevel(page, limit, level string) []web.UserDetailResponse
	UpdateOccupation(ctx *fiber.Ctx, userid string, request web.UserUpdateOccupationRequest) web.UserResponse
	UpdatePassword(ctx *fiber.Ctx, userid string, request web.UserUpdatePasswordRequest) web.UserResponse

	UpdateAccess(ctx *fiber.Ctx, userid string, request web.UserUpdateAccessRequest) web.UserResponse
	GetAccess(userId string) web.UserResponse

	FindRecruitedUserByUseriD(userId, level string) []web.UserResponse

	FindWithAyrshareToken() []web.UserResponseAyrshare
}

func NewUserService(
	userRepository repository.UserRepository,
	occupationRepository repository.OccupationRepository,
	recruitRepository repository.RecruitRepository,
	recruitedUserRepository repository.RecruitedUserRepository,
	districtRepository repository.DistrictRepository,
	validate *validator.Validate) UserService {
	return &UserServiceImpl{
		UserRepository:          userRepository,
		OccupationRepository:    occupationRepository,
		RecruitRepository:       recruitRepository,
		RecruitedUserRepository: recruitedUserRepository,
		DistrictRepository:      districtRepository,
		Validate:                validate,
	}
}

func (service *UserServiceImpl) FindWithAyrshareToken() []web.UserResponseAyrshare {
	users := service.UserRepository.FindWithAyrshareToken()
	return helper.ToAllUserAyrshareResponses(users)
}

func (service *UserServiceImpl) Create(ctx *fiber.Ctx, request web.UserCreateRequest) web.UserResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	if !helper.IsNumeric(request.NIK) {
		panic(exception.NewError(fiber.StatusBadRequest, "nik should numeric"))
	}

	if !helper.IsNumeric(request.Phone) {
		panic(exception.NewError(fiber.StatusBadRequest, "phone should numeric"))
	}

	if len([]rune(request.NIK)) != 16 {
		panic(exception.NewError(fiber.StatusBadRequest, "nik should 16 digit"))
	}

	if len([]rune(request.Phone)) < 10 || len([]rune(request.Phone)) > 13 {
		panic(exception.NewError(fiber.StatusBadRequest, "phone should 10-13 digit"))
	}

	if nik, _ := service.UserRepository.FindByQuery("nik", request.NIK); nik.Id != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "nik has been registered"))
	}
	if phone, _ := service.UserRepository.FindByQuery("phone", request.Phone); phone.Id != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "phone has been registered"))
	}
	if email, _ := service.UserRepository.FindByQuery("email", request.Email); email.Id != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "email has been registered"))
	}

	occupation, err := service.OccupationRepository.FindById(request.OccupationId)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "occupation not found"))
	}

	checkAuth(ctx, occupation)

	status := true

	user := domain.User{
		Id:           strings.ToLower(randstr.String(10)),
		OccupationId: request.OccupationId,
		NIK:          request.NIK,
		Name:         request.Name,
		Email:        request.Email,
		Phone:        request.Phone,
		Gender:       request.Gender,
		//ReferralCode: randstr.String(5),
		Longitude: request.Longitude,
		Latitude:  request.Latitude,
		Status:    &status,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Accesses:  request.Accesses,
	}

	if request.DistrictId != "" {
		districtExist, err := service.DistrictRepository.FindDistrictById(request.DistrictId)
		if err != nil || districtExist.ID == "" {
			panic(exception.NewError(fiber.StatusNotFound, "check again regency id"))
		}
		user.DistrictId = request.DistrictId
	}

	if occupation.Level == 2 || occupation.Level == 3 {
		user.ReferralCode = randstr.String(7)
	}

	if occupation.Level == 2 || occupation.Level == 3 || occupation.Level == 4 {
		// update user recruit
		userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))
		userRecruitment, err := service.RecruitRepository.FindByUserId(userId)

		if err != nil || userRecruitment.ID == "" {
			defaultGoal, _ := strconv.Atoi(helper.DefaultGoalRecruitment)

			userRecruitment = domain.UserRecruit{
				ID:              strings.ToLower(randstr.String(10)),
				UserId:          userId,
				RecruitProgress: 1,
				RecruitGoal:     defaultGoal,
			}

			service.RecruitRepository.Create(userRecruitment)
		} else {
			userRecruitment.RecruitProgress = userRecruitment.RecruitProgress + 1
			service.RecruitRepository.Update(userRecruitment)
		}

		//add user to recruited user
		recrutedUser := domain.RecruitedUser{
			ID:            strings.ToLower(randstr.String(10)),
			UserId:        user.Id,
			UserRecruitId: userRecruitment.ID,
			RecruiterId:   userId,
		}
		service.RecruitedUserRepository.Create(recrutedUser)
	}

	user.SetPassword(request.Password)

	service.UserRepository.Create(user)

	user.Occupation = occupation

	return helper.ToUserResponse(user)
}

func (service *UserServiceImpl) FindById(userId string) web.UserDetailResponse {
	user, err := service.UserRepository.FindDetailById(userId)
	if err != nil || user.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	return helper.ToDetailUserResponse(user)
}

func (service *UserServiceImpl) FindAll(page, limit, level, districtId, searchName string) []web.UserResponse {
	users := service.UserRepository.FindAll(page, limit, level, districtId, searchName)
	return helper.ToAllUserResponses(users)
}

func (service *UserServiceImpl) Update(ctx *fiber.Ctx, userid string, request web.UserUpdateRequest) web.UserResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.UserRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	user.Name = request.Name
	if request.Email != "" {
		user.Email = request.Email
	}

	if request.Phone != "" {
		if !helper.IsNumeric(request.Phone) {
			panic(exception.NewError(fiber.StatusBadRequest, "phone should numeric"))
		}

		if len([]rune(request.Phone)) < 10 || len([]rune(request.Phone)) > 13 {
			panic(exception.NewError(fiber.StatusBadRequest, "phone should 10-13 digit"))
		}

		if phone, _ := service.UserRepository.FindByQuery("phone", request.Phone); phone.Id != "" && phone.Id != userid {
			panic(exception.NewError(fiber.StatusBadRequest, "phone has been registered"))
		}

		user.Phone = request.Phone
	}

	if request.DistrictId != "" {
		districtExist, err := service.DistrictRepository.FindDistrictById(request.DistrictId)
		if err != nil || districtExist.ID == "" {
			panic(exception.NewError(fiber.StatusNotFound, "check again regency id"))
		}
		user.DistrictId = request.DistrictId
	}

	if request.Latitude != "" {
		user.Latitude = request.Latitude
	}

	if request.Longitude != "" {
		user.Longitude = request.Longitude
	}

	if request.Gender != "" {
		user.Gender = request.Gender
	}

	if request.Status != nil {
		user.Status = request.Status
	}

	occupation, _ := service.OccupationRepository.FindById(user.OccupationId)
	user.Occupation = occupation

	checkAuth(ctx, occupation)

	user.UpdatedAt = time.Now()
	service.UserRepository.Update(user, userid)

	return helper.ToUserResponse(user)
}

func (service *UserServiceImpl) Delete(ctx *fiber.Ctx, userId string) {

	actorId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user, err := service.UserRepository.FindDetailById(userId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}
	occupation, _ := service.OccupationRepository.FindById(user.OccupationId)

	checkAuth(ctx, occupation)
	if isReferal := service.IsReferral(userId, actorId); !isReferal {
		panic(exception.NewError(fiber.StatusUnauthorized, "this user not your referral"))
	}

	recruit, err := service.RecruitedUserRepository.FindRecruitedByRecruiterId(userId)
	if recruit.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "this user have downline"))
	}

	err = service.UserRepository.Delete(userId)
	helper.PanicIfError(err)

	recruitedRelation, _ := service.RecruitedUserRepository.FindByUserId(userId)

	// delete from relation
	service.RecruitedUserRepository.Delete(userId)

	// delete progres
	service.RecruitRepository.Delete(userId)

	//update user recruit
	userRecruitment, err := service.RecruitRepository.FindByUserId(recruitedRelation.RecruiterId)

	if err == nil || userRecruitment.ID != "" {
		userRecruitment.RecruitProgress = userRecruitment.RecruitProgress - 1
		service.RecruitRepository.Update(userRecruitment)
	}

	log.Println("LOG", "deleted user", user.Name)
}

func (service *UserServiceImpl) IsReferral(userId, recuiterId string) bool {
	recruit, err := service.RecruitedUserRepository.IsReferral(userId, recuiterId)

	if err != nil || recruit.ID == "" {
		return false
	}

	return true
}

func checkAuth(ctx *fiber.Ctx, occupation domain.Occupation) {
	if occupation.Level == 2 {
		if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
			panic(exception.NewError(fiber.StatusUnauthorized, fmt.Sprintf("only admin can manage %s", occupation.Name)))
		}
	} else if occupation.Level == 3 {
		if isCoor := middleware.IsCoordinator(ctx); !isCoor {
			panic(exception.NewError(fiber.StatusUnauthorized, fmt.Sprintf("only coordinator can manage %s", occupation.Name)))
		}
	} else if occupation.Level == 4 {
		if isRelawan := middleware.IsRelawan(ctx); !isRelawan {
			panic(exception.NewError(fiber.StatusUnauthorized, fmt.Sprintf("only relawan can manage %s", occupation.Name)))
		}
	} else if occupation.Level == 5 {
		if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
			panic(exception.NewError(fiber.StatusUnauthorized, fmt.Sprintf("only admin can manage %s", occupation.Name)))
		}
	} else {
		panic(exception.NewError(fiber.StatusUnauthorized, fmt.Sprintf("dont have authorization to manage user %s", occupation.Name)))
	}
}

func (service *UserServiceImpl) CreateRespondentForSurvey(ctx *fiber.Ctx, request web.UserResponse) {
	//Create respondent
	responden := domain.User{
		Id:        request.Id,
		Name:      request.Name,
		Phone:     request.Phone,
		Email:     request.Email,
		Gender:    request.Gender,
		CreatedAt: time.Now(),
	}
	service.UserRepository.CreateRespondent(responden)
}

func (service *UserServiceImpl) FindAllCoor(page, limit, level, districtId, searchName string) []web.UserDetailResponse {
	users := service.UserRepository.GetAllCoordinator(page, limit, level, districtId, searchName)
	return helper.ToAllUserDetailResponses(users)
}

func (service *UserServiceImpl) TotalRelawan(districtId string) int {
	return service.UserRepository.TotalRelawan(districtId)
}

func (service *UserServiceImpl) TotalKoordinator(districtId string) int {
	return service.UserRepository.TotalKoordinator(districtId)
}

func (service *UserServiceImpl) TotalPemilih() int {
	return service.UserRepository.TotalPemilih()
}

func (service *UserServiceImpl) TotalPemilihBaru() int {
	return service.UserRepository.TotalPemilihBaru()
}

func (service *UserServiceImpl) GetRankByLevel(page, limit, level string) []web.UserDetailResponse {
	users := service.UserRepository.GetRankByLevel(page, limit, level)
	return helper.ToAllUserDetailResponses(users)
}

func (service *UserServiceImpl) UpdatePassword(ctx *fiber.Ctx, userid string, request web.UserUpdatePasswordRequest) web.UserResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, level, _ := helper.ParseJwt(ctx.Cookies("token"))

	if level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can change occupation"))
	}

	user, err := service.UserRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	occupation, _ := service.OccupationRepository.FindById(user.OccupationId)
	user.Occupation = occupation

	user.SetPassword(request.Password)

	user.UpdatedAt = time.Now()
	service.UserRepository.UpdatePassword(user, userid)

	return helper.ToUserResponse(user)
}

func (service *UserServiceImpl) UpdateOccupation(ctx *fiber.Ctx, userid string, request web.UserUpdateOccupationRequest) web.UserResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	actorId, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	newOccupation, err := service.OccupationRepository.FindById(request.OccupationId)
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "occupation not found"))
	}

	user, err := service.UserRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	occupation, _ := service.OccupationRepository.FindById(user.OccupationId)
	user.Occupation = occupation

	if level != "1" && newOccupation.Level != 5 && occupation.Level == 5 {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can change occupation black list user"))
	}

	if level != "1" && newOccupation.Level != 5 {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can change occupation"))
	}

	// checkAuth(ctx, occupation)
	if isReferal := service.IsReferral(userid, actorId); !isReferal {
		panic(exception.NewError(fiber.StatusUnauthorized, "this user not your referral"))
	}

	user.OccupationId = request.OccupationId
	user.Occupation = newOccupation

	user.UpdatedAt = time.Now()
	service.UserRepository.UpdateOccupation(user, userid)

	return helper.ToUserResponse(user)
}

func (service *UserServiceImpl) FindRecruitedUserByUseriD(userId, level string) []web.UserResponse {
	user, _ := service.UserRepository.FindRecruitedUserByUseriD(userId, level)

	return helper.ToAllUserResponses(user)
}

func (service *UserServiceImpl) UpdateAccess(ctx *fiber.Ctx, userid string, request web.UserUpdateAccessRequest) web.UserResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.UserRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	if request.Accesses != nil {
		user.Accesses = request.Accesses
	}

	user.UpdatedAt = time.Now()
	service.UserRepository.Update(user, userid)

	return helper.ToUserResponse(user)
}

func (service *UserServiceImpl) GetAccess(userId string) web.UserResponse {
	user, err := service.UserRepository.FindById(userId)
	if err != nil || user.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	return helper.ToUserResponse(user)
}
