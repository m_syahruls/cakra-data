package domain

type Options struct {
	ID         string `json:"id" bson:"_id"`
	QuestionId string `json:"question_id" bson:"question_id"`
	OptionName string `json:"option_name" bson:"option_name"`
	Value      *int   `json:"value" bson:"value"`
	Color      string `json:"color" bson:"color"`
}
