package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type OccupationRepositoryImpl struct {
	Collection *mongo.Collection
}

type OccupationRepository interface {
	Create(occupation domain.Occupation)
	FindById(occupationId string) (domain.Occupation, error)
	FindByLevel(query string, value int) (domain.Occupation, error)
	FindAll() []domain.Occupation
	Update(occupation domain.Occupation, occupationId string)
	Delete(occupationId string) error
}

func NewOccupationRepository(database *mongo.Database) OccupationRepository {
	return &OccupationRepositoryImpl{
		Collection: database.Collection("occupations"),
	}
}

func (repository *OccupationRepositoryImpl) Create(occupation domain.Occupation) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":   occupation.ID,
		"level": occupation.Level,
		"name":  occupation.Name,
	})

	helper.PanicIfError(err)
}

func (repository *OccupationRepositoryImpl) FindById(occupationId string) (domain.Occupation, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var occupation domain.Occupation
	query := bson.M{"_id": occupationId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&occupation)

	return occupation, result.Err()
}

func (repository *OccupationRepositoryImpl) FindByLevel(query string, value int) (domain.Occupation, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var occupation domain.Occupation
	key := query
	find := bson.M{key: value}

	result := repository.Collection.FindOne(ctx, find)
	result.Decode(&occupation)

	return occupation, nil
}

func (repository *OccupationRepositoryImpl) FindAll() []domain.Occupation {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	options := options.Find()
	options.SetSort(bson.D{{Key: "created_at", Value: -1}})

	cursor, err := repository.Collection.Find(ctx, bson.M{}, options)
	helper.PanicIfError(err)

	var occupations []domain.Occupation

	for cursor.Next(ctx) {
		var occupation domain.Occupation
		cursor.Decode(&occupation)
		occupations = append(occupations, occupation)
	}

	return occupations
}

func (repository *OccupationRepositoryImpl) Update(occupation domain.Occupation, occupationId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": occupationId}
	update := bson.M{
		"$set": bson.M{
			"name": occupation.Name,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *OccupationRepositoryImpl) Delete(occupationId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": occupationId,
	})

	helper.PanicIfError(err)

	return nil
}
