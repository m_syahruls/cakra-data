package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type UserPemilihRepositoryImpl struct {
	Collection           *mongo.Collection
	CollectionRespondent *mongo.Collection
}

type UserPemilihRepository interface {
	Create(userPemilih domain.UserPemilih)
	FindById(userPemilihId string) (domain.UserPemilih, error)
	FindByQuery(query, value string) (domain.UserPemilih, error)
}

func NewUserPemilihRepository(database *mongo.Database) UserPemilihRepository {
	return &UserPemilihRepositoryImpl{
		Collection:           database.Collection("users"),
		CollectionRespondent: database.Collection("respondent_user"),
	}
}

func (repository *UserPemilihRepositoryImpl) Create(userPemilih domain.UserPemilih) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":           userPemilih.Id,
		"occupation_id": userPemilih.OccupationId,
		"nik":           userPemilih.NIK,
		"name":          userPemilih.Name,
		"email":         userPemilih.Email,
		"phone":         userPemilih.Phone,
		"gender":        userPemilih.Gender,
		"village_id":    userPemilih.VillageId,
		"district_id":   userPemilih.DistrictId,
		"regency_id":    userPemilih.RegencyId,
		"longitude":     userPemilih.Longitude,
		"latitude":      userPemilih.Latitude,
		"image_ktp":     userPemilih.KTP,
		"foto_selfie":   userPemilih.Selfie,
		"dpt_id":        userPemilih.DptId,
		"candidate_id":  userPemilih.CandidateId,
		"password":      userPemilih.Password,
		"created_at":    userPemilih.CreatedAt,
		"updated_at":    userPemilih.UpdatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *UserPemilihRepositoryImpl) FindById(userPemilihId string) (domain.UserPemilih, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var survey domain.UserPemilih
	query := bson.M{"_id": userPemilihId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&survey)

	return survey, result.Err()
}

func (repository *UserPemilihRepositoryImpl) FindByQuery(query, value string) (domain.UserPemilih, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var user domain.UserPemilih
	key := query
	find := bson.M{key: value}

	result := repository.Collection.FindOne(ctx, find)
	result.Decode(&user)

	return user, nil
}
