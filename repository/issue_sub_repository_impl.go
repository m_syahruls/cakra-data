package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type IssueSubRepositoryImpl struct {
	Collection      *mongo.Collection
	CollectionIssue *mongo.Collection
}

type IssueSubRepository interface {
	FindById(issueSubId string) (domain.IssueSub, error)
	FindAll(page, limit, issueId string, label string, value string) []domain.IssueSub

	FindIsExist(label string, value string) bool

	Create(issueSub domain.IssueSub)
	Update(issueSub domain.IssueSub, issueSubId string)
	Delete(issueSubId string) error
}

func NewIssueSubRepository(database *mongo.Database) IssueSubRepository {
	return &IssueSubRepositoryImpl{
		Collection:      database.Collection("issues_sub"),
		CollectionIssue: database.Collection("issueSubs"),
	}
}

func (repository *IssueSubRepositoryImpl) FindById(issueSubId string) (domain.IssueSub, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var issueSub domain.IssueSub
	query := bson.M{"_id": issueSubId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&issueSub)

	return issueSub, result.Err()
}

func (repository *IssueSubRepositoryImpl) FindAll(page, limit, issueId string, label string, value string) []domain.IssueSub {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryIssueSub(issueId, label, value)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var issueSubs []domain.IssueSub

	for cursor.Next(ctx) {
		var issueSub domain.IssueSub
		cursor.Decode(&issueSub)
		issueSubs = append(issueSubs, issueSub)
	}

	return issueSubs
}

func (repository *IssueSubRepositoryImpl) FindIsExist(label string, value string) bool {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssueExist(label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	for cursor.Next(ctx) {
		return true
	}

	return false
}

func (repository *IssueSubRepositoryImpl) Create(issueSub domain.IssueSub) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":       issueSub.ID,
		"issue_id":  issueSub.IssueID,
		"label":     issueSub.Label,
		"value":     issueSub.Value,
		"image_url": issueSub.ImageUrl,
	})

	helper.PanicIfError(err)
}

func (repository *IssueSubRepositoryImpl) Update(issueSub domain.IssueSub, issueSubId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": issueSubId}
	update := bson.M{
		"$set": bson.M{
			"issue_id":  issueSub.IssueID,
			"label":     issueSub.Label,
			"value":     issueSub.Value,
			"image_url": issueSub.ImageUrl,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *IssueSubRepositoryImpl) Delete(issueSubId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": issueSubId,
	})

	helper.PanicIfError(err)

	return nil
}
