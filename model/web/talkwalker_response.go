package web

import "time"

type TalkwalkerResponse struct {
	ID        string    `json:"id" bson:"_id"`
	Name      string    `json:"name" bson:"name"`
	FileUrl   string    `json:"file_url"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
}
