package web

type PermissionCreateRequest struct {
	Label string `json:"label" validate:"required"`
	Value string `json:"value" validate:"required"`
}

type PermissionUpdateRequest struct {
	Label string `json:"label" validate:"required"`
	Value string `json:"value" validate:"required"`
}
