package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ProgresSurveySumRepositoryImpl struct {
	Collection *mongo.Collection
}

type ProgresSurveySumRepository interface {
	Create(recruit domain.ProgressSurveySummary)
	IsReferral(userId, recruiterId string) (domain.ProgressSurveySummary, error)
	FindByQuery(query bson.M) (domain.ProgressSurveySummary, error)
	Delete(surveySummaryId string) error
}

func NewProgresSurveySumRepository(database *mongo.Database) ProgresSurveySumRepository {
	return &ProgresSurveySumRepositoryImpl{
		Collection: database.Collection("progress_survey_summary"),
	}
}

func (repository *ProgresSurveySumRepositoryImpl) Create(recruited domain.ProgressSurveySummary) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":                recruited.ID,
		"respondent_id":      recruited.RespondentId,
		"progress_survey_id": recruited.ProgressSurveyId,
		"surveyor_id":        recruited.SurveyorId,
	})

	helper.PanicIfError(err)
}

func (repository *ProgresSurveySumRepositoryImpl) IsReferral(userId, recruiterId string) (domain.ProgressSurveySummary, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var recruit domain.ProgressSurveySummary
	query := bson.M{"user_id": userId, "recruiter_id": recruiterId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&recruit)

	return recruit, result.Err()
}

func (repository *ProgresSurveySumRepositoryImpl) FindByQuery(query bson.M) (domain.ProgressSurveySummary, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var recruit domain.ProgressSurveySummary

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&recruit)

	return recruit, result.Err()
}

func (repository *ProgresSurveySumRepositoryImpl) Delete(surveySummaryId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": surveySummaryId,
	})

	helper.PanicIfError(err)

	return nil
}
