package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type ElectionTypeControllerImpl struct {
	ElectionTypeService service.ElectionTypeService
}

type ElectionTypeController interface {
	NewElectionTypeRouter(app *fiber.App)
}

func NewElectionTypeController(electionTypeService service.ElectionTypeService) ElectionTypeController {
	return &ElectionTypeControllerImpl{
		ElectionTypeService: electionTypeService,
	}
}

func (controller *ElectionTypeControllerImpl) NewElectionTypeRouter(app *fiber.App) {
	electionType := app.Group("/election-types")
	electionType.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	electionType.Get("/", controller.FindAll)
}

func (controller *ElectionTypeControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")

	electionType := controller.ElectionTypeService.FindAll(page, limit)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    electionType,
	})
}
