package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type VillageControllerImpl struct {
	VillageService service.VillageService
}

type VillageController interface {
	NewVillageRouter(app *fiber.App)
}

func NewVillageController(villageService service.VillageService) VillageController {
	return &VillageControllerImpl{
		VillageService: villageService,
	}
}

func (controller *VillageControllerImpl) NewVillageRouter(app *fiber.App) {
	village := app.Group("/villages")
	village.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	village.Get("/:villageid", controller.FindById)
	village.Get("/", controller.FindAll)
}

func (controller *VillageControllerImpl) FindById(ctx *fiber.Ctx) error {
	villageId := ctx.Params("villageid")

	villageResponse := controller.VillageService.FindById(villageId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    villageResponse,
	})
}

func (controller *VillageControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query by question id
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	districtId := ctx.Query("districtid")
	regencyId := ctx.Query("regencyid")
	name := ctx.Query("name")
	villageResponse := controller.VillageService.FindAll(page, limit, districtId, name, regencyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    villageResponse,
	})
}
