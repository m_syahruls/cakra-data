package web

import (
	"time"

	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

// request from our client

type MTKPostReportRequest struct {
	KeywordId string `json:"keyword_id"`
	FromTime  string `json:"from_time"`
	ToTime    string `json:"to_time"`
}

type MTKCreateGroupRequest struct {
	Name   string `json:"name"`
	Public bool   `json:"public"`
}

// Create Topic Request

type MTKCreateTopicRequest struct {
	Name    string       `json:"name"`
	Keyword topicKeyword `json:"keyword"`
}

type topicKeyword struct {
	Query         keywordQuery `json:"query"`
	MayLocations  []string     `json:"may_locations"`
	MayLanguages  []string     `json:"may_languages"`
	MaySourceType []string     `json:"may_source_types"`
	NotTags       []string     `json:"not_tags"`
	NotLangs      []string     `json:"not_langs"`
	NotSourceType []string     `json:"not_source_types"`
}

type keywordQuery struct {
	Boolean []queryBoolean `json:"boolean"`
	Phrase  queryPhrase    `json:"phrase"`
}

type queryBoolean struct {
	Clause string `json:"clause"`
	Sub    struct {
		Phrase struct {
			CaseSensitive bool   `json:"case_sensitive"`
			Text          string `json:"text"`
		} `json:"phrase"`
	} `json:"sub"`
}

type queryPhrase struct {
	CaseSensitive bool   `json:"case_sensitive"`
	Section       string `json:"section"`
	Text          string `json:"text"`
	Fuzzy         string `json:"fuzzy"`
}

// end of create topic request

// type MTKPostReportRequest struct {
// 	KeywordId      string `json:"keyword_id"`
// 	FeedType       string `json:"feed_type"`
// 	FromTime       string `json:"from_time"`
// 	ToTime         string `json:"to_time"`
// 	TimeResolution string `json:"time_resolution"`
// 	DimensionType  string `json:"dimension_type"`
// 	SortDirection  string `json:"sort_direction"`
// 	SortBy         string `json:"sort_by"`
// 	ValueType      string `json:"value_type"`
// 	MergeOperator  string `json:"merge_operator"`
// }

// type MTKPostReportDetailRequest struct {
// 	KeywordId      string `json:"keyword_id"`
// 	FeedType       string `json:"feed_type"`
// 	FromTime       string `json:"from_time"`
// 	ToTime         string `json:"to_time"`
// 	TimeResolution string `json:"time_resolution"`
// 	DimensionType  string `json:"dimension_type"`
// 	SubDimension   string `json:"sub_dimension"`
// 	SortDirection  string `json:"sort_direction"`
// 	SortBy         string `json:"sort_by"`
// 	ValueType      string `json:"value_type"`
// 	MergeOperator  string `json:"merge_operator"`
// }

// end of request from our client

// start of completed post report request to Mediatoolkit
type MTKPostReportRequestComplete struct {
	AccessToken string      `json:"access_token"`
	DataRequest DataRequest `json:"data_request"`
}

type MTKPostReportRequestBriefComplete struct {
	AccessToken string           `json:"access_token"`
	DataRequest DataRequestBrief `json:"data_request"`
}

type MTKPostReportRequestDetailComplete struct {
	AccessToken string            `json:"access_token"`
	DataRequest DataRequestDetail `json:"data_request"`
}

type DataRequest struct {
	SourceFeeds    []SourceFeeds `json:"source_feeds"`
	FromTime       string        `json:"from_time"`
	ToTime         string        `json:"to_time"`
	TimeResolution string        `json:"time_resolution"`
	Dimension      Dimension     `json:"dimension"`
	ReportValue    ReportValue   `json:"report_value"`
}

type DataRequestBrief struct {
	SourceFeeds    []SourceFeeds `json:"source_feeds"`
	FromTime       string        `json:"from_time"`
	ToTime         string        `json:"to_time"`
	TimeResolution string        `json:"time_resolution"`
	ReportValue    ReportValue   `json:"report_value"`
}

type DataRequestDetail struct {
	SourceFeeds    []SourceFeeds `json:"source_feeds"`
	FromTime       string        `json:"from_time"`
	ToTime         string        `json:"to_time"`
	TimeResolution string        `json:"time_resolution"`
	Dimension      Dimension     `json:"dimension"`
	SubDimension   Dimension     `json:"sub_dimension"`
	ReportValue    ReportValue   `json:"report_value"`
}

type SourceFeeds struct {
	FeedType  string `json:"feed_type"`
	KeywordId string `json:"keyword_id"`
}

type Dimension struct {
	DimensionType string        `json:"dimension_type"`
	DimensionSort DimensionSort `json:"dimension_sort"`
}

type DimensionSort struct {
	SortDirection string `json:"sort_direction"`
	SortBy        string `json:"sort_by"`
}

type ReportValue struct {
	ValueType     string `json:"value_type"`
	MergeOperator string `json:"merge_operator"`
}

// end of completed post report request

type SocialCreateRequest struct {
	Summary        Summary                `json:"summary" bson:"summary"`
	AudienceGrowth domain.SocialData      `json:"audience_growth" bson:"audience_growth"`
	Impression     domain.SocialData      `json:"impression" bson:"impression"`
	Engagement     domain.SocialData      `json:"engagement" bson:"engagement"`
	EngagementRate domain.SocialDataFloat `json:"engagement_rate" bson:"engagement_rate"`
	VideoViews     domain.SocialData      `json:"video_views" bson:"video_views"`
	Timestamp      time.Time              `json:"timestamp" bson:"timestamp"`
}

type SocialUpdateSummaryRequest struct {
	Impressions    float32 `json:"impressions" bson:"impressions"`
	Engagements    float32 `json:"engagements" bson:"engagements"`
	EngagementRate float32 `json:"engagement_rate" bson:"engagement_rate"`
	PostLinkClicks float32 `json:"post_link_clicks" bson:"post_link_clicks"`
}

type SocialUpdateDatapointRequest struct {
	AudienceGrowth domain.SocialData      `json:"audience_growth" bson:"audience_growth"`
	Impression     domain.SocialData      `json:"impression" bson:"impression"`
	Engagement     domain.SocialData      `json:"engagement" bson:"engagement"`
	EngagementRate domain.SocialDataFloat `json:"engagement_rate" bson:"engagement_rate"`
	VideoViews     domain.SocialData      `json:"video_views" bson:"video_views"`
	Timestamp      time.Time              `json:"timestamp" bson:"timestamp"`
}

type Summary struct {
	Impressions    float32 `json:"impressions" bson:"impressions"`
	Engagements    float32 `json:"engagements" bson:"engagements"`
	EngagementRate float32 `json:"engagement_rate" bson:"engagement_rate"`
	PostLinkClicks float32 `json:"post_link_clicks" bson:"post_link_clicks"`
}
