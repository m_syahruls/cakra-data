package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type ProvinceControllerImpl struct {
	ProvinceService service.ProvinceService
}

type ProvinceController interface {
	NewProvinceRouter(app *fiber.App)
}

func NewProvinceController(provinceService service.ProvinceService) ProvinceController {
	return &ProvinceControllerImpl{
		ProvinceService: provinceService,
	}
}

func (controller *ProvinceControllerImpl) NewProvinceRouter(app *fiber.App) {
	province := app.Group("/provinces")
	province.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	province.Get("/:provinceid", controller.FindById)
	province.Get("/", controller.FindAll)
}

func (controller *ProvinceControllerImpl) FindById(ctx *fiber.Ctx) error {
	provinceId := ctx.Params("provinceid")

	provinceResponse := controller.ProvinceService.FindById(provinceId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    provinceResponse,
	})
}

func (controller *ProvinceControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query by question id
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	name := ctx.Query("name")
	provinceResponse := controller.ProvinceService.FindAll(page, limit, name)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    provinceResponse,
	})
}
