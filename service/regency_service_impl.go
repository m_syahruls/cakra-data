package service

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type RegencyServiceImpl struct {
	RegencyRepository repository.RegencyRepository
}

type RegencyService interface {
	FindAll(page, limit, provinceId, name string) []web.RegencyResponse
	FindById(regencyId string) web.RegencyResponse
	FindByName(regencyName string) web.RegencyResponse
}

func NewRegencyService(regencyRepository repository.RegencyRepository) RegencyService {
	return &RegencyServiceImpl{
		RegencyRepository: regencyRepository,
	}
}

func (service *RegencyServiceImpl) FindById(regencyId string) web.RegencyResponse {
	regency, err := service.RegencyRepository.FindById(regencyId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "regency not found"))
	}

	return helper.ToRegencyResponse(regency)
}

func (service *RegencyServiceImpl) FindByName(regencyName string) web.RegencyResponse {
	regency, err := service.RegencyRepository.FindByName(regencyName)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "regency not found"))
	}

	return helper.ToRegencyResponse(regency)
}

func (service *RegencyServiceImpl) FindAll(page, limit, provinceId, name string) []web.RegencyResponse {
	regency := service.RegencyRepository.FindAll(page, limit, provinceId, name)
	return helper.ToRegencyResponses(regency)
}
