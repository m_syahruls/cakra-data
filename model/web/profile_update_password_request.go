package web

type UpdatePasswordProfile struct {
	OldPassword     string `json:"old_password" validate:"required"`
	Password        string `json:"password" validate:"required"`
	PasswordConfirm string `json:"password_confirm" validate:"required"`
}
