package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type PartaiRepositoryImpl struct {
	Collection *mongo.Collection
}

type PartaiRepository interface {
	Create(partai domain.Partai)
	FindAll(page, limit string) []domain.Partai
	FindById(partaiId string) domain.Partai
}

func NewPartaiRepository(database *mongo.Database) PartaiRepository {
	return &PartaiRepositoryImpl{
		Collection: database.Collection("partai"),
	}
}

func (repository *PartaiRepositoryImpl) Create(partai domain.Partai) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":  partai.Id,
		"name": partai.Name,
		"slug": partai.Slug,
	})

	helper.PanicIfError(err)
}

func (repository *PartaiRepositoryImpl) FindAll(page, limit string) []domain.Partai {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	cursor, err := repository.Collection.Find(ctx, bson.M{}, filter)
	helper.PanicIfError(err)

	var parties []domain.Partai

	for cursor.Next(ctx) {
		var partai domain.Partai
		cursor.Decode(&partai)
		parties = append(parties, partai)
	}

	return parties
}

func (repository *PartaiRepositoryImpl) FindById(partaiId string) domain.Partai {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor := repository.Collection.FindOne(ctx, bson.M{"_id": partaiId})

	var partai domain.Partai
	cursor.Decode(&partai)

	return partai
}
