package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type SurveyResultControllerImpl struct {
	SurveyResultService service.SurveyResultService
}

type SurveyResultController interface {
	NewSurveyResultRouter(app *fiber.App)
}

func NewSurveyResultController(surveyService service.SurveyResultService) SurveyResultController {
	return &SurveyResultControllerImpl{
		SurveyResultService: surveyService,
	}
}

func (controller *SurveyResultControllerImpl) NewSurveyResultRouter(app *fiber.App) {
	survey := app.Group("/survey-result")
	survey.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})
	survey.Get("/:surveyid", controller.FindDetailById)
	survey.Get("/date-count/:surveyid", controller.FindCountResponseDateById)
}

func (controller *SurveyResultControllerImpl) FindDetailById(ctx *fiber.Ctx) error {
	surveyId := ctx.Params("surveyid")
	questionId := ctx.Query("questionid")
	villageId := ctx.Query("villageid")
	districtId := ctx.Query("districtid")
	regencyId := ctx.Query("regencyid")

	isAdmin := middleware.IsAdmin(ctx)
	isKoordinator := middleware.IsCoordinator(ctx)

	if !isAdmin && !isKoordinator {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can see"))
	}

	surveyResponse := controller.SurveyResultService.FindDetailById(surveyId, questionId, villageId, districtId, regencyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    surveyResponse,
	})
}

func (controller *SurveyResultControllerImpl) FindCountResponseDateById(ctx *fiber.Ctx) error {
	surveyId := ctx.Params("surveyid")

	isAdmin := middleware.IsAdmin(ctx)
	isKoordinator := middleware.IsCoordinator(ctx)

	if !isAdmin && !isKoordinator {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can see"))
	}

	surveyResponse := controller.SurveyResultService.FindCountResponseDateById(surveyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    surveyResponse,
	})
}
