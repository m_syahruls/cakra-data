package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type ProgresSurveyControllerImpl struct {
	ProgresSurveyService service.ProgresSurveyService
	LogService           service.LogService
}

type ProgresSurveyController interface {
	NewProgresSurveyRouter(app *fiber.App)
}

func NewProgresSurveyController(progresSurveyService service.ProgresSurveyService, logService service.LogService) ProgresSurveyController {
	return &ProgresSurveyControllerImpl{
		ProgresSurveyService: progresSurveyService,
		LogService:           logService,
	}
}

func (controller *ProgresSurveyControllerImpl) NewProgresSurveyRouter(app *fiber.App) {
	progresSurvey := app.Group("/progress-survey")
	progresSurvey.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})
	progresSurvey.Get("/", controller.FindAll)
	progresSurvey.Get("/:surveyorid", controller.FindDetailBySurveyorId)
	progresSurvey.Put("/:surveyorid", controller.UpdateGoal)
}

func (controller *ProgresSurveyControllerImpl) FindDetailBySurveyorId(ctx *fiber.Ctx) error {
	surveyorId := ctx.Params("surveyorid")

	recruitResponse := controller.ProgresSurveyService.FindDetailByUserId(ctx, surveyorId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    recruitResponse,
	})
}

func (controller *ProgresSurveyControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	userId := ctx.Query("userid")
	recruitResponse := controller.ProgresSurveyService.FindAll(ctx, page, limit, userId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    recruitResponse,
	})
}

func (controller *ProgresSurveyControllerImpl) UpdateGoal(ctx *fiber.Ctx) error {
	var request web.UpdateGoalSurvey
	surveyorId := ctx.Params("surveyorid")

	if admin := middleware.IsAdmin(ctx); !admin {
		return UnauthorizeReturn(ctx, "only admin can change goal")
	}

	request.UserId = surveyorId
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	recruitResponse := controller.ProgresSurveyService.UpdateGoal(ctx, request)

	action := fmt.Sprintf("update survey goal for %s", request.UserId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    recruitResponse,
	})
}
