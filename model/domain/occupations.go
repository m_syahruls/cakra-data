package domain

type Occupation struct {
	ID    string `json:"id" bson:"_id"`
	Level int    `json:"level" bson:"level"`
	Name  string `json:"name" bson:"name"`
}
