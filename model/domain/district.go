package domain

type Districts struct {
	ID        string      `json:"id" bson:"_id"`
	RegencyId string      `json:"regency_id" bson:"regency_id"`
	Name      string      `json:"name" bson:"name"`
	AltName   string      `json:"alt_name" bson:"alt_name"`
	Latitude  interface{} `json:"latitude" bson:"latitude"`
	Longitude interface{} `json:"longitude" bson:"longitude"`
}
