package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type TalkwalkerServiceImpl struct {
	TalkwalkerRepository repository.TalkwalkerRepository
	Validate             *validator.Validate
}

type TalkwalkerService interface {
	FindAll(page, limit, name string) []web.TalkwalkerResponse
	Create(ctx *fiber.Ctx, request web.CreateTalkwalkerRequest) web.TalkwalkerResponse
	UploadFileTalkwalker(c *fiber.Ctx) string
}

func NewTalkwalkerService(TalkwalkerRepository repository.TalkwalkerRepository, validate *validator.Validate) TalkwalkerService {
	return &TalkwalkerServiceImpl{
		TalkwalkerRepository: TalkwalkerRepository,
		Validate:             validate,
	}
}

func (service *TalkwalkerServiceImpl) FindAll(page, limit, name string) []web.TalkwalkerResponse {
	Talkwalker := service.TalkwalkerRepository.FindAll(page, limit, name)
	return helper.ToTalkwalkerResponses(Talkwalker)
}

func (service *TalkwalkerServiceImpl) Create(ctx *fiber.Ctx, request web.CreateTalkwalkerRequest) web.TalkwalkerResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	talkwalker := domain.Talkwalker{
		ID:        strings.ToLower(randstr.String(10)),
		Name:      request.Name,
		CreatedAt: time.Now(),
	}
	linkFile := service.UploadFileTalkwalker(ctx)
	talkwalker.FileUrl = linkFile
	service.TalkwalkerRepository.Create(talkwalker)

	return helper.ToTalkwalkerResponse(talkwalker)
}

func (service *TalkwalkerServiceImpl) UploadFileTalkwalker(c *fiber.Ctx) string {
	ctx := context.Background()

	bucketName := os.Getenv("MINIO_BUCKET_TALKWALKER")
	file, err := c.FormFile("files")
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request, files cant be empty"))
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.pdf", os.Getenv("MINIO_PATH_FILES"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	// if fileSize > 512000 {
	// 	panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	// }

	if fileSize > 2048000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 2MB"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}
