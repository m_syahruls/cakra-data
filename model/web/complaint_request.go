package web

type ComplaintCreate struct {
	SenderId   string `json:"sender_id" validate:"required"`
	Title      string `json:"title" validate:"required"`
	Content    string `json:"content" validate:"required"`
	CategoryId string `json:"category_id" validate:"required"`
	LinkImage  string `json:"link_image"`
	Address    string `json:"address"`
	Longitude  string `json:"longitude"`
	Latitude   string `json:"latitude"`
}

type ComplaintUpdate struct {
	Title   string `json:"title" validate:"required"`
	Content string `json:"content" validate:"required"`
}

type ComplaintUpdateStatus struct {
	ReadStatus string `json:"read_status" validate:"required"`
}

type UpdateComplaintStatus struct {
	ComplaintStatusId   string `json:"complaint_status_id" validate:"required"`
	ComplaintStatusDesc string `json:"complaint_status_desc"`
}

type ComplaintChat struct {
	ComplaintId string `json:"complaint_id" validate:"required"`
	SenderId    string `json:"sender_id" validate:"required"`
	Message     string `json:"message"`
	LinkImage   string `json:"link_image"`
	Longitude   string `json:"longitude"`
	Latitude    string `json:"latitude"`
}

type ComplaintImageChat struct {
	ComplaintId string `json:"complaint_id" validate:"required"`
	SenderId    string `json:"sender_id" validate:"required"`
	LinkImage   string `json:"link_image"`
}

type ComplaintChatUpdate struct {
	Message string `json:"message" validate:"required"`
}

type ComplaintStatus struct {
	ID         string `json:"id" validate:"required"`
	StatusName string `json:"status_name" validate:"required"`
}

type ComplaintCategory struct {
	ID           string `json:"id" validate:"required"`
	CategoryName string `json:"category_name" validate:"required"`
}

type UpdateComplaintStatusName struct {
	StatusName string `json:"status_name" validate:"required"`
}
