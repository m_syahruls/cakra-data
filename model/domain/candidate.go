package domain

type Candidate struct {
	Id             string       `json:"id" bson:"_id"`
	Name           string       `json:"name" bson:"name"`
	PemiluTypeId   string       `json:"election_type_id" bson:"election_type_id"`
	DaerahPemiluId string       `json:"daerah_pemilu_id" bson:"daerah_pemilu_id"`
	PartaiId       string       `json:"partai_id" bson:"partai_id"`
	Partai         Partai       `json:"partai" bson:"partai"`
	ElectionType   ElectionType `json:"election_type" bson:"election_type"`
}
