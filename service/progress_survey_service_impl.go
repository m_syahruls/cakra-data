package service

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type ProgresSurveyServiceImpl struct {
	ProgresSurveyRepository    repository.ProgresSurveyRepository
	ProgresSurveySumRepository repository.ProgresSurveySumRepository
	Validate                   *validator.Validate
}

type ProgresSurveyService interface {
	FindDetailByUserId(ctx *fiber.Ctx, userId string) domain.ProgressSurveyDetail
	FindAll(ctx *fiber.Ctx, page, limit, userId string) []domain.ProgressSurveyDetail

	UpdateGoal(ctx *fiber.Ctx, request web.UpdateGoalSurvey) domain.ProgressSurvey
}

func NewProgresSurveyService(progresSurveyRepository repository.ProgresSurveyRepository, progresSurveySumRepository repository.ProgresSurveySumRepository, validate *validator.Validate) ProgresSurveyService {
	return &ProgresSurveyServiceImpl{
		ProgresSurveyRepository:    progresSurveyRepository,
		ProgresSurveySumRepository: progresSurveySumRepository,
		Validate:                   validate,
	}
}

func (service *ProgresSurveyServiceImpl) FindDetailByUserId(ctx *fiber.Ctx, userId string) domain.ProgressSurveyDetail {
	user, err := service.ProgresSurveyRepository.FindUserByUserId(userId)
	if err != nil || user.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	progresSurvey, err := service.ProgresSurveyRepository.FindDetailByUserId(userId)

	if err != nil || progresSurvey.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "progresSurvey not found"))
	}

	return progresSurvey
}

func (service *ProgresSurveyServiceImpl) FindAll(ctx *fiber.Ctx, page, limit, userId string) []domain.ProgressSurveyDetail {

	progresSurveys := service.ProgresSurveyRepository.FindAll(page, limit, userId)
	return progresSurveys
}

func (service *ProgresSurveyServiceImpl) UpdateGoal(ctx *fiber.Ctx, request web.UpdateGoalSurvey) domain.ProgressSurvey {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, err = service.ProgresSurveyRepository.FindUserByUserId(request.UserId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	progres, _ := service.ProgresSurveyRepository.FindByUserId(request.UserId)

	if progres.ID == "" {
		panic(exception.NewError(fiber.StatusBadRequest, "user dont have progresSurveyment goal, created it first"))
	}

	progres.SurveyGoal = request.RecruitGoal

	service.ProgresSurveyRepository.UpdateGoal(progres)

	return progres
}
