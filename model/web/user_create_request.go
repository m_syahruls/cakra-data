package web

type UserCreateRequest struct {
	OccupationId string   `json:"occupation_id" validate:"required"`
	NIK          string   `json:"nik" validate:"required"`
	Name         string   `json:"name" validate:"required"`
	Email        string   `json:"email" validate:"required"`
	Phone        string   `json:"phone" validate:"required"`
	Gender       string   `json:"gender" validate:"required"`
	DistrictId   string   `json:"district_id"`
	Longitude    string   `json:"longitude"`
	Latitude     string   `json:"latitude"`
	Password     string   `json:"password" validate:"required"`
	Accesses     []string `json:"accesses"`
}

type UserForSurvey struct {
	Phone     string `json:"phone" validate:"required"`
	Name      string `json:"name" validate:"required"`
	Gender    string `json:"gender"`
	Longitude string `json:"longitude"`
	Latitude  string `json:"latitude"`
}
