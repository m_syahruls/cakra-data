package web

import (
	"time"
)

type UserDetailResponse struct {
	Id            string                 `json:"id"`
	OccupationId  string                 `json:"occupation_id"`
	NIK           string                 `json:"nik"`
	Name          string                 `json:"name"`
	Email         string                 `json:"email"`
	Phone         string                 `json:"phone"`
	Gender        string                 `json:"gender"`
	DistrictId    string                 `json:"district_id"`
	Status        *bool                  `json:"status"`
	Longitude     string                 `json:"longitude"`
	Latitude      string                 `json:"latitude"`
	ReferralCode  string                 `json:"referral_code"`
	ProfileImage  string                 `json:"profile_image"`
	GoogleAdsID   string                 `json:"google_ads_id" bson:"google_ads_id"`
	AyrshareToken string                 `json:"ayrshare_token"`
	DetermID      string                 `json:"determ_id" bson:"determ_id"`
	CreatedAt     time.Time              `json:"created_at"`
	UpdatedAt     time.Time              `json:"updated_at"`
	Occupation    OccupationResponseUser `json:"occupation"`
	Recruitment   UserRecruitResponse    `json:"recruitment"`
	Logistic      ProgressLogistic       `json:"logistic"`
	Information   UserRecruitResponse    `json:"information"`
	Survey        ProgressSurvey         `json:"survey"`
	Accesses      []string               `json:"accesses"`
}
