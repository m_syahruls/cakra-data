package web

type UserUpdateRequest struct {
	Name         string   `json:"name" validate:"required"`
	NIK          string   `json:"nik"`
	Gender       string   `json:"gender"`
	Email        string   `json:"email"`
	Phone        string   `json:"phone"`
	OccupationId string   `json:"occupation_id"`
	DistrictId   string   `json:"district_id"`
	Longitude    string   `json:"longitude"`
	Latitude     string   `json:"latitude"`
	Status       *bool    `json:"status"`
	Accesses     []string `json:"accesses"`
}

type UserUpdatePasswordRequest struct {
	Password string `json:"password" validate:"required"`
}

type UserUpdateOccupationRequest struct {
	OccupationId string `json:"occupation_id" validate:"required"`
}

type UserUpdateAccessRequest struct {
	Accesses []string `json:"accesses"`
}
