package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type LogisticControllerImpl struct {
	LogisticService service.LogisticService
}

type LogisticController interface {
	NewLogisticRouter(app *fiber.App)
}

func NewLogisticController(logisticService service.LogisticService) LogisticController {
	return &LogisticControllerImpl{
		LogisticService: logisticService,
	}
}

func (controller *LogisticControllerImpl) NewLogisticRouter(app *fiber.App) {
	logistic := app.Group("/logistics")
	logistic.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	logistic.Post("/category", controller.CreateLogisticCategory)
	logistic.Get("/category", controller.FindAllLogisticCategory)
	logistic.Put("/category/:categoryid", controller.UpdateCategoryById)
	logistic.Delete("/category/:categoryid", controller.DeleteCategoryLogistic)

	logistic.Post("/", controller.CreateLogistic)
	logistic.Get("/", controller.FindAll)
	logistic.Get("/:logisticid", controller.FindDetailByLogisticId)
	logistic.Put("/:logisticid", controller.UpdateByLogisticId)
	logistic.Delete("/:logisticid", controller.DeleteLogistic)
}

func (controller *LogisticControllerImpl) CreateLogistic(ctx *fiber.Ctx) error {
	senderId, _, err := helper.ParseJwt(ctx.Cookies("token"))
	helper.PanicIfError(err)

	if isAdmin := middlewares.IsAdmin(ctx); isAdmin {
		return UnauthorizeReturn(ctx, "admin cant add logistic")
	}

	var request web.LogisticCreate

	request.Nama = ctx.FormValue("name")
	request.Longitude = ctx.FormValue("longitude")
	request.Latitude = ctx.FormValue("latitude")
	request.Location = ctx.FormValue("location")
	request.CategoryId = ctx.FormValue("categoryid")
	request.UserId = senderId

	logistic := controller.LogisticService.CreateLogistic(ctx, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    logistic,
	})
}

func (controller *LogisticControllerImpl) FindAll(ctx *fiber.Ctx) error {
	userid := ctx.Query("userid")

	if isadmin := middlewares.IsAdmin(ctx); !isadmin {
		userid, _, _ = helper.ParseJwt(ctx.Cookies("token"))
	}

	page := ctx.Query("page")
	limit := ctx.Query("limit")
	categoryId := ctx.Query("categoryid")

	logistic := controller.LogisticService.FindAllLogistic(userid, categoryId, page, limit)
	sum := controller.LogisticService.FindSumLogistic(userid, categoryId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponseAll{
		Code:         fiber.StatusOK,
		Status:       true,
		Message:      "success",
		TotalAllData: sum,
		Data:         logistic,
	})
}

func (controller *LogisticControllerImpl) FindDetailByLogisticId(ctx *fiber.Ctx) error {
	logisticId := ctx.Params("logisticid")

	logistic := controller.LogisticService.FindDetailByLogisticId(ctx, logisticId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    logistic,
	})
}

func (controller *LogisticControllerImpl) UpdateByLogisticId(ctx *fiber.Ctx) error {
	var request web.LogisticUpdate
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	logisticId := ctx.Params("logisticid")

	logistic := controller.LogisticService.UpdateByLogisticId(ctx, logisticId, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    logistic,
	})
}

func (controller *LogisticControllerImpl) DeleteLogistic(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can delete event")
	}

	logisticId := ctx.Params("logisticid")

	controller.LogisticService.DeleteLogistic(logisticId)

	//action := fmt.Sprintf("delete event %s", eventId)
	//actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	//controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

// Logistic Category
func (controller *LogisticControllerImpl) CreateLogisticCategory(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can create logistic category")
	}

	var request web.LogisticCategoryRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	response := controller.LogisticService.CreateLogisticCategory(ctx, request)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *LogisticControllerImpl) FindAllLogisticCategory(ctx *fiber.Ctx) error {

	page := ctx.Query("page")
	limit := ctx.Query("limit")

	logisticCategory := controller.LogisticService.FindAllLogisticCategory(page, limit)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    logisticCategory,
	})
}

func (controller *LogisticControllerImpl) UpdateCategoryById(ctx *fiber.Ctx) error {
	var request web.UpdateLogisticCategory
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	categoryId := ctx.Params("categoryid")

	logistic := controller.LogisticService.UpdateCategoryLogistic(categoryId, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    logistic,
	})
}

func (controller *LogisticControllerImpl) DeleteCategoryLogistic(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can delete category logistic")
	}

	categoryId := ctx.Params("categoryid")

	controller.LogisticService.DeleteCategoryLogistic(categoryId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
