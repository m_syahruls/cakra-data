package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type IssueManagementDataControllerImpl struct {
	IssueManagementDataService service.IssueManagementDataService
	LogService                 service.LogService
}

type IssueManagementDataController interface {
	NewIssueManagementDataRouter(app *fiber.App)
}

func NewIssueManagementDataController(issueManagementDataService service.IssueManagementDataService, logService service.LogService) IssueManagementDataController {
	return &IssueManagementDataControllerImpl{
		IssueManagementDataService: issueManagementDataService,
		LogService:                 logService,
	}
}

func (controller *IssueManagementDataControllerImpl) NewIssueManagementDataRouter(app *fiber.App) {
	issueManagementData := app.Group("/management-data")
	issueManagementData.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	issueManagementData.Get("/", controller.FindAll)
	// issueManagementData.Delete("/:issueManagementDataId", controller.Delete)
}

func (controller *IssueManagementDataControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	name := ctx.Query("name")
	issue := ctx.Query("issue")
	sub_issue := ctx.Query("sub_issue")
	date := ctx.Query("date")
	created_at := ctx.Query("created_at")

	issueManagementDataResponse := controller.IssueManagementDataService.FindAll(page, limit, name, issue, sub_issue, date, created_at)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    issueManagementDataResponse,
	})
}

// func (controller *IssueManagementDataControllerImpl) Delete(ctx *fiber.Ctx) error {
// 	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
// 		return UnauthorizeReturn(ctx, "only admin can delete sub issue")
// 	}

// 	issueManagementDataId := ctx.Params("issueManagementDataId")

// 	controller.IssueManagementDataService.Delete(issueManagementDataId)

// 	action := fmt.Sprintf("delete sub issue %s", issueManagementDataId)
// 	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
// 	controller.LogService.Create(actor, action)

// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    nil,
// 	})
// }
