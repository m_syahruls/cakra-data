package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type ReligionControllerImpl struct {
	ReligionService service.ReligionService
}

type ReligionController interface {
	NewReligionRouter(app *fiber.App)
}

func NewReligionController(religionService service.ReligionService) ReligionController {
	return &ReligionControllerImpl{
		ReligionService: religionService,
	}
}

func (controller *ReligionControllerImpl) NewReligionRouter(app *fiber.App) {
	religion := app.Group("/religions")
	religion.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	religion.Get("/", controller.FindAll)
}

func (controller *ReligionControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")

	religion := controller.ReligionService.FindAll(page, limit)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    religion,
	})
}
