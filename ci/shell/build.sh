#!/bin/bash
export APP_NAME=$1
export APP_VERSION=$2
export SERVICE_NAME=$3
export GIT_BRANCH=$4
export GIT_COMMIT_SHA=$( git rev-parse --short HEAD )

if [ $# -lt 3 ]; then
    echo "Usage: <app name> <app version> <service name> <branch name>"
    exit 1
fi

echo "Building $APP_NAME:$APP_VERSION-$SERVICE_NAME-svc for branch $GIT_BRANCH"

if [ $GIT_BRANCH == "master" ]; then
    export DOCKER_IMG_TAG="$APP_VERSION"-"$SERVICE_NAME"-svc
    docker build -f ./ci/docker/Dockerfile -t synapsisid/$APP_NAME:$DOCKER_IMG_TAG --no-cache . && \
        docker push synapsisid/$APP_NAME:$DOCKER_IMG_TAG && \
        exit 0
fi

if [ $GIT_BRANCH == "development" ]; then
    export DOCKER_IMG_TAG="$GIT_COMMIT_SHA"-"$SERVICE_NAME"-svc
    docker build -f ./ci/docker/Dockerfile -t synapsisid/$APP_NAME:$DOCKER_IMG_TAG --no-cache . && \
        docker push synapsisid/$APP_NAME:$DOCKER_IMG_TAG  && \
        exit 0
fi

if [ $GIT_BRANCH == "staging" ]; then
    export DOCKER_IMG_TAG="$APP_VERSION"-"$SERVICE_NAME"-staging-svc
    docker build -f ./ci/docker/Dockerfile -t synapsisid/$APP_NAME:$DOCKER_IMG_TAG --no-cache . && \
        docker push synapsisid/$APP_NAME:$DOCKER_IMG_TAG  && \
        exit 0
fi

export DOCKER_IMG_TAG="$GIT_COMMIT_SHA"-"$SERVICE_NAME"-svc
docker build -f ./ci/docker/Dockerfile -t synapsisid/$APP_NAME:$DOCKER_IMG_TAG --no-cache . && \
    exit 0
