package domain

import "time"

type Event struct {
	ID            string    `json:"id" bson:"_id"`
	EventName     string    `json:"event_name" bson:"event_name"`
	ImageUrl      string    `json:"image_url" bson:"image_url"`
	Link          string    `json:"link" bson:"link"`
	Description   string    `json:"description" bson:"description"`
	DateStart     time.Time `json:"date_start" bson:"date_start"`
	DateEnd       time.Time `json:"date_end" bson:"date_end"`
	Location      string    `json:"location" bson:"location"`
	ContactPerson string    `json:"contact_person" bson:"contact_person"`
	Status        *bool     `json:"status" bson:"status"`
	CreatedAt     time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt     time.Time `json:"updated_at" bson:"updated_at"`
	User          []User    `json:"user" bson:"user"`
}

type EventParticipant struct {
	ID      string `json:"id" bson:"_id"`
	EventId string `json:"event_id" bson:"event_id"`
	UserId  string `json:"user_id" bson:"user_id"`
}
