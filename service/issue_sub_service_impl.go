package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type IssueSubServiceImpl struct {
	IssueSubRepository repository.IssueSubRepository
	Validate           *validator.Validate
}

type IssueSubService interface {
	FindAll(page, limit, issueId string, label string, value string) []web.IssueSubResponse
	FindById(IssueSubId string) web.IssueSubResponse
	Create(ctx *fiber.Ctx, request web.CreateIssueSubRequest) web.IssueSubResponse
	Update(ctx *fiber.Ctx, issueSubId string, request web.UpdateIssueSubRequest) web.IssueSubResponse
	Delete(issueSubId string)

	UploadImageSubIssue(c *fiber.Ctx) string
}

func NewIssueSubService(IssueSubRepository repository.IssueSubRepository, validate *validator.Validate) IssueSubService {
	return &IssueSubServiceImpl{
		IssueSubRepository: IssueSubRepository,
		Validate:           validate,
	}
}

func (service *IssueSubServiceImpl) FindById(issueSubId string) web.IssueSubResponse {
	IssueSub, err := service.IssueSubRepository.FindById(issueSubId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Sub issue not found"))
	}

	return helper.ToIssueSubResponse(IssueSub)
}

func (service *IssueSubServiceImpl) FindAll(page, limit, issueId string, label string, value string) []web.IssueSubResponse {
	IssueSub := service.IssueSubRepository.FindAll(page, limit, issueId, label, value)
	return helper.ToIssueSubResponses(IssueSub)
}

func (service *IssueSubServiceImpl) Create(ctx *fiber.Ctx, request web.CreateIssueSubRequest) web.IssueSubResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isIssueExist := service.IssueSubRepository.FindIsExist(request.Label, request.Value)
	if isIssueExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Sub issue already exists"))
	}

	issueSub := domain.IssueSub{
		ID:      strings.ToLower(randstr.String(10)),
		IssueID: request.IssueID,
		Label:   request.Label,
		Value:   request.Value,
	}
	linkImage := service.UploadImageSubIssue(ctx)
	issueSub.ImageUrl = linkImage
	service.IssueSubRepository.Create(issueSub)

	return helper.ToIssueSubResponse(issueSub)
}

func (service *IssueSubServiceImpl) Update(ctx *fiber.Ctx, issueSubId string, request web.UpdateIssueSubRequest) web.IssueSubResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	// isIssueExist := service.IssueSubRepository.FindIsExist(request.Label, request.Value)
	// if isIssueExist {
	// 	panic(exception.NewError(fiber.StatusBadRequest, "Sub issue already exists"))
	// }

	issueSub, err := service.IssueSubRepository.FindById(issueSubId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Sub issue not found"))
	}

	_, err = ctx.FormFile("images")
	if err == nil {
		service.RemoveImageSubIssue(issueSub.ImageUrl)

		linkImage := service.UploadImageSubIssue(ctx)
		issueSub.ImageUrl = linkImage
	}

	issueSub.IssueID = request.IssueID
	issueSub.Label = request.Label
	issueSub.Value = request.Value

	service.IssueSubRepository.Update(issueSub, issueSubId)

	return helper.ToIssueSubResponse(issueSub)
}

func (service *IssueSubServiceImpl) Delete(issueSubId string) {
	issueSub, err := service.IssueSubRepository.FindById(issueSubId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Sub issue not found"))
	}

	if issueSub.ImageUrl != "" {
		service.RemoveImageSubIssue(issueSub.ImageUrl)
	}

	err = service.IssueSubRepository.Delete(issueSubId)
	helper.PanicIfError(err)
	log.Println("LOG", "deleted sub issue", issueSub.Label)
}

func (service *IssueSubServiceImpl) UploadImageSubIssue(c *fiber.Ctx) string {
	ctx := context.Background()

	bucketName := os.Getenv("MINIO_BUCKET_ISSUESUB")
	file, err := c.FormFile("images")
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request, images cant be empty"))
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.jpg", os.Getenv("MINIO_PATH"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	if fileSize > 512000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}

func (service *IssueSubServiceImpl) RemoveImageSubIssue(objectName string) {

	bucketName := os.Getenv("MINIO_BUCKET_ISSUESUB")
	objName := strings.Split(objectName, bucketName+"/")

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	err = minioClient.RemoveObject(context.Background(), bucketName, objName[1], minio.RemoveObjectOptions{})
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error remove"))
	}

	log.Printf("Successfully removed %s\n", objectName)
}
