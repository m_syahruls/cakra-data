package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type PermissionControllerImpl struct {
	PermissionService service.PermissionService
	LogService        service.LogService
}

type PermissionController interface {
	NewPermissionRouter(app *fiber.App)
}

func NewPermissionController(permissionService service.PermissionService, logService service.LogService) PermissionController {
	return &PermissionControllerImpl{
		PermissionService: permissionService,
		LogService:        logService,
	}
}

func (controller *PermissionControllerImpl) NewPermissionRouter(app *fiber.App) {
	permission := app.Group("/permissions")
	permission.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	// //Restricted (JUST FOR ADMIN)
	// permission.Use(middlewares.IsAuthenticated)
	permission.Post("/", controller.Create)
	permission.Get("/:permissionId", controller.FindById)
	permission.Get("/", controller.FindAll)
	permission.Put("/:permissionId", controller.Update)
	permission.Delete("/:permissionId", controller.Delete)
}

func (controller *PermissionControllerImpl) Create(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can create permission")
	// }

	var request web.PermissionCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	permissionResponse := controller.PermissionService.Create(request)

	action := fmt.Sprintf("create permission %s", permissionResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    permissionResponse,
	})
}

func (controller *PermissionControllerImpl) FindById(ctx *fiber.Ctx) error {
	permissionId := ctx.Params("permissionId")

	permissionResponse := controller.PermissionService.FindById(permissionId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    permissionResponse,
	})
}

func (controller *PermissionControllerImpl) FindAll(ctx *fiber.Ctx) error {
	label := ctx.Query("label")
	value := ctx.Query("value")
	permissionResponse := controller.PermissionService.FindAll(label, value)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    permissionResponse,
	})
}

func (controller *PermissionControllerImpl) Update(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can update permission")
	// }

	var request web.PermissionUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	permissionId := ctx.Params("permissionId")

	permissionResponse := controller.PermissionService.Update(permissionId, request)

	action := fmt.Sprintf("update permission %s", permissionResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    permissionResponse,
	})
}

func (controller *PermissionControllerImpl) Delete(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can delete permission")
	// }

	permissionId := ctx.Params("permissionId")

	controller.PermissionService.Delete(permissionId)

	action := fmt.Sprintf("delete permission %s", permissionId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
