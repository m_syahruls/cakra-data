package web

type Candidate struct {
	Id             string         `json:"id"`
	Name           string         `json:"name" validate:"required"`
	PemiluTypeId   string         `json:"election_type_id" validate:"required"`
	DaerahPemiluId string         `json:"daerah_pemilu_id" validate:"required"`
	PartaiId       string         `json:"partai_id" validate:"required"`
	Partai         PartaiResponse `json:"partai"`
	ElectionType   ElectionType   `json:"election_type"`
}
