package web

type RegisterUserRequest struct {
	NIK          string `json:"nik" validate:"required"`
	Name         string `json:"name" validate:"required"`
	Email        string `json:"email" validate:"required"`
	Phone        string `json:"phone" validate:"required"`
	Gender       string `json:"gender" validate:"required"`
	Password     string `json:"password" validate:"required"`
	ReferralCode string `json:"referral_code"`
	Longitude    string `json:"longitude"`
	Latitude     string `json:"latitude"`
}
