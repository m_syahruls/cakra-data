package service

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/bson"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type SurveyServiceImpl struct {
	SurveyRepository   repository.SurveyRepository
	QuestionRepository repository.QuestionRepository
	OptionRepository   repository.OptionRepository
	ResponseRepository repository.ResponseRepository
	Validate           *validator.Validate
}

type SurveyService interface {
	Create(request web.SurveyCreateRequest) web.SurveyResponse
	FindDetailById(surveyId string) web.SurveyDetailResponse
	FindAll(page, limit, searchName, status string) []web.SurveyResponse
	UpdateStatus(surveyId string) web.SurveyResponse
	UpdateSurvey(request web.SurveyUpdateRequest) web.SurveyResponse
	Delete(surveyId string)

	FindAllSurveyByUserId(userId, status string) []web.SurveyResponseByUserId
	CountAllSurvey(searchName, status string) int
}

func NewSurveyService(
	surveyRepository repository.SurveyRepository,
	questionRepository repository.QuestionRepository,
	optionRepository repository.OptionRepository,
	responseRepository repository.ResponseRepository,
	validate *validator.Validate) SurveyService {
	return &SurveyServiceImpl{
		SurveyRepository:   surveyRepository,
		QuestionRepository: questionRepository,
		OptionRepository:   optionRepository,
		ResponseRepository: responseRepository,
		Validate:           validate,
	}
}

func (service *SurveyServiceImpl) Create(request web.SurveyCreateRequest) web.SurveyResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	// validate request if question exist when create survey
	if request.Questions != nil {
		for i, question := range request.Questions {
			err := service.Validate.Struct(question)
			if err != nil {
				panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("at question %d error %s", i+1, err.Error())))
			}
			// validate request if option exist when create question
			for i, option := range question.Options {
				err := service.Validate.Struct(option)
				if err != nil {
					panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("at option %d error %s", i+1, err.Error())))
				}
			}
		}
	}

	survey := domain.Survey{
		ID:              strings.ToLower(randstr.String(10)),
		SurveyName:      request.SurveyName,
		Status:          *request.Status,
		TotalRespondent: 0,
		CreatedAt:       time.Now(),
		UpdatedAt:       time.Now(),
	}

	service.SurveyRepository.Create(survey)

	// create question
	questions := request.Questions
	for _, reqQuestion := range questions {
		question := domain.Questions{
			ID:              strings.ToLower(randstr.String(10)),
			SurveyId:        survey.ID,
			Section:         reqQuestion.Section,
			InputType:       reqQuestion.InputType,
			QuestionNumber:  reqQuestion.QuestionNumber,
			QuestionName:    reqQuestion.QuestionName,
			QuestionSubject: reqQuestion.QuestionSubject,
		}
		service.QuestionRepository.Create(question)

		for _, reqOption := range reqQuestion.Options {
			option := domain.Options{
				ID:         strings.ToLower(randstr.String(10)),
				QuestionId: question.ID,
				OptionName: reqOption.OptionName,
				Value:      reqOption.Value,
				Color:      reqOption.Color,
			}
			service.OptionRepository.Create(option)
		}
	}

	return helper.ToSurveyResponse(survey)
}

func (service *SurveyServiceImpl) FindDetailById(surveyId string) web.SurveyDetailResponse {
	survey, err := service.SurveyRepository.FindDetailById(surveyId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "survey not found"))
	}

	return helper.ToSurveyDetailResponse(survey)
}

func (service *SurveyServiceImpl) FindAll(page, limit, searchName, status string) []web.SurveyResponse {
	surveys := service.SurveyRepository.FindAll(page, limit, searchName, status)
	return helper.ToAllSurveyResponses(surveys)
}

func (service *SurveyServiceImpl) UpdateStatus(surveyId string) web.SurveyResponse {

	survey, err := service.SurveyRepository.FindById(surveyId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "survey not found"))
	}

	if survey.Status == 1 {
		survey.Status = 0
	} else {
		survey.Status = 1
	}

	survey.UpdatedAt = time.Now()

	service.SurveyRepository.UpdateStatus(survey)

	return helper.ToSurveyResponse(survey)
}

func (service *SurveyServiceImpl) UpdateSurvey(request web.SurveyUpdateRequest) web.SurveyResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	survey, err := service.SurveyRepository.FindById(request.ID)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "survey not found"))
	}

	// check if survey already response by user
	responseExist, _ := service.ResponseRepository.FindByQuery(bson.D{{Key: "survey_id", Value: request.ID}})
	if responseExist.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "survey already response"))
	}

	survey.ID = request.ID
	survey.Status = *request.Status
	survey.SurveyName = request.SurveyName
	survey.UpdatedAt = time.Now()

	service.SurveyRepository.UpdateSurvey(survey)

	if request.Questions != nil {
		for i, question := range request.Questions {
			err := service.Validate.Struct(question)
			if err != nil {
				panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("at question %d error %s", i+1, err.Error())))
			}
			// validate request if option exist when create question
			for i, option := range question.Options {
				err := service.Validate.Struct(option)
				if err != nil {
					panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("at option %d error %s", i+1, err.Error())))
				}
			}
		}

		// if no error then delete all question and option from survey (update to new question and option from request)
		allQuestion := service.QuestionRepository.FindAll("", "", request.ID)
		for _, deleteQuestion := range allQuestion {
			// delete all option by question id
			service.OptionRepository.DeleteAllOptionByQuestionId(deleteQuestion.ID)
		}
		// delete all question by survey id
		service.QuestionRepository.DeleteAllQuestionBySurveyId(request.ID)

		// create new question for survey
		questions := request.Questions
		for _, reqQuestion := range questions {
			question := domain.Questions{
				ID:              strings.ToLower(randstr.String(10)),
				SurveyId:        survey.ID,
				Section:         reqQuestion.Section,
				InputType:       reqQuestion.InputType,
				QuestionNumber:  reqQuestion.QuestionNumber,
				QuestionName:    reqQuestion.QuestionName,
				QuestionSubject: reqQuestion.QuestionSubject,
			}
			service.QuestionRepository.Create(question)

			for _, reqOption := range reqQuestion.Options {
				option := domain.Options{
					ID:         strings.ToLower(randstr.String(10)),
					QuestionId: question.ID,
					OptionName: reqOption.OptionName,
					Value:      reqOption.Value,
					Color:      reqOption.Color,
				}
				service.OptionRepository.Create(option)
			}
		}

	}

	return helper.ToSurveyResponse(survey)
}

func (service *SurveyServiceImpl) Delete(surveyId string) {
	survey, err := service.SurveyRepository.FindById(surveyId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "survey not found"))
	}

	err = service.SurveyRepository.Delete(surveyId)
	helper.PanicIfError(err)
	log.Println("LOG", "deleted survey", survey.SurveyName)
}

func (service *SurveyServiceImpl) FindAllSurveyByUserId(userId, status string) []web.SurveyResponseByUserId {
	surveys, err := service.SurveyRepository.FindByUserId(userId, status)
	helper.PanicIfError(err)
	responseSurvey := helper.ToAllSurveyResponsesByUserId(surveys)

	for i, survey := range responseSurvey {
		responseSurvey[i].TotalQuestion = service.QuestionRepository.FindCountBySurveyId(survey.ID)
	}

	return responseSurvey
}

func (service *SurveyServiceImpl) CountAllSurvey(searchName, status string) int {
	return service.SurveyRepository.CountAllSurvey(searchName, status)
}
