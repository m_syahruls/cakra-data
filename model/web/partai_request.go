package web

type Partai struct {
	Name string `json:"name" validate:"required"`
	Slug string `json:"slug" validate:"required"`
	Logo string `json:"logo"`
}

type PartaiResponse struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
	Logo string `json:"logo"`
}

type ElectionType struct {
	Id   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
	Slug string `json:"slug" bson:"slug"`
}
