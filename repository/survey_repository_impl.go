package repository

import (
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type SurveyRepositoryImpl struct {
	Collection         *mongo.Collection
	CollectionResponse *mongo.Collection
}

type SurveyRepository interface {
	Create(survey domain.Survey)
	FindById(surveyId string) (domain.Survey, error)
	FindDetailById(surveyId string) (domain.Survey, error)
	FindAll(page, limit, searchName, status string) []domain.Survey
	UpdateTotalRespondent(survey domain.Survey)
	UpdateStatus(survey domain.Survey)
	UpdateSurvey(survey domain.Survey)
	Delete(surveyId string) error

	FindByUserId(userId, status string) ([]domain.SurveyWithResponseId, error)
	CountAllSurvey(searchName, status string) int
}

func NewSurveyRepository(database *mongo.Database) SurveyRepository {
	return &SurveyRepositoryImpl{
		Collection:         database.Collection("surveys"),
		CollectionResponse: database.Collection("responses"),
	}
}

func (repository *SurveyRepositoryImpl) Create(survey domain.Survey) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":              survey.ID,
		"survey_name":      survey.SurveyName,
		"status":           survey.Status,
		"total_respondent": survey.TotalRespondent,
		"created_at":       survey.CreatedAt,
		"updated_at":       survey.UpdatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *SurveyRepositoryImpl) FindById(surveyId string) (domain.Survey, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var survey domain.Survey
	query := bson.M{"_id": surveyId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&survey)

	return survey, result.Err()
}

func (repository *SurveyRepositoryImpl) FindDetailById(surveyId string) (domain.Survey, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: surveyId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "questions"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "survey_id"}, {Key: "as", Value: "questions"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$questions"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "questions.question_number", Value: 1}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "questions._id"}, {Key: "foreignField", Value: "question_id"}, {Key: "as", Value: "questions.options"}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "survey_name", Value: bson.D{{Key: "$first", Value: "$survey_name"}}},
		{Key: "status", Value: bson.D{{Key: "$first", Value: "$status"}}},
		{Key: "total_respondent", Value: bson.D{{Key: "$first", Value: "$total_respondent"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "updated_at", Value: bson.D{{Key: "$first", Value: "$updated_at"}}},
		{Key: "questions", Value: bson.D{{Key: "$push", Value: "$questions"}}},
	}}}
	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, sortStage, lookupStage2, groupStage})
	helper.PanicIfError(err)

	var survey domain.Survey
	result.Next(ctx)
	result.Decode(&survey)

	return survey, result.Err()
}

func (repository *SurveyRepositoryImpl) FindAll(page, limit, searchName, status string) []domain.Survey {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "questions"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "survey_id"}, {Key: "as", Value: "questions"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$questions"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "questions.question_number", Value: 1}}}}
	sortStage1 := bson.D{{Key: "$sort", Value: bson.D{{Key: "created_at", Value: -1}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "questions._id"}, {Key: "foreignField", Value: "question_id"}, {Key: "as", Value: "questions.options"}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "survey_name", Value: bson.D{{Key: "$first", Value: "$survey_name"}}},
		{Key: "status", Value: bson.D{{Key: "$first", Value: "$status"}}},
		{Key: "total_respondent", Value: bson.D{{Key: "$first", Value: "$total_respondent"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "updated_at", Value: bson.D{{Key: "$first", Value: "$updated_at"}}},
		{Key: "questions", Value: bson.D{{Key: "$push", Value: "$questions"}}},
	}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, sortStage, lookupStage2, groupStage, sortStage1}

	helper.QuerySurvey(searchName, status, &pipeline)

	helper.PaggingAggregat(page, limit, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var surveys []domain.Survey

	for result.Next(ctx) {
		var survey domain.Survey
		result.Decode(&survey)
		surveys = append(surveys, survey)
	}

	return surveys
}

func (repository *SurveyRepositoryImpl) UpdateTotalRespondent(survey domain.Survey) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": survey.ID}
	update := bson.M{
		"$set": bson.M{
			"total_respondent": survey.TotalRespondent,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *SurveyRepositoryImpl) UpdateStatus(survey domain.Survey) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": survey.ID}
	update := bson.M{
		"$set": bson.M{
			"status":     survey.Status,
			"updated_at": survey.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *SurveyRepositoryImpl) UpdateSurvey(survey domain.Survey) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": survey.ID}
	update := bson.M{
		"$set": bson.M{
			"survey_name":   survey.SurveyName,
			"survey_status": survey.Status,
			"updated_at":    survey.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *SurveyRepositoryImpl) Delete(surveyId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": surveyId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *SurveyRepositoryImpl) FindByUserId(userId, status string) ([]domain.SurveyWithResponseId, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	options := options.Find()
	query := bson.M{"respondent_id": userId}

	cursor, err := repository.CollectionResponse.Find(ctx, query, options)
	helper.PanicIfError(err)

	responsesByUserId := make(map[string]string)

	for cursor.Next(ctx) {
		var response domain.Response
		cursor.Decode(&response)
		responsesByUserId[response.SurveyId] = response.ID
	}

	querySurvey := bson.M{}
	if status != "" {
		statusInt, _ := strconv.Atoi(status)
		querySurvey["status"] = statusInt
	}

	results, err := repository.Collection.Find(ctx, querySurvey, options)
	helper.PanicIfError(err)

	var surveys []domain.SurveyWithResponseId
	for results.Next(ctx) {
		var survey domain.SurveyWithResponseId
		results.Decode(&survey)
		survey.ResponseId = responsesByUserId[survey.ID]
		surveys = append(surveys, survey)
	}

	return surveys, results.Err()
}

func (repository *SurveyRepositoryImpl) CountAllSurvey(searchName, status string) int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "questions"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "survey_id"}, {Key: "as", Value: "questions"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$questions"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "questions.question_number", Value: 1}}}}
	sortStage1 := bson.D{{Key: "$sort", Value: bson.D{{Key: "created_at", Value: -1}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "questions._id"}, {Key: "foreignField", Value: "question_id"}, {Key: "as", Value: "questions.options"}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "survey_name", Value: bson.D{{Key: "$first", Value: "$survey_name"}}},
		{Key: "status", Value: bson.D{{Key: "$first", Value: "$status"}}},
		{Key: "total_respondent", Value: bson.D{{Key: "$first", Value: "$total_respondent"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "updated_at", Value: bson.D{{Key: "$first", Value: "$updated_at"}}},
		{Key: "questions", Value: bson.D{{Key: "$push", Value: "$questions"}}},
	}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, sortStage, lookupStage2, groupStage, sortStage1}

	helper.QuerySurvey(searchName, status, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var surveys int

	for result.Next(ctx) {
		surveys = surveys + 1
	}

	return surveys
}
