package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type OccupationControllerImpl struct {
	OccupationService service.OccupationService
	LogService        service.LogService
}

type OccupationController interface {
	NewOccupationRouter(app *fiber.App)
}

func NewOccupationController(occupationService service.OccupationService, logService service.LogService) OccupationController {
	return &OccupationControllerImpl{
		OccupationService: occupationService,
		LogService:        logService,
	}
}

func (controller *OccupationControllerImpl) NewOccupationRouter(app *fiber.App) {
	occupation := app.Group("/occupations")
	occupation.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	//Restricted (JUST FOR ADMIN)
	occupation.Use(middlewares.IsAuthenticated)
	occupation.Post("/", controller.Create)
	occupation.Get("/:occupationid", controller.FindById)
	occupation.Get("/", controller.FindAll)
	occupation.Put("/:occupationid", controller.Update)
	occupation.Delete("/:occupationid", controller.Delete)
}

func (controller *OccupationControllerImpl) Create(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can create occupation")
	}

	var request web.OccupationCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	occupationResponse := controller.OccupationService.Create(request)

	action := fmt.Sprintf("create occupation %s", occupationResponse.Name)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    occupationResponse,
	})
}

func (controller *OccupationControllerImpl) FindById(ctx *fiber.Ctx) error {
	occupationId := ctx.Params("occupationid")

	occupationResponse := controller.OccupationService.FindById(occupationId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    occupationResponse,
	})
}

func (controller *OccupationControllerImpl) FindAll(ctx *fiber.Ctx) error {
	occupationResponse := controller.OccupationService.FindAll()
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    occupationResponse,
	})
}

func (controller *OccupationControllerImpl) Update(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can update occupation")
	}

	var request web.OccupationUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	occupationId := ctx.Params("occupationId")

	occupationResponse := controller.OccupationService.Update(occupationId, request)

	action := fmt.Sprintf("update occupation %s", occupationResponse.Name)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    occupationResponse,
	})
}

func (controller *OccupationControllerImpl) Delete(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can delete occupation")
	}

	occupationId := ctx.Params("occupationid")

	controller.OccupationService.Delete(occupationId)

	action := fmt.Sprintf("delete occupation %s", occupationId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
