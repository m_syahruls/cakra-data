package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type DistrictRepositoryImpl struct {
	CollectionDistrict *mongo.Collection
	CollectionVillage  *mongo.Collection
	CollectionRegency  *mongo.Collection
}

type DistrictRepository interface {
	FindDistrictById(districtId string) (domain.Districts, error)
	FindAll(page, limit, provinceId, name string) []domain.Districts
	FindVillageById(villageId string) (domain.Village, error)
	FindRegencyById(regencyId string) (domain.Regency, error)
}

func NewDistrictRepository(database *mongo.Database) DistrictRepository {
	return &DistrictRepositoryImpl{
		CollectionDistrict: database.Collection("districts"),
		CollectionVillage:  database.Collection("villages"),
		CollectionRegency:  database.Collection("regencies"),
	}
}

func (repository *DistrictRepositoryImpl) FindDistrictById(districtId string) (domain.Districts, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var district domain.Districts
	query := bson.M{"_id": districtId}

	result := repository.CollectionDistrict.FindOne(ctx, query)
	result.Decode(&district)

	return district, result.Err()
}

func (repository *DistrictRepositoryImpl) FindVillageById(villageId string) (domain.Village, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var village domain.Village
	query := bson.M{"_id": villageId}

	result := repository.CollectionVillage.FindOne(ctx, query)
	result.Decode(&village)

	return village, result.Err()
}

func (repository *DistrictRepositoryImpl) FindRegencyById(regencyId string) (domain.Regency, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var regency domain.Regency
	query := bson.M{"_id": regencyId}

	result := repository.CollectionRegency.FindOne(ctx, query)
	result.Decode(&regency)

	return regency, result.Err()
}

func (repository *DistrictRepositoryImpl) FindAll(page, limit, provinceId, name string) []domain.Districts {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryDistricts(provinceId, name)

	cursor, err := repository.CollectionDistrict.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var districts []domain.Districts

	for cursor.Next(ctx) {
		var district domain.Districts
		cursor.Decode(&district)
		districts = append(districts, district)
	}

	return districts
}
