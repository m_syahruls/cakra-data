package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type DataKpuRepositoryImpl struct {
	Collection *mongo.Collection
}

type DataKpuRepository interface {
	FindAllWithRegency(regencyId []string) []domain.KpuData
	FindAll() []domain.KpuData
}

func NewDataKpuRepository(database *mongo.Database) DataKpuRepository {
	return &DataKpuRepositoryImpl{
		Collection: database.Collection("data_kpu"),
	}
}

func (repository DataKpuRepositoryImpl) FindAllWithRegency(regencyId []string) []domain.KpuData {
	ctx, cancel := config.NewDBContext()
	var regency []interface{}

	defer cancel()
	regency = append(regency, "$regency._id", regencyId)

	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "villageid"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "village"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "districts"}, {Key: "localField", Value: "village.district_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "district"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$district"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "regencies"}, {Key: "localField", Value: "district.regency_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "regency"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$regency"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	matchStage1 := bson.D{{Key: "$match", Value: bson.D{{Key: "$expr", Value: bson.D{{Key: "$in", Value: bson.A{"$regency._id", regencyId}}}}}}}

	pipeline := mongo.Pipeline{lookupStage1, unwindStage1, lookupStage2, unwindStage2, lookupStage3, unwindStage3, matchStage1}

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var responses []domain.KpuData
	for result.Next(ctx) {
		var response domain.KpuData
		result.Decode(&response)
		responses = append(responses, response)
	}

	return responses
}

func (repository DataKpuRepositoryImpl) FindAll() []domain.KpuData {
	ctx, cancel := config.NewDBContext()

	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging("", "", -1)

	result, err := repository.Collection.Find(ctx, bson.M{}, filter)
	helper.PanicIfError(err)

	var responses []domain.KpuData
	for result.Next(ctx) {
		var response domain.KpuData
		result.Decode(&response)
		responses = append(responses, response)
	}

	return responses
}
