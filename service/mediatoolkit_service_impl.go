package service

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
)

type MediatoolkitServiceImpl struct {
	MediatoolkitToken string
	MediatoolkitURL   string
	Validate          validator.Validate
}

type MediatoolkitService interface {
	GetAvailableLocations() web.MTKGetLanguagesReponses
	GetAvailableLanguages() web.MTKGetLanguagesReponses
	GetAvailableSourceTypes() web.MTKGetLanguagesReponses

	CreateGroup(orgId string, request web.MTKCreateGroupRequest)
	GetGroups(orgId string) web.MTKGetGroupsResponse
	DeleteGroup(orgId string, groupId string)

	CreateTopic(orgId string, groupId string, request web.MTKCreateTopicRequest)
	DeleteTopic(orgId, groupId, topicId string)

	MentionsOverTime(orgId string, request web.MTKPostReportRequest) web.MTKPostReportResponse
	SumOfMentions(orgId string, request web.MTKPostReportRequest) web.MTKPostReportBriefResponse
	SumOfImpressions(orgId string, request web.MTKPostReportRequest) web.MTKPostReportBriefResponse
	SumOfAllSource(orgId string, request web.MTKPostReportRequest) web.MTKPostReportResponse
	EffectiveSentiment(orgId string, request web.MTKPostReportRequest) web.MTKPostReportResponse
	MentionsOverTimeBySource(orgId string, request web.MTKPostReportRequest) web.MTKPostReportDetailResponse
	SentimentOverTime(orgId string, request web.MTKPostReportRequest) web.MTKPostReportDetailResponse
	WordCloud(orgId string, request web.MTKPostReportRequest) []web.DummyWordCloudResponse

	postReport(orgId string, origin string, request web.MTKPostReportRequestComplete) web.MTKPostReportResponse
	postReportBrief(orgId string, origin string, request web.MTKPostReportRequestBriefComplete) web.MTKPostReportBriefResponse
	postReportDetail(orgId string, origin string, request web.MTKPostReportRequestDetailComplete) web.MTKPostReportDetailResponse

	// Create(userId string, request web.SocialCreateRequest) web.SocialResponse
	// UpdateSummary(userId string, request web.SocialUpdateSummaryRequest) web.SocialSummaryResponse
	// UpdateDataPoint(userId string, dataId string, request web.SocialUpdateDatapointRequest) web.SocialDatapointResponse
	// Delete(userId, dataId string)
	// FindDatapointById(userId, dataId string) web.SocialDatapointResponse
	// GetCompleteReport(query map[string]string) web.SocialResponse
}

func NewMediatoolkitService(url string, token string, validate *validator.Validate) MediatoolkitService {
	return &MediatoolkitServiceImpl{
		MediatoolkitURL:   url,
		MediatoolkitToken: token,
		Validate:          *validate,
	}
}

func (service *MediatoolkitServiceImpl) GetAvailableLocations() web.MTKGetLanguagesReponses {
	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodGet)
	req.SetRequestURI(fmt.Sprintf("%slocations", service.MediatoolkitURL))

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKGetLanguagesReponses
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}

	return data
}

func (service *MediatoolkitServiceImpl) GetAvailableLanguages() web.MTKGetLanguagesReponses {
	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodGet)
	req.SetRequestURI(fmt.Sprintf("%slanguages", service.MediatoolkitURL))

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKGetLanguagesReponses
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}

	return data
}

func (service *MediatoolkitServiceImpl) GetAvailableSourceTypes() web.MTKGetLanguagesReponses {
	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodGet)
	req.SetRequestURI(fmt.Sprintf("%ssource_types", service.MediatoolkitURL))

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKGetLanguagesReponses
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}

	return data
}

func (service *MediatoolkitServiceImpl) CreateGroup(orgId string, request web.MTKCreateGroupRequest) {
	requestByte, _ := json.Marshal(request)

	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodPost)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/groups?access_token=%s", service.MediatoolkitURL, orgId, service.MediatoolkitToken))
	req.SetBody(requestByte)

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKPostReportBriefResponse
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}
}

func (service *MediatoolkitServiceImpl) GetGroups(orgId string) web.MTKGetGroupsResponse {
	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodGet)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/groups?access_token=%s", service.MediatoolkitURL, orgId, service.MediatoolkitToken))

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKGetGroupsResponse
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}
	data.OrganizationID = orgId

	return data
}

func (service *MediatoolkitServiceImpl) DeleteGroup(orgId string, groupId string) {
	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodDelete)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/groups/%s?access_token=%s", service.MediatoolkitURL, orgId, groupId, service.MediatoolkitToken))

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKGetGroupsResponse
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}
}

func (service *MediatoolkitServiceImpl) CreateTopic(orgId string, groupId string, request web.MTKCreateTopicRequest) {
	requestByte, _ := json.Marshal(request)

	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodPost)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/groups/%s/keywords?access_token=%s", service.MediatoolkitURL, orgId, groupId, service.MediatoolkitToken))
	req.SetBody(requestByte)

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKPostReportBriefResponse
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}
}

func (service *MediatoolkitServiceImpl) DeleteTopic(orgId string, groupId string, topicId string) {
	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodDelete)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/groups/%s/keywords/%s?access_token=%s", service.MediatoolkitURL, orgId, groupId, topicId, service.MediatoolkitToken))

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKGetGroupsResponse
	code, _, err := agent.Struct(&data)

	if len(err) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, err[0].Error()))
	}
	if code != 200 {
		if code == 404 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("orgId not found %d", code)))
		} else if code == 403 {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("forbidden orgId %d", code)))
		} else {
			panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("bad request %d", code)))
		}
	}
}

func (service *MediatoolkitServiceImpl) MentionsOverTime(orgId string, request web.MTKPostReportRequest) web.MTKPostReportResponse {
	// fullRequest := web.MTKPostReportRequestComplete{
	// 	AccessToken: service.MediatoolkitToken,
	// 	DataRequest: web.DataRequest{
	// 		SourceFeeds: []web.SourceFeeds{
	// 			{
	// 				FeedType:  "keyword",
	// 				KeywordId: request.KeywordId,
	// 			},
	// 		},
	// 		FromTime:       request.FromTime,
	// 		ToTime:         request.ToTime,
	// 		TimeResolution: "day",
	// 		Dimension: web.Dimension{
	// 			DimensionType: "time",
	// 			DimensionSort: web.DimensionSort{
	// 				SortDirection: "asc",
	// 				SortBy:        "key",
	// 			},
	// 		},
	// 		ReportValue: web.ReportValue{
	// 			ValueType:     "count",
	// 			MergeOperator: "sum",
	// 		},
	// 	},
	// }
	// return service.postReport(orgId, "mentions_over_time", fullRequest)

	from, _ := strconv.Atoi(request.FromTime)
	until, _ := strconv.Atoi(request.ToTime)

	var entries []struct {
		KeyType string      "json:\"key_type\""
		Value   interface{} "json:\"value\""
		Key     interface{} "json:\"key\""
	}

	for i := from; i <= until; i = i + 86400 {
		if i == 1678294800 {
			entries = append(entries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   157,
			})
		} else if i == 1678381200 {
			entries = append(entries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678381200,
				Value:   95,
			})
		} else if i == 1678467600 {
			entries = append(entries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678467600,
				Value:   68,
			})
		} else if i == 1678554000 {
			entries = append(entries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678554000,
				Value:   80,
			})
		} else if i == 1678640400 {
			entries = append(entries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678640400,
				Value:   263,
			})
		} else {
			entries = append(entries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   0,
			})
		}
	}

	return web.MTKPostReportResponse{
		Duration: 100,
		Code:     200,
		Method:   "post",
		Data: struct {
			TotalValue interface{} "json:\"total_value\""
			Entries    []struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			} "json:\"entries\""
		}{
			TotalValue: 490,
			Entries:    entries,
		},
	}
}

func (service *MediatoolkitServiceImpl) SumOfMentions(orgId string, request web.MTKPostReportRequest) web.MTKPostReportBriefResponse {
	// fullRequest := web.MTKPostReportRequestBriefComplete{
	// 	AccessToken: service.MediatoolkitToken,
	// 	DataRequest: web.DataRequestBrief{
	// 		SourceFeeds: []web.SourceFeeds{
	// 			{
	// 				FeedType:  "keyword",
	// 				KeywordId: request.KeywordId,
	// 			},
	// 		},
	// 		FromTime:       request.FromTime,
	// 		ToTime:         request.ToTime,
	// 		TimeResolution: "day",
	// 		ReportValue: web.ReportValue{
	// 			ValueType:     "count",
	// 			MergeOperator: "sum",
	// 		},
	// 	},
	// }
	// return service.postReportBrief(orgId, "sum_of_mentions", fullRequest)

	return web.MTKPostReportBriefResponse{
		Duration: 100,
		Code:     200,
		Method:   "post",
		Data: struct {
			TotalValue interface{} "json:\"total_value\""
			Progress   struct {
				Total                 interface{} "json:\"total\""
				CalculatingFeed       interface{} "json:\"calculating_feed\""
				Done                  bool        "json:\"done\""
				FeedDone              bool        "json:\"feed_done\""
				CollectingMentions    interface{} "json:\"collecting_mentions\""
				AnalysingPartialSlots interface{} "json:\"analysing_partial_slots\""
			} "json:\"progress\""
		}{
			TotalValue: 663,
			Progress: struct {
				Total                 interface{} "json:\"total\""
				CalculatingFeed       interface{} "json:\"calculating_feed\""
				Done                  bool        "json:\"done\""
				FeedDone              bool        "json:\"feed_done\""
				CollectingMentions    interface{} "json:\"collecting_mentions\""
				AnalysingPartialSlots interface{} "json:\"analysing_partial_slots\""
			}{
				Total:                 100,
				CalculatingFeed:       100,
				Done:                  true,
				FeedDone:              true,
				CollectingMentions:    100,
				AnalysingPartialSlots: 100,
			},
		},
	}
}

func (service *MediatoolkitServiceImpl) SumOfImpressions(orgId string, request web.MTKPostReportRequest) web.MTKPostReportBriefResponse {
	// fullRequest := web.MTKPostReportRequestBriefComplete{
	// 	AccessToken: service.MediatoolkitToken,
	// 	DataRequest: web.DataRequestBrief{
	// 		SourceFeeds: []web.SourceFeeds{
	// 			{
	// 				FeedType:  "keyword",
	// 				KeywordId: request.KeywordId,
	// 			},
	// 		},
	// 		FromTime:       request.FromTime,
	// 		ToTime:         request.ToTime,
	// 		TimeResolution: "day",
	// 		ReportValue: web.ReportValue{
	// 			ValueType:     "reach",
	// 			MergeOperator: "sum",
	// 		},
	// 	},
	// }
	// return service.postReportBrief(orgId, "sum_of_impressions", fullRequest)

	return web.MTKPostReportBriefResponse{
		Duration: 100,
		Code:     200,
		Method:   "post",
		Data: struct {
			TotalValue interface{} "json:\"total_value\""
			Progress   struct {
				Total                 interface{} "json:\"total\""
				CalculatingFeed       interface{} "json:\"calculating_feed\""
				Done                  bool        "json:\"done\""
				FeedDone              bool        "json:\"feed_done\""
				CollectingMentions    interface{} "json:\"collecting_mentions\""
				AnalysingPartialSlots interface{} "json:\"analysing_partial_slots\""
			} "json:\"progress\""
		}{
			TotalValue: 537,
			Progress: struct {
				Total                 interface{} "json:\"total\""
				CalculatingFeed       interface{} "json:\"calculating_feed\""
				Done                  bool        "json:\"done\""
				FeedDone              bool        "json:\"feed_done\""
				CollectingMentions    interface{} "json:\"collecting_mentions\""
				AnalysingPartialSlots interface{} "json:\"analysing_partial_slots\""
			}{
				Total:                 100,
				CalculatingFeed:       100,
				Done:                  true,
				FeedDone:              true,
				CollectingMentions:    100,
				AnalysingPartialSlots: 100,
			},
		},
	}
}

func (service *MediatoolkitServiceImpl) SumOfAllSource(orgId string, request web.MTKPostReportRequest) web.MTKPostReportResponse {
	// fullRequest := web.MTKPostReportRequestComplete{
	// 	AccessToken: service.MediatoolkitToken,
	// 	DataRequest: web.DataRequest{
	// 		SourceFeeds: []web.SourceFeeds{
	// 			{
	// 				FeedType:  "keyword",
	// 				KeywordId: request.KeywordId,
	// 			},
	// 		},
	// 		FromTime:       request.FromTime,
	// 		ToTime:         request.ToTime,
	// 		TimeResolution: "day",
	// 		Dimension: web.Dimension{
	// 			DimensionType: "source_type",
	// 			DimensionSort: web.DimensionSort{
	// 				SortDirection: "dsc",
	// 				SortBy:        "value",
	// 			},
	// 		},
	// 		ReportValue: web.ReportValue{
	// 			ValueType:     "count",
	// 			MergeOperator: "sum",
	// 		},
	// 	},
	// }
	// return service.postReport(orgId, "sum_of_all_source", fullRequest)

	return web.MTKPostReportResponse{
		Duration: 100,
		Code:     200,
		Method:   "post",
		Data: struct {
			TotalValue interface{} "json:\"total_value\""
			Entries    []struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			} "json:\"entries\""
		}{
			TotalValue: 490,
			Entries: []struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{{
				KeyType: "normal",
				Key:     "twitter",
				Value:   327,
			}, {
				KeyType: "normal",
				Key:     "facebook",
				Value:   127,
			}, {
				KeyType: "normal",
				Key:     "web",
				Value:   34,
			}, {
				KeyType: "normal",
				Key:     "youtube",
				Value:   12,
			},
			},
		},
	}
}

func (service *MediatoolkitServiceImpl) EffectiveSentiment(orgId string, request web.MTKPostReportRequest) web.MTKPostReportResponse {
	// fullRequest := web.MTKPostReportRequestComplete{
	// 	AccessToken: service.MediatoolkitToken,
	// 	DataRequest: web.DataRequest{
	// 		SourceFeeds: []web.SourceFeeds{
	// 			{
	// 				FeedType:  "keyword",
	// 				KeywordId: request.KeywordId,
	// 			},
	// 		},
	// 		FromTime:       request.FromTime,
	// 		ToTime:         request.ToTime,
	// 		TimeResolution: "day",
	// 		Dimension: web.Dimension{
	// 			DimensionType: "effective_sentiment",
	// 			DimensionSort: web.DimensionSort{
	// 				SortDirection: "dsc",
	// 				SortBy:        "value",
	// 			},
	// 		},
	// 		ReportValue: web.ReportValue{
	// 			ValueType:     "count",
	// 			MergeOperator: "sum",
	// 		},
	// 	},
	// }
	// return service.postReport(orgId, "effective_sentiment", fullRequest)

	return web.MTKPostReportResponse{
		Duration: 100,
		Code:     200,
		Method:   "post",
		Data: struct {
			TotalValue interface{} "json:\"total_value\""
			Entries    []struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			} "json:\"entries\""
		}{
			TotalValue: 490,
			Entries: []struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{{
				KeyType: "normal",
				Key:     "neutral",
				Value:   127,
			}, {
				KeyType: "normal",
				Key:     "negative",
				Value:   14,
			}, {
				KeyType: "normal",
				Key:     "positive",
				Value:   54,
			}, {
				KeyType: "normal",
				Key:     "undefined",
				Value:   3,
			},
			},
		},
	}
}

func (service *MediatoolkitServiceImpl) MentionsOverTimeBySource(orgId string, request web.MTKPostReportRequest) web.MTKPostReportDetailResponse {
	// fullRequest := web.MTKPostReportRequestDetailComplete{
	// 	AccessToken: service.MediatoolkitToken,
	// 	DataRequest: web.DataRequestDetail{
	// 		SourceFeeds: []web.SourceFeeds{
	// 			{
	// 				FeedType:  "keyword",
	// 				KeywordId: request.KeywordId,
	// 			},
	// 		},
	// 		FromTime:       request.FromTime,
	// 		ToTime:         request.ToTime,
	// 		TimeResolution: "day",
	// 		Dimension: web.Dimension{
	// 			DimensionType: "source_type",
	// 			DimensionSort: web.DimensionSort{
	// 				SortDirection: "asc",
	// 				SortBy:        "key",
	// 			},
	// 		},
	// 		SubDimension: web.Dimension{
	// 			DimensionType: "time",
	// 			DimensionSort: web.DimensionSort{
	// 				SortDirection: "asc",
	// 				SortBy:        "key",
	// 			},
	// 		},
	// 		ReportValue: web.ReportValue{
	// 			ValueType:     "count",
	// 			MergeOperator: "sum",
	// 		},
	// 	},
	// }
	// return service.postReportDetail(orgId, "mentions_over_time_by_source", fullRequest)

	from, _ := strconv.Atoi(request.FromTime)
	until, _ := strconv.Atoi(request.ToTime)

	var twitterEntries, facebookEntries []struct {
		KeyType string      "json:\"key_type\""
		Value   interface{} "json:\"value\""
		Key     interface{} "json:\"key\""
	}

	for i := from; i <= until; i = i + 86400 {
		if i == 1678294800 {
			twitterEntries = append(twitterEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   123,
			})
			facebookEntries = append(facebookEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   34,
			})
		} else if i == 1678381200 {
			twitterEntries = append(twitterEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678381200,
				Value:   63,
			})
			facebookEntries = append(facebookEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   32,
			})
		} else if i == 1678467600 {
			twitterEntries = append(twitterEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678467600,
				Value:   45,
			})
			facebookEntries = append(facebookEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   23,
			})
		} else if i == 1678554000 {
			twitterEntries = append(twitterEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678554000,
				Value:   55,
			})
			facebookEntries = append(facebookEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   25,
			})
		} else if i == 1678640400 {
			twitterEntries = append(twitterEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678640400,
				Value:   176,
			})
			facebookEntries = append(facebookEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   87,
			})
		} else {
			twitterEntries = append(twitterEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   0,
			})
			facebookEntries = append(facebookEntries, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   0,
			})
		}
	}

	return web.MTKPostReportDetailResponse{
		Duration: 100,
		Code:     200,
		Method:   "post",
		Data: struct {
			TotalValue interface{} "json:\"total_value\""
			Entries    []struct {
				Entries []struct {
					KeyType string      "json:\"key_type\""
					Value   interface{} "json:\"value\""
					Key     interface{} "json:\"key\""
				} "json:\"entries\""
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			} "json:\"entries\""
		}{
			TotalValue: 100,
			Entries: []struct {
				Entries []struct {
					KeyType string      "json:\"key_type\""
					Value   interface{} "json:\"value\""
					Key     interface{} "json:\"key\""
				} "json:\"entries\""
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{{
				Entries: twitterEntries,
				KeyType: "normal",
				Value:   200,
				Key:     "twitter",
			}, {
				Entries: facebookEntries,
				KeyType: "normal",
				Value:   200,
				Key:     "facebook",
			}},
		},
	}
}

func (service *MediatoolkitServiceImpl) SentimentOverTime(orgId string, request web.MTKPostReportRequest) web.MTKPostReportDetailResponse {
	// fullRequest := web.MTKPostReportRequestDetailComplete{
	// 	AccessToken: service.MediatoolkitToken,
	// 	DataRequest: web.DataRequestDetail{
	// 		SourceFeeds: []web.SourceFeeds{
	// 			{
	// 				FeedType:  "keyword",
	// 				KeywordId: request.KeywordId,
	// 			},
	// 		},
	// 		FromTime:       request.FromTime,
	// 		ToTime:         request.ToTime,
	// 		TimeResolution: "day",
	// 		Dimension: web.Dimension{
	// 			DimensionType: "effective_sentiment",
	// 			DimensionSort: web.DimensionSort{
	// 				SortDirection: "dsc",
	// 				SortBy:        "value",
	// 			},
	// 		},
	// 		SubDimension: web.Dimension{
	// 			DimensionType: "time",
	// 			DimensionSort: web.DimensionSort{
	// 				SortDirection: "asc",
	// 				SortBy:        "key",
	// 			},
	// 		},
	// 		ReportValue: web.ReportValue{
	// 			ValueType:     "count",
	// 			MergeOperator: "sum",
	// 		},
	// 	},
	// }
	// return service.postReportDetail(orgId, "sentiment_over_time", fullRequest)

	from, _ := strconv.Atoi(request.FromTime)
	until, _ := strconv.Atoi(request.ToTime)

	var positive, negative, neutral []struct {
		KeyType string      "json:\"key_type\""
		Value   interface{} "json:\"value\""
		Key     interface{} "json:\"key\""
	}

	for i := from; i <= until; i = i + 86400 {
		if i == 1678294800 {
			positive = append(positive, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   12,
			})
			negative = append(negative, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   17,
			})
			neutral = append(neutral, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   87,
			})
		} else if i == 1678381200 {
			positive = append(positive, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678381200,
				Value:   32,
			})
			negative = append(negative, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   14,
			})
			neutral = append(neutral, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   12,
			})
		} else if i == 1678467600 {
			positive = append(positive, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678467600,
				Value:   12,
			})
			negative = append(negative, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   26,
			})
			neutral = append(neutral, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   23,
			})
		} else if i == 1678554000 {
			positive = append(positive, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678554000,
				Value:   43,
			})
			negative = append(negative, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   21,
			})
			neutral = append(neutral, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   34,
			})
		} else if i == 1678640400 {
			positive = append(positive, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678640400,
				Value:   31,
			})
			negative = append(negative, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     1678294800,
				Value:   31,
			})
			neutral = append(neutral, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   54,
			})
		} else {
			positive = append(positive, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   0,
			})
			negative = append(negative, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   0,
			})
			neutral = append(neutral, struct {
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{
				KeyType: "normal",
				Key:     i,
				Value:   0,
			})
		}
	}

	return web.MTKPostReportDetailResponse{
		Duration: 100,
		Code:     200,
		Method:   "post",
		Data: struct {
			TotalValue interface{} "json:\"total_value\""
			Entries    []struct {
				Entries []struct {
					KeyType string      "json:\"key_type\""
					Value   interface{} "json:\"value\""
					Key     interface{} "json:\"key\""
				} "json:\"entries\""
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			} "json:\"entries\""
		}{
			TotalValue: 100,
			Entries: []struct {
				Entries []struct {
					KeyType string      "json:\"key_type\""
					Value   interface{} "json:\"value\""
					Key     interface{} "json:\"key\""
				} "json:\"entries\""
				KeyType string      "json:\"key_type\""
				Value   interface{} "json:\"value\""
				Key     interface{} "json:\"key\""
			}{{
				Entries: neutral,
				KeyType: "normal",
				Value:   200,
				Key:     "neutral",
			}, {
				Entries: positive,
				KeyType: "normal",
				Value:   200,
				Key:     "positive",
			}, {
				Entries: negative,
				KeyType: "normal",
				Value:   200,
				Key:     "negative",
			}},
		},
	}
}

func (service *MediatoolkitServiceImpl) WordCloud(orgId string, request web.MTKPostReportRequest) []web.DummyWordCloudResponse {
	// return []web.DummyWordCloudResponse{}
	return []web.DummyWordCloudResponse{
		{
			Text:  "anies",
			Value: 945,
		},
		{
			Text:  "anies baswedan",
			Value: 899,
		},
		{
			Text:  "baswedan",
			Value: 873,
		},
		{
			Text:  "lampung",
			Value: 869,
		},
		{
			Text:  "pks",
			Value: 844,
		},
		{
			Text:  "ini",
			Value: 591,
		},
		{
			Text:  "yang",
			Value: 489,
		},
		{
			Text:  "jakarta",
			Value: 465,
		},
		{
			Text:  "presiden",
			Value: 458,
		},
		{
			Text:  "untuk",
			Value: 453,
		},
		{
			Text:  "jadi",
			Value: 441,
		},
		{
			Text:  "ganjar",
			Value: 422,
		},
		{
			Text:  "perubahan",
			Value: 373,
		},
		{
			Text:  "ada",
			Value: 349,
		},
		{
			Text:  "akan",
			Value: 347,
		},
		{
			Text:  "capres",
			Value: 322,
		},
		{
			Text:  "masyarakat",
			Value: 304,
		},
		{
			Text:  "dukung",
			Value: 376,
		},
		{
			Text:  "jangan",
			Value: 363,
		},
		{
			Text:  "punya",
			Value: 256,
		},
		{
			Text:  "bahwa",
			Value: 227,
		},
		{
			Text:  "menjadi",
			Value: 227,
		},
		{
			Text:  "warga",
			Value: 218,
		},
		{
			Text:  "indonesia",
			Value: 218,
		},
		{
			Text:  "apel",
			Value: 100,
		},
		{
			Text:  "cawapres",
			Value: 175,
		},
		{
			Text:  "kalau",
			Value: 168,
		},
		{
			Text:  "pendukung",
			Value: 139,
		},
		{
			Text:  "partai",
			Value: 131,
		},
		{
			Text:  "nasdem",
			Value: 127,
		},
		{
			Text:  "ahy",
			Value: 186,
		},
		{
			Text:  "jokowi",
			Value: 173,
		},
	}
}

func (service *MediatoolkitServiceImpl) postReport(orgId string, origin string, request web.MTKPostReportRequestComplete) web.MTKPostReportResponse {
	requestByte, _ := json.Marshal(request)

	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodPost)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/reports", service.MediatoolkitURL, orgId))
	req.SetBody(requestByte)

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKPostReportResponse
	var errors []error
	code, _, errors := agent.Struct(&data)

	if len(errors) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, errors[0].Error()))
	}
	if code == 400 {
		panic(exception.NewError(code, fmt.Sprintf("bad request %d, when calling: %s", code, origin)))
	} else if code == 403 {
		panic(exception.NewError(code, fmt.Sprintf("forbidden %d, when calling: %s", code, origin)))
	} else if code != 200 {
		panic(exception.NewError(fiber.StatusInternalServerError, fmt.Sprintf("internal server error %d, when calling: %s", code, origin)))
	}

	return data
}

func (service *MediatoolkitServiceImpl) postReportBrief(orgId string, origin string, request web.MTKPostReportRequestBriefComplete) web.MTKPostReportBriefResponse {
	requestByte, _ := json.Marshal(request)

	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodPost)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/reports", service.MediatoolkitURL, orgId))
	req.SetBody(requestByte)

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKPostReportBriefResponse
	var errors []error
	code, _, errors := agent.Struct(&data)

	if len(errors) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, errors[0].Error()))
	}
	if code == 400 {
		panic(exception.NewError(code, fmt.Sprintf("bad request %d, when calling: %s", code, origin)))
	} else if code == 403 {
		panic(exception.NewError(code, fmt.Sprintf("forbidden %d, when calling: %s", code, origin)))
	} else if code != 200 {
		panic(exception.NewError(fiber.StatusInternalServerError, fmt.Sprintf("internal server error %d, when calling: %s", code, origin)))
	}

	return data
}

func (service *MediatoolkitServiceImpl) postReportDetail(orgId string, origin string, request web.MTKPostReportRequestDetailComplete) web.MTKPostReportDetailResponse {
	fullRequestByte, _ := json.Marshal(request)

	agent := fiber.AcquireAgent()
	req := agent.Request()
	req.Header.SetMethod(fiber.MethodPost)
	req.SetRequestURI(fmt.Sprintf("%sorganizations/%s/reports", service.MediatoolkitURL, orgId))
	req.SetBody(fullRequestByte)

	if err := agent.Parse(); err != nil {
		panic(err)
	}

	var data web.MTKPostReportDetailResponse
	var errors []error
	code, _, errors := agent.Struct(&data)

	if len(errors) > 0 {
		panic(exception.NewError(fiber.StatusInternalServerError, errors[0].Error()))
	}
	if code == 400 {
		panic(exception.NewError(code, fmt.Sprintf("bad request %d, when calling: %s", code, origin)))
	} else if code == 403 {
		panic(exception.NewError(code, fmt.Sprintf("forbidden %d, when calling: %s", code, origin)))
	} else if code != 200 {
		panic(exception.NewError(fiber.StatusInternalServerError, fmt.Sprintf("internal server error %d, when calling: %s", code, origin)))
	}

	return data
}

// func (service *MediatoolkitServiceImpl) Create(userId string, request web.SocialCreateRequest) web.SocialResponse {
// 	err := service.Validate.Struct(request)
// 	helper.PanicIfError(err)

// 	summary := domain.Summary{
// 		UserId:         userId,
// 		Impressions:    request.Summary.Impressions,
// 		Engagements:    request.Summary.Engagements,
// 		EngagementRate: request.Summary.EngagementRate,
// 		PostLinkClicks: request.Summary.PostLinkClicks,
// 		UpdatedAt:      time.Now(),
// 	}

// 	// if summary already recorded, update it. Otherwise, create e new record
// 	if query, _ := service.SocialSummaryRepository.FindByQuery("user_id", userId); query.UserId != "" {
// 		service.SocialSummaryRepository.Update(summary)
// 	} else {
// 		summary.Id = strings.ToLower(randstr.String(10))
// 		service.SocialSummaryRepository.Create(summary)
// 	}

// 	// creating datapoint record
// 	data := domain.DataPoint{
// 		Id:             strings.ToLower(randstr.String(10)),
// 		UserId:         userId,
// 		AudienceGrowth: request.AudienceGrowth,
// 		Impression:     request.Impression,
// 		Engagement:     request.Engagement,
// 		EngagementRate: request.EngagementRate,
// 		VideoViews:     request.VideoViews,
// 		Timestamp:      request.Timestamp,
// 	}

// 	service.SocialDataPointRepository.Create(data)

// 	var datas []domain.DataPoint
// 	datas = append(datas, data)

// 	return helper.ToSocialResponse(summary, datas)
// }

// func (service *MediatoolkitServiceImpl) UpdateSummary(userId string, request web.SocialUpdateSummaryRequest) web.SocialSummaryResponse {
// 	err := service.Validate.Struct(request)
// 	helper.PanicIfError(err)

// 	summary, err := service.SocialSummaryRepository.FindByQuery("user_id", userId)

// 	if err != nil {
// 		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
// 	}

// 	summary.Impressions = request.Impressions
// 	summary.Engagements = request.Engagements
// 	summary.EngagementRate = request.EngagementRate
// 	summary.PostLinkClicks = request.PostLinkClicks
// 	summary.UpdatedAt = time.Now()

// 	service.SocialSummaryRepository.Update(summary)

// 	return helper.ToSocialSummaryResponse(summary)
// }

// func (service *MediatoolkitServiceImpl) UpdateDataPoint(userid string, dataId string, request web.SocialUpdateDatapointRequest) web.SocialDatapointResponse {
// 	err := service.Validate.Struct(request)
// 	helper.PanicIfError(err)

// 	data, err := service.SocialDataPointRepository.FindByQuery("_id", dataId)

// 	if err != nil {
// 		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
// 	}
// 	if data.UserId != userid {
// 		panic(exception.NewError(fiber.StatusNotFound, "datapoint not found"))
// 	}

// 	data.Timestamp = request.Timestamp
// 	data.AudienceGrowth = request.AudienceGrowth
// 	data.Impression = request.Impression
// 	data.Engagement = request.Engagement
// 	data.EngagementRate = request.EngagementRate
// 	data.VideoViews = request.VideoViews

// 	// data.Impressions = request.Impressions
// 	// data.Engagements = request.Engagements
// 	// data.EngagementRate = request.EngagementRate
// 	// data.PostLinkClicks = request.PostLinkClicks
// 	// data.UpdatedAt = time.Now()

// 	service.SocialDataPointRepository.Update(data)

// 	return helper.ToSocialDatapointResponse(data)
// }

// func (service *MediatoolkitServiceImpl) Delete(userId, dataId string) {

// 	data, err := service.SocialDataPointRepository.FindByQuery("_id", dataId)
// 	if err != nil {
// 		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
// 	}
// 	if data.UserId != userId {
// 		panic(exception.NewError(fiber.StatusNotFound, "data not found"))
// 	}

// 	err = service.SocialDataPointRepository.Delete(dataId)
// 	helper.PanicIfError(err)

// 	log.Println("LOG", "deleted social_datapoint", data.Id, data.UserId, data.Timestamp)
// }

// func (service *MediatoolkitServiceImpl) FindDatapointById(userId, dataId string) web.SocialDatapointResponse {
// 	data, err := service.SocialDataPointRepository.FindByQuery("_id", dataId)
// 	if err != nil || data.Id == "" || data.UserId != userId {
// 		panic(exception.NewError(fiber.StatusNotFound, "data not found"))
// 	}

// 	return helper.ToSocialDatapointResponse(data)
// }

// func (service *MediatoolkitServiceImpl) GetCompleteReport(query map[string]string) web.SocialResponse {
// 	summary, err := service.SocialSummaryRepository.FindByQuery("user_id", query["user_id"])
// 	if err != nil || summary.Id == "" {
// 		panic(exception.NewError(fiber.StatusNotFound, "data not found"))
// 	}

// 	data := service.SocialDataPointRepository.FindAll(query)

// 	return helper.ToSocialResponse(summary, data)
// }
