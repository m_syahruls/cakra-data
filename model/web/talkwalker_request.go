package web

type CreateTalkwalkerRequest struct {
	Name    string `json:"name" validate:"required"`
	FileUrl string `json:"file_url"`
}

type UpdateTalkwalkerRequest struct {
	Name    string `json:"name" validate:"required"`
	FileUrl string `json:"file_url"`
}
