package web

// 200 OK List Accessible Customers

type GoogleAdsCustomersList struct {
	ResourceNames []string `json:"resourceNames"`
}

// end of 200 list accessible customers

// 200 OK Campaign Details

type GoogleAdsCampaignDetailComplete struct {
	Results   []googleAdsCampaignDetail `json:"results"`
	FieldMask string                    `json:"fieldMask"`
	RequestId string                    `json:"requestId"`
}

type googleAdsCampaignDetail struct {
	Customer       googleAdsCustomer       `json:"customer"`
	Campaign       googleAdsCampaign       `json:"campaign"`
	Metricts       googleAdsMetricts       `json:"metrics"`
	CampaignBudget googleAdsCampaignBudget `json:"campaignBudget"`
}

type googleAdsCustomer struct {
	ResourceName    string `json:"resourceName"`
	DescriptiveName string `json:"descriptiveName"`
	CurrencyCode    string `json:"currencyCode"`
}

type googleAdsCampaign struct {
	ID                     string `json:"id"`
	Name                   string `json:"name"`
	ResourceName           string `json:"resourceName"`
	Status                 string `json:"status"`
	AdvertisingChannelType string `json:"advertisingChannelType"`
	BiddingStrategyType    string `json:"biddingStrategyType"`
}

type googleAdsMetricts struct {
	Clicks                 interface{} `json:"clicks"`
	ViewThroughConversions interface{} `json:"viewThroughConversions"`
	Conversions            interface{} `json:"conversions"`
	CostMicros             interface{} `json:"costMicros"`
	Impressions            interface{} `json:"impressions"`
	CTR                    interface{} `json:"ctr"`
	AverageCPC             interface{} `json:"averageCpc"`
	CostPerConversions     interface{} `json:"costPerConversions"`
}

type googleAdsCampaignBudget struct {
	ResourceName string      `json:"resourceName"`
	AmountMicros interface{} `json:"amountMicros"`
}

// end of 200 Campaign Details

// 400 Bad Request

type GoogleAdsBadRequestComplete struct {
	Error googleAdsBadRequestErrorComplete `json:"error"`
}

type googleAdsBadRequestErrorComplete struct {
	Code    int                         `json:"code"`
	Message string                      `json:"message"`
	Status  string                      `json:"status"`
	Details []googleAdsBadRequestDetail `json:"details"`
}

type googleAdsBadRequestDetail struct {
	Type      string                     `json:"@type"`
	Errors    []googleAdsBadRequestError `json:"errors"`
	RequestId string                     `json:"requestId"`
}

type googleAdsBadRequestError struct {
	ErrorCode googleAdsErrorCode `json:"errorCode"`
	Message   string             `json:"message"`
}

type googleAdsErrorCode struct {
	QueryError string `json:"queryError"`
}

// end of 400

// 401 Unauthorized

type GoogleAdsUnauthorizedComplete struct {
	Error googleAdsUnauthorized `json:"error"`
}

type GoogleAdsEligibility struct {
	Eligibility bool   `json:"eligibility"`
	Message     string `json:"message"`
}

type googleAdsUnauthorized struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Status  string `json:"status"`
}

// end of 401
