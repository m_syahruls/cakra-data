package domain

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IssueLog struct {
	ID           primitive.ObjectID `json:"id" bson:"_id"`
	ProvinceID   int                `json:"province_id" bson:"province_id"`
	ProvinceName string             `json:"province_name" bson:"province_name"`
	RegencyID    int                `json:"regency_id" bson:"regency_id"`
	RegencyName  string             `json:"regency_name" bson:"regency_name"`
	DistrictID   int                `json:"district_id" bson:"district_id"`
	DistrictName string             `json:"district_name" bson:"district_name"`
	VillageID    int                `json:"village_id" bson:"village_id"`
	VillageName  string             `json:"village_name" bson:"village_name"`
	Year         string             `json:"year" bson:"year"`
	SubIssueId   string             `json:"sub_issue_id" bson:"sub_issue_id"`
	Data         IssueLogData       `json:"data" bson:"data`
	CreatedAt    time.Time          `json:"created_at" bson:"created_at"`
}

type IssueLogData struct {
	TypeIssue string `json:"type_issue" bson:"type_issue"`
	SubIssue  string `json:"sub_issue" bson:"sub_issue"`
	Latitude  string `json:"latitude" bson:"latitude"`
	Longitude string `json:"longitude" bson:"longitude"`
	Desc      string `json:"desc" bson:"desc"`
}
