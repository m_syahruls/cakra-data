package web

type CreateIssueRequest struct {
	Label string `json:"label" validate:"required"`
	Value string `json:"value" validate:"required"`
	Color string `json:"color" validate:"required"`
}

type UpdateIssueRequest struct {
	Label string `json:"label" validate:"required"`
	Value string `json:"value" validate:"required"`
	Color string `json:"color" validate:"required"`
}
