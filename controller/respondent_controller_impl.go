package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type RespondentControllerImpl struct {
	RespondentService service.RespondentService
	LogService        service.LogService
}

type RespondentController interface {
	NewRespondentRouter(app *fiber.App)
}

func NewRespondentController(respondentService service.RespondentService, logService service.LogService) RespondentController {
	return &RespondentControllerImpl{
		RespondentService: respondentService,
		LogService:        logService,
	}
}

func (controller *RespondentControllerImpl) NewRespondentRouter(app *fiber.App) {
	respondent := app.Group("/respondents")
	respondent.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})
	app.Post("users/create-for-survey", controller.CreateRespondentForSurvey)
	respondent.Get("/", controller.FindAll)
	respondent.Get("/:respondentid", controller.FindById)
	respondent.Put("/:respondentid", controller.Update)
	respondent.Delete("/:respondentid", controller.Delete)
}

func (controller *RespondentControllerImpl) CreateRespondentForSurvey(ctx *fiber.Ctx) error {
	var request web.CreateRespondent
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	respondentResponse := controller.RespondentService.CreateRespondentForSurvey(ctx, request)

	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	action := fmt.Sprintf("created respondent %s", respondentResponse.Name)
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    respondentResponse,
	})
}

func (controller *RespondentControllerImpl) FindById(ctx *fiber.Ctx) error {
	respondentId := ctx.Params("respondentid")

	userResponse := controller.RespondentService.FindById(respondentId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *RespondentControllerImpl) FindAll(ctx *fiber.Ctx) error {
	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	if level == "1" {
		userid = ""
	}
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	userResponse := controller.RespondentService.FindAll(page, limit, userid)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *RespondentControllerImpl) Update(ctx *fiber.Ctx) error {
	var request web.UserUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if admin := middleware.IsAdmin(ctx); !admin {
		return UnauthorizeReturn(ctx, "only admin can update respondent")
	}

	respondnetId := ctx.Params("respondentid")

	userResponse := controller.RespondentService.Update(ctx, respondnetId, request)

	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	action := fmt.Sprintf("update user %s", userResponse.Name)
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *RespondentControllerImpl) Delete(ctx *fiber.Ctx) error {
	respondentId := ctx.Params("respondentid")

	if admin := middleware.IsAdmin(ctx); !admin {
		return UnauthorizeReturn(ctx, "only admin can delete respondent")
	}

	controller.RespondentService.Delete(ctx, respondentId)

	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	action := fmt.Sprintf("delete respondent %s", respondentId)
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
