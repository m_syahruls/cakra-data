package service

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type DistrictServiceImpl struct {
	DistrictRepository repository.DistrictRepository
}

type DistrictService interface {
	FindAll(page, limit, provinceId, name string) []web.DistrictsResponse
	FindById(districtId string) web.DistrictsResponse
}

func NewDistrictService(districtRepository repository.DistrictRepository) DistrictService {
	return &DistrictServiceImpl{
		DistrictRepository: districtRepository,
	}
}

func (service *DistrictServiceImpl) FindById(districtId string) web.DistrictsResponse {
	district, err := service.DistrictRepository.FindDistrictById(districtId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "district not found"))
	}

	return helper.ToDistrictResponse(district)
}

func (service *DistrictServiceImpl) FindAll(page, limit, provinceId, name string) []web.DistrictsResponse {
	district := service.DistrictRepository.FindAll(page, limit, provinceId, name)
	return helper.ToDistrictResponses(district)
}
