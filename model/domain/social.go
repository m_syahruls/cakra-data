package domain

import "time"

type Summary struct {
	Id             string    `json:"id" bson:"_id"`
	UserId         string    `json:"user_id" bson:"user_id"`
	Impressions    float32   `json:"impressions" bson:"impressions"`
	Engagements    float32   `json:"engagements" bson:"engagements"`
	EngagementRate float32   `json:"engagement_rate" bson:"engagement_rate"`
	PostLinkClicks float32   `json:"post_link_clicks" bson:"post_link_clicks"`
	UpdatedAt      time.Time `json:"updated_at" bson:"updated_at"`
}

type DataPoint struct {
	Id             string          `json:"id" bson:"_id"`
	UserId         string          `json:"user_id" bson:"user_id"`
	AudienceGrowth SocialData      `json:"audience_growth" bson:"audience_growth"`
	Impression     SocialData      `json:"impression" bson:"impression"`
	Engagement     SocialData      `json:"engagement" bson:"engagement"`
	EngagementRate SocialDataFloat `json:"engagement_rate" bson:"engagement_rate"`
	VideoViews     SocialData      `json:"video_views" bson:"video_views"`
	Timestamp      time.Time       `json:"timestamp" bson:"timestamp"`
}

type SocialData struct {
	Twitter   int `json:"twitter" bson:"twitter"`
	Facebook  int `json:"facebook" bson:"facebook"`
	Instagram int `json:"instagram" bson:"instagram"`
	Linkedin  int `json:"linkedin" bson:"linkedin"`
	Tiktok    int `json:"tiktok" bson:"tiktok"`
}

type SocialDataFloat struct {
	Twitter   float32 `json:"twitter" bson:"twitter"`
	Facebook  float32 `json:"facebook" bson:"facebook"`
	Instagram float32 `json:"instagram" bson:"instagram"`
	Linkedin  float32 `json:"linkedin" bson:"linkedin"`
	Tiktok    float32 `json:"tiktok" bson:"tiktok"`
}
