package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type PartaiControllerImpl struct {
	PartaiService service.PartaiService
}

type PartaiController interface {
	NewPartaiRouter(app *fiber.App)
}

func NewPartaiController(partaiService service.PartaiService) PartaiController {
	return &PartaiControllerImpl{
		PartaiService: partaiService,
	}
}

func (controller *PartaiControllerImpl) NewPartaiRouter(app *fiber.App) {
	partai := app.Group("/partai")
	partai.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	partai.Post("/", controller.Create)
	partai.Get("/", controller.FindAll)
}

func (controller *PartaiControllerImpl) Create(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can create partai")
	}

	var request web.Partai
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	data := controller.PartaiService.Create(request)

	//action := fmt.Sprintf("create partai %s", eventResponse.EventName)
	//actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	//controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    data,
	})
}

func (controller *PartaiControllerImpl) FindAll(ctx *fiber.Ctx) error {
	if isadmin := middlewares.IsAdmin(ctx); !isadmin {
		return UnauthorizeReturn(ctx, "unathorize")
	}

	page := ctx.Query("page")
	limit := ctx.Query("limit")

	partai := controller.PartaiService.FindAll(page, limit)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    partai,
	})
}
