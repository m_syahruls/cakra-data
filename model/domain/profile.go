package domain

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Profile struct {
	Id            string           `json:"id" bson:"_id"`
	OccupationId  string           `json:"occupation_id" bson:"occupation_id"`
	NIK           string           `json:"nik" bson:"nik"`
	Name          string           `json:"name" bson:"name"`
	Email         string           `json:"email" bson:"email"`
	Phone         string           `json:"phone" bson:"phone"`
	Gender        string           `json:"gender" bson:"gender"`
	Password      string           `json:"password" bson:"password"`
	ReferralCode  string           `json:"referral_code" bson:"referral_code"`
	ProfileImage  string           `json:"profile_image" bson:"profile_image"`
	GoogleAdsID   string           `json:"google_ads_id" bson:"google_ads_id"`
	AyrshareToken string           `json:"ayrshare_token" bson:"ayrshare_token"`
	DetermID      string           `json:"determ_id" bson:"determ_id"`
	CreatedAt     time.Time        `json:"created_at" bson:"created_at"`
	UpdatedAt     time.Time        `json:"updated_at" bson:"updated_at"`
	Occupation    Occupation       `json:"occupation" bson:"occupation"`
	Recruitment   UserRecruit      `json:"recruitment" bson:"recruitment"`
	Survey        ProgressSurvey   `json:"survey" bson:"survey"`
	Logistic      ProgressLogistic `json:"logistic" bson:"logistic"`
	Accesses      []string         `json:"accesses" bson:"accesses"`
}

func (user *Profile) SetPassword(password string) {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	user.Password = string(hashedPassword)
}

func (user *Profile) ComparePassword(correctPassword string, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(correctPassword), []byte(password))
}
