package repository

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type VillageRepositoryImpl struct {
	Collection        *mongo.Collection
	CollectionRegency *mongo.Collection
}

type VillageRepository interface {
	FindById(villageId string) (domain.Village, error)
	FindAll(page, limit, provinceId, name string) []domain.Village
	FindAllByRegencyId(page, limit, regencyId, name string) []domain.Village
}

func NewVillageRepository(database *mongo.Database) VillageRepository {
	return &VillageRepositoryImpl{
		Collection:        database.Collection("villages"),
		CollectionRegency: database.Collection("regencies"),
	}
}

func (repository *VillageRepositoryImpl) FindById(villageId string) (domain.Village, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var Village domain.Village
	query := bson.M{"_id": villageId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&Village)

	return Village, result.Err()
}

func (repository *VillageRepositoryImpl) FindAll(page, limit, districtId, name string) []domain.Village {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryVillage(districtId, name)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var villages []domain.Village

	for cursor.Next(ctx) {
		var village domain.Village
		cursor.Decode(&village)
		villages = append(villages, village)
	}

	return villages
}

func (repository *VillageRepositoryImpl) FindAllByRegencyId(page, limit, regencyId, name string) []domain.Village {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: regencyId}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "districts"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "regency_id"}, {Key: "as", Value: "district"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$district"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "district._id"}, {Key: "foreignField", Value: "district_id"}, {Key: "as", Value: "village"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$village._id"},
		{Key: "district_id", Value: bson.D{{Key: "$first", Value: "$village.district_id"}}},
		{Key: "name", Value: bson.D{{Key: "$first", Value: "$village.name"}}},
		{Key: "latitude", Value: bson.D{{Key: "$first", Value: "$village.longitude"}}},
		{Key: "longitude", Value: bson.D{{Key: "$first", Value: "$village.latitude"}}},
	}}}

	pipeline := mongo.Pipeline{matchStage, lookupStage1, unwindStage1, lookupStage2, unwindStage2, groupStage}

	helper.QueryVillageAggregat(name, &pipeline)
	helper.PaggingAggregat(page, limit, &pipeline)

	cursor, err := repository.CollectionRegency.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var villages []domain.Village

	for cursor.Next(ctx) {
		var village domain.Village
		cursor.Decode(&village)
		villages = append(villages, village)
		fmt.Println(village)
	}

	return villages
}
