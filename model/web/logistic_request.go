package web

import "time"

type LogisticCreate struct {
	CategoryId    string    `json:"category_id" bson:"category_id"`
	Nama          string    `json:"name" bson:"name"`
	Location      string    `json:"location" bson:"location"`
	ImageLogistic string    `json:"image_logistic" bson:"image_logistic"`
	Longitude     string    `json:"longitude" bson:"longitude"`
	Latitude      string    `json:"latitude" bson:"latitude"`
	UserId        string    `json:"user_id" bson:"user_id"`
	CreatedAt     time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt     time.Time `json:"updated_at" bson:"updated_at"`
}

type LogisticUpdate struct {
	CategoryId string `json:"category_id"`
	Nama       string `json:"name" validate:"required"`
}

type LogisticCategoryRequest struct {
	Id   string `json:"id" validate:"required"`
	Name string `json:"name" validate:"required"`
}

type UpdateLogisticCategory struct {
	Name string `json:"name" validate:"required"`
}
