package web

type CreateResponseRequest struct {
	SurveyId     string                        `json:"survey_id" validate:"required"`
	RespondentId string                        `json:"respondent_id" validate:"required"`
	Answers      []CreateResponseAnswerRequest `json:"answers" validate:"required"`
	Location     interface{}                   `json:"location"`
}

type UpdateResponseRequest struct {
	Answers []AnswerCreateRequest `json:"answers" validate:"required"`
}

type CreateResponseAnswerRequest struct {
	QuestionId string      `json:"question_id" validate:"required"`
	Answer     interface{} `json:"answer" validate:"required"`
	//OptionsId     string `json:"option_id"`
	//AnswerText    string `json:"answer_text"`
	//AnswerNumeric int    `json:"answer_numeric"`
}

type UpdateAnswerResponseRequest struct {
	OptionsId     string `json:"option_id"`
	AnswerText    string `json:"answer_text"`
	AnswerNumeric int32  `json:"answer_numeric"`
}

type Location struct {
	Villageid string `json:"village_id"`
	Address   string `json:"address"`
	Longitude string `json:"longitude"`
	Latitude  string `json:"latitude"`
}
