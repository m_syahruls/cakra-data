package web

type VillageResponse struct {
	ID         string      `json:"id" bson:"_id"`
	DistrictId string      `json:"district_id" bson:"district_id"`
	Name       string      `json:"name" bson:"name"`
	Latitude   interface{} `json:"latitude" bson:"latitude"`
	Longitude  interface{} `json:"longitude" bson:"longitude"`
}
