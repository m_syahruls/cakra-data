package main

import (
	"fmt"
	"os"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/controller"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

func main() {
	time.Local = time.UTC
	db := config.NewDB()
	validate := validator.New()

	logRepository := repository.NewLogRepository(db)
	logService := service.NewLogService(logRepository)
	logController := controller.NewLogController(logService)

	dataKpuRepository := repository.NewDataKpuRepository(db)
	dataKpuService := service.NewDataKpuService(dataKpuRepository, validate)
	dataKpuController := controller.NewDataKpuController(dataKpuService)

	recruitedUserRepository := repository.NewRecruitedUserRepository(db)
	occupationRepository := repository.NewOccupationRepository(db)
	provinceRepository := repository.NewProvinceRepository(db)
	regencyRepository := repository.NewRegencyRepository(db)
	districtRepository := repository.NewDistrictRepository(db)
	villageRepository := repository.NewVillageRepository(db)
	IssueDataRepository := repository.NewIssueDataRepository(db)

	provinceService := service.NewProvinceService(provinceRepository)
	provinceController := controller.NewProvinceController(provinceService)

	regencyService := service.NewRegencyService(regencyRepository)
	regencyController := controller.NewRegencyController(regencyService)

	districtService := service.NewDistrictService(districtRepository)
	districtController := controller.NewDistrictController(districtService)

	villageService := service.NewVillageService(villageRepository)
	villageController := controller.NewVillageController(villageService)

	recruitRepository := repository.NewRecruitRepository(db)
	recruitService := service.NewRecruitService(recruitRepository, recruitedUserRepository, occupationRepository, validate)
	recruitController := controller.NewRecruitController(recruitService, logService)

	occupationService := service.NewOccupationService(occupationRepository, validate)
	occupationController := controller.NewOccupationController(occupationService, logService)

	authRepository := repository.NewAuthRepository(db)
	authService := service.NewAuthService(authRepository, occupationRepository, recruitRepository, recruitedUserRepository, validate)
	authController := controller.NewAuthController(authService, logService)

	userRepository := repository.NewUserRepository(db)
	userService := service.NewUserService(userRepository, occupationRepository, recruitRepository, recruitedUserRepository, districtRepository, validate)
	userController := controller.NewUserController(userService, logService)

	googleAdsService := service.NewGoogleAdsService(
		os.Getenv("GOOGLE_CLIENT_ID"),
		os.Getenv("GOOGLE_CLIENT_SECRET"),
		os.Getenv("GOOGLE_REFRESH_TOKEN"),
		os.Getenv("GOOGLE_DEVELOPER_TOKEN"),
		os.Getenv("GOOGLE_MANAGER_ID"),
		os.Getenv("GOOGLE_REDIRECT_URL"),
		os.Getenv("GOOGLE_API_SCOPES"),
	)
	googleAdsController := controller.NewGoogleAdsController(googleAdsService)

	socialmediaRepository := repository.NewSocialmediaHistoricalRepository(db)
	socialmediaService := service.NewAyrshareService(socialmediaRepository, os.Getenv("AYRSHARE_API_URL"))
	scheduler := controller.NewScheduler(socialmediaService, userService)

	logisticRepository := repository.NewLogisticRepository(db)
	logisticService := service.NewLogisticService(logisticRepository, validate)
	logisticController := controller.NewLogisticController(logisticService)

	sentimentService := service.NewMediatoolkitService(os.Getenv("MEDIATOOLKIT_URL"), os.Getenv("MEDIATOOLKIT_TOKEN"), validate)

	profileRepository := repository.NewProfileRepository(db)
	profileService := service.NewProfileService(profileRepository, userRepository, logisticRepository, validate)
	profileController := controller.NewProfileController(profileService, googleAdsService, sentimentService, socialmediaService, logService)

	sentimentController := controller.NewSentimentController(sentimentService, profileService)

	optionRepository := repository.NewOptionRepository(db)
	questionRepository := repository.NewQuestionRepository(db)
	surveyRepository := repository.NewSurveyRepository(db)
	answerRepository := repository.NewAnswerRepository(db)
	responseRepository := repository.NewResponseRepository(db)
	respondentRepository := repository.NewRespondentRepository(db)
	progressSurveyRepository := repository.NewProgresSurveyRepository(db)
	progressSurveySumRepository := repository.NewProgresSurveySumRepository(db)

	optionService := service.NewOptionService(optionRepository, questionRepository, validate)
	optionController := controller.NewOptionController(optionService, logService)

	questionService := service.NewQuestionService(questionRepository, surveyRepository, responseRepository, validate)
	questionController := controller.NewQuestionController(questionService, logService)

	surveyService := service.NewSurveyService(surveyRepository, questionRepository, optionRepository, responseRepository, validate)
	surveyController := controller.NewSurveyController(surveyService, logService)

	answerService := service.NewAnswerService(answerRepository, responseRepository, questionRepository, optionRepository, validate)
	answerController := controller.NewAnswerController(answerService, logService)

	responseService := service.NewResponseService(responseRepository, surveyRepository, questionRepository, answerRepository, optionRepository, userRepository, validate, respondentRepository, progressSurveyRepository, progressSurveySumRepository, regencyRepository, districtRepository, villageRepository)
	responseController := controller.NewResponseController(responseService, logService)

	respondentService := service.NewRespondentService(respondentRepository, progressSurveyRepository, progressSurveySumRepository, responseRepository, validate)
	respondentController := controller.NewRespondentController(respondentService, logService)

	progresSurveyService := service.NewProgresSurveyService(progressSurveyRepository, progressSurveySumRepository, validate)
	progresSurveyController := controller.NewProgresSurveyController(progresSurveyService, logService)

	eventRepository := repository.NewEventRepository(db)
	eventService := service.NewEventService(eventRepository, validate)
	eventController := controller.NewEventController(eventService, logService)

	userDptRepository := repository.NewUserDptRepository(db)
	userDptService := service.NewUserDptService(userDptRepository)
	userDptController := controller.NewUserDptController(userDptService)

	surveyResultRepository := repository.NewSurveyResultRepository(db)
	surveyResultService := service.NewSurveyResultService(surveyResultRepository, answerRepository)
	surveyResultController := controller.NewSurveyResultController(surveyResultService)

	complaintRepository := repository.NewComplaintRepository(db)
	complaintService := service.NewComplaintService(complaintRepository, validate)
	complaintController := controller.NewComplaintController(complaintService)

	electionRepository := repository.NewElectionTypeRepository(db)
	electionService := service.NewElectionTypeService(electionRepository)
	electionController := controller.NewElectionTypeController(electionService)

	partaiRepository := repository.NewPartaiRepository(db)
	partaiService := service.NewPartaiService(partaiRepository)
	partaiController := controller.NewPartaiController(partaiService)

	candidateRepository := repository.NewCandidateRepository(db)
	candidateService := service.NewCandidateService(candidateRepository, electionRepository, partaiRepository, regencyRepository, validate)
	candidateController := controller.NewCandidateController(candidateService)

	religionRepository := repository.NewReligionRepository(db)
	religionService := service.NewReligionService(religionRepository)
	religionController := controller.NewReligionController(religionService)

	userPemilihRepository := repository.NewUserPemilihRepository(db)
	userPemilihService := service.NewUserPemilihService(userPemilihRepository, occupationRepository, recruitRepository, recruitedUserRepository, districtRepository, userDptRepository, religionRepository, candidateRepository, validate)
	userPemilihController := controller.NewUserPemilihController(userPemilihService)

	issueSubRepository := repository.NewIssueSubRepository(db)
	issueSubService := service.NewIssueSubService(issueSubRepository, validate)
	issueSubController := controller.NewIssueSubController(issueSubService, logService)

	permissionRepository := repository.NewPermissionRepository(db)
	permissionService := service.NewPermissionService(permissionRepository, validate)
	permissionController := controller.NewPermissionController(permissionService, logService)

	roleRepository := repository.NewRoleRepository(db)
	roleService := service.NewRoleService(roleRepository, validate)
	roleController := controller.NewRoleController(roleService, logService)

	accessRepository := repository.NewAccessRepository(db)
	accessService := service.NewAccessService(accessRepository, validate)
	accessController := controller.NewAccessController(accessService, logService)

	talkwalkerRepository := repository.NewTalkwalkerRepository(db)
	talkwalkerService := service.NewTalkwalkerService(talkwalkerRepository, validate)
	talkwalkerController := controller.NewTalkwalkerController(talkwalkerService, logService)

	issueManagementDataRepository := repository.NewIssueManagementDataRepository(db)
	issueManagementDataService := service.NewIssueManagementDataService(issueManagementDataRepository, validate)
	issueManagementDataController := controller.NewIssueManagementDataController(issueManagementDataService, logService)

	issueLogRepository := repository.NewIssueLogRepository(db)
	// issueLogService := service.NewIssueLogService(issueLogRepository, validate)
	// issueLogController := controller.NewIssueLogController(issueLogService, logService)

	issueRepository := repository.NewIssueRepository(db)
	issueService := service.NewIssueService(issueRepository, issueSubRepository, validate, provinceRepository, regencyRepository, districtRepository, villageRepository, issueLogRepository, issueManagementDataRepository)
	issueDataService := service.NewIssueDataService(IssueDataRepository)
	issueController := controller.NewIssueController(issueService, logService, regencyService, provinceService, issueDataService)

	app := fiber.New(config.NewFiberConfig())
	app.Use(recover.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowMethods:     "*",
		AllowHeaders:     "*",
		AllowCredentials: true,
	}))

	authController.NewAuthRouter(app)
	userController.NewUserRouter(app)
	occupationController.NewOccupationRouter(app)
	recruitController.NewRecruitRouter(app)
	profileController.NewProfileRouter(app)
	surveyController.NewSurveyRouter(app)
	optionController.NewOptionRouter(app)
	questionController.NewQuestionRouter(app)
	answerController.NewAnswerRouter(app)
	responseController.NewResponseRouter(app)
	eventController.NewEventRouter(app)
	sentimentController.NewSentimentRouter(app)
	provinceController.NewProvinceRouter(app)
	regencyController.NewRegencyRouter(app)
	districtController.NewDistrictRouter(app)
	userDptController.NewUserDptRouter(app)
	surveyResultController.NewSurveyResultRouter(app)
	complaintController.NewComplaintRouter(app)
	logisticController.NewLogisticRouter(app)
	logController.NewLogRouter(app)
	respondentController.NewRespondentRouter(app)
	progresSurveyController.NewProgresSurveyRouter(app)
	electionController.NewElectionTypeRouter(app)
	partaiController.NewPartaiRouter(app)
	candidateController.NewCandidateRouter(app)
	religionController.NewReligionRouter(app)
	userPemilihController.NewUserPemilihRouter(app)
	villageController.NewVillageRouter(app)
	googleAdsController.NewGoogleAdsRouter(app)
	scheduler.StartScheduler()
	dataKpuController.NewDataKpuRouter(app)
	issueController.NewIssueRouter(app)
	issueSubController.NewIssueSubRouter(app)
	permissionController.NewPermissionRouter(app)
	roleController.NewRoleRouter(app)
	accessController.NewAccessRouter(app)
	talkwalkerController.NewTalkwalkerRouter(app)
	issueManagementDataController.NewIssueManagementDataRouter(app)

	host := fmt.Sprintf("%s:%s", os.Getenv("SERVER_URI"), os.Getenv("SERVER_PORT"))
	err := app.Listen(host)
	helper.PanicIfError(err)
}
