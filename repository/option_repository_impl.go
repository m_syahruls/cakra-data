package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type OptionRepositoryImpl struct {
	Collection *mongo.Collection
}

type OptionRepository interface {
	Create(option domain.Options)
	FindByQuery(query bson.D) (domain.Options, error)
	FindById(optionId string) (domain.Options, error)
	FindAll(page, limit, questionId string) []domain.Options
	Update(option domain.Options)
	Delete(optionId string) error

	DeleteAllOptionByQuestionId(questionId string) error
}

func NewOptionRepository(database *mongo.Database) OptionRepository {
	return &OptionRepositoryImpl{
		Collection: database.Collection("options"),
	}
}

func (repository *OptionRepositoryImpl) Create(option domain.Options) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":         option.ID,
		"question_id": option.QuestionId,
		"option_name": option.OptionName,
		"value":       option.Value,
		"color":       option.Color,
	})

	helper.PanicIfError(err)
}

func (repository *OptionRepositoryImpl) FindById(optionId string) (domain.Options, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var option domain.Options
	query := bson.M{"_id": optionId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&option)

	return option, result.Err()
}

func (repository *OptionRepositoryImpl) FindByQuery(query bson.D) (domain.Options, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var option domain.Options

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&option)

	return option, result.Err()
}

func (repository *OptionRepositoryImpl) FindAll(page, limit, questionId string) []domain.Options {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryOption(questionId)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var options []domain.Options

	for cursor.Next(ctx) {
		var option domain.Options
		cursor.Decode(&option)
		options = append(options, option)
	}

	return options
}

func (repository *OptionRepositoryImpl) Update(option domain.Options) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": option.ID}
	update := bson.M{
		"$set": bson.M{
			"option_name": option.OptionName,
			"value":       option.Value,
			"color":       option.Color,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *OptionRepositoryImpl) Delete(optionId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": optionId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *OptionRepositoryImpl) DeleteAllOptionByQuestionId(questionId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteMany(ctx, bson.M{
		"question_id": questionId,
	})

	helper.PanicIfError(err)

	return nil
}
