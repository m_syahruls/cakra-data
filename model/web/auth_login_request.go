package web

type LoginRequest struct {
	Email    string `json:"email"`
	Phone    string `json:"phone"`
	Password string `json:"password" validate:"required"`
}

type LoginResponse struct {
	OccupationLevel int    `json:"occupation_level"`
	OccupationName  string `json:"occupation_name"`
}

type ForgetPassowrd struct {
	Email string `json:"email"`
	Phone string `json:"phone"`
}

type ResetPassword struct {
	Password        string `json:"password"`
	PasswordConfirm string `json:"password_confirm"`
}
