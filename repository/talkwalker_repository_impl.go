package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type TalkwalkerRepositoryImpl struct {
	Collection      *mongo.Collection
	CollectionIssue *mongo.Collection
}

type TalkwalkerRepository interface {
	FindAll(page, limit, name string) []domain.Talkwalker
	Create(talkwalker domain.Talkwalker)
}

func NewTalkwalkerRepository(database *mongo.Database) TalkwalkerRepository {
	return &TalkwalkerRepositoryImpl{
		Collection: database.Collection("talkwalkers"),
	}
}

func (repository *TalkwalkerRepositoryImpl) FindAll(page, limit, name string) []domain.Talkwalker {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryTalkwalker(name)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var talkwalkers []domain.Talkwalker

	for cursor.Next(ctx) {
		var talkwalker domain.Talkwalker
		cursor.Decode(&talkwalker)
		talkwalkers = append(talkwalkers, talkwalker)
	}

	return talkwalkers
}

func (repository *TalkwalkerRepositoryImpl) Create(talkwalker domain.Talkwalker) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":        talkwalker.ID,
		"name":       talkwalker.Name,
		"file_url":   talkwalker.FileUrl,
		"created_at": talkwalker.CreatedAt,
	})

	helper.PanicIfError(err)
}
