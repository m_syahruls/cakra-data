package web

type CreateEventRequest struct {
	EventName     string `json:"event_name" validate:"required"`
	ImageUrl      string `json:"image_url"`
	Description   string `json:"description" validate:"required"`
	Link          string `json:"link"`
	DateStart     string `json:"date_start" validate:"required"`
	DateEnd       string `json:"date_end" validate:"required"`
	Location      string `json:"location" validate:"required"`
	ContactPerson string `json:"contact_person" validate:"required"`
	Status        *bool  `json:"status" validate:"required"`
}

type UpdateEventRequest struct {
	EventName     string `json:"event_name" validate:"required"`
	ImageUrl      string `json:"image_url"`
	Description   string `json:"description" validate:"required"`
	Link          string `json:"link"`
	DateStart     string `json:"date_start" bson:"date_start" validate:"required"`
	DateEnd       string `json:"date_end" bson:"date_end" validate:"required"`
	Location      string `json:"location" bson:"location"`
	ContactPerson string `json:"contact_person" bson:"contact_person"`
}

type UpdateEventStatusRequest struct {
	Status *bool `json:"status" validate:"required"`
}

type CreateEventParticipantRequest struct {
	EventId string `json:"event_id" validate:"required"`
	UserId  string `json:"user_id" validate:"required"`
}
