package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type RespondentRepositoryImpl struct {
	Collection         *mongo.Collection
	CollectionProgress *mongo.Collection
}

type RespondentRepository interface {
	Create(user domain.Respondent)
	FindById(userId string) (domain.Respondent, error)
	FindAll(page, limit, surveyorId string) []domain.UserProgressDetail
	FindByQuery(query bson.M) (domain.Respondent, error)
	Update(user domain.Respondent, userId string)
	Delete(userId string) error
}

func NewRespondentRepository(database *mongo.Database) RespondentRepository {
	return &RespondentRepositoryImpl{
		Collection:         database.Collection("respondent_user"),
		CollectionProgress: database.Collection("progress_surveys"),
	}
}

func (repository *RespondentRepositoryImpl) Create(user domain.Respondent) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":        user.Id,
		"name":       user.Name,
		"email":      user.Email,
		"phone":      user.Phone,
		"gender":     user.Gender,
		"created_at": user.CreatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *RespondentRepositoryImpl) FindById(userId string) (domain.Respondent, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var respondent domain.Respondent
	query := bson.M{"_id": userId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&respondent)

	return respondent, result.Err()
}

func (repository *RespondentRepositoryImpl) FindAll(page, limit, surveyorId string) []domain.UserProgressDetail {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	//options := helper.Pagging(page, limit, -1)

	//cursor, err := repository.Collection.Find(ctx, bson.M{}, options)
	//helper.PanicIfError(err)

	//var event []domain.Respondent

	//for cursor.Next(ctx) {
	//	var question domain.Respondent
	//	cursor.Decode(&question)
	//	event = append(event, question)
	//}

	pipeline := mongo.Pipeline{}

	helper.QueryProgressSurveyUserId(surveyorId, &pipeline)

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "user.occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user.occupation"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user.occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}

	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_survey_summary"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "progress_survey_id"}, {Key: "as", Value: "respondent_user"}}}}

	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "respondent_user"}, {Key: "localField", Value: "respondent_user.respondent_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "respondent_user"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$respondent_user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	lookupStage4 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_survey"}, {Key: "localField", Value: "respondent._id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "respondent_user.respondent"}}}}
	unwindStage4 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$respondent_user.respondent"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "respondent_user.survey_progress", Value: -1}}}}

	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "user", Value: bson.D{{Key: "$first", Value: "$user"}}},
		{Key: "survey_progress", Value: bson.D{{Key: "$first", Value: "$survey_progress"}}},
		{Key: "survey_goal", Value: bson.D{{Key: "$first", Value: "$survey_goal"}}},
		{Key: "respondent_user", Value: bson.D{{Key: "$push", Value: "$respondent_user"}}},
	}}}

	pipeline = append(pipeline, lookupStage, unwindStage, lookupStage1, unwindStage1, lookupStage2, lookupStage3, unwindStage3, lookupStage4, unwindStage4, sortStage, groupStage)

	//helper.QueryRecruit(searchName, level, &pipeline)

	helper.PaggingAggregat(page, limit, &pipeline)

	cursor, err := repository.CollectionProgress.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var progressDetails []domain.UserProgressDetail

	for cursor.Next(ctx) {
		var progressDetail domain.UserProgressDetail
		cursor.Decode(&progressDetail)
		progressDetails = append(progressDetails, progressDetail)
	}

	return progressDetails
}

func (repository *RespondentRepositoryImpl) FindByQuery(query bson.M) (domain.Respondent, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var user domain.Respondent
	find := query

	result := repository.Collection.FindOne(ctx, find)
	result.Decode(&user)

	return user, nil
}

func (repository *RespondentRepositoryImpl) Update(user domain.Respondent, userId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": userId}
	update := bson.M{
		"$set": bson.M{
			"name":   user.Name,
			"email":  user.Email,
			"phone":  user.Phone,
			"gender": user.Gender,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *RespondentRepositoryImpl) Delete(respondentId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": respondentId,
	})

	helper.PanicIfError(err)

	return nil
}
