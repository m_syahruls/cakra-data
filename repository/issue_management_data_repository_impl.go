package repository

import (
	"context"
	"log"
	"os"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type IssueManagementDataRepositoryImpl struct {
	Collection *mongo.Collection
}

type IssueManagementDataRepository interface {
	FindAll(page, limit, name string, issue string, sub_issue string, year string, created_at string) []domain.IssueManagementData
	Delete(issueType string, subIssue string, year string) error
}

func NewIssueManagementDataRepository(database *mongo.Database) IssueManagementDataRepository {
	return &IssueManagementDataRepositoryImpl{
		Collection: database.Collection("issues_management"),
	}
}

func (repository *IssueManagementDataRepositoryImpl) FindAll(page, limit, name string, issue string, sub_issue string, year string, created_at string) []domain.IssueManagementData {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryIssueManagementData(name, issue, sub_issue, year, created_at)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var issueSubs []domain.IssueManagementData

	for cursor.Next(ctx) {
		var issueSub domain.IssueManagementData
		cursor.Decode(&issueSub)
		issueSubs = append(issueSubs, issueSub)
	}

	return issueSubs
}

func (repository *IssueManagementDataRepositoryImpl) Delete(issueType string, subIssue string, year string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var managementData domain.IssueManagementData
	filter := bson.M{
		"$and": []bson.M{
			{"issue_type": issueType},
			{"sub_issue": subIssue},
			{"year": year},
		},
	}

	result := repository.Collection.FindOne(ctx, filter)
	result.Decode(&managementData)

	if managementData.FileUrl != "" {
		repository.RemoveFileExcel(managementData.FileUrl)
	}

	_, err := repository.Collection.DeleteOne(ctx, filter)

	helper.PanicIfError(err)

	return nil
}

func (service *IssueManagementDataRepositoryImpl) RemoveFileExcel(objectName string) {

	bucketName := os.Getenv("MINIO_BUCKET_DATA")
	objName := strings.Split(objectName, bucketName+"/")

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	err = minioClient.RemoveObject(context.Background(), bucketName, objName[1], minio.RemoveObjectOptions{})
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error remove"))
	}

	log.Printf("Successfully removed %s\n", objectName)
}
