package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type QuestionRepositoryImpl struct {
	Collection *mongo.Collection
}

type QuestionRepository interface {
	Create(question domain.Questions)
	FindById(questionId string) (domain.Questions, error)
	FindDetailById(questionId string) (domain.Questions, error)
	FindByQuery(query bson.D) (domain.Questions, error)
	FindAll(page, limit, surveyId string) []domain.Questions
	Update(question domain.Questions)
	Delete(questionId string) error

	FindCountBySurveyId(surveyId string) int
	DeleteAllQuestionBySurveyId(surveyId string) error
}

func NewQuestionRepository(database *mongo.Database) QuestionRepository {
	return &QuestionRepositoryImpl{
		Collection: database.Collection("questions"),
	}
}

func (repository *QuestionRepositoryImpl) Create(question domain.Questions) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":              question.ID,
		"survey_id":        question.SurveyId,
		"section":          question.Section,
		"input_type":       question.InputType,
		"question_number":  question.QuestionNumber,
		"question_name":    question.QuestionName,
		"question_subject": question.QuestionSubject,
	})

	helper.PanicIfError(err)
}

func (repository *QuestionRepositoryImpl) FindDetailById(questionId string) (domain.Questions, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: questionId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "question_id"}, {Key: "as", Value: "options"}}}}
	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage})
	helper.PanicIfError(err)

	var question domain.Questions
	result.Next(ctx)
	result.Decode(&question)

	return question, result.Err()
}

func (repository *QuestionRepositoryImpl) FindById(questionId string) (domain.Questions, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var question domain.Questions
	query := bson.M{"_id": questionId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&question)

	return question, result.Err()
}

func (repository *QuestionRepositoryImpl) FindByQuery(query bson.D) (domain.Questions, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var question domain.Questions

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&question)

	return question, result.Err()
}

func (repository *QuestionRepositoryImpl) FindAll(page, limit, surveyId string) []domain.Questions {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	options := helper.Pagging(page, limit, -1)

	query := helper.QueryQuestion(surveyId)

	cursor, err := repository.Collection.Find(ctx, query, options)
	helper.PanicIfError(err)

	var questions []domain.Questions

	for cursor.Next(ctx) {
		var question domain.Questions
		cursor.Decode(&question)
		questions = append(questions, question)
	}

	return questions
}

func (repository *QuestionRepositoryImpl) Update(question domain.Questions) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": question.ID}
	update := bson.M{
		"$set": bson.M{
			"section":          question.Section,
			"input_type":       question.InputType,
			"question_number":  question.QuestionNumber,
			"question_name":    question.QuestionName,
			"question_subject": question.QuestionSubject,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *QuestionRepositoryImpl) Delete(questionId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": questionId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *QuestionRepositoryImpl) FindCountBySurveyId(surveyId string) int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	result, _ := repository.Collection.CountDocuments(ctx, bson.M{"survey_id": surveyId})

	return int(result)
}

func (repository *QuestionRepositoryImpl) DeleteAllQuestionBySurveyId(surveyId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteMany(ctx, bson.M{
		"survey_id": surveyId,
	})

	helper.PanicIfError(err)

	return nil
}
