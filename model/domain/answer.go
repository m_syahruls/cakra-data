package domain

type Answer struct {
	ID            string    `json:"id" bson:"_id"`
	Name          string    `json:"name" bson:"name"`
	ResponseId    string    `json:"response_id" bson:"response_id"`
	QuestionId    string    `json:"question_id" bson:"question_id"`
	OptionsId     string    `json:"option_id" bson:"option_id"`
	AnswerText    string    `json:"answer_text" bson:"answer_text"`
	AnswerNumeric int32     `json:"answer_numeric" bson:"answer_numeric"`
	Question      Questions `json:"question" bson:"question"`
	Option        Options   `json:"option" bson:"option"`
	Regency       Regency   `json:"regency" bson:"regency"`
	District      Districts `json:"disctric" bson:"district"`
	Village       Village   `json:"village" bson:"village"`
}

type TotalInt struct {
	Total int `json:"total" bson:"total"`
}
