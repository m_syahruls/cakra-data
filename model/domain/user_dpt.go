package domain

type UserDpt struct {
	ID                 string         `json:"id" bson:"_id"`
	NKK                string         `json:"nkk" bson:"nkk"`
	NIK                string         `json:"nik" bson:"nik"`
	Name               string         `json:"name" bson:"name"`
	TempatLahir        string         `json:"tempat_lahir" bson:"tempat_lahir"`
	TglLahir           string         `json:"tgl_lahir" bson:"tgl_lahir"`
	StatusPerkawinan   string         `json:"status_perkawinan" bson:"status_perkawinan"`
	JenisKelamin       string         `json:"jenis_kelamin" bson:"jenis_kelamin"`
	JlnDukuh           string         `json:"jln_dukuh" bson:"jln_dukuh"`
	Rt                 string         `json:"rt" bson:"rt"`
	Rw                 string         `json:"rw" bson:"rw"`
	Disabilitas        string         `json:"disabilitas" bson:"disabilitas"`
	StatusPerekamanKtp string         `json:"status_perekaman_ktp_el" bson:"status_perekaman_ktp_el"`
	Keterangan         string         `json:"keterangan" bson:"keterangan"`
	PollingStationId   string         `json:"polling_station_id" bson:"polling_station_id"`
	VillageId          string         `json:"village_id" bson:"village_id"`
	CreatedAt          string         `json:"created_at" bson:"created_at"`
	UpdatedAt          string         `json:"updated_at" bson:"updated_at"`
	PollingStation     PollingStation `json:"polling_station" bson:"polling_station"`
	Village            Village        `json:"village" bson:"village"`
}

type PollingStation struct {
	ID   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
}
