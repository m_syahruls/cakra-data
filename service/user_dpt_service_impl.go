package service

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type UserDptServiceImpl struct {
	UserDptRepository repository.UserDptRepository
}

type UserDptService interface {
	CreateMany(userDpts []map[string]interface{})
	FindAll(page, limit, pollStationId, villageId, searchName string) []web.UserDptResponse
	FindById(userDptId string) web.UserDptResponse
}

func NewUserDptService(userDptRepository repository.UserDptRepository) UserDptService {
	return &UserDptServiceImpl{
		UserDptRepository: userDptRepository,
	}
}

func (service *UserDptServiceImpl) CreateMany(userDpts []map[string]interface{}) {
	var p []interface{}
	//fmt.Println(userDpts)
	for _, userDpt := range userDpts {
		var he domain.UserDpt
		he.ID = fmt.Sprintf("%0.f", userDpt["_id"])
		he.NKK = userDpt["nkk"].(string)
		he.NIK = userDpt["nik"].(string)
		he.Name = userDpt["name"].(string)
		he.TempatLahir = userDpt["tempat_lahir"].(string)
		he.TglLahir = userDpt["tgl_lahir"].(string)
		he.StatusPerkawinan = userDpt["status_perkawinan"].(string)
		he.JenisKelamin = userDpt["jenis_kelamin"].(string)
		he.JlnDukuh = userDpt["jln_dukuh"].(string)
		he.Rt = userDpt["rt"].(string)
		he.Rw = userDpt["rw"].(string)
		he.Disabilitas = userDpt["disabilitas"].(string)
		he.StatusPerekamanKtp = userDpt["status_perekaman_ktp_el"].(string)
		he.Keterangan = userDpt["keterangan"].(string)
		he.CreatedAt = userDpt["created_at"].(string)
		he.UpdatedAt = userDpt["updated_at"].(string)
		he.PollingStationId = fmt.Sprintf("%0.f", userDpt["polling_station_id"])
		he.VillageId = fmt.Sprintf("%0.f", userDpt["village_id"])
		p = append(p, he)
	}

	//fmt.Println(p)
	service.UserDptRepository.InsertMany(p)
}

func (service *UserDptServiceImpl) FindById(userDptId string) web.UserDptResponse {
	userDpt, err := service.UserDptRepository.FindById(userDptId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "userDpt not found"))
	}

	return helper.ToUserDptResponse(userDpt)
}

func (service *UserDptServiceImpl) FindAll(page, limit, pollStationId, villageId, searchName string) []web.UserDptResponse {
	userDpt := service.UserDptRepository.FindAll(page, limit, pollStationId, villageId, searchName)
	return helper.ToUserDptResponses(userDpt)
}
