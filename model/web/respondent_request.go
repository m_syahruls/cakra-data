package web

type CreateRespondent struct {
	Name   string `json:"name"`
	Email  string `json:"email"`
	Phone  string `json:"phone" validate:"required"`
	Gender string `json:"gender"`
}
