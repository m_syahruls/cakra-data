package service

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type VillageServiceImpl struct {
	VillageRepository repository.VillageRepository
}

type VillageService interface {
	FindAll(page, limit, provinceId, name, regencyId string) []web.VillageResponse
	FindById(villageId string) web.VillageResponse
}

func NewVillageService(villageRepository repository.VillageRepository) VillageService {
	return &VillageServiceImpl{
		VillageRepository: villageRepository,
	}
}

func (service *VillageServiceImpl) FindById(villageId string) web.VillageResponse {
	village, err := service.VillageRepository.FindById(villageId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "village not found"))
	}

	return helper.ToVillageResponse(village)
}

func (service *VillageServiceImpl) FindAll(page, limit, districtId, name, regencyId string) []web.VillageResponse {
	if regencyId != "" {
		village := service.VillageRepository.FindAllByRegencyId(page, limit, regencyId, name)
		return helper.ToVillageResponses(village)
	}
	village := service.VillageRepository.FindAll(page, limit, districtId, name)
	return helper.ToVillageResponses(village)
}
