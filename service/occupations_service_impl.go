package service

import (
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type OccupationServiceImpl struct {
	OccupationRepository repository.OccupationRepository
	Validate             *validator.Validate
}

type OccupationService interface {
	Create(request web.OccupationCreateRequest) web.OccupationResponse
	FindById(occupationId string) web.OccupationResponse
	FindAll() []web.OccupationResponse
	Update(occupationid string, request web.OccupationUpdateRequest) web.OccupationResponse
	Delete(occupationId string)
}

func NewOccupationService(occupationRepository repository.OccupationRepository, validate *validator.Validate) OccupationService {
	return &OccupationServiceImpl{
		OccupationRepository: occupationRepository,
		Validate:             validate,
	}
}

func (service *OccupationServiceImpl) Create(request web.OccupationCreateRequest) web.OccupationResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	occupation := domain.Occupation{
		ID:    strings.ToLower(randstr.String(10)),
		Level: request.Level,
		Name:  request.Name,
	}

	service.OccupationRepository.Create(occupation)

	return helper.ToOccupationResponse(occupation)
}

func (service *OccupationServiceImpl) FindById(occupationId string) web.OccupationResponse {
	occupation, err := service.OccupationRepository.FindById(occupationId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "occupation not found"))
	}

	return helper.ToOccupationResponse(occupation)
}

func (service *OccupationServiceImpl) FindAll() []web.OccupationResponse {
	occupations := service.OccupationRepository.FindAll()
	return helper.ToOccupationResponses(occupations)
}

func (service *OccupationServiceImpl) Update(occupationId string, request web.OccupationUpdateRequest) web.OccupationResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	occupation, err := service.OccupationRepository.FindById(occupationId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "occupation not found"))
	}

	occupation.Name = request.Name

	service.OccupationRepository.Update(occupation, occupationId)

	return helper.ToOccupationResponse(occupation)
}

func (service *OccupationServiceImpl) Delete(occupationId string) {
	occupation, err := service.OccupationRepository.FindById(occupationId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "occupation not found"))
	}

	err = service.OccupationRepository.Delete(occupationId)
	helper.PanicIfError(err)
	log.Println("LOG ", "deleted occupation", occupation.Name)
}
