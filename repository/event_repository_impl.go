package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type EventRepositoryImpl struct {
	Collection                 *mongo.Collection
	CollectionEventParticipant *mongo.Collection
}

type EventRepository interface {
	Create(event domain.Event)
	FindById(eventId string) (domain.Event, error)
	FindAll(page, limit, status string) []domain.Event
	Update(event domain.Event, eventId string)
	UpdateStatus(event domain.Event, eventId string)
	Delete(questionId string) error

	CreateParticipant(participate domain.EventParticipant)
	FindDetailById(eventId string) (domain.Event, error)
	IsParticipate(eventId, userId string) domain.EventParticipant

	FindSum() int
}

func NewEventRepository(database *mongo.Database) EventRepository {
	return &EventRepositoryImpl{
		Collection:                 database.Collection("events"),
		CollectionEventParticipant: database.Collection("event_participant"),
	}
}

func (repository *EventRepositoryImpl) Create(event domain.Event) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":            event.ID,
		"event_name":     event.EventName,
		"image_url":      event.ImageUrl,
		"link":           event.Link,
		"contact_person": event.ContactPerson,
		"date_start":     event.DateStart,
		"date_end":       event.DateEnd,
		"location":       event.Location,
		"description":    event.Description,
		"status":         event.Status,
		"created_at":     event.CreatedAt,
		"updated_at":     event.UpdatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *EventRepositoryImpl) FindById(eventId string) (domain.Event, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var event domain.Event
	query := bson.M{"_id": eventId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&event)

	return event, result.Err()
}

func (repository *EventRepositoryImpl) FindAll(page, limit, status string) []domain.Event {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	options := helper.Pagging(page, limit, -1)
	options.SetSort(bson.M{"created_at": -1})

	query := helper.QueryEvent(status)

	cursor, err := repository.Collection.Find(ctx, query, options)
	helper.PanicIfError(err)

	var event []domain.Event

	for cursor.Next(ctx) {
		var question domain.Event
		cursor.Decode(&question)
		event = append(event, question)
	}

	return event
}

func (repository *EventRepositoryImpl) Update(event domain.Event, eventId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": eventId}
	update := bson.M{
		"$set": bson.M{
			"event_name":     event.EventName,
			"image_url":      event.ImageUrl,
			"link":           event.Link,
			"contact_person": event.ContactPerson,
			"date_start":     event.DateStart,
			"date_end":       event.DateEnd,
			"location":       event.Location,
			"description":    event.Description,
			"updated_at":     event.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *EventRepositoryImpl) UpdateStatus(event domain.Event, eventId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": eventId}
	update := bson.M{
		"$set": bson.M{
			"status":     &event.Status,
			"updated_at": event.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *EventRepositoryImpl) Delete(questionId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": questionId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *EventRepositoryImpl) CreateParticipant(participate domain.EventParticipant) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionEventParticipant.InsertOne(ctx, bson.M{
		"_id":      participate.ID,
		"user_id":  participate.UserId,
		"event_id": participate.EventId,
	})

	helper.PanicIfError(err)
}

func (repository *EventRepositoryImpl) FindDetailById(eventId string) (domain.Event, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: eventId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "event_participant"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "event_id"}, {Key: "as", Value: "participant"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$participant"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "participant.user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "event_name", Value: bson.D{{Key: "$first", Value: "$event_name"}}},
		{Key: "image_url", Value: bson.D{{Key: "$first", Value: "$image_url"}}},
		{Key: "description", Value: bson.D{{Key: "$first", Value: "$description"}}},
		{Key: "link", Value: bson.D{{Key: "$first", Value: "$link"}}},
		{Key: "date_start", Value: bson.D{{Key: "$first", Value: "$date_start"}}},
		{Key: "date_end", Value: bson.D{{Key: "$first", Value: "$date_end"}}},
		{Key: "location", Value: bson.D{{Key: "$first", Value: "$location"}}},
		{Key: "contact_person", Value: bson.D{{Key: "$first", Value: "$contact_person"}}},
		{Key: "status", Value: bson.D{{Key: "$first", Value: "$status"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "updated_at", Value: bson.D{{Key: "$first", Value: "$updated_at"}}},
		{Key: "user", Value: bson.D{{Key: "$push", Value: "$user"}}},
	}}}

	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage2, unwindStage2, groupStage})
	helper.PanicIfError(err)

	var event domain.Event

	result.Next(ctx)
	result.Decode(&event)

	return event, result.Err()
}

func (repository *EventRepositoryImpl) IsParticipate(eventId, userId string) domain.EventParticipant {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var event domain.EventParticipant
	query := bson.M{"event_id": eventId, "user_id": userId}

	result := repository.CollectionEventParticipant.FindOne(ctx, query)
	result.Decode(&event)

	return event
}

func (repository *EventRepositoryImpl) FindSum() int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor, err := repository.Collection.Find(ctx, bson.M{})
	helper.PanicIfError(err)

	var event int

	for cursor.Next(ctx) {
		event = event + 1
	}

	return event
}
