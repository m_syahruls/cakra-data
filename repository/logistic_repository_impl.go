package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type LogisticRepositoryImpl struct {
	Collection                 *mongo.Collection
	CollectionLogisticCategory *mongo.Collection
}

type LogisticRepository interface {
	CreateLogistic(logistic domain.Logistic)
	FindAllLogistic(userid, categoryId, page, limit string) []domain.Logistic
	FindDetailLogisticById(logisticid string) domain.Logistic
	FindLogisticById(logisticid string) domain.Logistic
	UpdateLogistic(logistic domain.Logistic)
	DeleteLogistic(logisticId string) error

	CreateLogisticCategory(logistic domain.LogisticCategory)
	FindAllLogisticCategory(page, limit string) []domain.LogisticCategory
	FindLogisticCategoryById(logisticCategoryId string) (domain.LogisticCategory, error)
	UpdateCategoryLogistic(categoryId string, category domain.LogisticCategory) error
	DeleteCategoryLogistic(categoryId string) error

	FindSumLogistic(userid, categoryId string) int
}

func NewLogisticRepository(database *mongo.Database) LogisticRepository {
	return &LogisticRepositoryImpl{
		Collection:                 database.Collection("logistics"),
		CollectionLogisticCategory: database.Collection("logistic_categories"),
	}
}

func (repository *LogisticRepositoryImpl) CreateLogistic(logistic domain.Logistic) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":            logistic.Id,
		"name":           logistic.Nama,
		"location":       logistic.Location,
		"image_logistic": logistic.ImageLogistic,
		"longitude":      logistic.Longitude,
		"latitude":       logistic.Latitude,
		"category_id":    logistic.CategoryId,
		"user_id":        logistic.UserId,
		"created_at":     logistic.CreatedAt,
		"updated_at":     logistic.UpdatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *LogisticRepositoryImpl) FindAllLogistic(userid, categoryId, page, limit string) []domain.Logistic {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "logistic_categories"}, {Key: "localField", Value: "category_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "category"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$category"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "created_at", Value: -1}}}}
	pipeline := mongo.Pipeline{lookupStage, unwindStage, lookupStage1, unwindStage1, sortStage}

	helper.QueryLogistic(userid, categoryId, &pipeline)
	helper.PaggingAggregat(page, limit, &pipeline)

	cursor, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var systemLogistics []domain.Logistic

	for cursor.Next(ctx) {
		var systemLogistic domain.Logistic
		cursor.Decode(&systemLogistic)
		systemLogistics = append(systemLogistics, systemLogistic)
	}

	return systemLogistics
}

func (repository *LogisticRepositoryImpl) FindDetailLogisticById(logisticid string) domain.Logistic {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: logisticid}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "logistic_categories"}, {Key: "localField", Value: "category_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "category"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$category"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage1, unwindStage1})
	helper.PanicIfError(err)

	result.Next(ctx)
	var systemLogistic domain.Logistic
	result.Decode(&systemLogistic)

	return systemLogistic
}

func (repository *LogisticRepositoryImpl) FindLogisticById(logisticid string) domain.Logistic {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor, err := repository.Collection.Find(ctx, bson.M{"_id": logisticid})
	helper.PanicIfError(err)

	cursor.Next(ctx)
	var systemLogistic domain.Logistic
	cursor.Decode(&systemLogistic)

	return systemLogistic
}

func (repository *LogisticRepositoryImpl) UpdateLogistic(logistic domain.Logistic) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": logistic.Id}
	update := bson.M{
		"$set": bson.M{
			"category_id": logistic.CategoryId,
			"name":        logistic.Nama,
			"updated_at":  logistic.UpdatedAt,
		},
	}
	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *LogisticRepositoryImpl) DeleteLogistic(logisticId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": logisticId,
	})

	helper.PanicIfError(err)

	return nil
}

// Logistic Category
func (repository *LogisticRepositoryImpl) CreateLogisticCategory(logistic domain.LogisticCategory) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionLogisticCategory.InsertOne(ctx, bson.M{
		"_id":  logistic.Id,
		"name": logistic.Name,
	})

	helper.PanicIfError(err)
}

func (repository *LogisticRepositoryImpl) FindAllLogisticCategory(page, limit string) []domain.LogisticCategory {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	options := helper.Pagging(page, limit, -1)

	cursor, err := repository.CollectionLogisticCategory.Find(ctx, bson.M{}, options)
	helper.PanicIfError(err)

	var logisticCategory []domain.LogisticCategory

	for cursor.Next(ctx) {
		var logistic domain.LogisticCategory
		cursor.Decode(&logistic)
		logisticCategory = append(logisticCategory, logistic)
	}

	return logisticCategory
}

func (repository *LogisticRepositoryImpl) FindLogisticCategoryById(logisticCategoryId string) (domain.LogisticCategory, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor := repository.CollectionLogisticCategory.FindOne(ctx, bson.M{"_id": logisticCategoryId})

	var logistic domain.LogisticCategory
	cursor.Decode(&logistic)

	return logistic, cursor.Err()
}

func (repository *LogisticRepositoryImpl) UpdateCategoryLogistic(categoryId string, category domain.LogisticCategory) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": category.Id}
	update := bson.M{
		"$set": bson.M{
			"name": category.Name,
		},
	}
	_, err := repository.CollectionLogisticCategory.UpdateOne(ctx, query, update)
	helper.PanicIfError(err)

	return nil
}

func (repository *LogisticRepositoryImpl) DeleteCategoryLogistic(categoryId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionLogisticCategory.DeleteOne(ctx, bson.M{
		"_id": categoryId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *LogisticRepositoryImpl) FindSumLogistic(userid, categoryId string) int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "logistic_categories"}, {Key: "localField", Value: "category_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "category"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$category"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "created_at", Value: -1}}}}
	pipeline := mongo.Pipeline{lookupStage, unwindStage, lookupStage1, unwindStage1, sortStage}

	helper.QueryLogistic(userid, categoryId, &pipeline)

	cursor, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var sum int

	for cursor.Next(ctx) {
		sum = sum + 1
	}

	return sum
}
