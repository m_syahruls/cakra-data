package service

import (
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type RecruitServiceImpl struct {
	RecruitRepository       repository.RecruitRepository
	RecruitedUserRepository repository.RecruitedUserRepository
	OccupationRepository    repository.OccupationRepository
	Validate                *validator.Validate
}

type RecruitService interface {
	Create(ctx *fiber.Ctx, request web.UserCreateRecruit) web.UserRecruitResponse
	FindDetailByUserId(ctx *fiber.Ctx, userId string) web.RecruitDetailUserResponse
	FindAll(ctx *fiber.Ctx, page, limit, userId, searchName, level string) []web.UserRecruitDetail

	IsReferral(userId, recuiterId string) bool
	UpdateGoal(ctx *fiber.Ctx, request web.UserCreateRecruit) web.UserRecruitResponse
}

func NewRecruitService(recruitRepository repository.RecruitRepository, recruitedUserRepository repository.RecruitedUserRepository, occupationRepository repository.OccupationRepository, validate *validator.Validate) RecruitService {
	return &RecruitServiceImpl{
		RecruitRepository:       recruitRepository,
		RecruitedUserRepository: recruitedUserRepository,
		OccupationRepository:    occupationRepository,
		Validate:                validate,
	}
}

func (service *RecruitServiceImpl) Create(ctx *fiber.Ctx, request web.UserCreateRecruit) web.UserRecruitResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.RecruitRepository.FindUserByUserId(request.UserId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}
	occupation, _ := service.OccupationRepository.FindById(user.OccupationId)

	checkAuth(ctx, occupation)

	actorid, level, _ := helper.ParseJwt(ctx.Cookies("token"))

	userreq, err := service.RecruitedUserRepository.IsReferral(request.UserId, actorid)

	if (err != nil || userreq.ID == "") && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "this user is not your referral"))
	}

	progres, _ := service.RecruitRepository.FindByUserId(request.UserId)

	if progres.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "user already have progres"))
	}

	recruit := domain.UserRecruit{
		ID:              strings.ToLower(randstr.String(10)),
		UserId:          request.UserId,
		RecruitProgress: 0,
		RecruitGoal:     request.RecruitGoal,
	}

	service.RecruitRepository.Create(recruit)

	return helper.ToRecruitResponse(recruit)
}

func (service *RecruitServiceImpl) FindDetailByUserId(ctx *fiber.Ctx, userId string) web.RecruitDetailUserResponse {
	user, err := service.RecruitRepository.FindUserByUserId(userId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}
	occupation, _ := service.OccupationRepository.FindById(user.OccupationId)

	checkAuth(ctx, occupation)

	recruit, err := service.RecruitRepository.FindDetailByUserId(userId)

	if err != nil || recruit.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "recruit not found"))
	}

	return helper.ToRecruitDetailResponse(recruit)
}

func (service *RecruitServiceImpl) FindAll(ctx *fiber.Ctx, page, limit, userId, searchName, level string) []web.UserRecruitDetail {

	_, levelactor, _ := helper.ParseJwt(ctx.Cookies("token"))
	if level == "" && levelactor != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "you cant get all, use query level"))
	} else if level == "2" && (levelactor != "1" && levelactor != "2") {
		panic(exception.NewError(fiber.StatusUnauthorized, "only coor and admin can see coor"))
	}

	recruits := service.RecruitRepository.FindAll(page, limit, userId, searchName, level)
	return helper.ToAllRecruitResponses(recruits)
}

func (service *RecruitServiceImpl) IsReferral(userId, recuiterId string) bool {
	recruit, err := service.RecruitedUserRepository.IsReferral(userId, recuiterId)

	if err != nil || recruit.ID == "" {
		return false
	}

	return true
}

func (service *RecruitServiceImpl) UpdateGoal(ctx *fiber.Ctx, request web.UserCreateRecruit) web.UserRecruitResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.RecruitRepository.FindUserByUserId(request.UserId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}
	occupation, _ := service.OccupationRepository.FindById(user.OccupationId)

	checkAuth(ctx, occupation)

	actorid, level, _ := helper.ParseJwt(ctx.Cookies("token"))

	userreq, err := service.RecruitedUserRepository.IsReferral(request.UserId, actorid)

	if (err != nil || userreq.ID == "") && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "this user is not your referral"))
	}

	progres, _ := service.RecruitRepository.FindByUserId(request.UserId)

	if progres.ID == "" {
		panic(exception.NewError(fiber.StatusBadRequest, "user dont have recruitment goal, created it first"))
	}

	progres.RecruitGoal = request.RecruitGoal

	service.RecruitRepository.UpdateGoal(progres)

	return helper.ToRecruitResponse(progres)
}
