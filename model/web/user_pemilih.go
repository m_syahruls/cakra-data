package web

type UserPemilih struct {
	ReligionId  string `json:"religion_id" validate:"required"`
	Phone       string `json:"phone" validate:"required"`
	KTP         string `json:"image_ktp"`
	Selfie      string `json:"foto_selfie"`
	CandidateId string `json:"candidate_id" validate:"required"`
	DptId       string `json:"dpt_id" validate:"required"`
	Longitude   string `json:"longitude"`
	Latitude    string `json:"latitude"`
}
