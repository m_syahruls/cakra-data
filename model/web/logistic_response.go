package web

import "time"

// Response
type LogisticResponse struct {
	Id            string           `json:"id" bson:"_id"`
	CategoryId    string           `json:"category_id" bson:"category_id"`
	Nama          string           `json:"name" bson:"name"`
	Location      string           `json:"location" bson:"location"`
	ImageLogistic string           `json:"image_logistic" bson:"image_logistic"`
	Longitude     string           `json:"longitude" bson:"longitude"`
	Latitude      string           `json:"latitude" bson:"latitude"`
	UserId        string           `json:"user_id" bson:"user_id"`
	CreatedAt     time.Time        `json:"created_at" bson:"created_at"`
	UpdatedAt     time.Time        `json:"updated_at" bson:"updated_at"`
	Category      LogisticCategory `json:"category" bson:"category"`
	User          LogisticUser     `json:"user" bson:"user"`
}

type LogisticCategory struct {
	Id   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
}

type LogisticUser struct {
	Id   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
}

type ProgressLogistic struct {
	RecruitProgress int `json:"progess"`
	RecruitGoal     int `json:"goal"`
}
