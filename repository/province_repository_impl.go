package repository

import (
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ProvinceRepositoryImpl struct {
	Collection *mongo.Collection
}

type ProvinceRepository interface {
	FindById(provinceId string) (domain.Province, error)
	FindByName(provinceName string) (domain.Province, error)
	FindAll(page, limit, name string) []domain.Province
}

func NewProvinceRepository(database *mongo.Database) ProvinceRepository {
	return &ProvinceRepositoryImpl{
		Collection: database.Collection("provinces"),
	}
}

func (repository *ProvinceRepositoryImpl) FindById(provinceId string) (domain.Province, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()
	provinceIdInt, _ := strconv.Atoi(provinceId)
	var province domain.Province
	query := bson.M{"_id": provinceIdInt}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&province)

	return province, result.Err()
}

func (repository *ProvinceRepositoryImpl) FindByName(provinceName string) (domain.Province, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var province domain.Province
	query := bson.M{"name": provinceName}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&province)

	return province, result.Err()
}

func (repository *ProvinceRepositoryImpl) FindAll(page, limit, name string) []domain.Province {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryProvince(name)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var provinces []domain.Province

	for cursor.Next(ctx) {
		var province domain.Province
		cursor.Decode(&province)
		provinces = append(provinces, province)
	}

	return provinces
}
