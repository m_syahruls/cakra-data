package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type UserPemilihControllerImpl struct {
	UserPemilihService service.UserPemilihService
}

type UserPemilihController interface {
	NewUserPemilihRouter(app *fiber.App)
}

func NewUserPemilihController(userPemilihService service.UserPemilihService) UserPemilihController {
	return &UserPemilihControllerImpl{
		UserPemilihService: userPemilihService,
	}
}

func (controller *UserPemilihControllerImpl) NewUserPemilihRouter(app *fiber.App) {
	userPemilih := app.Group("/user-pemilih")
	userPemilih.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	userPemilih.Post("/", controller.CreateUserPemilih)
}

func (controller *UserPemilihControllerImpl) CreateUserPemilih(ctx *fiber.Ctx) error {
	var request web.UserPemilih

	request.DptId = ctx.FormValue("dptid")
	request.Phone = ctx.FormValue("phone")
	request.Longitude = ctx.FormValue("longitude")
	request.Latitude = ctx.FormValue("latitude")
	request.ReligionId = ctx.FormValue("religionid")
	request.CandidateId = ctx.FormValue("candidateid")

	userPemilih := controller.UserPemilihService.Create(ctx, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userPemilih,
	})
}
