package controller

import (
	"fmt"
	"time"

	"github.com/go-co-op/gocron"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type SchedulerImpl struct {
	SocialMediaService service.AyrshareService
	UserService        service.UserService
	Gocron             *gocron.Scheduler
}

type Scheduler interface {
	StartScheduler()
	fetchSocmedAnalytics()
}

func NewScheduler(socmedservice service.AyrshareService, userService service.UserService) Scheduler {
	return &SchedulerImpl{
		SocialMediaService: socmedservice,
		UserService:        userService,
		Gocron:             gocron.NewScheduler(time.UTC),
	}
}

func (scheduler *SchedulerImpl) StartScheduler() {
	// scheduler.Gocron.Every(30).Seconds().Do(func() {
	// 	scheduler.fetchSocmedAnalytics()
	// })
	scheduler.Gocron.Every(1).Day().At("17:00").Do(func() {
		scheduler.fetchSocmedAnalytics()
	})
	scheduler.Gocron.StartAsync()
}

func (scheduler *SchedulerImpl) fetchSocmedAnalytics() {
	// get users that already have ayrshare token registered
	users := scheduler.UserService.FindWithAyrshareToken()

	for _, user := range users {
		check := scheduler.SocialMediaService.CheckEligibility(user.AyrshareToken)
		if !check.Eligibility {
			continue
		}

		fmt.Println("token:", user.AyrshareToken)
		socialAccounts := scheduler.SocialMediaService.GetUserDetails(user.AyrshareToken).ActiveSocialAccounts

		analytics := scheduler.SocialMediaService.GetUserAnalytics(
			user.AyrshareToken,
			web.AyrshareUserAnalyticsRequest{
				Platforms: socialAccounts,
			},
		)

		for _, platform := range socialAccounts {
			var id string
			if platform == "twitter" {
				id = analytics.Twitter.Analytics.Username
			} else if platform == "facebook" {
				id = analytics.Facebook.Analytics.ID
			} else if platform == "instagram" {
				id = analytics.Instagram.Analytics.ID
			} else {
				continue
			}

			lastTotal := scheduler.SocialMediaService.FindTotalBySocialmediaId(id)
			if lastTotal.CreatedAt.IsZero() {
				fmt.Println("last data is zero", lastTotal)
				scheduler.SocialMediaService.CreateNewTotal(id, analytics, platform)
				continue
			}

			result := helper.AnalyticWebToHistoricalDomain(id, lastTotal, analytics, platform, false)

			scheduler.SocialMediaService.CreateHistory(user.AyrshareToken, result)

			total := helper.AnalyticWebToTotalDomain(id, analytics, platform, time.Now(), time.Now())
			scheduler.SocialMediaService.UpdateTotalBySocialmediaId(id, total)
			fmt.Println("result:", result)
		}
	}
}
