package domain

type UserRecruit struct {
	ID              string `json:"id" bson:"_id"`
	UserId          string `json:"user_id" bson:"user_id"`
	RecruitProgress int    `json:"recruit_progess" bson:"recruit_progress"`
	RecruitGoal     int    `json:"recruit_goal" bson:"recruit_goal"`
}

type RecruitedUser struct {
	ID            string `json:"id" bson:"_id"`
	UserId        string `json:"user_id" bson:"user_id"`
	UserRecruitId string `json:"user_recruit_id" bson:"user_recruit_id"`
	RecruiterId   string `json:"recruiter_id" bson:"recruiter_id"`
}

type RecruitedDetail struct {
	ID              string `json:"id" bson:"_id"`
	UserId          string `json:"user_id" bson:"user_id"`
	RecruitProgress int    `json:"recruit_progess" bson:"recruit_progress"`
	RecruitGoal     int    `json:"recruit_goal" bson:"recruit_goal"`
	User            User   `json:"user" bson:"user"`
	RecruitedUser   []User `json:"recruited_user" bson:"recruited_user"`
}

type UserRecruitDetail struct {
	ID              string `json:"id" bson:"_id"`
	RecruitProgress int    `json:"recruit_progess" bson:"recruit_progress"`
	RecruitGoal     int    `json:"recruit_goal" bson:"recruit_goal"`
	User            User   `json:"user" bson:"user"`
	RecruitedUser   []User `json:"recruited_user" bson:"recruited_user"`
}
