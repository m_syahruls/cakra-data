package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ProgresSurveyRepositoryImpl struct {
	Collection     *mongo.Collection
	CollectionUser *mongo.Collection
}

type ProgresSurveyRepository interface {
	Create(progresSurvey domain.ProgressSurvey)
	FindByUserId(userId string) (domain.ProgressSurvey, error)
	FindDetailByUserId(userId string) (domain.ProgressSurveyDetail, error)
	FindAll(page, limit, searchName string) []domain.ProgressSurveyDetail
	Update(progresSurvey domain.ProgressSurvey)
	FindUserByUserId(userId string) (domain.User, error)
	Delete(userId string) error

	UpdateGoal(progresSurvey domain.ProgressSurvey)
}

func NewProgresSurveyRepository(database *mongo.Database) ProgresSurveyRepository {
	return &ProgresSurveyRepositoryImpl{
		Collection:     database.Collection("progress_surveys"),
		CollectionUser: database.Collection("users"),
	}
}

func (repository *ProgresSurveyRepositoryImpl) Create(progresSurvey domain.ProgressSurvey) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":             progresSurvey.ID,
		"user_id":         progresSurvey.UserId,
		"survey_progress": progresSurvey.SurveyProgress,
		"survey_goal":     progresSurvey.SurveyGoal,
	})

	helper.PanicIfError(err)
}

func (repository *ProgresSurveyRepositoryImpl) FindByUserId(userId string) (domain.ProgressSurvey, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var progresSurvey domain.ProgressSurvey
	query := bson.M{"user_id": userId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&progresSurvey)

	return progresSurvey, result.Err()
}

func (repository *ProgresSurveyRepositoryImpl) FindDetailByUserId(userId string) (domain.ProgressSurveyDetail, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "user_id", Value: userId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_survey_summary"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "progress_survey_id"}, {Key: "as", Value: "progress_summary"}}}}
	//unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$progress_summary"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "respondent_user"}, {Key: "localField", Value: "progress_summary.respondent_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "respondent_user"}}}}

	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage1, lookupStage2})
	helper.PanicIfError(err)

	var progresSurvey domain.ProgressSurveyDetail

	result.Next(ctx)
	result.Decode(&progresSurvey)

	return progresSurvey, result.Err()
}

func (repository *ProgresSurveyRepositoryImpl) FindAll(page, limit, searchName string) []domain.ProgressSurveyDetail {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	pipeline := mongo.Pipeline{}

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_survey_summary"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "progress_survey_id"}, {Key: "as", Value: "progress_summary"}}}}
	//unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$progress_summary"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "respondent_user"}, {Key: "localField", Value: "progress_summary.respondent_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "respondent_user"}}}}

	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "progresSurveyed_user.progresSurveyment.progresSurvey_progress", Value: -1}}}}

	pipeline = append(pipeline, lookupStage, unwindStage, lookupStage1, lookupStage2, sortStage)

	helper.QueryProgresSurvey(searchName, &pipeline)

	helper.PaggingAggregat(page, limit, &pipeline)

	cursor, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var progresSurveys []domain.ProgressSurveyDetail

	for cursor.Next(ctx) {
		var progresSurvey domain.ProgressSurveyDetail
		cursor.Decode(&progresSurvey)
		progresSurveys = append(progresSurveys, progresSurvey)
	}

	return progresSurveys
}

func (repository *ProgresSurveyRepositoryImpl) Update(progresSurvey domain.ProgressSurvey) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": progresSurvey.ID}
	update := bson.M{
		"$set": bson.M{
			"survey_progress": progresSurvey.SurveyProgress,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProgresSurveyRepositoryImpl) FindUserByUserId(userId string) (domain.User, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var survey domain.User
	query := bson.M{"_id": userId}

	result := repository.CollectionUser.FindOne(ctx, query)
	result.Decode(&survey)

	return survey, result.Err()
}

func (repository *ProgresSurveyRepositoryImpl) Delete(userId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"user_id": userId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *ProgresSurveyRepositoryImpl) UpdateGoal(progresSurvey domain.ProgressSurvey) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": progresSurvey.ID}
	update := bson.M{
		"$set": bson.M{
			"survey_goal": progresSurvey.SurveyGoal,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}
