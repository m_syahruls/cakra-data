package domain

type Questions struct {
	ID              string    `json:"id" bson:"_id"`
	SurveyId        string    `json:"survey_id" bson:"survey_id"`
	Section         string    `json:"section" bson:"section"`
	InputType       string    `json:"input_type" bson:"input_type"`
	QuestionNumber  int       `json:"question_number" bson:"question_number"`
	QuestionName    string    `json:"question_name" bson:"question_name"`
	QuestionSubject string    `json:"question_subject" bson:"question_subject"`
	Options         []Options `json:"options" bson:"options"`
}
