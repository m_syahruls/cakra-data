package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type RecruitControllerImpl struct {
	RecruitService service.RecruitService
	LogService     service.LogService
}

type RecruitController interface {
	NewRecruitRouter(app *fiber.App)
}

func NewRecruitController(recruitService service.RecruitService, logService service.LogService) RecruitController {
	return &RecruitControllerImpl{
		RecruitService: recruitService,
		LogService:     logService,
	}
}

func (controller *RecruitControllerImpl) NewRecruitRouter(app *fiber.App) {
	recruit := app.Group("")
	recruit.Get("/recruit/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	// JUST FOR User under recruit
	recruit.Use(middlewares.IsAuthenticated)
	recruit.Post("/recruits", controller.Create)
	recruit.Get("/users/recruitment/:userid", controller.FindDetailByUserId)
	recruit.Get("/recruits", controller.FindAll)
	recruit.Put("/recruits/goal", controller.UpdateGoal)
	//recruit.Put("/:recruitid", controller.Update)
	//recruit.Delete("/:recruitid", controller.Delete)
}

func (controller *RecruitControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.UserCreateRecruit
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	recruitResponse := controller.RecruitService.Create(ctx, request)

	action := fmt.Sprintf("create recruit for %s", request.UserId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    recruitResponse,
	})
}

func (controller *RecruitControllerImpl) FindDetailByUserId(ctx *fiber.Ctx) error {
	recruitId := ctx.Params("userid")

	recruitResponse := controller.RecruitService.FindDetailByUserId(ctx, recruitId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    recruitResponse,
	})
}

func (controller *RecruitControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	userId := ctx.Query("userid")
	searchName := ctx.Query("search")
	level := ctx.Query("level")
	recruitResponse := controller.RecruitService.FindAll(ctx, page, limit, userId, searchName, level)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    recruitResponse,
	})
}

func (controller *RecruitControllerImpl) UpdateGoal(ctx *fiber.Ctx) error {
	var request web.UserCreateRecruit
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	recruitResponse := controller.RecruitService.UpdateGoal(ctx, request)

	action := fmt.Sprintf("update recruit for %s", request.UserId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    recruitResponse,
	})
}
