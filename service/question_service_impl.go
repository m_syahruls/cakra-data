package service

import (
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/bson"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type QuestionServiceImpl struct {
	QuestionRepository repository.QuestionRepository
	SurveyRepository   repository.SurveyRepository
	ResponseRepository repository.ResponseRepository
	Validate           validator.Validate
}

type QuestionService interface {
	Create(request web.QuestionCreateRequest) web.QuestionResponse
	FindDetailById(questionId string) web.QuestionResponse
	FindAll(page, limit, surveyId string) []web.QuestionResponse
	Update(questionId string, request web.QuestionUpdateRequest) web.QuestionResponse
	Delete(questionId string)
}

func NewQuestionService(questionRepository repository.QuestionRepository, surveyRepository repository.SurveyRepository, responseRepository repository.ResponseRepository, validate *validator.Validate) QuestionService {
	return &QuestionServiceImpl{
		QuestionRepository: questionRepository,
		SurveyRepository:   surveyRepository,
		ResponseRepository: responseRepository,
		Validate:           *validate,
	}
}

func (service *QuestionServiceImpl) Create(request web.QuestionCreateRequest) web.QuestionResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, err = service.SurveyRepository.FindById(request.SurveyId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "survey not found"))
	}

	// check if also create option
	numberQuestionExist, _ := service.QuestionRepository.FindByQuery(bson.D{{Key: "question_number", Value: request.QuestionNumber}, {Key: "survey_id", Value: request.SurveyId}})
	if numberQuestionExist.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "question number already exist"))
	}

	question := domain.Questions{
		ID:              strings.ToLower(randstr.String(10)),
		SurveyId:        request.SurveyId,
		Section:         request.Section,
		InputType:       request.InputType,
		QuestionNumber:  request.QuestionNumber,
		QuestionName:    request.QuestionName,
		QuestionSubject: request.QuestionSubject,
	}

	service.QuestionRepository.Create(question)

	return helper.ToQuestionResponse(question)
}

func (service *QuestionServiceImpl) FindDetailById(questionId string) web.QuestionResponse {
	question, err := service.QuestionRepository.FindDetailById(questionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "question not found"))
	}

	return helper.ToQuestionResponse(question)
}

func (service *QuestionServiceImpl) FindAll(page, limit, surveyId string) []web.QuestionResponse {
	questions := service.QuestionRepository.FindAll(page, limit, surveyId)
	return helper.ToQuestionResponses(questions)
}

func (service *QuestionServiceImpl) Update(questionId string, request web.QuestionUpdateRequest) web.QuestionResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	question, err := service.QuestionRepository.FindById(questionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "question not found"))
	}

	// check if survey already response by user
	responseExist, _ := service.ResponseRepository.FindByQuery(bson.D{{Key: "survey_id", Value: question.SurveyId}})
	if responseExist.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "survey already response"))
	}

	questionExist, _ := service.QuestionRepository.FindByQuery(bson.D{{Key: "question_number", Value: request.QuestionNumber}, {Key: "survey_id", Value: question.SurveyId}})
	if questionExist.ID != "" && questionExist.ID != questionId {
		panic(exception.NewError(fiber.StatusBadRequest, "this number already exist in question"))
	}

	question.Section = request.Section
	question.InputType = request.InputType
	question.QuestionNumber = request.QuestionNumber
	question.QuestionName = request.QuestionName
	question.QuestionSubject = request.QuestionSubject

	service.QuestionRepository.Update(question)

	return helper.ToQuestionResponse(question)
}

func (service *QuestionServiceImpl) Delete(questionId string) {
	question, err := service.QuestionRepository.FindById(questionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "question not found"))
	}

	err = service.QuestionRepository.Delete(questionId)
	helper.PanicIfError(err)
	log.Println("LOG", "deleted survey", question.QuestionName)
}
