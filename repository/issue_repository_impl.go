package repository

import (
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type IssueRepositoryImpl struct {
	Collection           *mongo.Collection
	CollectionData       *mongo.Collection
	CollectionSub        *mongo.Collection
	CollectionLog        *mongo.Collection
	CollectionManagement *mongo.Collection
}

type IssueRepository interface {
	FindById(issueId string) (domain.Issue, error)
	FindAll(page, limit, label string, value string) []domain.Issue
	FindYearSub(issueId string, year string) (domain.Issue, error)
	FindYearById(issueId string) (domain.Issue, error)
	FindAllYear(issueId string) []domain.Issue
	FindAllPintPoints(id string, year string, issue string, province string) []domain.IssueResponsePoints

	FindIsExist(label string, value string) bool

	Create(issue domain.Issue)
	Update(issue domain.Issue, issueId string)
	Delete(issueId string) error

	InsertMany(issueLogs []interface{})

	CreateManagementData(issue domain.IssueManagementData)
}

func NewIssueRepository(database *mongo.Database) IssueRepository {
	return &IssueRepositoryImpl{
		Collection:           database.Collection("issues"),
		CollectionData:       database.Collection("issues_data"),
		CollectionSub:        database.Collection("issues_sub"),
		CollectionLog:        database.Collection("issues_logs"),
		CollectionManagement: database.Collection("issues_management"),
	}
}

func (repository *IssueRepositoryImpl) FindAllPintPoints(id string, year string, issues string, province string) []domain.IssueResponsePoints {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	subIssueIDs := strings.Split(id, ",")

	query := bson.M{
		"year": year,
		"data.type_issue": bson.M{
			"$regex":   issues,
			"$options": "i", // "i" option makes the regular expression case-insensitive
		},
		"sub_issue_id": bson.M{
			"$in": subIssueIDs,
		},
	}

	if province != "" {
		provinceId, _ := strconv.Atoi(province)
		query["province_id"] = provinceId
	}

	cursor, err := repository.CollectionLog.Find(ctx, query)
	helper.PanicIfError(err)
	var dataIssueLog []domain.IssueResponsePoints
	for cursor.Next(ctx) {
		var IssueDataLog domain.IssueResponsePoints
		cursor.Decode(&IssueDataLog)
		dataIssueLog = append(dataIssueLog, IssueDataLog)
	}

	return dataIssueLog
}

func (repository *IssueRepositoryImpl) FindById(issueId string) (domain.Issue, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var issue domain.Issue
	query := bson.M{"_id": issueId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&issue)

	return issue, result.Err()
}

func (repository *IssueRepositoryImpl) FindAll(page, limit, label string, value string) []domain.Issue {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryIssue("", label, value)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var issues []domain.Issue

	for cursor.Next(ctx) {
		var issue domain.Issue
		cursor.Decode(&issue)
		issues = append(issues, issue)
	}

	return issues
}

func (repository *IssueRepositoryImpl) FindYearSub(issueId string, year string) (domain.Issue, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var issue domain.Issue
	query := bson.M{"_id": issueId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&issue)
	issueType := issue.Value

	queryYear := helper.QueryIssueYear(year, issueType)
	cursorYear, err := repository.CollectionData.Find(ctx, queryYear)
	helper.PanicIfError(err)

	var years []domain.IssueYear

	for cursorYear.Next(ctx) {
		var year domain.IssueYear
		cursorYear.Decode(&year)

		var issueSubs []domain.IssueSub
		for _, issueSubData := range year.IssueSubsData {
			var issueSub domain.IssueSub
			querySub := bson.M{"label": issueSubData.Name}
			resultSub := repository.CollectionSub.FindOne(ctx, querySub)
			resultSub.Decode(&issueSub)

			issueSubs = append(issueSubs, issueSub)
		}
		year.IssueSubs = issueSubs
		years = append(years, year)
	}
	issue.Years = years

	return issue, result.Err()
}

func (repository *IssueRepositoryImpl) FindYearById(issueId string) (domain.Issue, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var issue domain.Issue
	query := bson.M{"_id": issueId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&issue)
	issueType := issue.Value

	year := ""
	queryYear := helper.QueryIssueYear(year, issueType)
	cursorYear, err := repository.CollectionData.Find(ctx, queryYear)
	helper.PanicIfError(err)

	var years []domain.IssueYear

	for cursorYear.Next(ctx) {
		var year domain.IssueYear
		cursorYear.Decode(&year)
		years = append(years, year)
	}
	issue.Years = years

	return issue, result.Err()
}

func (repository *IssueRepositoryImpl) FindAllYear(issueId string) []domain.Issue {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	label := ""
	value := ""
	query := helper.QueryIssue(issueId, label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	var issues []domain.Issue

	for cursor.Next(ctx) {
		var issue domain.Issue
		cursor.Decode(&issue)
		issueType := issue.Value

		year := ""
		queryYear := helper.QueryIssueYear(year, issueType)
		cursorYear, err := repository.CollectionData.Find(ctx, queryYear)
		helper.PanicIfError(err)

		var years []domain.IssueYear

		for cursorYear.Next(ctx) {
			var year domain.IssueYear
			cursorYear.Decode(&year)
			years = append(years, year)
		}
		issue.Years = years

		issues = append(issues, issue)
	}

	return issues
}

func (repository *IssueRepositoryImpl) FindIsExist(label string, value string) bool {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssueExist(label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	for cursor.Next(ctx) {
		return true
	}

	return false
}

func (repository *IssueRepositoryImpl) Create(issue domain.Issue) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":   issue.ID,
		"label": issue.Label,
		"value": issue.Value,
		"color": issue.Color,
	})

	helper.PanicIfError(err)
}

func (repository *IssueRepositoryImpl) Update(issue domain.Issue, issueId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": issueId}
	update := bson.M{
		"$set": bson.M{
			"label": issue.Label,
			"value": issue.Value,
			"color": issue.Color,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *IssueRepositoryImpl) Delete(issueId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": issueId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *IssueRepositoryImpl) InsertMany(issueLogs []interface{}) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	options := options.InsertMany()
	options.SetOrdered(false)
	_, err := repository.CollectionLog.InsertMany(ctx, issueLogs, options)

	helper.PanicIfError(err)
}

func (repository *IssueRepositoryImpl) CreateManagementData(issue domain.IssueManagementData) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionManagement.InsertOne(ctx, bson.M{
		"_id":        issue.ID,
		"name":       issue.Name,
		"file_url":   issue.FileUrl,
		"year":       issue.Year,
		"created_at": issue.CreatedAt,
		"issue_type": issue.IssueType,
		"sub_issue":  issue.SubIssue,
		"data":       issue.Data,
	})

	helper.PanicIfError(err)
}
