package repository

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type SocialmediaHistoricalRepositoryImpl struct {
	HistoricalCollection *mongo.Collection
	TotalCollection      *mongo.Collection
}

type SocialmediaHistoricalRepository interface {
	CreateHistory(history domain.SocialmediaHistorical)
	FindHistoricalById(socmedId string, query map[string]interface{}) []domain.SocialmediaHistorical
	// FindLastHistoricalByToken(token string) domain.SocialmediaHistorical

	CreateTotal(total domain.SocialmediaLastTotal)
	FindTotalBySocialmediaId(totalId string) domain.SocialmediaLastTotal
	UpdateTotalBySocialmediaId(totalId string, total domain.SocialmediaLastTotal)
}

func NewSocialmediaHistoricalRepository(database *mongo.Database) SocialmediaHistoricalRepository {
	return &SocialmediaHistoricalRepositoryImpl{
		HistoricalCollection: database.Collection("socialmedia_historical"),
		TotalCollection:      database.Collection("socialmedia_total"),
	}
}

func (repository *SocialmediaHistoricalRepositoryImpl) CreateHistory(history domain.SocialmediaHistorical) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.HistoricalCollection.InsertOne(ctx, bson.M{
		"_id":            history.ID,
		"socialmedia_id": history.SocialMediaId,
		"platform":       history.Platform,
		"historical":     history.Historical,
		"created_at":     history.CreatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *SocialmediaHistoricalRepositoryImpl) FindHistoricalById(socmedId string, query map[string]interface{}) []domain.SocialmediaHistorical {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	options := options.Find()
	options.SetSort(bson.D{{Key: "created_at", Value: -1}})

	query["socialmedia_id"] = socmedId

	var from, until time.Time
	if query["from"] != nil {
		from = query["from"].(time.Time)
		query["from"] = nil
	}
	if query["until"] != nil {
		until = query["until"].(time.Time)
		query["until"] = nil
	} else {
		until = time.Now()
	}

	if until.Before(from) {
		panic(exception.NewError(fiber.StatusBadRequest, "date 'until' cannot be before date 'from'"))
	}

	createdAt := bson.M{
		"$gte": primitive.NewDateTimeFromTime(from),
		"$lt":  primitive.NewDateTimeFromTime(until),
	}
	query["created_at"] = createdAt

	var cursor *mongo.Cursor
	var err error
	cursor, err = repository.HistoricalCollection.Find(ctx, query, options)
	helper.PanicIfError(err)

	var histories []domain.SocialmediaHistorical

	timezone, _ := strconv.Atoi(os.Getenv("TIMEZONE"))

	for cursor.Next(ctx) {
		var history domain.SocialmediaHistorical
		cursor.Decode(&history)
		history.CreatedAt = history.CreatedAt.Add((time.Hour * time.Duration(timezone)) - time.Minute*30)
		histories = append(histories, history)
	}

	var platform string
	if len(histories) > 0 {
		platform = histories[0].Platform
	}

	if !from.IsZero() && !until.IsZero() {
		var results []domain.SocialmediaHistorical
		for i := from; i.Before(until); i = i.Add(time.Hour * 24) {
			isFound := false
			for _, j := range histories {
				y1, m1, d1 := i.Date()
				y2, m2, d2 := j.CreatedAt.Date()
				if y1 == y2 && m1 == m2 && d1 == d2 {
					results = append(results, j)
					isFound = true
					break
				}
			}
			if !isFound {
				results = append(results, domain.SocialmediaHistorical{
					CreatedAt:     i,
					SocialMediaId: socmedId,
					Platform:      platform,
				})
			}
		}
		return results
	}

	return histories
}

// func (repository *SocialmediaHistoricalRepositoryImpl) FindLastHistoricalByToken(token string) domain.SocialmediaHistorical {
// 	ctx, cancel := config.NewDBContext()
// 	defer cancel()

// 	options := options.FindOne()
// 	options.SetSort(bson.D{{Key: "created_at", Value: -1}})

// 	var err error
// 	var data domain.SocialmediaHistorical
// 	query := bson.M{"ayrshare_token": token}

// 	result := repository.HistoricalCollection.FindOne(ctx, query, options)
// 	result.Decode(&data)
// 	helper.PanicIfError(err)

// 	return data
// }

func (repository *SocialmediaHistoricalRepositoryImpl) CreateTotal(total domain.SocialmediaLastTotal) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.TotalCollection.InsertOne(ctx, bson.M{
		"_id":        total.ID,
		"platform":   total.Platform,
		"total":      total.Total,
		"created_at": total.CreatedAt,
		"updated_at": total.UpdatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *SocialmediaHistoricalRepositoryImpl) FindTotalBySocialmediaId(totalId string) domain.SocialmediaLastTotal {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	options := options.FindOne()
	options.SetSort(bson.D{{Key: "created_at", Value: -1}})

	var err error
	var data domain.SocialmediaLastTotal
	query := bson.M{"_id": totalId}

	result := repository.TotalCollection.FindOne(ctx, query, options)
	result.Decode(&data)
	// timezone, _ := strconv.Atoi(os.Getenv("TIMEZONE"))
	// data.CreatedAt = data.CreatedAt.Add(time.Hour * time.Duration(timezone))
	// data.UpdatedAt = data.UpdatedAt.Add(time.Hour * time.Duration(timezone))
	helper.PanicIfError(err)
	fmt.Println("repo", data)

	return data
}

func (repository *SocialmediaHistoricalRepositoryImpl) UpdateTotalBySocialmediaId(totalId string, total domain.SocialmediaLastTotal) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": totalId}
	update := bson.M{
		"$set": bson.M{
			"total":      total.Total,
			"updated_at": total.UpdatedAt,
		},
	}

	_, err := repository.TotalCollection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}
