package web

type IssueResponse struct {
	ID    string              `json:"id" bson:"_id"`
	Label string              `json:"label" bson:"label"`
	Value string              `json:"value" bson:"value"`
	Color string              `json:"color" bson:"color"`
	Years []IssueYearResponse `json:"years,omitempty" bson:"-"`
}

type IssueYearResponse struct {
	ID        string             `json:"id" bson:"_id"`
	Label     string             `json:"label" bson:"year"`
	Value     string             `json:"value" bson:"year"`
	IssueSubs []IssueSubResponse `json:"sub-issues,omitempty" bson:"sub_issue_data"`
}

type IssueResponsePoints struct {
	ID         string       `json:"id" bson:"_id"`
	ProvinceId int          `json:"province_id" bson:"province_id"`
	Year       string       `json:"year" bson:"year"`
	SubIssueId string       `json:"sub_issue_id" bson:"sub_issue_id"`
	Data       DataPinPoint `json:"data" bson:"data"`
}

type DataPinPoint struct {
	TypeIssue string `json:"type_issue" bson:"type_issue"`
	SubIssue  string `json:"sub_issue" bson:"sub_issue"`
	Latitude  string `json:"latitude" bson:"latitude"`
	Longitude string `json:"longitude" bson:"longitude"`
	Desc      string `json:"desc" bson:"desc"`
}

type DataPinPoinResult struct {
	ID string `json:"id"`
	// SubIssueID string `json:"sub_issue_id"`
	Data struct {
		Desc      string `json:"desc"`
		Latitude  string `json:"latitude"`
		Longitude string `json:"longitude"`
	} `json:"data" bson:"-"`
}

type GroupedDataPintPoint struct {
	SubIssueID string              `json:"sub_issue_id"`
	Points     []DataPinPoinResult `json:"points"`
}
