package controller

import (
	"fmt"

	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type UserControllerImpl struct {
	UserService service.UserService
	LogService  service.LogService
}

type UserController interface {
	NewUserRouter(app *fiber.App)
}

func NewUserController(userService service.UserService, logService service.LogService) UserController {
	return &UserControllerImpl{
		UserService: userService,
		LogService:  logService,
	}
}

func (controller *UserControllerImpl) NewUserRouter(app *fiber.App) {
	user := app.Group("/users")
	user.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	// Restricted by level
	user.Use(middlewares.IsAuthenticated)
	user.Post("/create", controller.Create)
	user.Get("/", controller.FindAll)
	user.Get("/user-recruited", controller.FindAllByUserId)
	user.Get("/coordinator", controller.FindAllCoor)
	user.Get("/total-koordinator", controller.TotalKoordinator)
	user.Get("/total-relawan", controller.TotalRelawan)
	user.Get("/total-pemilih", controller.TotalPemilih)
	user.Get("/total-pemilih-baru", controller.TotalPemilihBaru)
	user.Get("/rank/:level", controller.GetRankByLevel)
	user.Get("/:userid", controller.FindById)
	user.Put("/:userid", controller.Update)
	user.Delete("/:userid", controller.Delete)
	//user.Post("/create-for-survey", controller.CreateUserForSurvey)
	user.Put("/update-password/:userid", controller.UpdatePassword)
	user.Put("/update-occupation/:userid", controller.UpdateOccupation)

	user.Get("/:userid/accesses", controller.GetAccess)
	user.Put("/:userid/accesses", controller.UpdateAccess)

}

func (controller *UserControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.UserCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userResponse := controller.UserService.Create(ctx, request)

	if userResponse.Occupation.Level == 4 {
		controller.UserService.CreateRespondentForSurvey(ctx, userResponse)
	}

	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	action := fmt.Sprintf("created user %s", userResponse.Name)
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) FindById(ctx *fiber.Ctx) error {
	userId := ctx.Params("userid")

	userResponse := controller.UserService.FindById(userId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) FindAll(ctx *fiber.Ctx) error {
	// query page, limit, level, searchName
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	level := ctx.Query("level")
	districtId := ctx.Query("districtid")
	searchName := ctx.Query("search")
	userResponse := controller.UserService.FindAll(page, limit, level, districtId, searchName)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) Update(ctx *fiber.Ctx) error {
	var request web.UserUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if request.NIK != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "nik cant be change"))
	}
	userId := ctx.Params("userId")

	userResponse := controller.UserService.Update(ctx, userId, request)

	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	action := fmt.Sprintf("update user %s", userResponse.Name)
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) Delete(ctx *fiber.Ctx) error {
	userId := ctx.Params("userid")

	controller.UserService.Delete(ctx, userId)

	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	action := fmt.Sprintf("delete user %s", userId)
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

// move to respondent
//func (controller *UserControllerImpl) CreateUserForSurvey(ctx *fiber.Ctx) error {
//	var request web.UserForSurvey
//	err := ctx.BodyParser(&request)
//	helper.PanicIfError(err)
//
//	userResponse := controller.UserService.CreateUserForSurvey(ctx, request)
//
//	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
//	action := fmt.Sprintf("created user %s", userResponse.Name)
//	controller.LogService.Create(actor, action)
//
//	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
//		Code:    fiber.StatusCreated,
//		Status:  true,
//		Message: "success",
//		Data:    userResponse,
//	})
//}

func (controller *UserControllerImpl) FindAllCoor(ctx *fiber.Ctx) error {
	// query page, limit, level, searchName
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	level := ctx.Query("level")
	districtId := ctx.Query("districtid")
	searchName := ctx.Query("search")
	userResponse := controller.UserService.FindAllCoor(page, limit, level, districtId, searchName)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) TotalKoordinator(ctx *fiber.Ctx) error {
	districtId := ctx.Query("districtid")
	response := controller.UserService.TotalKoordinator(districtId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *UserControllerImpl) TotalRelawan(ctx *fiber.Ctx) error {
	districtId := ctx.Query("districtid")
	response := controller.UserService.TotalRelawan(districtId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *UserControllerImpl) TotalPemilih(ctx *fiber.Ctx) error {

	response := controller.UserService.TotalPemilih()
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *UserControllerImpl) TotalPemilihBaru(ctx *fiber.Ctx) error {

	response := controller.UserService.TotalPemilihBaru()
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *UserControllerImpl) GetRankByLevel(ctx *fiber.Ctx) error {
	// query page, limit, level, searchName
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	level := ctx.Params("level")
	userResponse := controller.UserService.GetRankByLevel(page, limit, level)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) UpdatePassword(ctx *fiber.Ctx) error {
	var request web.UserUpdatePasswordRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId := ctx.Params("userId")

	userResponse := controller.UserService.UpdatePassword(ctx, userId, request)

	action := fmt.Sprintf("updated password user %s", userResponse.Name)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) UpdateOccupation(ctx *fiber.Ctx) error {
	var request web.UserUpdateOccupationRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId := ctx.Params("userId")

	userResponse := controller.UserService.UpdateOccupation(ctx, userId, request)

	action := fmt.Sprintf("updated occupation user %s", userResponse.Name)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) FindAllByUserId(ctx *fiber.Ctx) error {
	// query page, limit, level, searchName
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	level := ctx.Query("level")
	userResponse := controller.UserService.FindRecruitedUserByUseriD(actor, level)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) UpdateAccess(ctx *fiber.Ctx) error {
	var request web.UserUpdateAccessRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId := ctx.Params("userId")

	userResponse := controller.UserService.UpdateAccess(ctx, userId, request)

	action := fmt.Sprintf("updated password user %s", userResponse.Name)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *UserControllerImpl) GetAccess(ctx *fiber.Ctx) error {
	userId := ctx.Params("userid")

	userResponse := controller.UserService.GetAccess(userId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}
