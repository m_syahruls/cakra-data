package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type ResponseControllerImpl struct {
	ResponseService service.ResponseService
	LogService      service.LogService
}

type ResponseController interface {
	NewResponseRouter(app *fiber.App)
}

func NewResponseController(responseService service.ResponseService, logService service.LogService) ResponseController {
	return &ResponseControllerImpl{
		ResponseService: responseService,
		LogService:      logService,
	}
}

func (controller *ResponseControllerImpl) NewResponseRouter(app *fiber.App) {
	response := app.Group("/response")
	response.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})
	response.Post("/", controller.Create)

	response.Get("/hirarki/:surveyid", controller.FindBySurveyIdWithHirarki)
	response.Get("/summary/:questionid", controller.FindSummaryByQuestionId)

	response.Get("/", controller.FindAll)
	response.Get("/:responseid", controller.FindById)
	//response.Put("/:responseid", controller.Update) cant update response
	response.Delete("/:responseid", controller.Delete)
}

func (controller *ResponseControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.CreateResponseRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	responseResponse := controller.ResponseService.Create(ctx, request)

	action := fmt.Sprintf("create response for survey %s", request.SurveyId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    responseResponse,
	})
}

func (controller *ResponseControllerImpl) FindById(ctx *fiber.Ctx) error {
	responseId := ctx.Params("responseid")

	responseResponse := controller.ResponseService.FindDetailById(responseId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    responseResponse,
	})
}

func (controller *ResponseControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query by page, limit, survey id & respondent id
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	surveyId := ctx.Query("surveyid")
	respondentId := ctx.Query("respondentid")
	recruiterid := ctx.Query("recruiterid")

	responseResponse := controller.ResponseService.FindAll(page, limit, surveyId, respondentId, recruiterid)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    responseResponse,
	})
}

func (controller *ResponseControllerImpl) Update(ctx *fiber.Ctx) error {
	responseId := ctx.Params("responseid")
	var request web.UpdateResponseRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	responseUpdated := controller.ResponseService.Update(responseId, request)

	action := fmt.Sprintf("update response for %s", responseId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    responseUpdated,
	})
}

func (controller *ResponseControllerImpl) Delete(ctx *fiber.Ctx) error {
	responseId := ctx.Params("responseid")

	controller.ResponseService.Delete(responseId)

	action := fmt.Sprintf("delete response for %s", responseId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

func (controller *ResponseControllerImpl) FindBySurveyIdWithHirarki(ctx *fiber.Ctx) error {
	surveyId := ctx.Params("surveyid")

	responseResponse := controller.ResponseService.FindResponseHirarkiSurveyorBySurveyId(surveyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    responseResponse,
	})
}

func (controller *ResponseControllerImpl) FindSummaryByQuestionId(ctx *fiber.Ctx) error {
	questionId := ctx.Params("questionid")
	regencyId := ctx.Query("regencyid")

	responseResponse := controller.ResponseService.SummaryQuestionByRegionId(questionId, regencyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    responseResponse,
	})
}
