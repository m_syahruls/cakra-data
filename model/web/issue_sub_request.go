package web

type CreateIssueSubRequest struct {
	IssueID  string `json:"issue_id" validate:"required"`
	Label    string `json:"label" validate:"required"`
	Value    string `json:"value" validate:"required"`
	ImageUrl string `json:"image_url"`
}

type UpdateIssueSubRequest struct {
	IssueID  string `json:"issue_id" validate:"required"`
	Label    string `json:"label" validate:"required"`
	Value    string `json:"value" validate:"required"`
	ImageUrl string `json:"image_url"`
}
