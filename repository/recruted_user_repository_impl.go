package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type RecruitedUserRepositoryImpl struct {
	Collection *mongo.Collection
}

type RecruitedUserRepository interface {
	Create(recruit domain.RecruitedUser)
	IsReferral(userId, recruiterId string) (domain.RecruitedUser, error)
	FindByUserId(userId string) (domain.RecruitedUser, error)
	Delete(userId string) error

	FindRecruitedByRecruiterId(recruiter string) (domain.RecruitedUser, error)
}

func NewRecruitedUserRepository(database *mongo.Database) RecruitedUserRepository {
	return &RecruitedUserRepositoryImpl{
		Collection: database.Collection("recruited_user"),
	}
}

func (repository *RecruitedUserRepositoryImpl) Create(recruited domain.RecruitedUser) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":             recruited.ID,
		"user_id":         recruited.UserId,
		"user_recruit_id": recruited.UserRecruitId,
		"recruiter_id":    recruited.RecruiterId,
	})

	helper.PanicIfError(err)
}

func (repository *RecruitedUserRepositoryImpl) IsReferral(userId, recruiterId string) (domain.RecruitedUser, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var recruit domain.RecruitedUser
	query := bson.M{"user_id": userId, "recruiter_id": recruiterId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&recruit)

	return recruit, result.Err()
}

func (repository *RecruitedUserRepositoryImpl) FindByUserId(userId string) (domain.RecruitedUser, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var recruit domain.RecruitedUser
	query := bson.M{"user_id": userId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&recruit)

	return recruit, result.Err()
}

func (repository *RecruitedUserRepositoryImpl) Delete(userId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"user_id": userId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *RecruitedUserRepositoryImpl) FindRecruitedByRecruiterId(recruiter string) (domain.RecruitedUser, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var recruit domain.RecruitedUser
	query := bson.M{"recruiter_id": recruiter}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&recruit)

	return recruit, result.Err()
}
