package domain

type IssueData struct {
	ID           string            `json:"id" bson:"_id"`
	Year         string            `json:"year" bson:"year"`
	IssueType    string            `json:"issue_type" bson:"issue_type"`
	MetaData     MetadataIssueData `json:"metadata" bson:"metadata"`
	SubIssueData []SubIssueData    `json:"sub_issue_data" bson:"sub_issue_data"`
	Data         []DataWilayah     `json:"data" bson:"data"`
}

// type IssueDataBencanaResponse struct {
type MetadataIssueData struct {
	TotalKejadian int                     `json:"total_incidents" bson:"total_incidents"`
	Data          []DataMetadataIssueData `json:"data" bson:"data"`
	SubIssues     []DataMetadataIssueData `json:"sub_issue" bson:"sub_issue"`
}

type DataMetadataIssueData struct {
	Name  string `json:"name" bson:"name"`
	Total int    `json:"total_incidents" bson:"total_incidents"`
}

type SubIssueData struct {
	Name           string             `json:"name" bson:"name"`
	TotalIncidents int                `json:"total_incidents" bson:"total_incidents"`
	Data           []DataSubIssueData `json:"data" bson:"data"`
}

type DataSubIssueData struct {
	Name           string `json:"name" bson:"name"`
	TotalIncidents int    `json:"total_incidents" bson:"total_incidents"`
}

type DataWilayah struct {
	ID        int                      `json:"id" bson:"id"`
	Wilayah   string                   `json:"wilayah" bson:"wilayah"`
	MetaData  MetadataWilayahIssueData `json:"metadata" bson:"metadata"`
	SubIssues []DataMetadataIssueData  `json:"sub_issue" bson:"sub_issue"`
}

type MetadataWilayahIssueData struct {
	TotalIncidents int                     `json:"total_incidents" bson:"total_incidents"`
	Data           []DataMetadataIssueData `json:"data" bson:"data"`
}

// type SubIssues struct {
// 	Name  string `json:"name" bson:"name"`
// 	Total int    `json:"total_incidents" bson:"total_incidents"`
// 	Rank  int    `json:"rank" bson:"_"`
// }

type IssueDataUpdateInsert struct {
	ID        string                  `json:"id" bson:"_id"`
	Year      string                  `json:"year" bson:"year"`
	IssueType string                  `json:"issue_type" bson:"issue_type"`
	MetaData  IssueDataBencanaPayload `json:"metadata" bson:"-"`
	Data      []DataWilayah           `json:"data" bson:"-"`
}

type IssueDataBencanaPayload struct {
	TotalKejadian int                     `json:"totalkejadian"`
	Data          []DataMetadataIssueData `json:"data" bson:"data"`
	SubIssues     []DataMetadataIssueData `json:"sub_issue" bson:"sub_issue"`
}

type IssueResponsePoints struct {
	ID         string       `json:"id" bson:"_id"`
	ProvinceId int          `json:"province_id" bson:"province_id"`
	Year       string       `json:"year" bson:"year"`
	SubIssueId string       `json:"sub_issue_id" bson:"sub_issue_id"`
	Data       DataPinPoint `json:"data" bson:"data"`
}

type DataPinPoint struct {
	TypeIssue string `json:"type_issue" bson:"type_issue"`
	SubIssue  string `json:"sub_issue" bson:"sub_issue"`
	Latitude  string `json:"latitude" bson:"latitude"`
	Longitude string `json:"longitude" bson:"longitude"`
	Desc      string `json:"desc" bson:"desc"`
}
