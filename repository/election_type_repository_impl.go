package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ElectionTypeRepositoryImpl struct {
	Collection *mongo.Collection
}

type ElectionTypeRepository interface {
	FindAll(page, limit string) []domain.ElectionType
	FindById(electionTypeId string) domain.ElectionType
}

func NewElectionTypeRepository(database *mongo.Database) ElectionTypeRepository {
	return &ElectionTypeRepositoryImpl{
		Collection: database.Collection("election_type"),
	}
}

func (repository *ElectionTypeRepositoryImpl) FindAll(page, limit string) []domain.ElectionType {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)
	filter.SetSort(bson.M{"_id": 1})

	cursor, err := repository.Collection.Find(ctx, bson.M{}, filter)
	helper.PanicIfError(err)

	var electionTypes []domain.ElectionType

	for cursor.Next(ctx) {
		var electionType domain.ElectionType
		cursor.Decode(&electionType)
		electionTypes = append(electionTypes, electionType)
	}

	return electionTypes
}

func (repository *ElectionTypeRepositoryImpl) FindById(electionTypeId string) domain.ElectionType {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor := repository.Collection.FindOne(ctx, bson.M{"_id": electionTypeId})

	var electiontype domain.ElectionType
	cursor.Decode(&electiontype)

	return electiontype
}
