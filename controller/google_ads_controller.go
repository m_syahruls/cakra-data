package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type GoogleAdsControllerImpl struct {
	GoogleAdsService service.GoogleAdsService
}

type GoogleAdsController interface {
	NewGoogleAdsRouter(app *fiber.App)
}

func NewGoogleAdsController(googleAdsService service.GoogleAdsService) GoogleAdsController {
	return &GoogleAdsControllerImpl{
		GoogleAdsService: googleAdsService,
	}
}

func (controller *GoogleAdsControllerImpl) NewGoogleAdsRouter(app *fiber.App) {
	googleAds := app.Group("/google-ads")
	googleAds.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	googleAds.Get("/:customerId", controller.GetAllCampaignsById)
	googleAds.Get("/:customerId/name", controller.GetCustomerNameById)
}

func (controller *GoogleAdsControllerImpl) GetAllCampaignsById(ctx *fiber.Ctx) error {
	customerId := ctx.Params("customerId")

	result := controller.GoogleAdsService.GetAllCampaignsById(customerId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}

func (controller *GoogleAdsControllerImpl) GetCustomerNameById(ctx *fiber.Ctx) error {
	customerId := ctx.Params("customerId")

	result := controller.GoogleAdsService.GetCustomerNameById(customerId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}
