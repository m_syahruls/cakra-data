package domain

import "time"

type Logistic struct {
	Id            string           `json:"id" bson:"_id"`
	CategoryId    string           `json:"category_id" bson:"category_id"`
	Nama          string           `json:"name" bson:"name"`
	Location      string           `json:"location" bson:"location"`
	ImageLogistic string           `json:"image_logistic" bson:"image_logistic"`
	Longitude     string           `json:"longitude" bson:"longitude"`
	Latitude      string           `json:"latitude" bson:"latitude"`
	UserId        string           `json:"user_id" bson:"user_id"`
	CreatedAt     time.Time        `json:"created_at" bson:"created_at"`
	UpdatedAt     time.Time        `json:"updated_at" bson:"updated_at"`
	Category      LogisticCategory `json:"category" bson:"category"`
	User          User             `json:"user" bson:"user"`
}

type LogisticCategory struct {
	Id   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
}

type ProgressLogistic struct {
	ID              string `json:"id" bson:"_id"`
	UserId          string `json:"user_id" bson:"user_id"`
	RecruitProgress int    `json:"logistic_progess" bson:"logistic_progress"`
	RecruitGoal     int    `json:"logistic_goal" bson:"logistic_goal"`
}
