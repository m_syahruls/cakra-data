package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type RegencyRepositoryImpl struct {
	Collection *mongo.Collection
}

type RegencyRepository interface {
	FindById(regencyId string) (domain.Regency, error)
	FindByName(regencyName string) (domain.Regency, error)
	FindAll(page, limit, provinceId, name string) []domain.Regency
}

func NewRegencyRepository(database *mongo.Database) RegencyRepository {
	return &RegencyRepositoryImpl{
		Collection: database.Collection("regencies"),
	}
}

func (repository *RegencyRepositoryImpl) FindById(regencyId string) (domain.Regency, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var regency domain.Regency
	query := bson.M{"_id": regencyId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&regency)

	return regency, result.Err()
}

func (repository *RegencyRepositoryImpl) FindByName(regencyName string) (domain.Regency, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var regency domain.Regency
	query := bson.M{"name": regencyName}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&regency)

	return regency, result.Err()
}

func (repository *RegencyRepositoryImpl) FindAll(page, limit, provinceId, name string) []domain.Regency {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	query := helper.QueryRegency(provinceId, name)

	cursor, err := repository.Collection.Find(ctx, query, filter)
	helper.PanicIfError(err)

	var regencys []domain.Regency

	for cursor.Next(ctx) {
		var regency domain.Regency
		cursor.Decode(&regency)
		regencys = append(regencys, regency)
	}

	return regencys
}
