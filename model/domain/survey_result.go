package domain

import "time"

type SurveyResult struct {
	ID              string                 `json:"id" bson:"_id"`
	SurveyName      string                 `json:"survey_name" bson:"survey_name"`
	Status          int                    `json:"status" bson:"status"`
	TotalRespondent int                    `json:"total_respondent" bson:"total_respondent"`
	CreatedAt       time.Time              `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time              `json:"updated_at" bson:"updated_at"`
	Questions       []SurveyQuestionResult `json:"questions" bson:"questions"`
}

type SurveyQuestionResult struct {
	ID              string               `json:"id" bson:"_id"`
	SurveyId        string               `json:"survey_id" bson:"survey_id"`
	Section         string               `json:"section" bson:"section"`
	InputType       string               `json:"input_type" bson:"input_type"`
	QuestionNumber  int                  `json:"question_number" bson:"question_number"`
	QuestionName    string               `json:"question_name" bson:"question_name"`
	QuestionSubject string               `json:"question_subject" bson:"question_subject"`
	Location        []Region             `json:"location" bson:"location"`
	AnswerText      []SurveyAnswerText   `json:"answers" bson:"answers"`
	Options         []SurveyOptionAnswer `json:"options" bson:"options"`
}

type SurveyOptionAnswer struct {
	ID          string `json:"id" bson:"_id"`
	QuestionId  string `json:"question_id" bson:"question_id"`
	OptionName  string `json:"option_name" bson:"option_name"`
	Value       int    `json:"value" bson:"value"`
	TotalAnswer int    `json:"total_answer" bson:"total_answer"`
}

type SurveyAnswerText struct {
	AnswerText string `json:"answer_text" bson:"answer_text"`
}

type ResponseCountDate struct {
	ID               interface{} `json:"id" bson:"_id"`
	Date             DateObj     `json:"date" bson:"date"`
	LastResponseDate interface{} `json:"last_response" bson:"last_response"`
	RespondentId     interface{} `json:"respondent_id" bson:"respondent_id"`
	Count            interface{} `json:"count" bson:"count"`
}

type DateObj struct {
	Year  interface{} `json:"year" bson:"year"`
	Month interface{} `json:"month" bson:"month"`
	Day   interface{} `json:"day" bson:"day"`
}

type Region struct {
	Regions []string `json:"regions"`
	Number  int      `json:"number"`
}
