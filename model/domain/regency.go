package domain

type Regency struct {
	ID         int         `json:"id" bson:"_id"`
	ProvinceId int         `json:"province_id" bson:"province_id"`
	Name       string      `json:"name" bson:"name"`
	AltName    string      `json:"alt_name" bson:"alt_name"`
	Latitude   interface{} `json:"latitude" bson:"latitude"`
	Longitude  interface{} `json:"longitude" bson:"longitude"`
}
