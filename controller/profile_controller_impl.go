package controller

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type ProfileControllerImpl struct {
	ProfileService      service.ProfileService
	GoogleAdsService    service.GoogleAdsService
	SocialListenService service.MediatoolkitService
	SocialMediaService  service.AyrshareService
	LogService          service.LogService
}

type ProfileController interface {
	NewProfileRouter(app *fiber.App)
}

func NewProfileController(profileService service.ProfileService, googleAdsService service.GoogleAdsService, mediatoolkitService service.MediatoolkitService, socmedService service.AyrshareService, logService service.LogService) ProfileController {
	return &ProfileControllerImpl{
		ProfileService:      profileService,
		GoogleAdsService:    googleAdsService,
		SocialListenService: mediatoolkitService,
		SocialMediaService:  socmedService,
		LogService:          logService,
	}
}

func (controller *ProfileControllerImpl) NewProfileRouter(app *fiber.App) {
	profile := app.Group("/profile")
	profile.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	profile.Use(middlewares.IsAuthenticated)
	profile.Get("/", controller.FindById)
	profile.Get("/google-ads", controller.GetGoogleAdsCampaignById)
	profile.Get("/google-ads/name", controller.GetGoogleAdsCustomerNameById)
	profile.Get("/social-media", controller.GetSocialmediaUserDetails)
	profile.Get("/social-media/analytics", controller.GetSocialmediaUserAnalytics)
	profile.Get("/social-media/analytics/historical", controller.GetSocialmediaUserAnalyticsHistorical)
	profile.Post("/social-media/post", controller.PostToSocialMedia)
	profile.Put("/change-password", controller.UpdatePasswordProfile)
	profile.Put("/update", controller.Update)
	profile.Put("/update/image", controller.UpdateProfileImage)
	profile.Put("/update/google-ads", controller.UpdateGoogleAdsID)
	profile.Put("/update/ayrshare-token", controller.UpdateAyrshareToken)
	profile.Put("/update/determ-id", controller.UpdateDetermID)
	profile.Delete("/update/google-ads", controller.DeleteGoogleAdsID)
	profile.Delete("/update/ayrshare-token", controller.DeleteAyrshareToken)
	profile.Delete("/update/determ-id", controller.DeleteDetermID)
}

func (controller *ProfileControllerImpl) FindById(ctx *fiber.Ctx) error {
	token := ctx.Cookies("token")
	userId, _, _ := helper.ParseJwt(token)

	profileResponse := controller.ProfileService.FindById(userId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    profileResponse,
	})
}

func (controller *ProfileControllerImpl) UpdatePasswordProfile(ctx *fiber.Ctx) error {
	var request web.UpdatePasswordProfile
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	token := ctx.Cookies("token")
	userId, _, _ := helper.ParseJwt(token)

	controller.ProfileService.UpdatePassword(userId, request)

	action := fmt.Sprintf("update password profile %s", userId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

func (controller *ProfileControllerImpl) Update(ctx *fiber.Ctx) error {
	var request web.ProfileUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	userResponse := controller.ProfileService.UpdateProfile(userId, request)

	action := fmt.Sprintf("update profile %s", userResponse.Name)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userResponse,
	})
}

func (controller *ProfileControllerImpl) UpdateProfileImage(ctx *fiber.Ctx) error {
	userId, _, err := helper.ParseJwt(ctx.Cookies("token"))
	helper.PanicIfError(err)

	user := controller.ProfileService.UpdateProfileImage(ctx, userId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    user,
	})
}

func (controller *ProfileControllerImpl) UpdateGoogleAdsID(ctx *fiber.Ctx) error {
	var request web.ProfileUpdateGoogleAdsID
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	result, code := controller.GoogleAdsService.CheckEligibility(request.GoogleAdsID)

	if code != 200 {
		return ctx.Status(code).JSON(web.WebResponse{
			Code:    code,
			Status:  false,
			Message: "failed",
			Data:    result,
		})
	}

	user := controller.ProfileService.UpdateGoogleAdsID(userId, request)
	name := controller.GoogleAdsService.GetCustomerNameById(user.GoogleAdsID)
	campaign := controller.GoogleAdsService.GetAllCampaignsById(user.GoogleAdsID)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data: web.ProfileUpdateGoogleAdsResponse{
			GoogleAdsID:   user.GoogleAdsID,
			GoogleAdsName: name.Results[0].Customer.DescriptiveName,
			Results:       campaign.Results,
			FieldMask:     campaign.FieldMask,
			RequestId:     campaign.RequestId,
		},
	})
}

func (controller *ProfileControllerImpl) UpdateAyrshareToken(ctx *fiber.Ctx) error {
	var request web.ProfileUpdateAyrshareToken
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	result := controller.SocialMediaService.CheckEligibility(request.AyrshareToken)

	if result.Code != 200 {
		return ctx.Status(result.Code).JSON(web.WebResponse{
			Code:    result.Code,
			Status:  false,
			Message: "failed",
			Data:    result,
		})
	}

	controller.ProfileService.UpdateAyrshareToken(userId, request)
	detail := controller.SocialMediaService.GetUserDetails(request.AyrshareToken)

	lastHistory := controller.SocialMediaService.FindTotalBySocialmediaId(request.AyrshareToken)
	if lastHistory.CreatedAt.IsZero() {
		details := controller.SocialMediaService.GetUserDetails(request.AyrshareToken)

		for _, data := range details.DisplayNames {
			var platform []string
			platform = append(platform, data.Platform)

			controller.SocialMediaService.GetUserAnalytics(request.AyrshareToken, web.AyrshareUserAnalyticsRequest{
				Platforms: platform,
			})

			if data.Platform == "twitter" {
				controller.SocialMediaService.FindTotalBySocialmediaId(data.ID)
			}
		}

		// analytic := controller.SocialMediaService.GetUserAnalytics(request.AyrshareToken, web.AyrshareUserAnalyticsRequest{
		// 	Platforms: detail.ActiveSocialAccounts,
		// })

		// new := helper.AnalyticWebToTotalDomain(request.AyrshareToken, domain.SocialmediaHistorical{}, analytic, true)
		// controller.SocialMediaService.CreateHistory(request.AyrshareToken, new)
	}

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data: web.ProfileUpdateAyrshareResponse{
			ActiveSocialAccounts: detail.ActiveSocialAccounts,
			DisplayNames:         detail.DisplayNames,
			Email:                detail.Email,
		},
	})
}

func (controller *ProfileControllerImpl) UpdateDetermID(ctx *fiber.Ctx) error {
	var request web.ProfileUpdateDetermID
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	result := controller.SocialListenService.GetGroups(request.DetermID)

	if result.Code != 200 && result.Code != 1 {
		return ctx.Status(result.Code).JSON(web.WebResponse{
			Code:    result.Code,
			Status:  false,
			Message: "failed",
			Data:    result,
		})
	}

	controller.ProfileService.UpdateDetermID(userId, request)
	// user := controller.ProfileService.UpdateDetermID(userId, request)
	// name := controller.GoogleAdsService.GetCustomerNameById(user.GoogleAdsID)
	// campaign := controller.GoogleAdsService.GetAllCampaignsById(user.GoogleAdsID)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		// Data: web.ProfileUpdateAyrshareResponse{
		// 	ActiveSocialAccounts: detail.ActiveSocialAccounts,
		// 	DisplayNames:         detail.DisplayNames,
		// 	Email:                detail.Email,
		// },
	})
}

func (controller *ProfileControllerImpl) DeleteGoogleAdsID(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.DeleteGoogleAdsID(userId)
	user.GoogleAdsID = ""

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    user,
	})
}

func (controller *ProfileControllerImpl) DeleteAyrshareToken(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.DeleteAyrshareToken(userId)
	user.AyrshareToken = ""

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    user,
	})
}

func (controller *ProfileControllerImpl) DeleteDetermID(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.DeleteDetermID(userId)
	user.DetermID = ""

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    user,
	})
}

func (controller *ProfileControllerImpl) GetGoogleAdsCampaignById(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.FindById(userId)
	result := controller.GoogleAdsService.GetAllCampaignsById(user.GoogleAdsID)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}

func (controller *ProfileControllerImpl) GetGoogleAdsCustomerNameById(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.FindById(userId)
	result := controller.GoogleAdsService.GetCustomerNameById(user.GoogleAdsID)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}

func (controller *ProfileControllerImpl) GetSocialmediaUserDetails(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.FindById(userId)
	if user.AyrshareToken == "" {
		return ctx.Status(fiber.StatusNotFound).JSON(web.WebResponse{
			Code:    fiber.StatusNotFound,
			Status:  false,
			Message: "this account have not register an Ayrshare token",
			Data:    nil,
		})
	}
	result := controller.SocialMediaService.GetUserDetails(user.AyrshareToken)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}

func (controller *ProfileControllerImpl) PostToSocialMedia(ctx *fiber.Ctx) error {
	var request web.AyrsharePostRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.FindById(userId)
	if user.AyrshareToken == "" {
		return ctx.Status(fiber.StatusNotFound).JSON(web.WebResponse{
			Code:    fiber.StatusNotFound,
			Status:  false,
			Message: "this account have not register an Ayrshare token",
			Data:    nil,
		})
	}
	result := controller.SocialMediaService.PostToSocialMedia(user.AyrshareToken, request)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}

func (controller *ProfileControllerImpl) GetSocialmediaUserAnalytics(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.FindById(userId)
	if user.AyrshareToken == "" {
		return ctx.Status(fiber.StatusNotFound).JSON(web.WebResponse{
			Code:    fiber.StatusNotFound,
			Status:  false,
			Message: "this account have not register an Ayrshare token",
			Data:    nil,
		})
	}

	request := web.AyrshareUserAnalyticsRequest{
		Platforms: controller.SocialMediaService.GetUserDetails(user.AyrshareToken).ActiveSocialAccounts,
	}

	result := controller.SocialMediaService.GetUserAnalytics(user.AyrshareToken, request)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}

func (controller *ProfileControllerImpl) GetSocialmediaUserAnalyticsHistorical(ctx *fiber.Ctx) error {
	userId, _, _ := helper.ParseJwt(ctx.Cookies("token"))

	user := controller.ProfileService.FindById(userId)
	if user.AyrshareToken == "" {
		return ctx.Status(fiber.StatusNotFound).JSON(web.WebResponse{
			Code:    fiber.StatusNotFound,
			Status:  false,
			Message: "this account have not register an Ayrshare token",
			Data:    nil,
		})
	}

	// var err error
	query := make(map[string]interface{})
	var id string
	if ctx.Query("platform") == "" {
		return ctx.Status(fiber.StatusBadRequest).JSON(web.WebResponse{
			Code:    fiber.StatusBadRequest,
			Status:  false,
			Message: "platform not found in request",
			Data:    nil,
		})
	} else {
		var array []string
		array = append(array, ctx.Query("platform"))
		details := controller.SocialMediaService.GetUserAnalytics(user.AyrshareToken, web.AyrshareUserAnalyticsRequest{
			Platforms: array,
		})
		if ctx.Query("platform") == "twitter" {
			id = details.Twitter.Analytics.Username
		} else if ctx.Query("platform") == "facebook" {
			id = details.Facebook.Analytics.ID
		} else if ctx.Query("platform") == "instagram" {
			id = details.Instagram.Analytics.ID
		} else {
			return ctx.Status(fiber.StatusBadRequest).JSON(web.WebResponse{
				Code:    fiber.StatusBadRequest,
				Status:  false,
				Message: "invalid platform",
				Data:    nil,
			})
		}
	}

	timezone, _ := strconv.Atoi(os.Getenv("TIMEZONE"))

	if ctx.Query("from") != "" {
		date := ctx.Query("from")
		temp, err := time.Parse("2006-01-02", date)
		if err != nil {
			return ctx.Status(fiber.StatusBadRequest).JSON(web.WebResponse{
				Code:    fiber.StatusBadRequest,
				Status:  false,
				Message: "query from: " + err.Error(),
			})
		}
		query["from"] = temp
	}
	if ctx.Query("until") != "" {
		date := ctx.Query("until")
		temp, err := time.Parse("2006-01-02", date)
		if err != nil {
			return ctx.Status(fiber.StatusBadRequest).JSON(web.WebResponse{
				Code:    fiber.StatusBadRequest,
				Status:  false,
				Message: "query until: " + err.Error(),
			})
		}
		query["until"] = temp.Add((time.Hour * time.Duration(25-timezone)))
	}

	result := controller.SocialMediaService.FindHistoricalById(id, query)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    result,
	})
}
