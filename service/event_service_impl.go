package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type EventServiceImpl struct {
	EventRepository repository.EventRepository
	Validate        *validator.Validate
}

type EventService interface {
	Create(ctx *fiber.Ctx, request web.CreateEventRequest) web.EventResponse
	FindById(eventId string) web.EventResponse
	FindAll(page, limit, status string) []web.EventResponse
	Update(ctx *fiber.Ctx, eventId string, request web.UpdateEventRequest) web.EventResponse
	UpdateStatus(ctx *fiber.Ctx, eventId string, request web.UpdateEventStatusRequest) web.EventResponse
	Delete(eventId string)

	UploadImageEvent(c *fiber.Ctx) string
	CreateParticipant(ctx *fiber.Ctx, request web.CreateEventParticipantRequest)
	FindDetailByUser(eventId, userId string) web.EventResponseDetailUser
	FindDetailByAdmin(eventId string) web.EventResponseDetailAdmin

	FindSum() int
}

func NewEventService(eventRepository repository.EventRepository, validate *validator.Validate) EventService {
	return &EventServiceImpl{
		EventRepository: eventRepository,
		Validate:        validate,
	}
}

func (service *EventServiceImpl) Create(ctx *fiber.Ctx, request web.CreateEventRequest) web.EventResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	dateStart, err := time.Parse("2006-01-02 15:04:05", request.DateStart)
	fmt.Println(dateStart, request.DateStart)
	helper.PanicIfError(err)

	dateEnd, err := time.Parse("2006-01-02 15:04:05", request.DateEnd)
	helper.PanicIfError(err)

	event := domain.Event{
		ID:            strings.ToLower(randstr.String(10)),
		EventName:     request.EventName,
		Link:          request.Link,
		Description:   request.Description,
		Location:      request.Location,
		DateStart:     dateStart,
		DateEnd:       dateEnd,
		Status:        request.Status,
		ContactPerson: request.ContactPerson,
		CreatedAt:     time.Now(),
		UpdatedAt:     time.Now(),
	}

	linkImage := service.UploadImageEvent(ctx)

	event.ImageUrl = linkImage

	service.EventRepository.Create(event)

	return helper.ToEventResponse(event)
}

func (service *EventServiceImpl) FindById(eventId string) web.EventResponse {
	event, err := service.EventRepository.FindById(eventId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "event not found"))
	}

	return helper.ToEventResponse(event)
}

func (service *EventServiceImpl) FindAll(page, limit, status string) []web.EventResponse {
	events := service.EventRepository.FindAll(page, limit, status)
	return helper.ToEventResponses(events)
}

func (service *EventServiceImpl) UpdateStatus(ctx *fiber.Ctx, eventId string, request web.UpdateEventStatusRequest) web.EventResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	event, err := service.EventRepository.FindById(eventId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "event not found"))
	}

	event.Status = request.Status
	event.UpdatedAt = time.Now()

	service.EventRepository.UpdateStatus(event, eventId)

	return helper.ToEventResponse(event)
}

func (service *EventServiceImpl) Update(ctx *fiber.Ctx, eventId string, request web.UpdateEventRequest) web.EventResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	event, err := service.EventRepository.FindById(eventId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "event not found"))
	}

	_, err = ctx.FormFile("images")
	if err == nil {
		linkImage := service.UploadImageEvent(ctx)
		event.ImageUrl = linkImage
	}

	dateStart, err := time.Parse("2006-01-02 15:04:05", request.DateStart)
	helper.PanicIfError(err)

	dateEnd, err := time.Parse("2006-01-02 15:04:05", request.DateEnd)
	helper.PanicIfError(err)

	event.EventName = request.EventName
	event.Link = request.Link
	event.Description = request.Description
	event.ContactPerson = request.ContactPerson
	event.DateStart = dateStart
	event.DateEnd = dateEnd
	event.Location = request.Location
	event.UpdatedAt = time.Now()

	service.EventRepository.Update(event, eventId)

	return helper.ToEventResponse(event)
}

func (service *EventServiceImpl) Delete(eventId string) {
	event, err := service.EventRepository.FindById(eventId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "event not found"))
	}

	err = service.EventRepository.Delete(eventId)
	helper.PanicIfError(err)
	log.Println("LOG", "deleted event", event.EventName)
}

func (service *EventServiceImpl) UploadImageEvent(c *fiber.Ctx) string {
	ctx := context.Background()

	bucketName := os.Getenv("MINIO_BUCKET_EVENT")
	file, err := c.FormFile("images")
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request, images cant be empty"))
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.jpg", os.Getenv("MINIO_PATH"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	if fileSize > 512000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}

func (service *EventServiceImpl) CreateParticipant(ctx *fiber.Ctx, request web.CreateEventParticipantRequest) {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, err = service.EventRepository.FindById(request.EventId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "event not found"))
	}

	participantResponse := service.EventRepository.IsParticipate(request.EventId, request.UserId)
	if participantResponse.ID != "" {
		panic(exception.NewError(400, "you've been participated"))
	}

	participant := domain.EventParticipant{
		ID:      strings.ToLower(randstr.String(10)),
		UserId:  request.UserId,
		EventId: request.EventId,
	}

	service.EventRepository.CreateParticipant(participant)

}

func (service *EventServiceImpl) FindDetailByUser(eventId, userId string) web.EventResponseDetailUser {
	event, err := service.EventRepository.FindDetailById(eventId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "event not found"))
	}

	isParticipate := false

	eventResponse := helper.ToEventDetailUserResponse(event)
	participate := service.EventRepository.IsParticipate(eventId, userId)
	if participate.ID != "" {
		isParticipate = true
	}
	eventResponse.IsParticipate = &isParticipate

	return eventResponse
}

func (service *EventServiceImpl) FindDetailByAdmin(eventId string) web.EventResponseDetailAdmin {
	event, err := service.EventRepository.FindDetailById(eventId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "event not found"))
	}

	return helper.ToEventDetailAdminResponse(event)
}

func (service *EventServiceImpl) FindSum() int {
	return service.EventRepository.FindSum()
}
