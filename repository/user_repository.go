package repository

import (
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type UserRepositoryImpl struct {
	Collection           *mongo.Collection
	CollectionRespondent *mongo.Collection
}

type UserRepository interface {
	Create(user domain.User)
	FindById(userId string) (domain.User, error)
	FindDetailById(userId string) (domain.User, error)
	FindAll(page, limit, level, districtId, searchName string) []domain.User
	FindByQuery(query, value string) (domain.User, error)
	Update(user domain.User, userId string)
	Delete(userId string) error

	CreateRespondent(user domain.User)
	FindRespondentByQuery(query bson.M) (domain.User, error)

	GetAllCoordinator(page, limit, level, districtId, searchName string) []domain.User
	TotalRelawan(districtId string) int
	TotalKoordinator(districtId string) int
	TotalPemilih() int
	TotalPemilihBaru() int
	GetRankByLevel(page, limit, level string) []domain.User
	UpdatePassword(user domain.User, userId string)
	UpdateOccupation(user domain.User, userId string)

	FindRecruitedUserByUseriD(userId, level string) ([]domain.User, error)

	FindWithAyrshareToken() []domain.UserAyrshare
}

func NewUserRepository(database *mongo.Database) UserRepository {
	return &UserRepositoryImpl{
		Collection:           database.Collection("users"),
		CollectionRespondent: database.Collection("respondent_user"),
	}
}

func (repository *UserRepositoryImpl) Create(user domain.User) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":           user.Id,
		"occupation_id": user.OccupationId,
		"nik":           user.NIK,
		"name":          user.Name,
		"email":         user.Email,
		"phone":         user.Phone,
		"gender":        user.Gender,
		"district_id":   user.DistrictId,
		"longitude":     user.Longitude,
		"latitude":      user.Latitude,
		"password":      user.Password,
		"status":        &user.Status,
		"referral_code": user.ReferralCode,
		"created_at":    user.CreatedAt,
		"updated_at":    user.UpdatedAt,
		"accesses":      user.Accesses,
	})

	helper.PanicIfError(err)
}

func (repository *UserRepositoryImpl) FindById(userId string) (domain.User, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var survey domain.User
	query := bson.M{"_id": userId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&survey)

	return survey, result.Err()
}

func (repository *UserRepositoryImpl) FindDetailById(userId string) (domain.User, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: userId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruitment"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruitment"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage2, unwindStage2})
	helper.PanicIfError(err)

	var user domain.User
	result.Next(ctx)
	result.Decode(&user)

	return user, result.Err()
}

func (repository *UserRepositoryImpl) FindAll(page, limit, level, districtId, searchName string) []domain.User {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage}

	helper.QueryUser(level, districtId, searchName, &pipeline)

	// check page & limit
	helper.PaggingAggregat(page, limit, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var users []domain.User

	for result.Next(ctx) {
		var user domain.User
		result.Decode(&user)
		users = append(users, user)
	}

	return users
}

func (repository *UserRepositoryImpl) FindByQuery(query, value string) (domain.User, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var user domain.User
	key := query
	find := bson.M{key: value}

	result := repository.Collection.FindOne(ctx, find)
	result.Decode(&user)

	return user, nil
}

func (repository *UserRepositoryImpl) Update(user domain.User, userId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": userId}
	update := bson.M{
		"$set": bson.M{
			"name":        user.Name,
			"email":       user.Email,
			"phone":       user.Phone,
			"updated_at":  user.UpdatedAt,
			"district_id": user.DistrictId,
			"longitude":   user.Longitude,
			"latitude":    user.Latitude,
			"gender":      user.Gender,
			"status":      &user.Status,
			"accesses":    user.Accesses,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *UserRepositoryImpl) Delete(userId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": userId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *UserRepositoryImpl) CreateRespondent(user domain.User) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.CollectionRespondent.InsertOne(ctx, bson.M{
		"_id":        user.Id,
		"name":       user.Name,
		"email":      user.Email,
		"phone":      user.Phone,
		"gender":     user.Gender,
		"created_at": user.CreatedAt,
	})

	helper.PanicIfError(err)
}

func (repository *UserRepositoryImpl) FindRespondentByQuery(query bson.M) (domain.User, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var user domain.User
	find := query

	result := repository.CollectionRespondent.FindOne(ctx, find)
	result.Decode(&user)

	return user, nil
}

func (repository *UserRepositoryImpl) GetAllCoordinator(page, limit, level, districtId, searchName string) []domain.User {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "occupation.level", Value: 2}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruitment"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruitment"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "recruitment.recruit_progress", Value: -1}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, matchStage, lookupStage2, unwindStage2, sortStage}

	helper.QueryUser(level, districtId, searchName, &pipeline)

	// check page & limit
	helper.PaggingAggregat(page, limit, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var users []domain.User

	for result.Next(ctx) {
		var user domain.User
		result.Decode(&user)
		users = append(users, user)
	}

	return users
}

func (repository *UserRepositoryImpl) TotalRelawan(districtId string) int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "occupation.level", Value: 3}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, matchStage}

	helper.QueryFilterDistrict(districtId, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)

	helper.PanicIfError(err)

	total := 0

	for result.Next(ctx) {
		total = total + 1
	}

	return total
}

func (repository *UserRepositoryImpl) TotalKoordinator(districtId string) int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "occupation.level", Value: 2}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, matchStage}

	helper.QueryFilterDistrict(districtId, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)

	helper.PanicIfError(err)

	total := 0

	for result.Next(ctx) {
		total = total + 1
	}

	return total
}

func (repository *UserRepositoryImpl) TotalPemilih() int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "occupation.level", Value: 4}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, matchStage}
	result, err := repository.Collection.Aggregate(ctx, pipeline)

	helper.PanicIfError(err)

	total := 0

	for result.Next(ctx) {
		total = total + 1
	}

	return total
}

func (repository *UserRepositoryImpl) TotalPemilihBaru() int {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lte := time.Now()
	gte := lte.Add(-168 * time.Hour)

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "occupation.level", Value: 4}}}}
	matchStage2 := bson.D{{Key: "$match", Value: bson.D{{Key: "created_at", Value: bson.D{{Key: "$lte", Value: primitive.NewDateTimeFromTime(lte)}, {Key: "$gte", Value: primitive.NewDateTimeFromTime(gte)}}}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, matchStage, matchStage2}
	result, err := repository.Collection.Aggregate(ctx, pipeline)

	helper.PanicIfError(err)

	total := 0

	for result.Next(ctx) {
		total = total + 1
	}

	return total
}

func (repository *UserRepositoryImpl) GetRankByLevel(page, limit, level string) []domain.User {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	intLevel, _ := strconv.ParseInt(level, 10, 32)
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: false}}}}
	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "occupation.level", Value: intLevel}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruitment"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruitment"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "recruitment.recruit_progress", Value: -1}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, matchStage, lookupStage2, unwindStage2, sortStage}

	// check page & limit
	helper.PaggingAggregat(page, limit, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var users []domain.User

	for result.Next(ctx) {
		var user domain.User
		result.Decode(&user)
		users = append(users, user)
	}

	return users
}

func (repository *UserRepositoryImpl) UpdatePassword(user domain.User, userId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": userId}
	update := bson.M{
		"$set": bson.M{
			"password": user.Password,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *UserRepositoryImpl) UpdateOccupation(user domain.User, userId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": userId}
	update := bson.M{
		"$set": bson.M{
			"occupation_id": user.OccupationId,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *UserRepositoryImpl) FindRecruitedUserByUseriD(userId, level string) ([]domain.User, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: userId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruitment"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruitment"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "recruited_user"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "recruiter_id"}, {Key: "as", Value: "recruited"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruited"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage4 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "recruited.user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage4 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage5 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "user.occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user.occupation"}}}}
	unwindStage5 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user.occupation"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	lookupStage6 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "user._id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruitment2"}}}}
	unwindStage6 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruitment2"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage7 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "recruited_user"}, {Key: "localField", Value: "user._id"}, {Key: "foreignField", Value: "recruiter_id"}, {Key: "as", Value: "recruited2"}}}}
	unwindStage7 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruited2"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage8 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "recruited2.user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user2"}}}}
	unwindStage8 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user2"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage9 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "user2.occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user2.occupation"}}}}
	unwindStage9 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user2.occupation"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	lookupStage10 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "user2._id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruitment3"}}}}
	unwindStage10 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruitment3"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage11 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "recruited_user"}, {Key: "localField", Value: "user2._id"}, {Key: "foreignField", Value: "recruiter_id"}, {Key: "as", Value: "recruited3"}}}}
	unwindStage11 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruited3"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage12 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "recruited3.user_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user3"}}}}
	unwindStage12 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user3"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage13 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "user3.occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user3.occupation"}}}}
	unwindStage13 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user3.occupation"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "user", Value: bson.D{{Key: "$addToSet", Value: "$user"}}},
		{Key: "user2", Value: bson.D{{Key: "$addToSet", Value: "$user2"}}},
		{Key: "user3", Value: bson.D{{Key: "$addToSet", Value: "$user3"}}},
	}}}

	arrusr := []string{"$user", "$user2", "$user3"}

	projectStage := bson.D{{Key: "$project", Value: bson.D{{Key: "user", Value: bson.D{{Key: "$concatArrays", Value: arrusr}}}}}}

	unwindproject := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	groupStage1 := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$user._id"},
		{Key: "nik", Value: bson.D{{Key: "$first", Value: "$user.nik"}}},
		{Key: "name", Value: bson.D{{Key: "$first", Value: "$user.name"}}},
		{Key: "email", Value: bson.D{{Key: "$first", Value: "$user.email"}}},
		{Key: "phone", Value: bson.D{{Key: "$first", Value: "$user.gender"}}},
		{Key: "gender", Value: bson.D{{Key: "$first", Value: "$user.gender"}}},
		{Key: "district_id", Value: bson.D{{Key: "$first", Value: "$user.district_id"}}},
		{Key: "longitude", Value: bson.D{{Key: "$first", Value: "$user.longitude"}}},
		{Key: "latitude", Value: bson.D{{Key: "$first", Value: "$user.latitude"}}},
		{Key: "referral_code", Value: bson.D{{Key: "$first", Value: "$user.referral_code"}}},
		{Key: "profile_image", Value: bson.D{{Key: "$first", Value: "$user.profile_image"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$user.created_at"}}},
		{Key: "updated_at", Value: bson.D{{Key: "$first", Value: "$user.updated_at"}}},
		{Key: "occupation", Value: bson.D{{Key: "$first", Value: "$user.occupation"}}},
	}}}

	sortStage1 := bson.D{{Key: "$sort", Value: bson.D{{Key: "created_at", Value: -1}}}}

	pipeline := mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage2, unwindStage2, lookupStage3, unwindStage3, lookupStage4, unwindStage4, lookupStage5, unwindStage5,
		lookupStage6, unwindStage6, lookupStage7, unwindStage7, lookupStage8, unwindStage8, lookupStage9, unwindStage9, lookupStage10, unwindStage10, lookupStage11, unwindStage11,
		lookupStage12, unwindStage12, lookupStage13, unwindStage13, groupStage, projectStage, unwindproject, groupStage1, sortStage1}

	helper.QueryUser(level, "", "", &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var users []domain.User

	for result.Next(ctx) {
		var user domain.User
		result.Decode(&user)
		users = append(users, user)
	}

	return users, result.Err()
}

func (repository *UserRepositoryImpl) FindWithAyrshareToken() []domain.UserAyrshare {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	options := options.Find()
	options.SetSort(bson.D{{Key: "created_at", Value: -1}})

	query := make(map[string]interface{})
	query["ayrshare_token"] = bson.M{
		"$exists": true,
	}
	var cursor *mongo.Cursor
	var err error
	cursor, err = repository.Collection.Find(ctx, query, options)
	helper.PanicIfError(err)

	var users []domain.UserAyrshare

	for cursor.Next(ctx) {
		var user domain.UserAyrshare
		cursor.Decode(&user)
		users = append(users, user)
	}

	return users
}
