package web

type QuestionResponse struct {
	ID              string                 `json:"id"`
	QuestionNumber  int                    `json:"question_number"`
	SurveyId        string                 `json:"survey_id"`
	Section         string                 `json:"section"`
	InputType       string                 `json:"input_type"`
	QuestionName    string                 `json:"question_name"`
	QuestionSubject string                 `json:"question_subject"`
	Options         []SurveyOptionResponse `json:"options"`
}
