package web

type AccessCreateRequest struct {
	Label string `json:"label" validate:"required"`
	Value string `json:"value" validate:"required"`
}

type AccessUpdateRequest struct {
	Label string `json:"label" validate:"required"`
	Value string `json:"value" validate:"required"`
}
