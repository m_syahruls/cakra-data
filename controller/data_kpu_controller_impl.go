package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type DataKpuControllerImpl struct {
	DataKpuService service.DataKpuService
}

type DataKpuController interface {
	NewDataKpuRouter(app *fiber.App)
}

func NewDataKpuController(dataKpuService service.DataKpuService) DataKpuController {
	return &DataKpuControllerImpl{
		DataKpuService: dataKpuService,
	}
}

func (controller *DataKpuControllerImpl) NewDataKpuRouter(app *fiber.App) {
	dataKpu := app.Group("/data-kpu")
	dataKpu.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	dataKpu.Get("/", controller.FindAll)
}

func (controller *DataKpuControllerImpl) FindAll(ctx *fiber.Ctx) error {
	regencyid := ctx.Query("regencyid")

	dataKpu := controller.DataKpuService.FindAll(regencyid)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    dataKpu,
	})
}
