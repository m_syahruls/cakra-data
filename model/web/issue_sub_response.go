package web

type IssueSubResponse struct {
	ID       string `json:"id" bson:"_id"`
	IssueID  string `json:"issue_id" bson:"issue_id"`
	Label    string `json:"label" bson:"label"`
	Value    string `json:"value" bson:"value"`
	ImageUrl string `json:"image_url"`
}
