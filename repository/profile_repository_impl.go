package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ProfileRepositoryImpl struct {
	Collection *mongo.Collection
}

type ProfileRepository interface {
	FindById(profileId string) (domain.Profile, error)
	Update(profile domain.Profile, profileId string)
	UpdatePassword(profile domain.Profile, profileId string)
	UpdateProfileImage(profile domain.Profile, profileId string)
	UpdateGoogleAdsID(profile domain.Profile, profileId string)
	UpdateAyrshareToken(profile domain.Profile, profileId string)
	UpdateDetermID(profile domain.Profile, profileId string)
	DeleteGoogleAdsID(profileId string)
	DeleteAyrshareToken(profileId string)
	DeleteDetermID(profileId string)
}

func NewProfileRepository(database *mongo.Database) ProfileRepository {
	return &ProfileRepositoryImpl{
		Collection: database.Collection("users"),
	}
}

func (repository *ProfileRepositoryImpl) FindById(profileId string) (domain.Profile, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: profileId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "occupations"}, {Key: "localField", Value: "occupation_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "occupation"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$occupation"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "user_recruits"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "recruitment"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$recruitment"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_surveys"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "user_id"}, {Key: "as", Value: "survey"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$survey"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage2, unwindStage2, lookupStage3, unwindStage3})
	helper.PanicIfError(err)

	var profile domain.Profile
	result.Next(ctx)
	result.Decode(&profile)

	return profile, result.Err()
}

func (repository *ProfileRepositoryImpl) Update(profile domain.Profile, profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"name":       profile.Name,
			"email":      profile.Email,
			"phone":      profile.Phone,
			"updated_at": profile.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) UpdatePassword(profile domain.Profile, profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"password": profile.Password,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) UpdateProfileImage(profile domain.Profile, profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"profile_image": profile.ProfileImage,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) UpdateGoogleAdsID(profile domain.Profile, profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"google_ads_id": profile.GoogleAdsID,
			"updated_at":    profile.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) UpdateAyrshareToken(profile domain.Profile, profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"ayrshare_token": profile.AyrshareToken,
			"updated_at":     profile.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) UpdateDetermID(profile domain.Profile, profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"determ_id":  profile.DetermID,
			"updated_at": profile.UpdatedAt,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) DeleteGoogleAdsID(profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"google_ads_id": "",
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) DeleteAyrshareToken(profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"ayrshare_token": "",
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *ProfileRepositoryImpl) DeleteDetermID(profileId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": profileId}
	update := bson.M{
		"$set": bson.M{
			"determ_id": "",
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}
