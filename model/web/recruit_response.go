package web

import "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"

type UserRecruitResponse struct {
	RecruitProgress int `json:"progess"`
	RecruitGoal     int `json:"goal"`
}

type RecruitDetailUserResponse struct {
	Name            string                   `json:"name"`
	RecruitProgress int                      `json:"progess"`
	RecruitGoal     int                      `json:"goal"`
	RecrutedUser    []UserForRecruitResponse `json:"recruted_user"`
}

type UserForRecruitResponse struct {
	ID          string             `json:"id"`
	Name        string             `json:"name"`
	Email       string             `json:"email"`
	Nik         string             `json:"nik" bson:"nik"`
	Phone       string             `json:"phone" bson:"phone"`
	Recruitment domain.UserRecruit `json:"recruitment"`
}

type UserRecruitDetail struct {
	ID              string                   `json:"id" bson:"_id"`
	RecruitProgress int                      `json:"recruit_progess" bson:"recruit_progress"`
	RecruitGoal     int                      `json:"recruit_goal" bson:"recruit_goal"`
	Name            string                   `json:"name" bson:"name"`
	RecruitedUser   []UserForRecruitResponse `json:"recruited_user" bson:"recruited_user"`
}
