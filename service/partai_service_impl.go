package service

import (
	"strings"

	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type PartaiServiceImpl struct {
	PartaiRepository repository.PartaiRepository
}

type PartaiService interface {
	Create(partai web.Partai) domain.Partai
	FindAll(page, limit string) []domain.Partai
	FindById(partaiId string) domain.Partai
}

func NewPartaiService(partaiRepository repository.PartaiRepository) PartaiService {
	return &PartaiServiceImpl{
		PartaiRepository: partaiRepository,
	}
}

func (service *PartaiServiceImpl) Create(partai web.Partai) domain.Partai {

	dataPartai := domain.Partai{
		Id:   strings.ToLower(randstr.String(10)),
		Name: partai.Name,
		Slug: partai.Slug,
	}
	service.PartaiRepository.Create(dataPartai)

	return dataPartai
}

func (service *PartaiServiceImpl) FindAll(page, limit string) []domain.Partai {

	partai := service.PartaiRepository.FindAll(page, limit)

	return partai
}

func (service *PartaiServiceImpl) FindById(partaiId string) domain.Partai {

	electionType := service.PartaiRepository.FindById(partaiId)

	return electionType
}
