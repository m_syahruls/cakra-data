package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type ComplaintServiceImpl struct {
	ComplaintRepository repository.ComplaintRepository
	Validate            *validator.Validate
}

type ComplaintService interface {
	CreateComplaint(ctx *fiber.Ctx, complaintReq web.ComplaintCreate) web.ComplaintResponse
	FindAllComplaint(userid, complaintStatusId, categoryId, page, limit string) []web.ComplaintResponse
	FindDetailByComplaintId(ctx *fiber.Ctx, complaintId string) web.ComplaintResponse
	UpdateByComplaintId(ctx *fiber.Ctx, complaintId string, request web.ComplaintUpdate) web.ComplaintResponse
	UpdateReadStatusComplaintId(ctx *fiber.Ctx, complaintId string, request web.ComplaintUpdateStatus) web.ComplaintResponse
	UpdateComplaintStatusByComplaintId(ctx *fiber.Ctx, complaintId string, request web.UpdateComplaintStatus) web.ComplaintResponse
	DeleteComplaint(complaintId string)

	CreateComplaintChat(ctx *fiber.Ctx, request web.ComplaintChat) web.ComplaintChatResponse
	FindChatByComplaintId(ctx *fiber.Ctx, complaintId, page, limit string) []web.ComplaintChatResponse
	UpdateChatByComplaintId(ctx *fiber.Ctx, complaintChatId string, request web.ComplaintChatUpdate)
	DeleteComplaintChat(complaintChatId string)

	UploadImage(ctx *fiber.Ctx) string
	FindAllComplaintWithLastChat(userid, complaintStatusId, categoryId, page, limit string) []web.ComplaintResponseWithLastChat

	CreateComplaintStatus(ctx *fiber.Ctx, request web.ComplaintStatus) web.ComplaintStatus
	FindAllComplaintStatus(page, limit string) []domain.ComplaintStatus
	UpdateComplaintStatusName(ctx *fiber.Ctx, complaintStatusId string, request web.UpdateComplaintStatusName) domain.ComplaintStatus

	CreateComplaintCategory(ctx *fiber.Ctx, request web.ComplaintCategory) web.ComplaintCategory
	FindAllComplaintCategory(page, limit string) []domain.ComplaintCategory
}

func NewComplaintService(complaintRepository repository.ComplaintRepository, validate *validator.Validate) ComplaintService {
	return &ComplaintServiceImpl{
		ComplaintRepository: complaintRepository,
		Validate:            validate,
	}
}

func (service *ComplaintServiceImpl) CreateComplaint(ctx *fiber.Ctx, request web.ComplaintCreate) web.ComplaintResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, err = service.ComplaintRepository.FindComplaintCategoryById(request.CategoryId)
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "category not found"))
	}

	linkImage := service.UploadImage(ctx)

	dataComplaint := domain.Complaint{
		ID:                strings.ToLower(randstr.String(10)),
		SenderId:          request.SenderId,
		Title:             request.Title,
		Content:           request.Content,
		LinkImage:         linkImage,
		ComplaintStatusId: "0",
		CategoryId:        request.CategoryId,
		Address:           request.Address,
		Longitude:         request.Longitude,
		Latitude:          request.Latitude,
		ReadStatus:        "0",
		CreatedAt:         time.Now(),
	}
	service.ComplaintRepository.CreateComplaint(dataComplaint)

	if helper.WsStatus == "on" {
		// publish to websocket
		channelWsLoc := fmt.Sprintf("%s/%s", helper.WsChannelStart, dataComplaint.ID)
		defer helper.PublishToWebSocket(dataComplaint, channelWsLoc, helper.WsMethod)
	}

	return helper.ToComplaintResponse(dataComplaint)
}

func (service *ComplaintServiceImpl) FindAllComplaint(userid, complaintStatusId, categoryId, page, limit string) []web.ComplaintResponse {

	complaint := service.ComplaintRepository.FindAllComplaint(userid, complaintStatusId, categoryId, page, limit)

	return helper.ToComplaintResultResponses(complaint)
}

func (service *ComplaintServiceImpl) FindAllComplaintWithLastChat(userid, complaintStatusId, categoryId, page, limit string) []web.ComplaintResponseWithLastChat {

	complaint := service.ComplaintRepository.FindAllComplaintWithLastChat(userid, complaintStatusId, categoryId, page, limit)

	return helper.ToComplaintWithLastChatResultResponses(complaint)
}

func (service *ComplaintServiceImpl) FindDetailByComplaintId(ctx *fiber.Ctx, complaintId string) web.ComplaintResponse {
	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindDetailComplaintById(complaintId)

	// check if not admin and not sender of complaint cant see complaint
	if complaint.SenderId != userid && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to complaint"))
	}

	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}

	return helper.ToComplaintResponse(complaint)
}

func (service *ComplaintServiceImpl) UpdateByComplaintId(ctx *fiber.Ctx, complaintId string, request web.ComplaintUpdate) web.ComplaintResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindComplaintById(complaintId)

	// check if not admin and not sender of complaint cant update complaint
	if complaint.SenderId != userid && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to update complaint"))
	}

	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}

	complaint.Title = request.Title
	complaint.Content = request.Content
	complaint.UpdatedAt = time.Now()

	service.ComplaintRepository.UpdateComplaint(complaint)

	return helper.ToComplaintResponse(complaint)
}

func (service *ComplaintServiceImpl) UpdateReadStatusComplaintId(ctx *fiber.Ctx, complaintId string, request web.ComplaintUpdateStatus) web.ComplaintResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindComplaintById(complaintId)

	// check if not admin cant update complaint status
	if level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to update complaint"))
	}

	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}

	complaint.ReadStatus = request.ReadStatus

	service.ComplaintRepository.UpdateReadStatusComplaint(complaint)

	return helper.ToComplaintResponse(complaint)
}

func (service *ComplaintServiceImpl) UpdateComplaintStatusByComplaintId(ctx *fiber.Ctx, complaintId string, request web.UpdateComplaintStatus) web.ComplaintResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindComplaintById(complaintId)

	// check if not admin cant update complaint status
	if level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to update complaint"))
	}

	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}

	complaint.ComplaintStatusId = request.ComplaintStatusId
	complaint.ComplaintStatusDesc = request.ComplaintStatusDesc

	service.ComplaintRepository.UpdateComplaintStatus(complaint)

	return helper.ToComplaintResponse(complaint)
}

func (service *ComplaintServiceImpl) DeleteComplaint(complaintId string) {
	complaint := service.ComplaintRepository.FindComplaintById(complaintId)
	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}
	err := service.ComplaintRepository.DeleteComplaint(complaintId)
	helper.PanicIfError(err)
}

// Chat
func (service *ComplaintServiceImpl) CreateComplaintChat(ctx *fiber.Ctx, request web.ComplaintChat) web.ComplaintChatResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindComplaintById(request.ComplaintId)

	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}

	// check if not admin and not sender of complaint cant see complaint chat
	if complaint.SenderId != userid && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to send message chat"))
	}

	var linkImage string

	_, err = ctx.FormFile("images")

	if err == nil {
		linkImage = service.UploadImage(ctx)
	} else if err.Error() != "there is no uploaded file associated with the given key" {
		panic(exception.NewError(fiber.StatusInternalServerError, err.Error()))
	}

	if request.Message == "" && linkImage == "" {
		panic(exception.NewError(fiber.StatusBadRequest, "should have message or image"))
	}

	dataComplaint := domain.ComplaintChat{
		ID:          strings.ToLower(randstr.String(10)),
		ComplaintId: request.ComplaintId,
		SenderId:    request.SenderId,
		Message:     request.Message,
		LinkImage:   linkImage,
		Longitude:   request.Longitude,
		Latitude:    request.Latitude,
		ReadStatus:  "0",
		CreatedAt:   time.Now(),
	}

	service.ComplaintRepository.CreateComplaintChat(dataComplaint)

	if helper.WsStatus == "on" {
		// publish to websocket
		channelWsLoc := fmt.Sprintf("%s/%s/%s", helper.WsChannelStart, dataComplaint.ComplaintId, helper.WsChannelEndChat)
		defer helper.PublishToWebSocket(dataComplaint, channelWsLoc, helper.WsMethod)
	}

	return helper.ToComplaintChatResponse(dataComplaint)
}

func (service *ComplaintServiceImpl) FindChatByComplaintId(ctx *fiber.Ctx, complaintId, page, limit string) []web.ComplaintChatResponse {
	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindComplaintById(complaintId)

	// check if not admin and not sender of complaint cant see complaint chat
	if complaint.SenderId != userid && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to see complaint chat"))
	}

	service.ComplaintRepository.UpdateReadStatusAllComplaintChat(userid, complaintId)
	complaintChat := service.ComplaintRepository.FindChatByComplaintId(complaintId, page, limit)

	return helper.ToComplaintChatResultResponses(complaintChat)
}

func (service *ComplaintServiceImpl) UpdateChatByComplaintId(ctx *fiber.Ctx, complaintChatId string, request web.ComplaintChatUpdate) {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindComplaintChatById(complaintChatId)

	// check if not admin and not sender of complaint cant update complaint
	if complaint.SenderId != userid && level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to update complaint chat"))
	}

	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}

	complaint.Message = request.Message
	complaint.UpdatedAt = time.Now()

	service.ComplaintRepository.UpdateComplaintChat(complaint)
}

func (service *ComplaintServiceImpl) DeleteComplaintChat(complaintChatId string) {
	complaint := service.ComplaintRepository.FindComplaintChatById(complaintChatId)
	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint not found"))
	}
	err := service.ComplaintRepository.DeleteComplaintChat(complaintChatId)
	helper.PanicIfError(err)
}

func (service *ComplaintServiceImpl) UploadImage(c *fiber.Ctx) string {
	ctx := context.Background()

	bucketName := os.Getenv("MINIO_BUCKET_COMPLAINT")
	file, err := c.FormFile("images")
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.jpg", os.Getenv("MINIO_PATH"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	if fileSize > 512000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}

// Complaint Status
func (service *ComplaintServiceImpl) CreateComplaintStatus(ctx *fiber.Ctx, request web.ComplaintStatus) web.ComplaintStatus {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	complaintStatus := domain.ComplaintStatus{
		ID:         request.ID,
		StatusName: request.StatusName,
	}
	service.ComplaintRepository.CreateComplaintStatus(complaintStatus)

	return request
}

func (service *ComplaintServiceImpl) UpdateComplaintStatusName(ctx *fiber.Ctx, complaintStatusId string, request web.UpdateComplaintStatusName) domain.ComplaintStatus {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, level, _ := helper.ParseJwt(ctx.Cookies("token"))
	complaint := service.ComplaintRepository.FindComplaintStatusById(complaintStatusId)

	// check if not admin cant update complaint status
	if level != "1" {
		panic(exception.NewError(fiber.StatusUnauthorized, "unauthorize to update complaint"))
	}

	if complaint.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "complaint status not found"))
	}

	complaint.ID = complaintStatusId
	complaint.StatusName = request.StatusName

	service.ComplaintRepository.UpdateStatusNameComplaint(complaint)

	return complaint
}

func (service *ComplaintServiceImpl) FindAllComplaintStatus(page, limit string) []domain.ComplaintStatus {

	complaintStatus := service.ComplaintRepository.FindAllComplaintStatus(page, limit)

	return complaintStatus
}

// Complaint Category
func (service *ComplaintServiceImpl) CreateComplaintCategory(ctx *fiber.Ctx, request web.ComplaintCategory) web.ComplaintCategory {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	complaintCategory := domain.ComplaintCategory{
		ID:           request.ID,
		CategoryName: request.CategoryName,
	}
	service.ComplaintRepository.CreateComplaintCategory(complaintCategory)

	return request
}

func (service *ComplaintServiceImpl) FindAllComplaintCategory(page, limit string) []domain.ComplaintCategory {

	complaintCategory := service.ComplaintRepository.FindAllComplaintCategory(page, limit)

	return complaintCategory
}
