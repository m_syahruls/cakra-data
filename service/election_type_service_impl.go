package service

import (
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type ElectionTypeServiceImpl struct {
	ElectionTypeRepository repository.ElectionTypeRepository
}

type ElectionTypeService interface {
	FindAll(page, limit string) []domain.ElectionType
	FindById(electionTypeId string) domain.ElectionType
}

func NewElectionTypeService(electionTypeRepository repository.ElectionTypeRepository) ElectionTypeService {
	return &ElectionTypeServiceImpl{
		ElectionTypeRepository: electionTypeRepository,
	}
}

func (service *ElectionTypeServiceImpl) FindAll(page, limit string) []domain.ElectionType {

	electionType := service.ElectionTypeRepository.FindAll(page, limit)

	return electionType
}

func (service *ElectionTypeServiceImpl) FindById(electionTypeId string) domain.ElectionType {

	electionType := service.ElectionTypeRepository.FindById(electionTypeId)

	return electionType
}
