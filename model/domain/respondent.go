package domain

import "time"

type Respondent struct {
	Id        string    `json:"id" bson:"_id"`
	Name      string    `json:"name" bson:"name"`
	Email     string    `json:"email" bson:"email"`
	Phone     string    `json:"phone" bson:"phone"`
	Gender    string    `json:"gender" bson:"gender"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
}

type UserProgressDetail struct {
	ID              string `json:"id" bson:"_id"`
	SurveyProgress int    `json:"survey_progess" bson:"survey_progress"`
	SurveyGoal     int    `json:"survey_goal" bson:"survey_goal"`
	User            User   `json:"user" bson:"user"`
	RecruitedUser   []Respondent `json:"respondent_user" bson:"respondent_user"`
}