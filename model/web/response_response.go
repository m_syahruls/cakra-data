package web

import (
	"time"

	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type DetailResponseResponse struct {
	ID           string                   `json:"id"`
	SurveyId     string                   `json:"survey_id"`
	SurveyName   string                   `json:"survey_name" bson:"survey_name"`
	Respondent   string                   `json:"respondent"`
	RespondentId string                   `json:"respondent_id"`
	Recruiter    string                   `json:"recruiter"`
	RecruiterId  string                   `json:"recruiter_id"`
	CreatedAt    time.Time                `json:"created_at"`
	Answers      []ResponseAnswerResponse `json:"answers"`
	Location     interface{}              `json:"location"`
}

type ResponseResponse struct {
	ID           string      `json:"id"`
	SurveyId     string      `json:"survey_id"`
	SurveyName   string      `json:"survey_name" bson:"survey_name"`
	Respondent   string      `json:"respondent"`
	RespondentId string      `json:"respondent_id"`
	Recruiter    string      `json:"recruiter"`
	RecruiterId  string      `json:"recruiter_id"`
	CreatedAt    time.Time   `json:"created_at"`
	Location     interface{} `json:"location"`
}

type ResponseAnswerResponse struct {
	ID            string                   `json:"id"`
	ResponseId    string                   `json:"response_id"`
	QuestionId    string                   `json:"question_id"`
	OptionsId     string                   `json:"option_id"`
	AnswerText    string                   `json:"answer_text"`
	AnswerNumeric int32                    `json:"answer_numeric"`
	Question      ResponseQuestionResponse `json:"question"`
	Answer        interface{}              `json:"answer"`
	//Options       interface{}              `json:"option"`
}

type ResponseQuestionResponse struct {
	ID              string `json:"id"`
	QuestionNumber  int    `json:"question_number"`
	SurveyId        string `json:"survey_id"`
	Section         string `json:"section"`
	InputType       string `json:"input_type"`
	QuestionName    string `json:"question_name"`
	QuestionSubject string `json:"question_subject"`
}

type ResponseOptionResponse struct {
	ID         string `json:"id"`
	QuestionId string `json:"question_id"`
	OptionName string `json:"option_name"`
	Value      int    `json:"value"`
	Color      string `json:"color"`
}

type ResponseBySurveyor struct {
	Id            string                 `json:"id" bson:"_id"`
	SurveyName    string                 `json:"survey_name" bson:"survey_name"`
	RecruiterName string                 `json:"recruiter_name" bson:"recruiter_name"`
	Response      []domain.ResponsesUser `json:"response" bson:"response"`
}

type ResponseQuestionByRegency struct {
	QuestionID      string                       `json:"question_id"`
	QuestionName    string                       `json:"question_name"`
	SurveyId        string                       `json:"survey_id"`
	TotalRespondent int                          `json:"total_respondent"`
	Options         []string                     `json:"options" bson:"options"`
	Color           []string                     `json:"color"`
	Responses       []ResponseQuestionByLocation `json:"responses"`
}

type ResponseQuestionByLocation struct {
	VillageId     string        `json:"village_id"`
	Count         []interface{} `json:"count"`
	DistrictId    string        `json:"district_id"`
	DistrictCount []interface{} `json:"district_count"`
}
