#!/bin/bash
export APP_NAME=$1
export APP_VERSION=$2
export SERVICE_NAME=$3
export GIT_BRANCH=$4
export GIT_COMMIT_SHA=$( git rev-parse --short HEAD )
export DOCKER_IMG_NAME=$APP_NAME
export DOCKER_PROJECT_NAME="$APP_NAME"-"$SERVICE_NAME"

if [ $# -lt 3 ]; then
    echo "Usage: <app name> <app version> <service name> <branch name>"
    exit 1
fi

echo "Deploying $APP_NAME:$APP_VERSION-$SERVICE_NAME-svc for branch $GIT_BRANCH"

cd ci/docker
if [ $GIT_BRANCH == "development" ]; then
    cp -f .env.development .env
    export DOCKER_IMG_TAG="$GIT_COMMIT_SHA"-"$SERVICE_NAME"-svc
    docker compose -p $DOCKER_PROJECT_NAME -f docker-compose.dev.yml down && \
        docker compose -p $DOCKER_PROJECT_NAME -f docker-compose.dev.yml up -d --remove-orphans
fi

if [ $GIT_BRANCH == "staging" ]; then
    cp -f .env.staging .env
    export DOCKER_IMG_TAG="$APP_VERSION"-"$SERVICE_NAME"-staging-svc
    docker compose -p $DOCKER_PROJECT_NAME -f docker-compose.staging.yml down && \
        docker compose -p $DOCKER_PROJECT_NAME -f docker-compose.staging.yml up -d
fi

if [ $GIT_BRANCH == "master" ]; then
    cp -f .env.production .env
    export DOCKER_IMG_TAG="$APP_VERSION"-"$SERVICE_NAME"-svc
    docker compose -p $DOCKER_PROJECT_NAME -f docker-compose.prod.yml down && \
        docker compose -p $DOCKER_PROJECT_NAME -f docker-compose.prod.yml up -d
fi
