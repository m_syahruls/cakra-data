package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type EventControllerImpl struct {
	EventService service.EventService
	LogService   service.LogService
}

type EventController interface {
	NewEventRouter(app *fiber.App)
}

func NewEventController(eventService service.EventService, logService service.LogService) EventController {
	return &EventControllerImpl{
		EventService: eventService,
		LogService:   logService,
	}
}

func (controller *EventControllerImpl) NewEventRouter(app *fiber.App) {
	event := app.Group("/events")
	event.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})
	event.Post("/", controller.Create)
	event.Get("/", controller.FindAll)
	event.Get("/:eventid", controller.FindById)
	event.Put("/:eventid", controller.Update)
	event.Put("/change-status/:eventid", controller.UpdateStatus)
	event.Delete("/:eventid", controller.Delete)

	event.Post("/join/:eventid", controller.JoinEvent)
}

func (controller *EventControllerImpl) Create(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can create event")
	}

	var request web.CreateEventRequest
	request.EventName = ctx.FormValue("event_name")
	request.Link = ctx.FormValue("link")
	request.Description = ctx.FormValue("description")
	request.ContactPerson = ctx.FormValue("contact_person")
	request.Location = ctx.FormValue("location")
	request.DateEnd = ctx.FormValue("date_end")
	request.DateStart = ctx.FormValue("date_start")

	f := false
	t := true

	stat := ctx.FormValue("status")
	if stat == "false" {
		request.Status = &f
	} else {
		request.Status = &t
	}

	eventResponse := controller.EventService.Create(ctx, request)

	action := fmt.Sprintf("create event %s", eventResponse.EventName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    eventResponse,
	})
}

func (controller *EventControllerImpl) FindById(ctx *fiber.Ctx) error {
	eventId := ctx.Params("eventid")

	userId, level, _ := helper.ParseJwt(ctx.Cookies("token"))

	if level == "1" {
		eventResponse := controller.EventService.FindDetailByAdmin(eventId)
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "success",
			Data:    eventResponse,
		})
	} else {
		eventResponse := controller.EventService.FindDetailByUser(eventId, userId)
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "success",
			Data:    eventResponse,
		})
	}
}

func (controller *EventControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	status := ctx.Query("status")
	eventResponse := controller.EventService.FindAll(page, limit, status)

	sum := controller.EventService.FindSum()

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponseAll{
		Code:         fiber.StatusOK,
		Status:       true,
		Message:      "success",
		TotalAllData: sum,
		Data:         eventResponse,
	})
}

func (controller *EventControllerImpl) Update(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can update event")
	}

	var request web.UpdateEventRequest
	request.EventName = ctx.FormValue("event_name")
	request.Link = ctx.FormValue("link")
	request.Description = ctx.FormValue("description")
	request.ContactPerson = ctx.FormValue("contact_person")
	request.Location = ctx.FormValue("location")
	request.DateEnd = ctx.FormValue("date_end")
	request.DateStart = ctx.FormValue("date_start")

	eventId := ctx.Params("eventid")

	eventResponse := controller.EventService.Update(ctx, eventId, request)

	action := fmt.Sprintf("updated event %s", eventResponse.EventName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    eventResponse,
	})
}

func (controller *EventControllerImpl) UpdateStatus(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can update event")
	}

	var request web.UpdateEventStatusRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	eventId := ctx.Params("eventid")

	eventResponse := controller.EventService.UpdateStatus(ctx, eventId, request)

	action := fmt.Sprintf("updated event %s", eventResponse.EventName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    eventResponse,
	})
}

func (controller *EventControllerImpl) Delete(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can delete event")
	}

	eventId := ctx.Params("eventid")

	controller.EventService.Delete(eventId)

	action := fmt.Sprintf("delete event %s", eventId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

func (controller *EventControllerImpl) JoinEvent(ctx *fiber.Ctx) error {
	var request web.CreateEventParticipantRequest
	eventId := ctx.Params("eventid")

	userid, level, _ := helper.ParseJwt(ctx.Cookies("token"))

	request.UserId = userid
	request.EventId = eventId

	if level == "1" {
		panic(exception.NewError(400, "admin cant join event"))
	}

	controller.EventService.CreateParticipant(ctx, request)

	eventResponse := controller.EventService.FindById(request.EventId)

	action := fmt.Sprintf("%s join event %s", userid, eventResponse.EventName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    eventResponse,
	})
}
