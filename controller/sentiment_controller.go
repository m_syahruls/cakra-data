package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type SentimentControllerImpl struct {
	MediatoolkitService service.MediatoolkitService
	ProfileService      service.ProfileService
}

type SentimentController interface {
	NewSentimentRouter(app *fiber.App)
}

func NewSentimentController(sentimentService service.MediatoolkitService, profileService service.ProfileService) SentimentController {
	return &SentimentControllerImpl{
		MediatoolkitService: sentimentService,
		ProfileService:      profileService,
	}
}

func (controller *SentimentControllerImpl) NewSentimentRouter(app *fiber.App) {
	sentiment := app.Group("/sentiment")
	sentiment.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	sentiment.Get("/", controller.GetGroups)
	sentiment.Get("/commons", controller.GetCommons)
	sentiment.Post("/groups", controller.CreateGroup)
	sentiment.Delete("/groups/:groupId", controller.DeleteGroup)
	sentiment.Post("/groups/:groupId/keyword", controller.CreateTopic)
	sentiment.Post("/reports", controller.PostReport)
	// sentiment.Post("/:orgId/reports/brief", controller.PostReportBrief)
	// sentiment.Post("/:orgId/reports/detail", controller.PostReportDetail)

	// sentiment.Get("/:userid", controller.GetCompleteReport)
	// sentiment.Post("/:userid", controller.Create)
	// sentiment.Put("/:userid", controller.UpdateSummary)
	// sentiment.Get("/:userid/:dataid", controller.FindDatapointById)
	// sentiment.Put("/:userid/:dataid", controller.UpdateDatapointById)
	// sentiment.Delete("/:userid/:dataid", controller.Delete)

}

func (controller *SentimentControllerImpl) GetCommons(ctx *fiber.Ctx) error {
	languages := controller.MediatoolkitService.GetAvailableLanguages().Data
	locations := controller.MediatoolkitService.GetAvailableLocations().Data
	sourceTypes := controller.MediatoolkitService.GetAvailableSourceTypes().Data

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data: web.MTKGetLanguagesLocationsResponse{
			SourceTypes: sourceTypes,
			Languages:   languages,
			Locations:   locations,
		},
	})
}

func (controller *SentimentControllerImpl) CreateGroup(ctx *fiber.Ctx) error {
	var request web.MTKCreateGroupRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	token := ctx.Cookies("token")
	userId, _, _ := helper.ParseJwt(token)
	orgId := controller.ProfileService.FindById(userId).DetermID

	controller.MediatoolkitService.CreateGroup(orgId, request)
	response := controller.MediatoolkitService.GetGroups(orgId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *SentimentControllerImpl) CreateTopic(ctx *fiber.Ctx) error {
	var request web.MTKCreateTopicRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	token := ctx.Cookies("token")
	userId, _, _ := helper.ParseJwt(token)
	orgId := controller.ProfileService.FindById(userId).DetermID
	groupId := ctx.Params("groupId")

	controller.MediatoolkitService.CreateTopic(orgId, groupId, request)
	response := controller.MediatoolkitService.GetGroups(orgId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *SentimentControllerImpl) GetGroups(ctx *fiber.Ctx) error {
	token := ctx.Cookies("token")
	userId, _, _ := helper.ParseJwt(token)
	orgId := controller.ProfileService.FindById(userId).DetermID

	response := controller.MediatoolkitService.GetGroups(orgId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *SentimentControllerImpl) DeleteGroup(ctx *fiber.Ctx) error {
	token := ctx.Cookies("token")
	userId, _, _ := helper.ParseJwt(token)
	orgId := controller.ProfileService.FindById(userId).DetermID

	groupId := ctx.Params("groupId")

	controller.MediatoolkitService.DeleteGroup(orgId, groupId)
	response := controller.MediatoolkitService.GetGroups(orgId)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *SentimentControllerImpl) PostReport(ctx *fiber.Ctx) error {
	var request web.MTKPostReportRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	token := ctx.Cookies("token")
	userId, _, _ := helper.ParseJwt(token)
	orgId := controller.ProfileService.FindById(userId).DetermID

	mentionsOverTime := controller.MediatoolkitService.MentionsOverTime(orgId, request)
	sumOfMentions := controller.MediatoolkitService.SumOfMentions(orgId, request)
	sumOfImpressions := controller.MediatoolkitService.SumOfImpressions(orgId, request)
	sumOfAllSource := controller.MediatoolkitService.SumOfAllSource(orgId, request)
	effectiveSentiment := controller.MediatoolkitService.EffectiveSentiment(orgId, request)
	mentionsOverTimeBySource := controller.MediatoolkitService.MentionsOverTimeBySource(orgId, request)
	sentimentOverTime := controller.MediatoolkitService.SentimentOverTime(orgId, request)
	wordcloud := controller.MediatoolkitService.WordCloud(orgId, request)

	fullResponse := web.MTKPostCompleteResponse{
		MentionsOverTime:         mentionsOverTime,
		SumOfMentions:            sumOfMentions,
		SumOfImpressions:         sumOfImpressions,
		SumOfAllSource:           sumOfAllSource,
		EffectiveSentiment:       effectiveSentiment,
		MentionsOverTimeBySource: mentionsOverTimeBySource,
		SentimentOverTime:        sentimentOverTime,
		WordCloud:                wordcloud,
	}

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    fullResponse,
	})
}

// func (controller *SentimentControllerImpl) PostReportBrief(ctx *fiber.Ctx) error {
// 	var request web.MTKPostReportRequest
// 	err := ctx.BodyParser(&request)
// 	helper.PanicIfError(err)

// 	orgId := ctx.Params("orgId")

// 	response := controller.MediatoolkitService.PostReportBrief(orgId, request)

// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    response,
// 	})
// }

// func (controller *SentimentControllerImpl) PostReportDetail(ctx *fiber.Ctx) error {
// 	var request web.MTKPostReportDetailRequest
// 	err := ctx.BodyParser(&request)
// 	helper.PanicIfError(err)

// 	orgId := ctx.Params("orgId")

// 	response := controller.MediatoolkitService.PostReportDetail(orgId, request)

// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    response,
// 	})
// }

// func (controller *SentimentControllerImpl) Create(ctx *fiber.Ctx) error {
// 	var request web.SentimentCreateRequest
// 	err := ctx.BodyParser(&request)
// 	helper.PanicIfError(err)

// 	userId := ctx.Params("userid")

// 	sentimentResponse := controller.MediatoolkitService.Create(userId, request)

// 	action := fmt.Sprintf("creating sentiment datapoint %s", request.Timestamp)
// 	log.Println("LOG ", action)

// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    sentimentResponse,
// 	})
// }

// func (controller *SentimentControllerImpl) FindDatapointById(ctx *fiber.Ctx) error {
// 	userId := ctx.Params("userid")
// 	dataId := ctx.Params("dataid")

// 	sentimentResponse := controller.MediatoolkitService.FindDatapointById(userId, dataId)
// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    sentimentResponse,
// 	})
// }

// func (controller *SentimentControllerImpl) UpdateDatapointById(ctx *fiber.Ctx) error {
// 	var request web.SentimentUpdateDatapointRequest
// 	err := ctx.BodyParser(&request)
// 	helper.PanicIfError(err)

// 	userId := ctx.Params("userid")
// 	dataId := ctx.Params("dataid")

// 	controller.MediatoolkitService.UpdateDataPoint(userId, dataId, request)
// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    nil,
// 	})
// }

// func (controller *SentimentControllerImpl) UpdateSummary(ctx *fiber.Ctx) error {
// 	var request web.SentimentUpdateSummaryRequest
// 	err := ctx.BodyParser(&request)
// 	helper.PanicIfError(err)

// 	userId := ctx.Params("userid")

// 	sentiment := controller.MediatoolkitService.UpdateSummary(userId, request)
// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    sentiment,
// 	})
// }

// func (controller *SentimentControllerImpl) GetCompleteReport(ctx *fiber.Ctx) error {
// 	// query by judul, status, page & limit
// 	// page := ctx.Query("page")
// 	// limit := ctx.Query("limit")
// 	// searchName := ctx.Query("search")
// 	// status := ctx.Query("status")

// 	userId := ctx.Params("userid")

// 	from := ctx.Query("from")
// 	until := ctx.Query("until")

// 	query := make(map[string]string)

// 	query["user_id"] = userId
// 	if from != "" {
// 		query["from"] = from
// 	}
// 	if until != "" {
// 		query["until"] = until
// 	}

// 	sentimentResponse := controller.MediatoolkitService.GetCompleteReport(query)
// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    sentimentResponse,
// 	})
// }

// func (controller *SentimentControllerImpl) Delete(ctx *fiber.Ctx) error {
// 	userId := ctx.Params("userid")
// 	dataId := ctx.Params("dataid")

// 	controller.MediatoolkitService.Delete(userId, dataId)

// 	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
// 		Code:    fiber.StatusOK,
// 		Status:  true,
// 		Message: "success",
// 		Data:    nil,
// 	})
// }
