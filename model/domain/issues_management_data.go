package domain

import "time"

type IssueManagementData struct {
	ID        string        `json:"id" bson:"_id"`
	Name      string        `json:"name" bson:"name"`
	FileUrl   string        `json:"file_url" bson:"file_url"`
	Year      string        `json:"year" bson:"year"`
	CreatedAt time.Time     `json:"created_at" bson:"created_at"`
	IssueType string        `json:"issue_type" bson:"issue_type"`
	SubIssue  string        `json:"sub_issue" bson:"sub_issue"`
	Data      []DataWilayah `json:"data" bson:"data"`
}
