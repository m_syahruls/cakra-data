package controller

import (
	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	middlewares "repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type ComplaintControllerImpl struct {
	ComplaintService service.ComplaintService
}

type ComplaintController interface {
	NewComplaintRouter(app *fiber.App)
}

func NewComplaintController(complaintService service.ComplaintService) ComplaintController {
	return &ComplaintControllerImpl{
		ComplaintService: complaintService,
	}
}

func (controller *ComplaintControllerImpl) NewComplaintRouter(app *fiber.App) {
	complaint := app.Group("/complaints")
	complaint.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	complaint.Post("/status", controller.CreateComplaintStatus)
	complaint.Get("/status", controller.FindAllComplaintStatus)
	complaint.Put("/status/:complaintstatusid", controller.UpdateStatusNameComplaint)

	complaint.Post("/category", controller.CreateComplaintCategory)
	complaint.Get("/category", controller.FindAllComplaintCategory)

	complaint.Post("/", controller.CreateComplaint)
	complaint.Get("/", controller.FindAll)
	complaint.Get("/:complaintid", controller.FindDetailByComplaintId)
	complaint.Put("/:complaintid", controller.UpdateByComplaintId)
	complaint.Put("/read-status/:complaintid", controller.UpdateReadStatusByComplaintId)
	complaint.Put("/complaint-status/:complaintid", controller.UpdateComplaintStatusByComplaintId)
	complaint.Delete("/:complaintid", controller.DeleteComplaint)

	complaint.Post("/chat/:complaintid", controller.CreateComplaintChat)
	complaint.Get("/chat/:complaintid", controller.FindChatByComplaintId)
	complaint.Put("/chat/:complaintchatid", controller.UpdateChatByComplaintChatId)
	complaint.Delete("/chat/:complaintchatid", controller.DeleteComplaintChat)
}

func (controller *ComplaintControllerImpl) CreateComplaint(ctx *fiber.Ctx) error {
	senderId, _, err := helper.ParseJwt(ctx.Cookies("token"))
	helper.PanicIfError(err)

	var request web.ComplaintCreate

	request.Title = ctx.FormValue("title")
	request.Content = ctx.FormValue("content")
	request.Longitude = ctx.FormValue("longitude")
	request.Latitude = ctx.FormValue("latitude")
	request.Address = ctx.FormValue("address")
	request.CategoryId = ctx.FormValue("categoryid")

	request.SenderId = senderId

	complaint := controller.ComplaintService.CreateComplaint(ctx, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) FindAll(ctx *fiber.Ctx) error {
	userid := ctx.Query("userid")

	if isadmin := middlewares.IsAdmin(ctx); !isadmin {
		userid, _, _ = helper.ParseJwt(ctx.Cookies("token"))
	}

	page := ctx.Query("page")
	limit := ctx.Query("limit")
	lastChat := ctx.Query("lastchat")
	complaintStatusId := ctx.Query("complaintstatusid")
	categoryId := ctx.Query("categoryid")

	if lastChat == "true" {
		complaint := controller.ComplaintService.FindAllComplaintWithLastChat(userid, complaintStatusId, categoryId, page, limit)
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "success",
			Data:    complaint,
		})
	}
	complaint := controller.ComplaintService.FindAllComplaint(userid, complaintStatusId, categoryId, page, limit)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) FindDetailByComplaintId(ctx *fiber.Ctx) error {
	complaintId := ctx.Params("complaintid")

	complaint := controller.ComplaintService.FindDetailByComplaintId(ctx, complaintId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) UpdateByComplaintId(ctx *fiber.Ctx) error {
	var request web.ComplaintUpdate
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	complaintId := ctx.Params("complaintid")

	complaint := controller.ComplaintService.UpdateByComplaintId(ctx, complaintId, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) UpdateReadStatusByComplaintId(ctx *fiber.Ctx) error {
	var request web.ComplaintUpdateStatus
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	complaintId := ctx.Params("complaintid")

	complaint := controller.ComplaintService.UpdateReadStatusComplaintId(ctx, complaintId, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) UpdateComplaintStatusByComplaintId(ctx *fiber.Ctx) error {
	var request web.UpdateComplaintStatus
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	complaintId := ctx.Params("complaintid")

	complaint := controller.ComplaintService.UpdateComplaintStatusByComplaintId(ctx, complaintId, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) DeleteComplaint(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can delete event")
	}

	complaintId := ctx.Params("complaintid")

	controller.ComplaintService.DeleteComplaint(complaintId)

	//action := fmt.Sprintf("delete event %s", eventId)
	//actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	//controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

// Chat
func (controller *ComplaintControllerImpl) CreateComplaintChat(ctx *fiber.Ctx) error {
	senderId, _, err := helper.ParseJwt(ctx.Cookies("token"))
	helper.PanicIfError(err)

	var request web.ComplaintChat

	complaintId := ctx.Params("complaintid")
	request.ComplaintId = complaintId
	request.SenderId = senderId
	request.Message = ctx.FormValue("message")
	request.Longitude = ctx.FormValue("longitude")
	request.Latitude = ctx.FormValue("latitude")

	complaint := controller.ComplaintService.CreateComplaintChat(ctx, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) FindChatByComplaintId(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	complaintId := ctx.Params("complaintid")

	complaint := controller.ComplaintService.FindChatByComplaintId(ctx, complaintId, page, limit)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaint,
	})
}

func (controller *ComplaintControllerImpl) UpdateChatByComplaintChatId(ctx *fiber.Ctx) error {
	var request web.ComplaintChatUpdate
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	complaintChatId := ctx.Params("complaintchatid")

	controller.ComplaintService.UpdateChatByComplaintId(ctx, complaintChatId, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

func (controller *ComplaintControllerImpl) DeleteComplaintChat(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can delete chat")
	}

	complaintChatId := ctx.Params("complaintchatid")

	controller.ComplaintService.DeleteComplaintChat(complaintChatId)

	//action := fmt.Sprintf("delete event %s", eventId)
	//actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	//controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

// Complaint Status
func (controller *ComplaintControllerImpl) CreateComplaintStatus(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can create complaint status")
	}

	var request web.ComplaintStatus
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	response := controller.ComplaintService.CreateComplaintStatus(ctx, request)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *ComplaintControllerImpl) FindAllComplaintStatus(ctx *fiber.Ctx) error {

	page := ctx.Query("page")
	limit := ctx.Query("limit")

	complaintStatus := controller.ComplaintService.FindAllComplaintStatus(page, limit)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaintStatus,
	})
}

func (controller *ComplaintControllerImpl) UpdateStatusNameComplaint(ctx *fiber.Ctx) error {
	var request web.UpdateComplaintStatusName
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	complaintStatusId := ctx.Params("complaintstatusid")

	complaintStatus := controller.ComplaintService.UpdateComplaintStatusName(ctx, complaintStatusId, request)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaintStatus,
	})
}

// Complaint Category
func (controller *ComplaintControllerImpl) CreateComplaintCategory(ctx *fiber.Ctx) error {
	if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
		return UnauthorizeReturn(ctx, "only admin can create complaint category")
	}

	var request web.ComplaintCategory
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	response := controller.ComplaintService.CreateComplaintCategory(ctx, request)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    response,
	})
}

func (controller *ComplaintControllerImpl) FindAllComplaintCategory(ctx *fiber.Ctx) error {

	page := ctx.Query("page")
	limit := ctx.Query("limit")

	complaintCategory := controller.ComplaintService.FindAllComplaintCategory(page, limit)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    complaintCategory,
	})
}
