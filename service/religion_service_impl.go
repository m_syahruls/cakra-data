package service

import (
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type ReligionServiceImpl struct {
	ReligionRepository repository.ReligionRepository
}

type ReligionService interface {
	FindAll(page, limit string) []domain.Religion
	FindById(electionTypeId string) domain.Religion
}

func NewReligionService(electionTypeRepository repository.ReligionRepository) ReligionService {
	return &ReligionServiceImpl{
		ReligionRepository: electionTypeRepository,
	}
}

func (service *ReligionServiceImpl) FindAll(page, limit string) []domain.Religion {

	electionType := service.ReligionRepository.FindAll(page, limit)

	return electionType
}

func (service *ReligionServiceImpl) FindById(electionTypeId string) domain.Religion {

	electionType := service.ReligionRepository.FindById(electionTypeId)

	return electionType
}
