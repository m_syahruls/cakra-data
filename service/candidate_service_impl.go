package service

import (
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type CandidateServiceImpl struct {
	CandidateRepository    repository.CandidateRepository
	ElectionTypeRepository repository.ElectionTypeRepository
	PartaiRepository       repository.PartaiRepository
	RegencyRepository      repository.RegencyRepository
	Validate               *validator.Validate
}

type CandidateService interface {
	Create(candidate web.Candidate) domain.Candidate
	FindAll(page, limit, partaiId, electionTypeId, daerahPemiluId string) []domain.Candidate
}

func NewCandidateService(candidateRepository repository.CandidateRepository, electionTypeRepository repository.ElectionTypeRepository, partaiRepository repository.PartaiRepository, regencyRepository repository.RegencyRepository, validate *validator.Validate) CandidateService {
	return &CandidateServiceImpl{
		CandidateRepository:    candidateRepository,
		ElectionTypeRepository: electionTypeRepository,
		PartaiRepository:       partaiRepository,
		RegencyRepository:      regencyRepository,
		Validate:               validate,
	}
}

func (service *CandidateServiceImpl) Create(candidate web.Candidate) domain.Candidate {
	err := service.Validate.Struct(candidate)
	helper.PanicIfError(err)

	partai := service.PartaiRepository.FindById(candidate.PartaiId)
	if partai.Id == "" {
		panic(exception.NewError(404, "partai not found"))
	}

	electionType := service.ElectionTypeRepository.FindById(candidate.PemiluTypeId)
	if electionType.Id == "" {
		panic(exception.NewError(404, "election type not found"))
	}

	if electionType.Slug == "dpr_ri" {
		// daerah pemilu id ke provinsi

	} else if electionType.Slug == "dprd_prov" {
		// daerah pemilu id ke regency
		//regency, _ := service.RegencyRepository.FindById(candidate.DaerahPemiluId)
		//if regency.ID == "" {
		//	panic(exception.NewError(404, "regency not found"))
		//}
	} else {
		//daerah pemilu id ke district
	}

	dataCandidate := domain.Candidate{
		Id:             strings.ToLower(randstr.String(10)),
		Name:           candidate.Name,
		PemiluTypeId:   candidate.PemiluTypeId,
		PartaiId:       candidate.PartaiId,
		DaerahPemiluId: candidate.DaerahPemiluId,
	}
	service.CandidateRepository.Create(dataCandidate)

	return dataCandidate
}

func (service *CandidateServiceImpl) FindAll(page, limit, partaiId, electionTypeId, daerahPemiluId string) []domain.Candidate {

	candidate := service.CandidateRepository.FindAll(page, limit, partaiId, electionTypeId, daerahPemiluId)

	return candidate
}
