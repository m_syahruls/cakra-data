package web

type OccupationResponse struct {
	ID    string `json:"id"`
	Level int    `json:"level"`
	Name  string `json:"name"`
}
