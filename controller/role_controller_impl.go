package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type RoleControllerImpl struct {
	RoleService service.RoleService
	LogService  service.LogService
}

type RoleController interface {
	NewRoleRouter(app *fiber.App)
}

func NewRoleController(roleService service.RoleService, logService service.LogService) RoleController {
	return &RoleControllerImpl{
		RoleService: roleService,
		LogService:  logService,
	}
}

func (controller *RoleControllerImpl) NewRoleRouter(app *fiber.App) {
	role := app.Group("/roles")
	role.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	// //Restricted (JUST FOR ADMIN)
	// role.Use(middlewares.IsAuthenticated)
	role.Post("/", controller.Create)
	role.Get("/:roleId", controller.FindById)
	role.Get("/", controller.FindAll)
	role.Put("/:roleId/assign", controller.AssignPermission)
	role.Put("/:roleId", controller.Update)
	role.Delete("/:roleId", controller.Delete)
}

func (controller *RoleControllerImpl) Create(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can create role")
	// }

	var request web.RoleCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	roleResponse := controller.RoleService.Create(request)

	action := fmt.Sprintf("create role %s", roleResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    roleResponse,
	})
}

func (controller *RoleControllerImpl) FindById(ctx *fiber.Ctx) error {
	roleId := ctx.Params("roleId")

	roleResponse := controller.RoleService.FindById(roleId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    roleResponse,
	})
}

func (controller *RoleControllerImpl) FindAll(ctx *fiber.Ctx) error {
	label := ctx.Query("label")
	value := ctx.Query("value")
	roleResponse := controller.RoleService.FindAll(label, value)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    roleResponse,
	})
}

func (controller *RoleControllerImpl) Update(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can update role")
	// }

	var request web.RoleUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	roleId := ctx.Params("roleId")

	roleResponse := controller.RoleService.Update(roleId, request)

	action := fmt.Sprintf("update role %s", roleResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    roleResponse,
	})
}

func (controller *RoleControllerImpl) AssignPermission(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can update role")
	// }

	var request web.RoleUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	roleId := ctx.Params("roleId")

	roleResponse := controller.RoleService.AssignPermission(roleId, request)

	action := fmt.Sprintf("update role permission %s", roleResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    roleResponse,
	})
}

func (controller *RoleControllerImpl) Delete(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can delete role")
	// }

	roleId := ctx.Params("roleId")

	controller.RoleService.Delete(roleId)

	action := fmt.Sprintf("delete role %s", roleId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
