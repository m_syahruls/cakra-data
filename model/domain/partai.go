package domain

type Partai struct {
	Id   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
	Slug string `json:"slug" bson:"slug"`
	Logo string `json:"logo" bson:"logo"`
}

type ElectionType struct {
	Id   string `json:"id" bson:"_id"`
	Name string `json:"name" bson:"name"`
	Slug string `json:"slug" bson:"slug"`
}
