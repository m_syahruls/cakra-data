package repository

import (
	"log"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type IssueDataRepositoryImpl struct {
	Collection    *mongo.Collection
	CollectionLog *mongo.Collection
}

type IssueDataRepository interface {
	FindIssueData(year string, issue_type string, sub_issue string) (domain.IssueData, error)
	Create(issue domain.IssueData)
	Update(issue_type string, year string, issue domain.IssueDataUpdateInsert)
	FindIssueDataRank(issue_type string, year string) (domain.IssueData, error)
	FindIssueDataRankProvince(issue_type string, year string, province string) []domain.IssueLog
	FindIssueDataRankAll(issue_type string, year string) []domain.IssueLog
}

func NewIssueDataRepository(database *mongo.Database) IssueDataRepository {
	return &IssueDataRepositoryImpl{
		Collection:    database.Collection("issues_data"),
		CollectionLog: database.Collection("issues_logs"),
	}
}

func (repository *IssueDataRepositoryImpl) FindIssueDataRank(issue_type string, year string) (domain.IssueData, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var issue domain.IssueData
	query := bson.M{"issue_type": issue_type, "year": year}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&issue)

	return issue, result.Err()
}

func (repository *IssueDataRepositoryImpl) FindIssueDataRankAll(issueType string, year string) []domain.IssueLog {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	issueTypeRegex := primitive.Regex{Pattern: issueType, Options: "i"}
	// num, _ := strconv.Atoi(province)
	cursor, err := repository.CollectionLog.Find(ctx, bson.M{"data.type_issue": issueTypeRegex, "year": year})
	helper.PanicIfError(err)

	var systemLogs []domain.IssueLog

	for cursor.Next(ctx) {
		var systemLog domain.IssueLog
		cursor.Decode(&systemLog)
		systemLogs = append(systemLogs, systemLog)
	}

	return systemLogs
}

func (repository *IssueDataRepositoryImpl) FindIssueDataRankProvince(issueType string, year string, province string) []domain.IssueLog {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	issueTypeRegex := primitive.Regex{Pattern: issueType, Options: "i"}
	num, _ := strconv.Atoi(province)
	cursor, err := repository.CollectionLog.Find(ctx, bson.M{"data.type_issue": issueTypeRegex, "year": year, "province_id": num})
	helper.PanicIfError(err)

	var systemLogs []domain.IssueLog

	for cursor.Next(ctx) {
		var systemLog domain.IssueLog
		cursor.Decode(&systemLog)
		systemLogs = append(systemLogs, systemLog)
	}

	return systemLogs
}

func (repository *IssueDataRepositoryImpl) Create(issueData domain.IssueData) {
	ctx, cancel := config.NewDBContext()
	defer cancel()
	defaultData := []string{}
	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"year":           issueData.Year,
		"issue_type":     issueData.IssueType,
		"metadata":       issueData.MetaData,
		"sub_issue_data": defaultData,
		"data":           defaultData,
	})

	helper.PanicIfError(err)
}

func (repository *IssueDataRepositoryImpl) Update(issue_type string, year string, issue domain.IssueDataUpdateInsert) {

	ctx, cancel := config.NewDBContext()
	defer cancel()
	query := bson.M{"issue_type": issue_type, "year": year}

	update := bson.M{
		"$set": bson.M{
			"metadata": issue.MetaData,
			"data":     issue.Data,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)
	helper.PanicIfError(err)
}

func (repository *IssueDataRepositoryImpl) FindIssueData(year string, issue_type string, sub_issue string) (domain.IssueData, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()
	log.Println(year)
	log.Println(issue_type)

	var issue domain.IssueData
	query := bson.M{"year": year, "issue_type": issue_type}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&issue)

	return issue, result.Err()
}

func (repository *IssueDataRepositoryImpl) FindAll(page, limit string) []domain.IssueData {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)

	cursor, err := repository.Collection.Find(ctx, bson.M{}, filter)
	helper.PanicIfError(err)

	var systemLogs []domain.IssueData

	for cursor.Next(ctx) {
		var systemLog domain.IssueData
		cursor.Decode(&systemLog)
		systemLogs = append(systemLogs, systemLog)
	}

	return systemLogs
}
