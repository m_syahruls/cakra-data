package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type QuestionControllerImpl struct {
	QuestionService service.QuestionService
	LogService      service.LogService
}

type QuestionController interface {
	NewQuestionRouter(app *fiber.App)
}

func NewQuestionController(questionService service.QuestionService, logService service.LogService) QuestionController {
	return &QuestionControllerImpl{
		QuestionService: questionService,
		LogService:      logService,
	}
}

func (controller *QuestionControllerImpl) NewQuestionRouter(app *fiber.App) {
	question := app.Group("/question")
	question.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	question.Post("/", controller.Create)
	question.Get("/:questionid", controller.FindById)
	question.Get("/", controller.FindAll)
	question.Put("/:questionid", controller.Update)
	question.Delete("/:questionid", controller.Delete)
}

func (controller *QuestionControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.QuestionCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can create question"))
	}

	questionResponse := controller.QuestionService.Create(request)

	action := fmt.Sprintf("create question %s", request.QuestionName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    questionResponse,
	})
}

func (controller *QuestionControllerImpl) FindById(ctx *fiber.Ctx) error {
	questionId := ctx.Params("questionid")

	questionResponse := controller.QuestionService.FindDetailById(questionId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    questionResponse,
	})
}

func (controller *QuestionControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query by survey_id & limit & page
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	surveyId := ctx.Query("surveyid")
	questionResponses := controller.QuestionService.FindAll(page, limit, surveyId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    questionResponses,
	})
}

func (controller *QuestionControllerImpl) Update(ctx *fiber.Ctx) error {
	questionId := ctx.Params("questionid")
	var request web.QuestionUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can update question"))
	}

	questionResponse := controller.QuestionService.Update(questionId, request)

	action := fmt.Sprintf("update question %s", request.QuestionName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    questionResponse,
	})
}

func (controller *QuestionControllerImpl) Delete(ctx *fiber.Ctx) error {
	questionId := ctx.Params("questionid")

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can delete question"))
	}

	controller.QuestionService.Delete(questionId)

	action := fmt.Sprintf("delete question %s", questionId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
