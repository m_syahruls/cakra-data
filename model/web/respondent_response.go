package web

import (
	"time"

	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type Respondent struct {
	Id        string    `json:"id" bson:"_id"`
	Name      string    `json:"name" bson:"name"`
	Email     string    `json:"email" bson:"email"`
	Phone     string    `json:"phone" bson:"phone"`
	Gender    string    `json:"gender" bson:"gender"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
}

type RespondentResponse struct {
	Id           string                 `json:"id"`
	Name         string                 `json:"name"`
	Email        string                 `json:"email"`
	Gender       string                 `json:"gender"`
	Phone        string                 `json:"phone"`
	NIK          string                 `json:"nik"`
	DistrictId   string                 `json:"district_id"`
	Longitude    string                 `json:"longitude"`
	Latitude     string                 `json:"latitude"`
	ProfileImage string                 `json:"profile_image"`
	Occupation   OccupationResponseUser `json:"occupation"`
	CreatedAt    time.Time              `json:"created_at"`
	UpdatedAt    time.Time              `json:"updated_at"`
}

type UserProgressRespondentDetail struct {
	ID             string                    `json:"id" bson:"_id"`
	SurveyProgress int                       `json:"survey_progess" bson:"survey_progress"`
	SurveyGoal     int                       `json:"survey_goal" bson:"survey_goal"`
	Name           string                    `json:"name" bson:"name"`
	RecruitedUser  []UserForProgressResponse `json:"respondent_user" bson:"respondent_user"`
}

type UserForProgressResponse struct {
	ID         string                `json:"id"`
	Name       string                `json:"name"`
	Email      string                `json:"email"`
	Nik        string                `json:"nik" bson:"nik"`
	Phone      string                `json:"phone" bson:"phone"`
	Respondent domain.ProgressSurvey `json:"respondent"`
}
