package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/mitchellh/mapstructure"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
)

type GoogleAdsServiceImpl struct {
	GoogleConfig *oauth2.Config

	GoogleManagerId      string
	GoogleRefreshToken   string
	GoogleDeveloperToken string
}

type GoogleAdsService interface {
	// FindDetailById(surveyId string) web.SurveyResult
	// FindCountResponseDateById(surveyId string) []web.ResponseCountDate
	GetAllCampaignsById(customerId string) web.GoogleAdsCampaignDetailComplete
	GetCustomerNameById(customerId string) web.GoogleAdsCampaignDetailComplete
	CheckEligibility(customerId string) (web.GoogleAdsEligibility, int)
	consumeGoogleApi(customerId string, query []byte) ([]interface{}, int)
	googleIdFormat(data string) string
}

func NewGoogleAdsService(clientId, clientSecret, refreshToken, developerToken, managerId, redirectUrl, scopes string) GoogleAdsService {
	conf := &oauth2.Config{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		Endpoint:     google.Endpoint,
		Scopes:       []string{scopes},
		RedirectURL:  redirectUrl,
	}

	return &GoogleAdsServiceImpl{
		GoogleConfig:         conf,
		GoogleManagerId:      managerId,
		GoogleRefreshToken:   refreshToken,
		GoogleDeveloperToken: developerToken,
	}
}

func (service *GoogleAdsServiceImpl) GetAllCampaignsById(customerId string) web.GoogleAdsCampaignDetailComplete {
	jsonStr := []byte(`{"query":"SELECT
		campaign.id,
		campaign.name,
		campaign_budget.amount_micros,
		campaign.status,
		campaign.optimization_score,
		campaign.advertising_channel_type,
		metrics.clicks,
		metrics.view_through_conversions,
		metrics.conversions,
		metrics.cost_micros,
		metrics.impressions,
		metrics.ctr,
		metrics.average_cpc,
		metrics.cost_per_conversion, 
		campaign.bidding_strategy_type,
		customer.descriptive_name,
		customer.currency_code
	FROM
		campaign
	WHERE
		segments.date
	DURING
		LAST_7_DAYS
	ORDER BY
		metrics.impressions
	DESC LIMIT 50"}`)
	var data web.GoogleAdsCampaignDetailComplete
	var res, statusCode = service.consumeGoogleApi(customerId, jsonStr)

	if len(res) == 0 {
		return data
	}

	if statusCode != 200 {
		if statusCode == 400 || statusCode == 403 {
			var badRequest web.GoogleAdsBadRequestComplete
			mapstructure.Decode(res[0], &badRequest)
			panic(exception.NewError(statusCode, fmt.Sprintf("%s: %s | %s: %s", badRequest.Error.Status, badRequest.Error.Message, badRequest.Error.Details[0].Errors[0].ErrorCode.QueryError, badRequest.Error.Details[0].Errors[0].Message)))
		} else if statusCode == 401 {
			var unauthrized web.GoogleAdsUnauthorizedComplete
			mapstructure.Decode(res[0], &unauthrized)
			panic(exception.NewError(statusCode, fmt.Sprintf("%s: %s", unauthrized.Error.Status, unauthrized.Error.Message)))
		}
	}

	mapstructure.Decode(res[0], &data)

	return data
}

func (service *GoogleAdsServiceImpl) GetCustomerNameById(customerId string) web.GoogleAdsCampaignDetailComplete {
	jsonStr := []byte(`{"query":"SELECT
		customer.resource_name,
		customer.descriptive_name,
		customer.currency_code
	FROM
		customer"}`)
	var data web.GoogleAdsCampaignDetailComplete
	var res, statusCode = service.consumeGoogleApi(customerId, jsonStr)

	if statusCode != 200 {
		if statusCode == 400 || statusCode == 403 {
			var badRequest web.GoogleAdsBadRequestComplete
			mapstructure.Decode(res[0], &badRequest)
			panic(exception.NewError(statusCode, fmt.Sprintf("%s: %s | %s: %s", badRequest.Error.Status, badRequest.Error.Message, badRequest.Error.Details[0].Errors[0].ErrorCode.QueryError, badRequest.Error.Details[0].Errors[0].Message)))
		} else if statusCode == 401 {
			var unauthrized web.GoogleAdsUnauthorizedComplete
			mapstructure.Decode(res[0], &unauthrized)
			panic(exception.NewError(statusCode, fmt.Sprintf("%s: %s", unauthrized.Error.Status, unauthrized.Error.Message)))
		}
	}

	mapstructure.Decode(res[0], &data)

	return data
}

func (service *GoogleAdsServiceImpl) CheckEligibility(customerId string) (web.GoogleAdsEligibility, int) {
	jsonStr := []byte(`{"query":"SELECT
		campaign.id,
		campaign.name,
		metrics.clicks
	FROM
		campaign
	WHERE
		segments.date
	DURING
		LAST_7_DAYS"}`)

	// var data web.GoogleAdsCampaignDetailComplete
	var res, statusCode = service.consumeGoogleApi(customerId, jsonStr)
	var ret = true
	var message = "eligible"

	if statusCode != 200 {
		if statusCode == 400 || statusCode == 403 {
			var badRequest web.GoogleAdsBadRequestComplete
			mapstructure.Decode(res[0], &badRequest)
			if badRequest.Error.Details[0].Errors[0].ErrorCode.QueryError == "REQUESTED_METRICS_FOR_MANAGER" {
				message = "registered account cannot be a manager-level account"
			} else {
				message = fmt.Sprintf("%s: %s", badRequest.Error.Status, badRequest.Error.Details[0].Errors[0].Message)
			}
		} else if statusCode == 401 {
			var unauthrized web.GoogleAdsUnauthorizedComplete
			mapstructure.Decode(res[0], &unauthrized)
			message = fmt.Sprintf("this user is not yet registered to our Google Ads manager's account: %s", service.googleIdFormat(service.GoogleManagerId))
		}
		ret = false
	}

	return web.GoogleAdsEligibility{
		Eligibility: ret,
		Message:     message,
	}, statusCode
}

func (service *GoogleAdsServiceImpl) consumeGoogleApi(customerId string, query []byte) ([]interface{}, int) {
	url := fmt.Sprintf("https://googleads.googleapis.com/v12/customers/%s/googleAds:searchStream", customerId)
	var array = []interface{}{}

	oauth := &oauth2.Token{
		RefreshToken: service.GoogleRefreshToken,
	}
	client := service.GoogleConfig.Client(context.Background(), oauth)
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(query))
	req.Header.Set("developer-token", service.GoogleDeveloperToken)
	req.Header.Set("login-customer-id", service.GoogleManagerId)

	res, err := client.Do(req)
	if err != nil {
		panic(exception.NewError(res.StatusCode, err.Error()))
	}

	bytes, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(bytes, &array)
	if err != nil {
		panic(exception.NewError(http.StatusInternalServerError, fmt.Sprintf("internal server error %v", err)))
	}

	return array, res.StatusCode
}

func (service *GoogleAdsServiceImpl) googleIdFormat(data string) string {
	return fmt.Sprintf("%s-%s-%s", data[:3], data[3:6], data[6:])
}
