package repository

import (
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type IssueLogRepositoryImpl struct {
	Collection *mongo.Collection
}

type IssueLogRepository interface {
	FindAll(provinceId, regencyId, issueType string, subIssue string, year string) []domain.IssueLog
	Delete(provinceId, regencyId, issueType string, subIssue string, year string) error
}

func NewIssueLogRepository(database *mongo.Database) IssueLogRepository {
	return &IssueLogRepositoryImpl{
		Collection: database.Collection("issues_logs"),
	}
}

func (repository *IssueLogRepositoryImpl) FindAll(provinceId, regencyId, issueType string, subIssue string, year string) []domain.IssueLog {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssueLog(provinceId, regencyId, issueType, subIssue, year)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	var issueSubs []domain.IssueLog

	for cursor.Next(ctx) {
		var issueSub domain.IssueLog
		cursor.Decode(&issueSub)
		issueSubs = append(issueSubs, issueSub)
	}

	return issueSubs
}

func (repository *IssueLogRepositoryImpl) Delete(provinceId, regencyId, issueType string, subIssue string, year string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	filter := bson.M{
		"$and": []bson.M{
			{"data.type_issue": issueType},
			{"data.sub_issue": subIssue},
			{"year": year},
		},
	}

	log.Println(filter)
	_, err := repository.Collection.DeleteMany(ctx, filter)

	helper.PanicIfError(err)

	return nil
}
