package service

import (
	"log"
	"sort"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/go-playground/validator/v10"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type IssueDataServiceImpl struct {
	IssueDataRepository repository.IssueDataRepository
	Validate            *validator.Validate
}

type IssueDataService interface {
	FindIssueData(year string, issue_type string, sub_issue string) domain.IssueData
	FindIssueDataRank(issue_type string, year string) []web.IssueDataResponseRank
	FindDensityIssue(issue_type string, year string, scope string, province string) []web.IssueDataResponseRank
	Create(year string, issue_type string, sub_issue string) web.IssueDataResponse
	Update(issue_type string, year string, payload domain.IssueDataUpdateInsert)
}

func NewIssueDataService(IssueDataRepository repository.IssueDataRepository) IssueDataService {
	return &IssueDataServiceImpl{
		IssueDataRepository: IssueDataRepository,
	}
}

// func (service *IssueDataServiceImpl) FindIssueDataRank(issue_type string, year string) []web.IssueDataResponseRank {
// 	Issue, err := service.IssueDataRepository.FindIssueDataRank(issue_type, year)
// 	if err != nil {
// 		panic(exception.NewError(fiber.StatusNotFound, "Issue not found"))
// 	}
// 	IssueList := make([]web.IssueDataResponseRank, 0)

// 	// Sort data by TotalIncidents in descending order
// 	sort.Slice(Issue.Data, func(i, j int) bool {
// 		// return Issue.Data[i].MetaData.TotalKejadian > Issue.Data[j].MetaData.TotalKejadian
// 		return Issue.Data[i].MetaData.TotalIncidents > Issue.Data[j].MetaData.TotalIncidents
// 	})

// 	for rank, data := range Issue.Data {
// 		IssueList = append(IssueList, web.IssueDataResponseRank{
// 			ID:   data.ID,
// 			Rank: rank + 1, // Set the rank based on the index (+1 to start from 1)
// 			MetaData: web.IssueMetaDataRank{
// 				Total: data.MetaData.TotalIncidents,
// 			},
// 			Wilayah: data.Wilayah,
// 		})
// 	}

// 	return helper.ToIssueResponseRanks(IssueList)
// }

func (service *IssueDataServiceImpl) FindIssueDataRank(issue_type string, year string) []web.IssueDataResponseRank {
	Issue := service.IssueDataRepository.FindIssueDataRankAll(issue_type, year)

	groupedData := make(map[string][]domain.IssueLog) // Assuming IssueLog is the type of elements in the Issue slice

	for _, data := range Issue {
		groupedData[data.ProvinceName] = append(groupedData[data.ProvinceName], data)
	}

	// Construct the final result
	result := make([]web.IssueDataResponseRank, 0)

	for provinceName, issueLogs := range groupedData {
		subIssueMap := make(map[string]int)
		for _, issueLog := range issueLogs {
			subIssue := issueLog.Data.SubIssue
			subIssueMap[subIssue]++
		}

		totalSum := 0
		for _, totalIncidents := range subIssueMap {
			totalSum += totalIncidents
		}

		provinceData := web.IssueDataResponseRank{
			ID: issueLogs[0].ProvinceID, // Assuming RegencyID is a field in the IssueLog struct
			MetaData: web.IssueMetaDataRank{
				Total: totalSum,
			},
			Wilayah: provinceName,
			// Rank:,
		}
		result = append(result, provinceData)
	}

	// Sort the result slice based on the total sum in descending order
	sort.Slice(result, func(i, j int) bool {
		return result[i].MetaData.Total > result[j].MetaData.Total
	})

	// Assign the ranks based on the sorted order
	for i := range result {
		result[i].Rank = i + 1
	}

	return helper.ToIssueResponseRanks(result)
}

func (service *IssueDataServiceImpl) FindDensityIssue(issue_type string, year string, scope string, province string) []web.IssueDataResponseRank {
	if province != "" {
		Issue := service.IssueDataRepository.FindIssueDataRankProvince(issue_type, year, province)

		groupedData := make(map[string][]domain.IssueLog) // Assuming IssueLog is the type of elements in the Issue slice

		for _, data := range Issue {
			groupedData[data.RegencyName] = append(groupedData[data.RegencyName], data)
		}

		// Construct the final result
		result := make([]web.IssueDataResponseRank, 0)

		for regencyName, issueLogs := range groupedData {
			subIssueMap := make(map[string]int)
			for _, issueLog := range issueLogs {
				subIssue := issueLog.Data.SubIssue
				subIssueMap[subIssue]++
			}

			subIssueList := make([]web.SubIssues, 0)
			totalSum := 0
			for subIssue, totalIncidents := range subIssueMap {
				subIssueData := web.SubIssues{
					Name:  subIssue,
					Total: totalIncidents,
				}
				subIssueList = append(subIssueList, subIssueData)
				totalSum += totalIncidents
			}

			sort.Slice(subIssueList, func(i, j int) bool {
				return subIssueList[i].Total > subIssueList[j].Total
			})

			for rank, subIssue := range subIssueList {

				subIssue.Rank = rank + 1
				subIssueList[rank] = subIssue
			}

			regencyData := web.IssueDataResponseRank{
				ID: issueLogs[0].RegencyID, // Assuming RegencyID is a field in the IssueLog struct
				MetaData: web.IssueMetaDataRank{
					Total: totalSum,
				},
				Wilayah: regencyName,
			}
			result = append(result, regencyData)
		}

		return helper.ToIssueResponseRanks(result)

	}

	Issue := service.IssueDataRepository.FindIssueDataRankAll(issue_type, year)

	groupedData := make(map[string][]domain.IssueLog) // Assuming IssueLog is the type of elements in the Issue slice

	for _, data := range Issue {
		groupedData[data.ProvinceName] = append(groupedData[data.ProvinceName], data)
	}

	// Construct the final result
	result := make([]web.IssueDataResponseRank, 0)

	for provinceName, issueLogs := range groupedData {
		subIssueMap := make(map[string]int)
		for _, issueLog := range issueLogs {
			subIssue := issueLog.Data.SubIssue
			subIssueMap[subIssue]++
		}

		subIssueList := make([]web.SubIssues, 0)
		totalSum := 0
		for subIssue, totalIncidents := range subIssueMap {
			subIssueData := web.SubIssues{
				Name:  subIssue,
				Total: totalIncidents,
			}
			subIssueList = append(subIssueList, subIssueData)
			totalSum += totalIncidents
		}

		sort.Slice(subIssueList, func(i, j int) bool {
			return subIssueList[i].Total > subIssueList[j].Total
		})

		listAllSubIssue := []int{}

		for rank, subIssue := range subIssueList {

			if scope == "all" {
				rankIssue := findRankSubIssue(subIssue.Total, listAllSubIssue)
				subIssue.Rank = rankIssue
			} else {
				subIssue.Rank = rank + 1
			}
			subIssueList[rank] = subIssue
		}

		provinceData := web.IssueDataResponseRank{
			ID: issueLogs[0].ProvinceID, // Assuming RegencyID is a field in the IssueLog struct
			MetaData: web.IssueMetaDataRank{
				Total: totalSum,
			},
			Wilayah:   provinceName,
			SubIssues: subIssueList,
		}
		result = append(result, provinceData)
	}

	log.Println(result)
	return helper.ToIssueResponseRanks(result)
}

func findRankSubIssue(number int, list []int) int {
	// Sort the list in descending order
	sort.Slice(list, func(i, j int) bool {
		return list[i] > list[j]
	})

	// Find the index of the number in the sorted list
	index := -1
	for i, n := range list {
		if n == number {
			index = i
			break
		}
	}

	// Add 1 to the index to get the rank (since indices start from 0)
	rank := index + 1

	return rank
}

func (service *IssueDataServiceImpl) FindIssueData(year string, issue_type string, sub_issue string) domain.IssueData {
	Issue, err := service.IssueDataRepository.FindIssueData(year, issue_type, sub_issue)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			// Handle the case where no documents are found
			return domain.IssueData{
				// Return an appropriate response or an empty response
			}
		}
		panic(exception.NewError(fiber.StatusNotFound, "Issue not found"))
	}

	return helper.ToIssueDataResponses(Issue)
}

func (service *IssueDataServiceImpl) Create(year string, issue_type string, sub_issue string) web.IssueDataResponse {
	// err := service.Validate.Struct(request)
	// helper.PanicIfError(err)

	issue := domain.IssueData{
		ID:        strings.ToLower(randstr.String(10)),
		Year:      year,
		IssueType: issue_type,
	}
	service.IssueDataRepository.Create(issue)

	return helper.ToIssueDataResponse(issue)
}

func (service *IssueDataServiceImpl) Update(issue_type string, year string, payload domain.IssueDataUpdateInsert) {
	// err := service.Validate.Struct(request)
	// helper.PanicIfError(err)

	service.IssueDataRepository.Update(issue_type, year, payload)
}
