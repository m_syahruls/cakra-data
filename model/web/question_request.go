package web

type QuestionCreateRequest struct {
	Section         string                `json:"section" validate:"required"`
	InputType       string                `json:"input_type" validate:"required"`
	SurveyId        string                `json:"survey_id" validate:"required"`
	QuestionNumber  int                   `json:"question_number" validate:"required"`
	QuestionName    string                `json:"question_name" validate:"required"`
	QuestionSubject string                `json:"question_subject" validate:"required"`
	Options         []OptionCreateRequest `json:"options"`
}

type QuestionUpdateRequest struct {
	Section         string                `json:"section" validate:"required"`
	InputType       string                `json:"input_type" validate:"required"`
	QuestionNumber  int                   `json:"question_number" validate:"required"`
	QuestionName    string                `json:"question_name" validate:"required"`
	QuestionSubject string                `json:"question_subject" validate:"required"`
	Options         []OptionCreateRequest `json:"options"`
}
