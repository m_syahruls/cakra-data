package web

type OptionResponse struct {
	ID         string `json:"id"`
	QuestionId string `json:"question_id"`
	OptionName string `json:"option_name"`
	Value      int    `json:"value"`
	Color      string `json:"color"`
}
