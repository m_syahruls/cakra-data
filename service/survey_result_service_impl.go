package service

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type SurveyResultServiceImpl struct {
	SurveyResultRepository repository.SurveyResultRepository
	AnswerRepository       repository.AnswerRepository
}

type SurveyResultService interface {
	FindDetailById(surveyId, questionId, villageId, districtId, regencyId string) web.SurveyResult
	FindCountResponseDateById(surveyId string) []web.ResponseCountDate
}

func NewSurveyResultService(surveyRepository repository.SurveyResultRepository, answerRepository repository.AnswerRepository) SurveyResultService {
	return &SurveyResultServiceImpl{
		SurveyResultRepository: surveyRepository,
		AnswerRepository:       answerRepository,
	}
}

func (service *SurveyResultServiceImpl) FindDetailById(surveyId, questionId, villageId, districtId, regencyId string) web.SurveyResult {
	survey, err := service.SurveyResultRepository.FindById(surveyId, questionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "survey not found"))
	}

	//if villageId != "" {
	var arrans []string
	ans, _ := service.SurveyResultRepository.FindResponseByQuery(surveyId, villageId, districtId, regencyId)

	for _, an := range ans {
		arrans = append(arrans, an.ID)
	}

	for i, question := range survey.Questions {

		if question.InputType == "text" || question.InputType == "long_text" {
			var answerText []domain.SurveyAnswerText

			answerText, err = service.AnswerRepository.FindAnswerTextByQuestionAndResponseId(question.ID, arrans)
			if err != nil {
				answerText = append(answerText, domain.SurveyAnswerText{})
			}
			question.AnswerText = answerText
		} else if question.InputType == "location" {
			// cek location village, district, regency
			vil := make(map[string]int)
			if len(ans) > 0 {
				for _, an := range ans {
					ansLocs := service.AnswerRepository.FindAll("", "", question.ID, an.ID)
					for _, anansLoc := range ansLocs {
						loc := fmt.Sprintf("%s,%s,%s", anansLoc.Regency.Name, anansLoc.District.Name, anansLoc.Village.Name)
						vil[loc] = vil[loc] + 1
					}
				}
			} else {
				ansLocs := service.AnswerRepository.FindLoc(villageId)
				for _, anansLoc := range ansLocs {
					fmt.Println(anansLoc.Name)
					loc := fmt.Sprintf("%s,%s,%s", anansLoc.Regency.Name, anansLoc.District.Name, anansLoc.Name)
					vil[loc] = 0
				}
			}
			for key, val := range vil {
				split := strings.Split(key, ",")
				question.Location = append(question.Location, domain.Region{Regions: split, Number: val})
			}
		} else {

			for j, option := range question.Options {

				totalanswer, err := service.AnswerRepository.FindCountByQueryUsingAgg(option.QuestionId, option.ID, arrans)
				if err == nil {
					question.Options[j].TotalAnswer = totalanswer
				}
			}
		}

		survey.Questions[i] = question
	}
	//} else {
	//	for i, question := range survey.Questions {
	//
	//		if question.InputType == "text" || question.InputType == "long_text" {
	//			answerText, _ := service.AnswerRepository.FindAnswerTextByQuestionId(question.ID)
	//			question.AnswerText = answerText
	//		} else if question.InputType == "location" {
	//			// cek location village, district, regency
	//			ansLocs := service.AnswerRepository.FindAll("", "", question.ID, "")
	//			vil := make(map[string]int)
	//			for _, anansLoc := range ansLocs {
	//				loc := fmt.Sprintf("%s,%s,%s", anansLoc.Regency.Name, anansLoc.District.Name, anansLoc.Village.Name)
	//				vil[loc] = vil[loc] + 1
	//			}
	//			for key, val := range vil {
	//				split := strings.Split(key, ",")
	//				question.Location = append(question.Location, domain.Region{Regions: split, Number: val})
	//			}
	//		} else {
	//			for j, option := range question.Options {
	//				totalanswer := service.AnswerRepository.FindCountByQuery(bson.D{{Key: "question_id", Value: option.QuestionId}, {Key: "option_id", Value: option.ID}})
	//				question.Options[j].TotalAnswer = totalanswer
	//			}
	//		}
	//
	//		survey.Questions[i] = question
	//	}
	//}

	return helper.ToSurveyResultResponse(survey)
}

func (service *SurveyResultServiceImpl) FindCountResponseDateById(surveyId string) []web.ResponseCountDate {
	res := service.SurveyResultRepository.FindCountResponseDateById(surveyId)
	return helper.ToResultCountResponses(res)
}
