package web

import (
	"time"
)

type ComplaintResponse struct {
	ID                  string            `json:"id"`
	SenderId            string            `json:"sender_id"`
	SenderName          string            `json:"sender_name"`
	Title               string            `json:"title"`
	Content             string            `json:"content"`
	LinkImage           string            `json:"link_image"`
	ReadStatus          string            `json:"read_status"`
	ComplaintStatus     ComplaintStatus   `json:"complaint_status"`
	ComplaintStatusDesc string            `json:"complaint_status_desc"`
	ComplaintCategory   ComplaintCategory `json:"category"`
	Address             string            `json:"address"`
	Longitude           string            `json:"longitude"`
	Latitude            string            `json:"latitude"`
	CreatedAt           time.Time         `json:"created_at"`
	UpdatedAt           time.Time         `json:"updated_at"`
}

type ComplaintResponseWithLastChat struct {
	ID                  string                `json:"id"`
	SenderId            string                `json:"sender_id"`
	SenderName          string                `json:"sender_name"`
	Title               string                `json:"title"`
	Content             string                `json:"content"`
	LinkImage           string                `json:"link_image"`
	ReadStatus          string                `json:"read_status"`
	ComplaintStatus     ComplaintStatus       `json:"complaint_status"`
	ComplaintStatusDesc string                `json:"complaint_status_desc"`
	ComplaintCategory   ComplaintCategory     `json:"category"`
	Address             string                `json:"address"`
	Longitude           string                `json:"longitude"`
	Latitude            string                `json:"latitude"`
	LastChat            ComplaintChatResponse `json:"last_chat"`
	CreatedAt           time.Time             `json:"created_at"`
	UpdatedAt           time.Time             `json:"updated_at"`
}

type ComplaintChatResponse struct {
	ID          string    `json:"id" bson:"_id"`
	ComplaintId string    `json:"complaint_id" bson:"complaint_id"`
	SenderId    string    `json:"sender_id" bson:"sender_id"`
	LinkImage   string    `json:"link_image" bson:"link_image"`
	Message     string    `json:"message" bson:"message"`
	ReadStatus  string    `json:"read_status" bson:"read_status"`
	Longitude   string    `json:"longitude" bson:"longitude"`
	Latitude    string    `json:"latitude" bson:"latitude"`
	CreatedAt   time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt   time.Time `json:"updated_at" bson:"updated_at"`
}
