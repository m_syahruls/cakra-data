package service

import (
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type AccessServiceImpl struct {
	AccessRepository repository.AccessRepository
	Validate         *validator.Validate
}

type AccessService interface {
	Create(request web.AccessCreateRequest) web.AccessResponse
	FindById(accessId string) web.AccessResponse
	FindAll(label string, value string) []web.AccessResponse
	Update(accessId string, request web.AccessUpdateRequest) web.AccessResponse
	Delete(accessId string)
}

func NewAccessService(accessRepository repository.AccessRepository, validate *validator.Validate) AccessService {
	return &AccessServiceImpl{
		AccessRepository: accessRepository,
		Validate:         validate,
	}
}

func (service *AccessServiceImpl) Create(request web.AccessCreateRequest) web.AccessResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isAccessExist := service.AccessRepository.FindIsExist(request.Label, request.Value)
	if isAccessExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Access already exists"))
	}

	access := domain.Access{
		ID:    strings.ToLower(randstr.String(10)),
		Label: request.Label,
		Value: request.Value,
	}

	service.AccessRepository.Create(access)

	return helper.ToAccessResponse(access)
}

func (service *AccessServiceImpl) FindById(accessId string) web.AccessResponse {
	access, err := service.AccessRepository.FindById(accessId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Access not found"))
	}

	return helper.ToAccessResponse(access)
}

func (service *AccessServiceImpl) FindAll(label string, value string) []web.AccessResponse {
	accesses := service.AccessRepository.FindAll(label, value)
	return helper.ToAccessResponses(accesses)
}

func (service *AccessServiceImpl) Update(accessId string, request web.AccessUpdateRequest) web.AccessResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isAccessExist := service.AccessRepository.FindIsExist(request.Label, request.Value)
	if isAccessExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Access already exists"))
	}

	access, err := service.AccessRepository.FindById(accessId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Access not found"))
	}

	access.Label = request.Label
	access.Value = request.Value

	service.AccessRepository.Update(access, accessId)

	return helper.ToAccessResponse(access)
}

func (service *AccessServiceImpl) Delete(accessId string) {
	access, err := service.AccessRepository.FindById(accessId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Access not found"))
	}

	err = service.AccessRepository.Delete(accessId)
	helper.PanicIfError(err)
	log.Println("LOG ", "deleted access", access.Label)
}
