package service

import (
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/bson"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type ResponseServiceImpl struct {
	ResponseRepository          repository.ResponseRepository
	SurveyRepository            repository.SurveyRepository
	QuestionRepository          repository.QuestionRepository
	AnswerRepository            repository.AnswerRepository
	OptionRepository            repository.OptionRepository
	UserRepository              repository.UserRepository
	RespondentRepository        repository.RespondentRepository
	ProgressSurveyRepository    repository.ProgresSurveyRepository
	ProgressSurveySumRepository repository.ProgresSurveySumRepository
	RegencyRepository           repository.RegencyRepository
	DistrictRepository          repository.DistrictRepository
	VillageRepository           repository.VillageRepository
	Validate                    validator.Validate
}

type ResponseService interface {
	Create(ctx *fiber.Ctx, request web.CreateResponseRequest) web.DetailResponseResponse
	FindDetailById(responseId string) web.DetailResponseResponse
	FindAll(page, limit, surveyId, respondentId, recruiterid string) []web.ResponseResponse
	Update(responseId string, request web.UpdateResponseRequest) web.ResponseResponse
	Delete(responseId string)

	FindResponseHirarkiSurveyorBySurveyId(surveyId string) []web.ResponseBySurveyor
	SummaryQuestionByRegionId(questionId, regencyId string) web.ResponseQuestionByRegency
}

func NewResponseService(
	responseRepository repository.ResponseRepository,
	surveyRepository repository.SurveyRepository,
	questionRepository repository.QuestionRepository,
	answerRepository repository.AnswerRepository,
	optionRepository repository.OptionRepository,
	userRepository repository.UserRepository,
	validate *validator.Validate,
	respondentRepository repository.RespondentRepository,
	progressSurveyRepository repository.ProgresSurveyRepository,
	progressSurveySumRepository repository.ProgresSurveySumRepository,
	regencyRepository repository.RegencyRepository,
	districtRepository repository.DistrictRepository,
	villageRepository repository.VillageRepository,
) ResponseService {
	return &ResponseServiceImpl{
		ResponseRepository:          responseRepository,
		SurveyRepository:            surveyRepository,
		QuestionRepository:          questionRepository,
		AnswerRepository:            answerRepository,
		OptionRepository:            optionRepository,
		UserRepository:              userRepository,
		Validate:                    *validate,
		RespondentRepository:        respondentRepository,
		ProgressSurveyRepository:    progressSurveyRepository,
		ProgressSurveySumRepository: progressSurveySumRepository,
		RegencyRepository:           regencyRepository,
		DistrictRepository:          districtRepository,
		VillageRepository:           villageRepository,
	}
}

func (service *ResponseServiceImpl) Create(ctx *fiber.Ctx, request web.CreateResponseRequest) web.DetailResponseResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	surveyorId, level, _ := helper.ParseJwt(ctx.Cookies("token"))

	// check user
	responden, err := service.RespondentRepository.FindById(request.RespondentId)
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "user doesn't exist"))
	}

	// check survey
	survey, err := service.SurveyRepository.FindById(request.SurveyId)
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "survey doesn't exist"))
	}

	if survey.Status == 0 {
		panic(exception.NewError(fiber.StatusBadRequest, "survey inactive"))
	}

	responseExist, _ := service.ResponseRepository.FindByQuery(bson.D{{Key: "survey_id", Value: request.SurveyId}, {Key: "respondent_id", Value: request.RespondentId}})
	if responseExist.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "user already response"))
	}

	progress, err := service.ProgressSurveyRepository.FindByUserId(surveyorId)

	response := domain.Response{
		ID:           strings.ToLower(randstr.String(10)),
		SurveyId:     request.SurveyId,
		RespondentId: request.RespondentId,
		CreatedAt:    time.Now(),
	}

	if level == "3" {
		if err != nil || progress.ID == "" {
			progress = domain.ProgressSurvey{
				ID:             strings.ToLower(randstr.String(10)),
				UserId:         surveyorId,
				SurveyProgress: 1,
				SurveyGoal:     10,
			}
			service.ProgressSurveyRepository.Create(progress)
		} else {
			progress.SurveyProgress = progress.SurveyProgress + 1
			service.ProgressSurveyRepository.Update(progress)
		}
	}

	_, err = service.ProgressSurveySumRepository.FindByQuery(bson.M{"respondent_id": request.RespondentId, "surveyor_id": surveyorId})
	if err != nil {
		progressSurveySUm := domain.ProgressSurveySummary{
			ID:               strings.ToLower(randstr.String(10)),
			ProgressSurveyId: progress.ID,
			RespondentId:     request.RespondentId,
			SurveyorId:       surveyorId,
		}

		service.ProgressSurveySumRepository.Create(progressSurveySUm)
	}

	// Create Answer
	var answers []domain.Answer
	for _, reqAnswer := range request.Answers {
		err := service.Validate.Struct(reqAnswer)
		helper.PanicIfError(err)

		if reqAnswer.Answer == nil || reqAnswer.Answer == "" {
			panic(exception.NewError(fiber.StatusBadRequest, "please answer the question"))
		}

		question, err := service.QuestionRepository.FindById(reqAnswer.QuestionId)
		if err != nil {
			panic(exception.NewError(fiber.StatusNotFound, "question id not found"))
		}

		if question.SurveyId != request.SurveyId {
			panic(exception.NewError(fiber.StatusBadRequest, "question id is not for this survey id"))
		}

		answer := domain.Answer{
			ID:         strings.ToLower(randstr.String(10)),
			ResponseId: response.ID,
			QuestionId: reqAnswer.QuestionId,
			Question:   question,
		}

		switch v := reqAnswer.Answer.(type) {
		case string:
			if question.InputType == "text" || question.InputType == "long_text" {
				answer.AnswerText = v
			} else {
				intv, _ := strconv.Atoi(v)

				if question.InputType == "location" {
					village, err := service.VillageRepository.FindById(v)
					if village.ID == "" || err != nil {
						panic(exception.NewError(400, "village id not found"))
					}
					answer.OptionsId = v
					answer.Village = village

					district, _ := service.DistrictRepository.FindDistrictById(village.DistrictId)
					answer.District = district

					regency, _ := service.RegencyRepository.FindById(district.RegencyId)
					answer.Regency = regency
				} else {
					option, _ := service.OptionRepository.FindByQuery(bson.D{{Key: "question_id", Value: question.ID}, {Key: "value", Value: intv}})
					//if err != nil {
					//	panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("error get option for question %s", question.QuestionName)))
					//}
					answer.OptionsId = option.ID
					answer.Option = option

					// add gender to user
					if option.OptionName == "Laki-laki" || option.OptionName == "Perempuan" {
						responden.Gender = option.OptionName
					}

				}
			}
		default:
			answer.AnswerNumeric = int32(reqAnswer.Answer.(float64))
		}

		service.AnswerRepository.Create(answer)
		answers = append(answers, answer)
	}

	response.Location = request.Location

	service.ResponseRepository.Create(response)
	response.Answers = answers

	// update responden
	survey.TotalRespondent = survey.TotalRespondent + 1
	service.SurveyRepository.UpdateTotalRespondent(survey)

	// update data user
	service.RespondentRepository.Update(responden, request.RespondentId)

	return helper.ToCreateResponseResponse(response)
}

func (service *ResponseServiceImpl) FindDetailById(responseId string) web.DetailResponseResponse {
	response, err := service.ResponseRepository.FindDetailById(responseId)
	if err != nil || response.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "response not found"))
	}

	return helper.ToDetailResponseResponse(response)
}

func (service *ResponseServiceImpl) FindAll(page, limit, surveyId, respondentId, recruiterid string) []web.ResponseResponse {
	response := service.ResponseRepository.FindAll(page, limit, surveyId, respondentId, recruiterid)
	return helper.ToResponseResponses(response)
}

func (service *ResponseServiceImpl) Update(responseId string, request web.UpdateResponseRequest) web.ResponseResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	response, err := service.ResponseRepository.FindById(responseId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "response not found"))
	}

	// update answer

	return helper.ToResponseResponse(response)
}

func (service *ResponseServiceImpl) Delete(responseId string) {
	response, err := service.ResponseRepository.FindById(responseId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "response not found"))
	}

	err = service.ResponseRepository.Delete(responseId)
	helper.PanicIfError(err)
	// check survey
	survey, _ := service.SurveyRepository.FindById(response.SurveyId)
	if survey.ID != "" {
		// update responden
		survey.TotalRespondent = survey.TotalRespondent - 1
		service.SurveyRepository.UpdateTotalRespondent(survey)
	}

	// Check survey summary
	progress, err := service.ProgressSurveySumRepository.FindByQuery(bson.M{"respondent_id": response.RespondentId})
	if err == nil || progress.ID != "" {
		// check survey progres surveyor
		progressSurvey, err := service.ProgressSurveyRepository.FindByUserId(progress.SurveyorId)
		if err == nil || progressSurvey.ID != "" {
			progressSurvey.SurveyProgress = progressSurvey.SurveyProgress - 1
			service.ProgressSurveyRepository.Update(progressSurvey)
		}
		service.ProgressSurveySumRepository.Delete(progress.ID)
	}

	// delete summary
	service.ProgressSurveySumRepository.Delete(progress.ID)

	service.AnswerRepository.DeleteByResponseId(responseId)

	log.Println("LOG", "deleted response and answer from", response.ID)
}

func (service *ResponseServiceImpl) FindResponseHirarkiSurveyorBySurveyId(surveyId string) []web.ResponseBySurveyor {
	response := service.ResponseRepository.FindAllWithHirarki(surveyId)
	return helper.ToResponsesBySurveyor(response)
}

func (service *ResponseServiceImpl) SummaryQuestionByRegionId(questionId, regencyId string) web.ResponseQuestionByRegency {

	summary := service.ResponseRepository.FindSummary(questionId)

	var data web.ResponseQuestionByRegency

	data.QuestionID = summary.QuestionID
	data.QuestionName = summary.QuestionName
	data.SurveyId = summary.SurveyId
	data.TotalRespondent = summary.Survey.TotalRespondent

	mapingOpt := make(map[string]map[string]interface{})
	var sums []domain.ResponseQuestionByLocation
	vill := make(map[string]int)
	dataVill := make(map[string]string)

	regencyCount := make(map[string]int32)

	for _, opt := range summary.Options {
		maping := make(map[string]interface{})
		data.Options = append(data.Options, opt.OptionName)
		data.Color = append(data.Color, opt.Color)

		if regencyId != "" {
			regencyarr := strings.SplitAfter(regencyId, ",")
			sums = service.ResponseRepository.FindSummaryByLocation(questionId, opt.ID, regencyarr)
			for _, sum := range sums {
				maping[sum.VillageId] = sum.Count
				vill[sum.VillageId] = 1
				dataVill[sum.VillageId] = sum.District
			}
		}
		mapingOpt[opt.OptionName] = maping
	}

	for k := range vill {
		for _, opt := range data.Options {
			districtiId := dataVill[k]
			if mapingOpt[opt][k] != nil {
				regencyCount[districtiId+opt] += mapingOpt[opt][k].(int32)
			}
		}
	}

	for key := range vill {
		var datum web.ResponseQuestionByLocation
		for _, optName := range data.Options {
			datum.VillageId = key
			datum.Count = append(datum.Count, mapingOpt[optName][key])
			datum.DistrictId = dataVill[key]
			datum.DistrictCount = append(datum.DistrictCount, regencyCount[datum.DistrictId+optName])
		}
		data.Responses = append(data.Responses, datum)
	}

	//for _, sum := range sums1 {
	//	fmt.Println(sum)
	//	var datum web.ResponseQuestionByLocation
	//	datum.VillageId = sum.VillageId
	//	datum.Count = append(datum.Count, mapingOpt["rajiv"][sum.VillageId])
	//	data.Responses = append(data.Responses, datum)
	//}

	return data
}
