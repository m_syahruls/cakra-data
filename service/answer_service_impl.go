package service

import (
	"fmt"
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/bson"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type AnswerServiceImpl struct {
	AnswerRepository   repository.AnswerRepository
	ResponseRepository repository.ResponseRepository
	QuestionRepository repository.QuestionRepository
	OptionRepository   repository.OptionRepository
	Validate           validator.Validate
}

type AnswerService interface {
	Create(request web.AnswerCreateRequest) web.AnswerResponse
	FindById(answerId string) web.AnswerResponse
	FindAll(page, limit, questionId, responseId string) []web.AnswerResponse
	Update(answerId string, request web.AnswerUpdateRequest) web.AnswerResponse
	Delete(answerId string)
}

func NewAnswerService(
	answerRepository repository.AnswerRepository,
	responseRepository repository.ResponseRepository,
	questionRepository repository.QuestionRepository,
	optionRepository repository.OptionRepository,
	validate *validator.Validate) AnswerService {
	return &AnswerServiceImpl{
		AnswerRepository:   answerRepository,
		ResponseRepository: responseRepository,
		QuestionRepository: questionRepository,
		OptionRepository:   optionRepository,
		Validate:           *validate,
	}
}

func (service *AnswerServiceImpl) Create(request web.AnswerCreateRequest) web.AnswerResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	response, err := service.ResponseRepository.FindById(request.ResponseId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "response id not found"))
	}

	question, err := service.QuestionRepository.FindById(request.QuestionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "question id not found"))
	}

	if question.SurveyId != response.SurveyId {
		panic(exception.NewError(fiber.StatusBadRequest, "question id is not for this survey id"))
	}

	valueAnswerExist, _ := service.AnswerRepository.FindByQuery(bson.D{{Key: "response_id", Value: request.ResponseId}, {Key: "question_id", Value: request.QuestionId}})
	if valueAnswerExist.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "this question already answered"))
	}

	answer := domain.Answer{
		ID:         strings.ToLower(randstr.String(10)),
		QuestionId: request.QuestionId,
		ResponseId: request.ResponseId,
	}

	switch v := request.Answer.(type) {
	case string:
		if question.InputType == "text" || question.InputType == "long_text" {
			answer.AnswerText = v
		} else {
			option, err := service.OptionRepository.FindByQuery(bson.D{{Key: "question_id", Value: question.ID}, {Key: "value", Value: v}})
			if err != nil {
				panic(exception.NewError(fiber.StatusBadRequest, fmt.Sprintf("error get option for question %s", question.QuestionName)))
			}
			answer.OptionsId = option.ID
			answer.Option = option
		}
	default:
		answer.AnswerNumeric = int32(request.Answer.(float64))
	}

	service.AnswerRepository.Create(answer)

	answer.Question = question

	return helper.ToAnswerResponse(answer)
}

func (service *AnswerServiceImpl) FindById(answerId string) web.AnswerResponse {
	answer, err := service.AnswerRepository.FindDetailById(answerId)
	if err != nil || answer.ID == "" {
		panic(exception.NewError(fiber.StatusNotFound, "answer not found"))
	}

	return helper.ToAnswerResponse(answer)
}

func (service *AnswerServiceImpl) FindAll(page, limit, questionId, responseId string) []web.AnswerResponse {
	answer := service.AnswerRepository.FindAll(page, limit, questionId, responseId)
	return helper.ToAnswerResponses(answer)
}

func (service *AnswerServiceImpl) Update(answerId string, request web.AnswerUpdateRequest) web.AnswerResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	answer, err := service.AnswerRepository.FindById(answerId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "answer not found"))
	}

	switch v := request.Answer.(type) {
	case string:
		option, err := service.OptionRepository.FindById(v)
		if err != nil {
			answer.AnswerText = v
		}
		answer.OptionsId = option.ID
		answer.Option = option
	default:
		answer.AnswerNumeric = int32(request.Answer.(float64))
	}

	service.AnswerRepository.Update(answer)

	return helper.ToAnswerResponse(answer)
}

func (service *AnswerServiceImpl) Delete(answerId string) {
	answer, err := service.AnswerRepository.FindById(answerId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "answer not found"))
	}

	err = service.AnswerRepository.Delete(answerId)
	helper.PanicIfError(err)
	log.Println("LOG ", "deleted survey", answer.ID)
}
