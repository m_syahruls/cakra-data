#!/bin/bash
export APP_NAME=$1
export APP_VERSION=$2
export SERVICE_NAME=$3
export GIT_BRANCH=$4
export GIT_COMMIT_SHA=$( git rev-parse --short HEAD )
export DOCKER_IMG_NAME=$APP_NAME
export DOCKER_PROJECT_NAME="$APP_NAME"-"$SERVICE_NAME"

if [ $GIT_BRANCH == "master" ]; then
    export DOCKER_IMG_TAG="$APP_VERSION"-"$SERVICE_NAME"-svc
elif [ $GIT_BRANCH == "staging" ]; then
    export DOCKER_IMG_TAG="$APP_VERSION"-"$SERVICE_NAME"-staging-svc
else
    export DOCKER_IMG_TAG="$GIT_COMMIT_SHA"-"$SERVICE_NAME"-svc
fi

if [ $# -lt 3 ]; then
    echo "Usage: <app name> <app version> <service name> <branch name>"
    exit 1
fi

echo "Scanning $APP_NAME:$APP_VERSION-$SERVICE_NAME-svc for branch $GIT_BRANCH"

grype synapsisid/$DOCKER_IMG_NAME:$DOCKER_IMG_TAG -f critical
