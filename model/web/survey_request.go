package web

type SurveyCreateRequest struct {
	SurveyName string                        `json:"survey_name" validate:"required"`
	Status     *int                          `json:"status" validate:"required"`
	Questions  []SurveyQuestionCreateRequest `json:"questions"`
}

type SurveyUpdateRequest struct {
	ID         string                        `json:"id"`
	Status     *int                          `json:"status" validate:"required"`
	SurveyName string                        `json:"survey_name" validate:"required"`
	Questions  []SurveyQuestionCreateRequest `json:"questions"`
}

type SurveyQuestionCreateRequest struct {
	Section         string                       `json:"section" validate:"required"`
	InputType       string                       `json:"input_type" validate:"required"`
	QuestionNumber  int                          `json:"question_number" validate:"required"`
	QuestionName    string                       `json:"question_name" validate:"required"`
	QuestionSubject string                       `json:"question_subject" validate:"required"`
	Options         []SurveyOptionsCreateRequest `json:"options"`
}

type SurveyOptionsCreateRequest struct {
	OptionName string `json:"option_name" validate:"required"`
	Value      *int   `json:"value" validate:"required"`
	Color      string `json:"color"`
}
