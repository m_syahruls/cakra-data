package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/middleware"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type OptionControllerImpl struct {
	OptionService service.OptionService
	LogService    service.LogService
}

type OptionController interface {
	NewOptionRouter(app *fiber.App)
}

func NewOptionController(optionService service.OptionService, logService service.LogService) OptionController {
	return &OptionControllerImpl{
		OptionService: optionService,
		LogService:    logService,
	}
}

func (controller *OptionControllerImpl) NewOptionRouter(app *fiber.App) {
	option := app.Group("/option")
	option.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	option.Post("/", controller.Create)
	option.Get("/:optionid", controller.FindById)
	option.Get("/", controller.FindAll)
	option.Put("/:optionid", controller.Update)
	option.Delete("/:optionid", controller.Delete)
}

func (controller *OptionControllerImpl) Create(ctx *fiber.Ctx) error {
	var request web.OptionCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can create option"))
	}

	optionResponse := controller.OptionService.Create(request)

	action := fmt.Sprintf("create option %s", request.OptionName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusCreated).JSON(web.WebResponse{
		Code:    fiber.StatusCreated,
		Status:  true,
		Message: "success",
		Data:    optionResponse,
	})
}

func (controller *OptionControllerImpl) FindById(ctx *fiber.Ctx) error {
	optionId := ctx.Params("optionid")

	optionResponse := controller.OptionService.FindById(optionId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    optionResponse,
	})
}

func (controller *OptionControllerImpl) FindAll(ctx *fiber.Ctx) error {
	//query by question id
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	questionId := ctx.Query("questionid")
	optionResponse := controller.OptionService.FindAll(page, limit, questionId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    optionResponse,
	})
}

// / Implement
func (controller *OptionControllerImpl) Update(ctx *fiber.Ctx) error {
	optionId := ctx.Params("optionid")
	var request web.OptionUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can update option"))
	}

	optionUpdated := controller.OptionService.Update(optionId, request)

	action := fmt.Sprintf("update option %s", request.OptionName)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    optionUpdated,
	})
}

func (controller *OptionControllerImpl) Delete(ctx *fiber.Ctx) error {
	optionId := ctx.Params("optionid")

	if isAdmin := middleware.IsAdmin(ctx); !isAdmin {
		panic(exception.NewError(fiber.StatusUnauthorized, "only admin can delete option"))
	}

	controller.OptionService.Delete(optionId)

	action := fmt.Sprintf("delete option %s", optionId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
