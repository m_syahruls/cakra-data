package service

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type ProfileServiceImpl struct {
	ProfileRepository  repository.ProfileRepository
	UserRepository     repository.UserRepository
	LogisticRepository repository.LogisticRepository
	Validate           *validator.Validate
}

type ProfileService interface {
	FindById(userId string) web.UserDetailResponse
	UpdatePassword(userId string, request web.UpdatePasswordProfile)

	UpdateProfile(userid string, request web.ProfileUpdateRequest) web.ProfileResponse
	UpdateProfileImage(ctx *fiber.Ctx, userId string) web.UserDetailResponse
	UpdateGoogleAdsID(userid string, request web.ProfileUpdateGoogleAdsID) web.UserDetailResponse
	UpdateAyrshareToken(userid string, request web.ProfileUpdateAyrshareToken) web.UserDetailResponse
	UpdateDetermID(userid string, request web.ProfileUpdateDetermID) web.UserDetailResponse
	DeleteGoogleAdsID(userid string) web.UserDetailResponse
	DeleteAyrshareToken(userid string) web.UserDetailResponse
	DeleteDetermID(userid string) web.UserDetailResponse
}

func NewProfileService(profileRepository repository.ProfileRepository, userRepository repository.UserRepository, logisticRepository repository.LogisticRepository, validate *validator.Validate) ProfileService {
	return &ProfileServiceImpl{
		ProfileRepository:  profileRepository,
		UserRepository:     userRepository,
		LogisticRepository: logisticRepository,
		Validate:           validate,
	}
}

func (service *ProfileServiceImpl) FindById(userId string) web.UserDetailResponse {
	user, err := service.ProfileRepository.FindById(userId)
	if err != nil || user.Id == "" {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	lgstc := service.LogisticRepository.FindSumLogistic(userId, "")

	user.Logistic.RecruitProgress = lgstc

	return helper.ToDetailProfileResponse(user)
}

func (service *ProfileServiceImpl) UpdatePassword(userId string, request web.UpdatePasswordProfile) {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	if request.Password != request.PasswordConfirm {
		panic(exception.NewError(fiber.StatusBadRequest, "new password didnt match"))
	}

	user, _ := service.ProfileRepository.FindById(userId)

	if err := user.ComparePassword(user.Password, request.OldPassword); err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "incorrect old Password"))
	}

	user.SetPassword(request.Password)

	service.ProfileRepository.UpdatePassword(user, userId)
}

func (service *ProfileServiceImpl) UpdateProfile(userid string, request web.ProfileUpdateRequest) web.ProfileResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.ProfileRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	user.Name = request.Name
	if request.Email != "" {
		user.Email = request.Email
	}

	if request.Phone != "" {
		if !helper.IsNumeric(request.Phone) {
			panic(exception.NewError(fiber.StatusBadRequest, "phone should numeric"))
		}

		if len([]rune(request.Phone)) < 10 || len([]rune(request.Phone)) > 13 {
			panic(exception.NewError(fiber.StatusBadRequest, "phone should 10-13 digit"))
		}

		if phone, _ := service.UserRepository.FindByQuery("phone", request.Phone); phone.Id != "" && phone.Id != userid {
			panic(exception.NewError(fiber.StatusBadRequest, "phone has been registered"))
		}
		user.Phone = request.Phone
	}

	user.UpdatedAt = time.Now()

	service.ProfileRepository.Update(user, userid)

	return helper.ToProfileResponse(user)
}

func (service *ProfileServiceImpl) UpdateProfileImage(ctx *fiber.Ctx, userId string) web.UserDetailResponse {

	user, err := service.ProfileRepository.FindById(userId)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	linkImage := service.UploadImageProfile(ctx)

	user.ProfileImage = linkImage

	service.ProfileRepository.UpdateProfileImage(user, userId)

	return helper.ToDetailProfileResponse(user)
}

func (service *ProfileServiceImpl) UploadImageProfile(c *fiber.Ctx) string {
	ctx := context.Background()

	bucketName := os.Getenv("MINIO_BUCKET_PROFILE")
	file, err := c.FormFile("images")
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}

	// Get Buffer from file
	buffer, err := file.Open()

	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "bad request"))
	}
	defer buffer.Close()

	// Create minio connection.
	minioClient, err := helper.MinioConnection(bucketName)

	// Return status 500 and minio connection error.
	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "connection error"))
	}

	encodedName := base64.StdEncoding.EncodeToString([]byte(file.Filename))

	objectName := fmt.Sprintf("%s/%s.jpg", os.Getenv("MINIO_PATH"), encodedName)
	fileBuffer := buffer
	contentType := file.Header["Content-Type"][0]
	fileSize := file.Size

	if fileSize > 512000 {
		panic(exception.NewError(fiber.StatusBadRequest, "file must be smaller then 500kb"))
	}

	info, err := minioClient.PutObject(ctx, bucketName, objectName, fileBuffer, fileSize, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		panic(exception.NewError(fiber.StatusInternalServerError, "error upload"))
	}

	log.Printf("Successfully uploaded %s of size %d\n", objectName, info.Size)

	paramLink := fmt.Sprintf("%s/%s", bucketName, objectName)

	return paramLink
}

func (service *ProfileServiceImpl) UpdateGoogleAdsID(userid string, request web.ProfileUpdateGoogleAdsID) web.UserDetailResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.ProfileRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	user.GoogleAdsID = request.GoogleAdsID

	if strings.Contains(user.GoogleAdsID, "-") {
		panic(exception.NewError(fiber.StatusBadRequest, "google_ads_id should not contain '-'"))
	}

	if len((user.GoogleAdsID)) < 10 {
		panic(exception.NewError(fiber.StatusBadRequest, "google_ads_id should 10 digit"))
	}

	if !helper.IsNumeric(user.GoogleAdsID) {
		panic(exception.NewError(fiber.StatusBadRequest, "google_ads_id should be a numeric"))
	}

	user.UpdatedAt = time.Now()

	service.ProfileRepository.UpdateGoogleAdsID(user, userid)

	return helper.ToDetailProfileResponse(user)
}

func (service *ProfileServiceImpl) UpdateAyrshareToken(userid string, request web.ProfileUpdateAyrshareToken) web.UserDetailResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.ProfileRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	user.AyrshareToken = request.AyrshareToken

	user.UpdatedAt = time.Now()

	service.ProfileRepository.UpdateAyrshareToken(user, userid)

	return helper.ToDetailProfileResponse(user)
}

func (service *ProfileServiceImpl) UpdateDetermID(userid string, request web.ProfileUpdateDetermID) web.UserDetailResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	user, err := service.ProfileRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	user.DetermID = request.DetermID

	user.UpdatedAt = time.Now()

	service.ProfileRepository.UpdateDetermID(user, userid)

	return helper.ToDetailProfileResponse(user)
}

func (service *ProfileServiceImpl) DeleteGoogleAdsID(userid string) web.UserDetailResponse {
	user, err := service.ProfileRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	service.ProfileRepository.DeleteGoogleAdsID(userid)

	return helper.ToDetailProfileResponse(user)
}

func (service *ProfileServiceImpl) DeleteAyrshareToken(userid string) web.UserDetailResponse {
	user, err := service.ProfileRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	service.ProfileRepository.DeleteAyrshareToken(userid)

	return helper.ToDetailProfileResponse(user)
}

func (service *ProfileServiceImpl) DeleteDetermID(userid string) web.UserDetailResponse {
	user, err := service.ProfileRepository.FindById(userid)

	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "user not found"))
	}

	service.ProfileRepository.DeleteDetermID(userid)

	return helper.ToDetailProfileResponse(user)
}
