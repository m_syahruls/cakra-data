package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ReligionRepositoryImpl struct {
	Collection *mongo.Collection
}

type ReligionRepository interface {
	FindAll(page, limit string) []domain.Religion
	FindById(religionId string) domain.Religion
}

func NewReligionRepository(database *mongo.Database) ReligionRepository {
	return &ReligionRepositoryImpl{
		Collection: database.Collection("religions"),
	}
}

func (repository *ReligionRepositoryImpl) FindAll(page, limit string) []domain.Religion {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	filter := helper.Pagging(page, limit, -1)
	filter.SetSort(bson.M{"_id": 1})

	cursor, err := repository.Collection.Find(ctx, bson.M{}, filter)
	helper.PanicIfError(err)

	var religions []domain.Religion

	for cursor.Next(ctx) {
		var religion domain.Religion
		cursor.Decode(&religion)
		religions = append(religions, religion)
	}

	return religions
}

func (repository *ReligionRepositoryImpl) FindById(religionId string) domain.Religion {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	cursor := repository.Collection.FindOne(ctx, bson.M{"_id": religionId})

	var electiontype domain.Religion
	cursor.Decode(&electiontype)

	return electiontype
}
