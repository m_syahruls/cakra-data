package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type ResponseRepositoryImpl struct {
	Collection         *mongo.Collection
	CollectionQuestion *mongo.Collection
	CollectionAnswer   *mongo.Collection
}

type ResponseRepository interface {
	Create(response domain.Response)
	FindById(responseId string) (domain.Response, error)
	FindDetailById(responseId string) (domain.Response, error)
	FindByQuery(query bson.D) (domain.Response, error)
	FindAll(page, limit, surveyId, RespondentId, recruiterid string) []domain.Response
	Delete(responseId string) error

	FindAllWithHirarki(surveyId string) []domain.ResponseBySurveyor
	DeleteManyByRespondentId(respondentId string) error
	FindSummary(questionId string) domain.ResponseQuestionByRegency
	FindSummaryByLocation(questionId, optionId string, regencyId []string) []domain.ResponseQuestionByLocation
}

func NewResponseRepository(database *mongo.Database) ResponseRepository {
	return &ResponseRepositoryImpl{
		Collection:         database.Collection("responses"),
		CollectionQuestion: database.Collection("questions"),
		CollectionAnswer:   database.Collection("answers"),
	}
}

func (repository *ResponseRepositoryImpl) Create(response domain.Response) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":           response.ID,
		"survey_id":     response.SurveyId,
		"respondent_id": response.RespondentId,
		"created_at":    response.CreatedAt,
		"location":      response.Location,
	})

	helper.PanicIfError(err)
}

func (repository *ResponseRepositoryImpl) FindById(responseId string) (domain.Response, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var response domain.Response
	query := bson.M{"_id": responseId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&response)

	return response, result.Err()
}

func (repository *ResponseRepositoryImpl) FindDetailById(responseId string) (domain.Response, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: responseId}}}}
	lookupSurvey := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "surveys"}, {Key: "localField", Value: "survey_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "survey"}}}}
	unwindSurvey := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$survey"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "respondent_user"}, {Key: "localField", Value: "respondent_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupRecruited := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_survey_summary"}, {Key: "localField", Value: "respondent_id"}, {Key: "foreignField", Value: "respondent_id"}, {Key: "as", Value: "progress_summary"}}}}
	unwindRecruited := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$progress_summary"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupRecruiter := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "progress_summary.surveyor_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "surveyor"}}}}
	unwindRecruiter := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$surveyor"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "answers"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "response_id"}, {Key: "as", Value: "answers"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$answers"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "questions"}, {Key: "localField", Value: "answers.question_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "answers.question"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$answers.question"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "answers.option_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "answers.option"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$answers.option"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage4 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "answers.option_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "answers.village"}}}}
	unwindStage4 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$answers.village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage5 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "districts"}, {Key: "localField", Value: "answers.village.district_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "answers.district"}}}}
	unwindStage5 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$answers.district"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage6 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "regencies"}, {Key: "localField", Value: "answers.district.regency_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "answers.regency"}}}}
	unwindStage6 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$answers.regency"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "survey_id", Value: bson.D{{Key: "$first", Value: "$survey_id"}}},
		{Key: "survey", Value: bson.D{{Key: "$first", Value: "$survey"}}},
		{Key: "progress_summary", Value: bson.D{{Key: "$first", Value: "$progress_summary"}}},
		{Key: "surveyor", Value: bson.D{{Key: "$first", Value: "$surveyor"}}},
		{Key: "user", Value: bson.D{{Key: "$first", Value: "$user"}}},
		{Key: "respondent_id", Value: bson.D{{Key: "$first", Value: "$respondent_id"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "location", Value: bson.D{{Key: "$first", Value: "$location"}}},
		{Key: "answers", Value: bson.D{{Key: "$addToSet", Value: "$answers"}}},
	}}}
	unwindStage7 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$answers"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	sortStage := bson.D{{Key: "$sort", Value: bson.D{{Key: "answers.question.question_number", Value: 1}}}}
	groupStage2 := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "survey_id", Value: bson.D{{Key: "$first", Value: "$survey_id"}}},
		{Key: "survey", Value: bson.D{{Key: "$first", Value: "$survey"}}},
		{Key: "progress_summary", Value: bson.D{{Key: "$first", Value: "$progress_summary"}}},
		{Key: "surveyor", Value: bson.D{{Key: "$first", Value: "$surveyor"}}},
		{Key: "user", Value: bson.D{{Key: "$first", Value: "$user"}}},
		{Key: "respondent_id", Value: bson.D{{Key: "$first", Value: "$respondent_id"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "location", Value: bson.D{{Key: "$first", Value: "$location"}}},
		{Key: "answers", Value: bson.D{{Key: "$push", Value: "$answers"}}},
	}}}
	result, err := repository.Collection.Aggregate(ctx, mongo.Pipeline{matchStage, lookupSurvey, unwindSurvey, lookupStage, unwindStage, lookupRecruited, unwindRecruited, lookupRecruiter, unwindRecruiter, lookupStage1, unwindStage1, lookupStage2, unwindStage2, lookupStage3, unwindStage3, lookupStage4, unwindStage4, lookupStage5, unwindStage5, lookupStage6, unwindStage6, groupStage, unwindStage7, sortStage, groupStage2})
	helper.PanicIfError(err)

	var response domain.Response

	result.Next(ctx)
	result.Decode(&response)

	return response, result.Err()
}

func (repository *ResponseRepositoryImpl) FindByQuery(query bson.D) (domain.Response, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var response domain.Response

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&response)

	return response, result.Err()
}

func (repository *ResponseRepositoryImpl) FindAll(page, limit, surveyId, respondentId, recruiterid string) []domain.Response {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupSurvey := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "surveys"}, {Key: "localField", Value: "survey_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "survey"}}}}
	unwindSurvey := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$survey"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "respondent_user"}, {Key: "localField", Value: "respondent_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_survey_summary"}, {Key: "localField", Value: "respondent_id"}, {Key: "foreignField", Value: "respondent_id"}, {Key: "as", Value: "progress_summary"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$progress_summary"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "progress_summary.surveyor_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "surveyor"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$surveyor"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$_id"},
		{Key: "survey_id", Value: bson.D{{Key: "$first", Value: "$survey_id"}}},
		{Key: "survey", Value: bson.D{{Key: "$first", Value: "$survey"}}},
		{Key: "progress_summary", Value: bson.D{{Key: "$first", Value: "$progress_summary"}}},
		{Key: "surveyor", Value: bson.D{{Key: "$first", Value: "$surveyor"}}},
		{Key: "recruiter_id", Value: bson.D{{Key: "$first", Value: "$surveyor._id"}}},
		{Key: "user", Value: bson.D{{Key: "$first", Value: "$user"}}},
		{Key: "respondent_id", Value: bson.D{{Key: "$first", Value: "$respondent_id"}}},
		{Key: "created_at", Value: bson.D{{Key: "$first", Value: "$created_at"}}},
		{Key: "location", Value: bson.D{{Key: "$first", Value: "$location"}}},
		{Key: "answers", Value: bson.D{{Key: "$addToSet", Value: "$answers"}}},
	}}}

	pipeline := mongo.Pipeline{lookupSurvey, unwindSurvey, lookupStage, unwindStage, lookupStage2, unwindStage2, lookupStage3, unwindStage3, groupStage}

	helper.QueryResponse(surveyId, respondentId, recruiterid, &pipeline)

	helper.PaggingAggregat(page, limit, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)

	helper.PanicIfError(err)
	var responses []domain.Response

	for result.Next(ctx) {
		var response domain.Response
		result.Decode(&response)
		responses = append(responses, response)
	}

	return responses
}

func (repository *ResponseRepositoryImpl) Delete(responseId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": responseId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *ResponseRepositoryImpl) DeleteManyByRespondentId(respondentId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteMany(ctx, bson.M{
		"respondent_id": respondentId,
	})

	helper.PanicIfError(err)

	return nil
}

func (repository *ResponseRepositoryImpl) FindAllWithHirarki(surveyId string) []domain.ResponseBySurveyor {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	match := bson.D{{Key: "$match", Value: bson.D{{Key: "survey_id", Value: surveyId}}}}
	lookupSurvey := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "surveys"}, {Key: "localField", Value: "survey_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "survey"}}}}
	unwindSurvey := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$survey"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "respondent_user"}, {Key: "localField", Value: "respondent_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "user"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$user"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "progress_survey_summary"}, {Key: "localField", Value: "respondent_id"}, {Key: "foreignField", Value: "respondent_id"}, {Key: "as", Value: "progress_summary"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$progress_summary"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "users"}, {Key: "localField", Value: "progress_summary.surveyor_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "surveyor"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$surveyor"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{
		{Key: "_id", Value: "$surveyor._id"},
		{Key: "surveyor_name", Value: bson.D{{Key: "$first", Value: "$surveyor.name"}}},
		{Key: "survey_name", Value: bson.D{{Key: "$first", Value: "$survey.survey_name"}}},
		{Key: "response", Value: bson.D{
			{Key: "$push", Value: bson.D{
				{Key: "response_id", Value: "$_id"},
				{Key: "respondent_id", Value: "$respondent_id"},
				{Key: "respondent_name", Value: "$user.name"},
				{Key: "created_at", Value: "$created_at"},
			},
			}}}},
	}}

	pipeline := mongo.Pipeline{match, lookupSurvey, unwindSurvey, lookupStage, unwindStage, lookupStage2, unwindStage2, lookupStage3, unwindStage3, groupStage}

	result, err := repository.Collection.Aggregate(ctx, pipeline)

	helper.PanicIfError(err)
	var responses []domain.ResponseBySurveyor

	for result.Next(ctx) {
		var response domain.ResponseBySurveyor
		result.Decode(&response)
		responses = append(responses, response)
	}

	return responses
}

func (repository *ResponseRepositoryImpl) FindSummary(questionId string) domain.ResponseQuestionByRegency {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: questionId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "surveys"}, {Key: "localField", Value: "survey_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "survey"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$survey"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "options"}, {Key: "localField", Value: "_id"}, {Key: "foreignField", Value: "question_id"}, {Key: "as", Value: "options"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$option"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	pipeline := mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage1, unwindStage1}

	result, err := repository.CollectionQuestion.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var response domain.ResponseQuestionByRegency
	result.Next(ctx)
	result.Decode(&response)

	return response
}

func (repository *ResponseRepositoryImpl) FindSummaryByLocation(questionId, optionId string, regencyId []string) []domain.ResponseQuestionByLocation {
	ctx, cancel := config.NewDBContext()
	var regency []interface{}

	regency = append(regency, "$regency._id", regencyId)

	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "question_id", Value: questionId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "responses"}, {Key: "localField", Value: "response_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "response"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$response"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "response.location.village_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "village"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "districts"}, {Key: "localField", Value: "village.district_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "district"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$district"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage3 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "regencies"}, {Key: "localField", Value: "district.regency_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "regency"}}}}
	unwindStage3 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$regency"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	matchStage1 := bson.D{{Key: "$match", Value: bson.D{{Key: "$expr", Value: bson.D{{Key: "$in", Value: bson.A{"$regency._id", regencyId}}}}}}}
	matchStage2 := bson.D{{Key: "$match", Value: bson.D{{Key: "option_id", Value: optionId}}}}
	groupStage := bson.D{{Key: "$group", Value: bson.D{{Key: "_id", Value: "$village._id"}, {Key: "district_id", Value: bson.D{{Key: "$first", Value: "$village.district_id"}}}, {Key: "count", Value: bson.M{"$count": bson.M{}}}}}}

	pipeline := mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage1, unwindStage1, lookupStage2, unwindStage2, lookupStage3, unwindStage3, matchStage1, matchStage2, groupStage}

	result, err := repository.CollectionAnswer.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var responses []domain.ResponseQuestionByLocation
	for result.Next(ctx) {
		var response domain.ResponseQuestionByLocation
		result.Decode(&response)
		responses = append(responses, response)
	}

	return responses
}
