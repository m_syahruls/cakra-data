package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type CandidateRepositoryImpl struct {
	Collection *mongo.Collection
}

type CandidateRepository interface {
	Create(candidate domain.Candidate)
	FindAll(page, limit, partaiId, electionTypeId, daerahPemiluId string) []domain.Candidate
	FindById(candidateId string) (domain.Candidate, error)
}

func NewCandidateRepository(database *mongo.Database) CandidateRepository {
	return &CandidateRepositoryImpl{
		Collection: database.Collection("candidates"),
	}
}

func (repository *CandidateRepositoryImpl) Create(candidate domain.Candidate) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":              candidate.Id,
		"name":             candidate.Name,
		"election_type_id": candidate.PemiluTypeId,
		"partai_id":        candidate.PartaiId,
		"daerah_pemilu_id": candidate.DaerahPemiluId,
	})

	helper.PanicIfError(err)
}

func (repository *CandidateRepositoryImpl) FindAll(page, limit, partaiId, electionTypeId, daerahPemiluId string) []domain.Candidate {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "partai"}, {Key: "localField", Value: "partai_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "partai"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$partai"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage2 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "election_type"}, {Key: "localField", Value: "election_type_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "election_type"}}}}
	unwindStage2 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$election_type"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	query := mongo.Pipeline{lookupStage, unwindStage, lookupStage2, unwindStage2}

	helper.QueryCandidate(partaiId, electionTypeId, daerahPemiluId, &query)

	cursor, err := repository.Collection.Aggregate(ctx, query)
	helper.PanicIfError(err)

	var parties []domain.Candidate

	for cursor.Next(ctx) {
		var candidate domain.Candidate
		cursor.Decode(&candidate)
		parties = append(parties, candidate)
	}

	return parties
}

func (repository *CandidateRepositoryImpl) FindById(candidateId string) (domain.Candidate, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var candidate domain.Candidate
	query := bson.M{"_id": candidateId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&candidate)

	return candidate, result.Err()
}
