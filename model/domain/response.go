package domain

import "time"

type Response struct {
	ID                    string                `json:"id" bson:"_id"`
	SurveyId              string                `json:"survey_id" bson:"survey_id"`
	Survey                Survey                `json:"survey" bson:"survey"`
	User                  User                  `json:"user" bson:"user"`
	RespondentId          string                `json:"respondent_id" bson:"respondent_id"`
	ProgressSurveySummary ProgressSurveySummary `json:"progress_summary" bson:"progress_summary"`
	Surveyor              User                  `json:"surveyor" bson:"surveyor"`
	CreatedAt             time.Time             `json:"created_at" bson:"created_at"`
	Location              interface{}           `json:"location" bson:"location"`
	Answers               []Answer              `json:"answers" bson:"answers"`
}

type Location struct {
	ID         string `json:"id" bson:"_id"`
	ResponseId string `json:"response_id" bson:"response_id"`
	Address    string `json:"address" bson:"address"`
	Longitude  string `json:"longitude" bson:"longitude"`
	Latitude   string `json:"latitude" bson:"latitude"`
}

type ResponseBySurveyor struct {
	Id           string          `json:"id" bson:"_id"`
	SurveyName   string          `json:"survey_name" bson:"survey_name"`
	SurveyorName string          `json:"surveyor_name" bson:"surveyor_name"`
	Response     []ResponsesUser `json:"response" bson:"response"`
}

type ResponsesUser struct {
	ResponseId     string    `json:"response_id" bson:"response_id"`
	RespondentId   string    `json:"respondent_id" bson:"respondent_id"`
	RespondentName string    `json:"respondent_name" bson:"respondent_name"`
	CreatedAt      time.Time `json:"created_at" bson:"created_at"`
}

type ResponseQuestionByRegency struct {
	QuestionID      string    `json:"question_id" bson:"_id"`
	QuestionName    string    `json:"question_name" bson:"question_name"`
	SurveyId        string    `json:"survey_id" bson:"survey_id"`
	QuestionSubject string    `json:"question_subject" bson:"question_subject"`
	Survey          Survey    `json:"survey" bson:"survey"`
	Options         []Options `json:"options" bson:"options"`
}

type ResponseQuestionByLocation struct {
	VillageId string      `json:"village_id" bson:"_id"`
	District  string      `json:"district_id" bson:"district_id"`
	Count     interface{} `json:"count" bson:"count"`
}
