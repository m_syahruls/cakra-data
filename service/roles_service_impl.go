package service

import (
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type RoleServiceImpl struct {
	RoleRepository repository.RoleRepository
	Validate       *validator.Validate
}

type RoleService interface {
	Create(request web.RoleCreateRequest) web.RoleResponse
	FindById(roleId string) web.RoleResponse
	FindAll(label string, value string) []web.RoleResponse
	Update(roleId string, request web.RoleUpdateRequest) web.RoleResponse
	AssignPermission(roleId string, request web.RoleUpdateRequest) web.RoleResponse
	Delete(roleId string)
}

func NewRoleService(roleRepository repository.RoleRepository, validate *validator.Validate) RoleService {
	return &RoleServiceImpl{
		RoleRepository: roleRepository,
		Validate:       validate,
	}
}

func (service *RoleServiceImpl) Create(request web.RoleCreateRequest) web.RoleResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isRoleExist := service.RoleRepository.FindIsExist(request.Label, request.Value)
	if isRoleExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Role already exists"))
	}

	role := domain.Role{
		ID:         strings.ToLower(randstr.String(10)),
		Label:      request.Label,
		Value:      request.Value,
		Permission: request.Permission,
	}

	service.RoleRepository.Create(role)

	return helper.ToRoleResponse(role)
}

func (service *RoleServiceImpl) FindById(roleId string) web.RoleResponse {
	role, err := service.RoleRepository.FindById(roleId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Role not found"))
	}

	return helper.ToRoleResponse(role)
}

func (service *RoleServiceImpl) FindAll(label string, value string) []web.RoleResponse {
	roles := service.RoleRepository.FindAll(label, value)
	return helper.ToRoleResponses(roles)
}

func (service *RoleServiceImpl) Update(roleId string, request web.RoleUpdateRequest) web.RoleResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	isRoleExist := service.RoleRepository.FindIsExist(request.Label, request.Value)
	if isRoleExist {
		panic(exception.NewError(fiber.StatusBadRequest, "Role already exists"))
	}

	role, err := service.RoleRepository.FindById(roleId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Role not found"))
	}

	role.Label = request.Label
	role.Value = request.Value
	// role.Permission = request.Permission

	service.RoleRepository.Update(role, roleId)

	return helper.ToRoleResponse(role)
}

func (service *RoleServiceImpl) AssignPermission(roleId string, request web.RoleUpdateRequest) web.RoleResponse {
	role, err := service.RoleRepository.FindById(roleId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Role not found"))
	}

	role.Permission = request.Permission

	service.RoleRepository.AssignPermission(role, roleId)

	return helper.ToRoleResponse(role)
}

func (service *RoleServiceImpl) Delete(roleId string) {
	role, err := service.RoleRepository.FindById(roleId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "Role not found"))
	}

	err = service.RoleRepository.Delete(roleId)
	helper.PanicIfError(err)
	log.Println("LOG ", "deleted role", role.Label)
}
