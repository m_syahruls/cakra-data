package web

// 200 OK User Details

type AyrshareUserDetail struct {
	ActiveSocialAccounts []string              `json:"activeSocialAccounts"`
	Created              ayrshareCreated       `json:"created"`
	DisplayNames         []ayrshareDisplayName `json:"displayNames"`
	Email                string                `json:"email"`
	MonthlyApiCalls      int                   `json:"monthlyApiCalls"`
	ReferenceID          string                `json:"refId"`
	Title                string                `json:"title"`
}

type ayrshareCreated struct {
	Seconds     int `json:"_seconds"`
	Nanoseconds int `json:"_nanoseconds"`
	UTC         int `json:"utc"`
}

type ayrshareDisplayName struct {
	ID          string `json:"id"`
	Username    string `json:"username"`
	DisplayName string `json:"displayName"`
	Platform    string `json:"platform"`
	ProfileURL  string `json:"profileUrl"`
	UserImage   string `json:"userImage"`
	Created     string `json:"created"`
}

// end of 200 User Details

// 403 Forbidden

type AyrshareForbidden struct {
	Action  string `json:"action"`
	Status  string `json:"status"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// end of 403

// 401 Unauthorized

// end of 401

type AyrshareEligibility struct {
	Eligibility bool   `json:"eligibility"`
	Message     string `json:"message"`
	Code        int    `json:"code"`
}

// 200 OK Post to Platform

type AyrsharePostResponse struct {
	Status      string              `json:"status"`
	Errors      []ayrsharePostError `json:"errors"`
	PostID      []ayrsharePostID    `json:"postIds"`
	ID          string              `json:"id"`
	ReferenceID string              `json:"refId"`
	Post        string              `json:"post"`
}

type ayrsharePostError struct {
	Action   string `json:"action"`
	Status   string `json:"status"`
	Code     int    `json:"code"`
	Message  string `json:"message"`
	Platform string `json:"platform"`
}

type ayrsharePostID struct {
	Status   string `json:"status"`
	ID       string `json:"id"`
	PostUrl  string `json:"postUrl"`
	Platform string `json:"platform"`
}

// end of 200 Post to Platform

type AyrshareUserAnalyticsRequest struct {
	Platforms []string `json:"platforms"`
}

type AyrshareAnalyticsResponse struct {
	Twitter   ayrshareAnalyticsTwitter   `json:"twitter"`
	Facebook  ayrshareAnalyticsFacebook  `json:"facebook"`
	Instagram ayrshareAnalyticsInstagram `json:"instagram"`
	Status    string                     `json:"status"`
}

type ayrshareAnalyticsTwitter struct {
	Analytics struct {
		Created         string `json:"created"`
		CreatedAt       string `json:"createdAt"`
		Description     string `json:"description"`
		DisplayName     string `json:"displayName"`
		FavoritesCount  int    `json:"favoritesCount"`
		FollowersCount  int    `json:"followersCount"`
		FriendsCount    int    `json:"friendsCount"`
		ID              int    `json:"id"`
		LikedCount      int    `json:"likedCount"`
		ListedCount     int    `json:"listedCount"`
		Location        string `json:"location"`
		Name            string `json:"name"`
		ProfileImageUrl string `json:"profileImageUrl"`
		Protected       bool   `json:"protected"`
		TweetCount      int    `json:"tweetCount"`
		URL             string `json:"url"`
		Username        string `json:"username"`
		Verified        bool   `json:"verified"`
	} `json:"analytics"`
	LastUpdated string `json:"lastUpdated"`
	NextUpdate  string `json:"nextUpdate"`
}

type ayrshareAnalyticsFacebook struct {
	Analytics struct {
		Birthday                          string      `json:"birthday"`
		FanCount                          int         `json:"fanCount"`
		FollowersCount                    int         `json:"followersCount"`
		ID                                string      `json:"id"`
		Link                              string      `json:"link"`
		Name                              string      `json:"name"`
		PageConsumptions                  int         `json:"pageConsumptions"`
		PageConsumptionsByConsumptionType interface{} `json:"pageConsumptionsByConsumptionType"`
		PageEngagedUsers                  int         `json:"pageEngagedUsers"`
		PageFanRemoves                    int         `json:"pageFanRemoves"`
		PageFansByLikeSource              interface{} `json:"pageFansByLikeSource"`
		PageFansByLikeSourceUnique        interface{} `json:"pageFansByLikeSourceUnique"`
		PageImpressions                   int         `json:"pageImpressions"`
		PageImpressionsPaid               int         `json:"pageImpressionsPaid"`
		PagePostEngagements               int         `json:"pagePostEngagements"`
		PagePostsImpressions              int         `json:"pagePostsImpressions"`
		PagePostsImpressionsPaid          int         `json:"pagePostsImpressionsPaid"`
		PageVideoViewTime                 int         `json:"pageVideoViewTime"`
		PageVideoViews                    int         `json:"pageVideoViews"`
		PageVideoViewsPaid                int         `json:"pageVideoViewsPaid"`
		Reactions                         struct {
			Anger int `json:"anger"`
			Like  int `json:"like"`
			Love  int `json:"love"`
			Wow   int `json:"wow"`
			Haha  int `json:"haha"`
			Sorry int `json:"sorry"`
			Total int `json:"total"`
		} `json:"reactions"`
		Verified bool `json:"verified"`
	} `json:"analytics"`
	LastUpdated string `json:"lastUpdated"`
	NextUpdate  string `json:"nextUpdate"`
}

type ayrshareAnalyticsInstagram struct {
	Analytics struct {
		AudienceCity         interface{} `json:"audienceCity"`
		AudienceCountry      interface{} `json:"audienceCountry"`
		AudienceGenderAge    interface{} `json:"audienceGenderAge"`
		AudienceLocale       interface{} `json:"audienceLocale"`
		Biography            string      `json:"biography"`
		CommentsCount        int         `json:"commentsCount"`
		EmailContactsCount   int         `json:"emailContactsCount"`
		FollowersCount       int         `json:"followersCount"`
		FollowingCount       int         `json:"followingCOunt"`
		ID                   string      `json:"id"`
		IgId                 int         `json:"igId"`
		ImpressionsCount     int         `json:"impressionsCount"`
		LikeCount            int         `json:"likeCount"`
		MediaCount           int         `json:"mediaCount"`
		Name                 string      `json:"name"`
		PhoneCallClicksCount int         `json:"phoneCallClicksCount"`
		ProfilePictureUrl    string      `json:"profilePictureUrl"`
		ProfileViewsCount    int         `json:"profileViewsCount"`
		ReachCount           int         `json:"reachCount"`
		TextMessageClicks    int         `json:"textMessageClicks"`
		Username             string      `json:"username"`
		Website              string      `json:"website:"`
		WebsiteClicksCount   int         `json:"websiteClicksCount"`
	} `json:"analytics"`
	LastUpdated string `json:"lastUpdated"`
	NextUpdate  string `json:"nextUpdate"`
}

// Ayrshare Post Requests

type AyrsharePostRequest struct {
	Post         string   `json:"post" validate:"required"`
	Platforms    []string `json:"platforms" validate:"required"`
	MediaUrls    []string `json:"mediaUrls"`
	ScheduleDate string   `json:"scheduleDate"`
}

// end of Post Requests
