package repository

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type PermissionRepositoryImpl struct {
	Collection *mongo.Collection
}

type PermissionRepository interface {
	Create(permission domain.Permission)
	FindById(permissionId string) (domain.Permission, error)
	FindAll(label string, value string) []domain.Permission
	FindIsExist(label string, value string) bool
	Update(permission domain.Permission, permissionId string)
	Delete(permissionId string) error
}

func NewPermissionRepository(database *mongo.Database) PermissionRepository {
	return &PermissionRepositoryImpl{
		Collection: database.Collection("permissions"),
	}
}

func (repository *PermissionRepositoryImpl) Create(permission domain.Permission) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.InsertOne(ctx, bson.M{
		"_id":   permission.ID,
		"label": permission.Label,
		"value": permission.Value,
	})

	helper.PanicIfError(err)
}

func (repository *PermissionRepositoryImpl) FindById(permissionId string) (domain.Permission, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	var permission domain.Permission
	query := bson.M{"_id": permissionId}

	result := repository.Collection.FindOne(ctx, query)
	result.Decode(&permission)

	return permission, result.Err()
}

func (repository *PermissionRepositoryImpl) FindAll(label string, value string) []domain.Permission {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssue("", label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	var permissions []domain.Permission

	for cursor.Next(ctx) {
		var permission domain.Permission
		cursor.Decode(&permission)
		permissions = append(permissions, permission)
	}

	return permissions
}

func (repository *PermissionRepositoryImpl) FindIsExist(label string, value string) bool {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := helper.QueryIssueExist(label, value)

	cursor, err := repository.Collection.Find(ctx, query)
	helper.PanicIfError(err)

	for cursor.Next(ctx) {
		return true
	}

	return false
}

func (repository *PermissionRepositoryImpl) Update(permission domain.Permission, permissionId string) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	query := bson.M{"_id": permissionId}
	update := bson.M{
		"$set": bson.M{
			"label": permission.Label,
			"value": permission.Value,
		},
	}

	_, err := repository.Collection.UpdateOne(ctx, query, update)

	helper.PanicIfError(err)
}

func (repository *PermissionRepositoryImpl) Delete(permissionId string) error {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	_, err := repository.Collection.DeleteOne(ctx, bson.M{
		"_id": permissionId,
	})

	helper.PanicIfError(err)

	return nil
}
