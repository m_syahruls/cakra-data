package domain

import "time"

type SocialmediaLastTotal struct {
	ID        string               `json:"id" bson:"_id"`
	Platform  string               `json:"platform"`
	Total     SocialMediaDatapoint `json:"total" bson:"total"`
	CreatedAt time.Time            `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time            `json:"updated_at" bson:"updated_at"`
}

type SocialmediaHistorical struct {
	ID            string               `json:"id" bson:"_id"`
	SocialMediaId string               `json:"socialmedia_id" bson:"socialmedia_id"`
	Platform      string               `json:"platform"`
	Historical    SocialMediaDatapoint `json:"historical" bson:"historical"`
	CreatedAt     time.Time            `json:"created_at" bson:"created_at"`
}

type SocialMediaDatapoint struct {
	Likes           int `json:"likes" bson:"likes"`
	Followers       int `json:"followers" bson:"followers"`
	Following       int `json:"following" bson:"following"`
	Engagement      int `json:"engagement" bson:"engagement"`
	Impressions     int `json:"impressions" bson:"impressions"`
	ImpressionsPaid int `json:"impressions_paid" bson:"impressions_paid"`
	VideoViewTime   int `json:"video_view_time"`
	VideoViews      int `json:"video_views"`
	VideoViewsPaid  int `json:"video_views_paid"`
	Reactions       int `json:"reactions"`
	UserLikes       int `json:"user_likes" bson:"user_likes"`
	UserTweetCount  int `json:"user_tweet_count" bson:"user_tweet_count"`
	Comments        int `json:"comments" bson:"comments"`
	PostCount       int `json:"post_count" bson:"post_count"`
	ProfileViews    int `json:"profile_views" bson:"profile_views"`
	Reach           int `json:"reach" bson:"reach"`
}
