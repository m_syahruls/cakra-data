package domain

type KpuData struct {
	Id         string `json:"id" bson:"_id"`
	VillageId  string `json:"villageid" bson:"villageid"`
	DistrictId string `json:"district_id" bson:"district_id"`
	PKB        int32  `json:"pkb" bson:"pkb"`
	Gerindra   int32  `json:"gerindra" bson:"gerindra"`
	PDIP       int32  `json:"pdip" bson:"pdip"`
	Golkar     int32  `json:"golkar" bson:"golkar"`
	NasDem     int32  `json:"nasdem" bson:"nasdem"`
	Garuda     int32  `json:"garuda" bson:"garuda"`
	Berkarya   int32  `json:"berkarya" bson:"berkarya"`
	PKS        int32  `json:"pks" bson:"pks"`
	Perindo    int32  `json:"perindo" bson:"perindo"`
	PPP        int32  `json:"ppp" bson:"ppp"`
	PSI        int32  `json:"psi" bson:"psi"`
	PAN        int32  `json:"pan" bson:"pan"`
	Hanura     int32  `json:"hanura" bson:"hanura"`
	Demokrat   int32  `json:"demokrat" bson:"demokrat"`
	PBB        int32  `json:"pbb" bson:"pbb"`
	PKPI       int32  `json:"pkpi" bson:"pkpi"`
}
