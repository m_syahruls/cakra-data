package controller

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type AccessControllerImpl struct {
	AccessService service.AccessService
	LogService    service.LogService
}

type AccessController interface {
	NewAccessRouter(app *fiber.App)
}

func NewAccessController(accessService service.AccessService, logService service.LogService) AccessController {
	return &AccessControllerImpl{
		AccessService: accessService,
		LogService:    logService,
	}
}

func (controller *AccessControllerImpl) NewAccessRouter(app *fiber.App) {
	access := app.Group("/accesses")
	access.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	// //Restricted (JUST FOR ADMIN)
	// access.Use(middlewares.IsAuthenticated)
	access.Post("/", controller.Create)
	access.Get("/:accessId", controller.FindById)
	access.Get("/", controller.FindAll)
	access.Put("/:accessId", controller.Update)
	access.Delete("/:accessId", controller.Delete)
}

func (controller *AccessControllerImpl) Create(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can create access")
	// }

	var request web.AccessCreateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	accessResponse := controller.AccessService.Create(request)

	action := fmt.Sprintf("create access %s", accessResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    accessResponse,
	})
}

func (controller *AccessControllerImpl) FindById(ctx *fiber.Ctx) error {
	accessId := ctx.Params("accessId")

	accessResponse := controller.AccessService.FindById(accessId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    accessResponse,
	})
}

func (controller *AccessControllerImpl) FindAll(ctx *fiber.Ctx) error {
	label := ctx.Query("label")
	value := ctx.Query("value")
	accessResponse := controller.AccessService.FindAll(label, value)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    accessResponse,
	})
}

func (controller *AccessControllerImpl) Update(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can update access")
	// }

	var request web.AccessUpdateRequest
	err := ctx.BodyParser(&request)
	helper.PanicIfError(err)

	accessId := ctx.Params("accessId")

	accessResponse := controller.AccessService.Update(accessId, request)

	action := fmt.Sprintf("update access %s", accessResponse.Label)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    accessResponse,
	})
}

func (controller *AccessControllerImpl) Delete(ctx *fiber.Ctx) error {
	// if isAdmin := middlewares.IsAdmin(ctx); !isAdmin {
	// 	return UnauthorizeReturn(ctx, "only admin can delete access")
	// }

	accessId := ctx.Params("accessId")

	controller.AccessService.Delete(accessId)

	action := fmt.Sprintf("delete access %s", accessId)
	actor, _, _ := helper.ParseJwt(ctx.Cookies("token"))
	controller.LogService.Create(actor, action)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}
