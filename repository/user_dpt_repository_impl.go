package repository

import (
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/config"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
)

type UserDptRepositoryImpl struct {
	Collection *mongo.Collection
}

type UserDptRepository interface {
	InsertMany(userDpt []interface{})
	FindById(userDptId string) (domain.UserDpt, error)
	FindAll(page, limit, pollStationId, villageId, searchName string) []domain.UserDpt
}

func NewUserDptRepository(database *mongo.Database) UserDptRepository {
	return &UserDptRepositoryImpl{
		Collection: database.Collection("data_dpt"),
	}
}

func (repository *UserDptRepositoryImpl) InsertMany(userDpt []interface{}) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	options := options.InsertMany()
	options.SetOrdered(false)
	_, err := repository.Collection.InsertMany(ctx, userDpt, options)

	helper.PanicIfError(err)
}

func (repository *UserDptRepositoryImpl) FindById(userDptId string) (domain.UserDpt, error) {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	matchStage := bson.D{{Key: "$match", Value: bson.D{{Key: "_id", Value: userDptId}}}}
	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "village_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "village"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "data_tps"}, {Key: "localField", Value: "polling_station_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "polling_station"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$polling_station"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	pipeline := mongo.Pipeline{matchStage, lookupStage, unwindStage, lookupStage1, unwindStage1}

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	result.Next(ctx)
	var userDpt domain.UserDpt
	result.Decode(&userDpt)

	return userDpt, result.Err()
}

func (repository *UserDptRepositoryImpl) FindAll(page, limit, pollStationId, villageId, searchName string) []domain.UserDpt {
	ctx, cancel := config.NewDBContext()
	defer cancel()

	// (page, limit, sort)
	if page == "" {
		page = "1"
	}
	if limit == "" {
		limit = os.Getenv("DEFAULT_LIMIT")
	}

	lookupStage := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "villages"}, {Key: "localField", Value: "village_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "village"}}}}
	unwindStage := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$village"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}
	lookupStage1 := bson.D{{Key: "$lookup", Value: bson.D{{Key: "from", Value: "data_tps"}, {Key: "localField", Value: "polling_station_id"}, {Key: "foreignField", Value: "_id"}, {Key: "as", Value: "polling_station"}}}}
	unwindStage1 := bson.D{{Key: "$unwind", Value: bson.D{{Key: "path", Value: "$polling_station"}, {Key: "preserveNullAndEmptyArrays", Value: true}}}}

	pipeline := mongo.Pipeline{lookupStage, unwindStage, lookupStage1, unwindStage1}

	helper.QueryUserDpt(pollStationId, villageId, searchName, &pipeline)

	helper.PaggingAggregat(page, limit, &pipeline)

	result, err := repository.Collection.Aggregate(ctx, pipeline)
	helper.PanicIfError(err)

	var userDpts []domain.UserDpt

	for result.Next(ctx) {
		var userDpt domain.UserDpt
		result.Decode(&userDpt)
		userDpts = append(userDpts, userDpt)
	}

	return userDpts
}
