package service

import (
	"log"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/bson"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/domain"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type OptionServiceImpl struct {
	OptionRepository   repository.OptionRepository
	QuestionRepository repository.QuestionRepository
	Validate           validator.Validate
}

type OptionService interface {
	Create(request web.OptionCreateRequest) web.OptionResponse
	FindById(optionId string) web.OptionResponse
	FindAll(page, limit, questionId string) []web.OptionResponse
	Update(optionId string, request web.OptionUpdateRequest) web.OptionResponse
	Delete(optionId string)
}

func NewOptionService(optionRepository repository.OptionRepository, questionRepository repository.QuestionRepository, validate *validator.Validate) OptionService {
	return &OptionServiceImpl{
		OptionRepository:   optionRepository,
		QuestionRepository: questionRepository,
		Validate:           *validate,
	}
}

func (service *OptionServiceImpl) Create(request web.OptionCreateRequest) web.OptionResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	_, err = service.QuestionRepository.FindById(request.QuestionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "question not found"))
	}

	valueOptionExist, _ := service.OptionRepository.FindByQuery(bson.D{{Key: "value", Value: request.Value}, {Key: "question_id", Value: request.QuestionId}})
	if valueOptionExist.ID != "" {
		panic(exception.NewError(fiber.StatusBadRequest, "this value already exist"))
	}

	option := domain.Options{
		ID:         strings.ToLower(randstr.String(10)),
		QuestionId: request.QuestionId,
		OptionName: request.OptionName,
		Value:      request.Value,
		Color:      request.Color,
	}

	service.OptionRepository.Create(option)

	return helper.ToOptionResponse(option)
}

func (service *OptionServiceImpl) FindById(optionId string) web.OptionResponse {
	option, err := service.OptionRepository.FindById(optionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "option not found"))
	}

	return helper.ToOptionResponse(option)
}

func (service *OptionServiceImpl) FindAll(page, limit, questionId string) []web.OptionResponse {
	option := service.OptionRepository.FindAll(page, limit, questionId)
	return helper.ToOptionResponses(option)
}

func (service *OptionServiceImpl) Update(optionId string, request web.OptionUpdateRequest) web.OptionResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	option, err := service.OptionRepository.FindById(optionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "option not found"))
	}

	optionExist, _ := service.OptionRepository.FindByQuery(bson.D{{Key: "value", Value: request.Value}, {Key: "question_id", Value: option.QuestionId}})
	if optionExist.ID != "" && optionExist.ID != optionId {
		panic(exception.NewError(fiber.StatusBadRequest, "this value already exist"))
	}

	option.OptionName = request.OptionName
	option.Value = request.Value

	if request.Color != "" {
		option.Color = request.Color
	}

	service.OptionRepository.Update(option)

	return helper.ToOptionResponse(option)
}

func (service *OptionServiceImpl) Delete(optionId string) {
	option, err := service.OptionRepository.FindById(optionId)
	if err != nil {
		panic(exception.NewError(fiber.StatusNotFound, "option not found"))
	}

	err = service.OptionRepository.Delete(optionId)
	helper.PanicIfError(err)
	log.Println("LOG ", "deleted survey", option.OptionName)
}
