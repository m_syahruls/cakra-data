package service

import (
	"github.com/go-playground/validator/v10"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/helper"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/repository"
)

type IssueManagementDataServiceImpl struct {
	IssueManagementDataRepository repository.IssueManagementDataRepository
	Validate                      *validator.Validate
}

type IssueManagementDataService interface {
	FindAll(page, limit, name string, issue string, sub_issue string, date string, created_at string) []web.IssueManagementDataResponse
	// Delete(issueSubId string)
}

func NewIssueManagementDataService(IssueManagementDataRepository repository.IssueManagementDataRepository, validate *validator.Validate) IssueManagementDataService {
	return &IssueManagementDataServiceImpl{
		IssueManagementDataRepository: IssueManagementDataRepository,
		Validate:                      validate,
	}
}

func (service *IssueManagementDataServiceImpl) FindAll(page, limit, name string, issue string, sub_issue string, date string, created_at string) []web.IssueManagementDataResponse {
	IssueManagementData := service.IssueManagementDataRepository.FindAll(page, limit, name, issue, sub_issue, date, created_at)
	return helper.ToIssueManagementDataResponses(IssueManagementData)
}

// func (service *IssueManagementDataServiceImpl) Delete(issueSubId string) {
// 	issueSub, err := service.IssueManagementDataRepository.FindById(issueSubId)
// 	if err != nil {
// 		panic(exception.NewError(fiber.StatusNotFound, "Sub issue not found"))
// 	}

// 	err = service.IssueManagementDataRepository.Delete(issueSubId)
// 	helper.PanicIfError(err)
// 	log.Println("LOG", "deleted sub issue", issueSub.Label)
// }
