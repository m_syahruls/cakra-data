package controller

import (
	"encoding/json"
	"io/ioutil"

	"github.com/gofiber/fiber/v2"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/exception"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/model/web"
	"repo.synapsis.id/software/projects/patrons/patron-monitoring/backend/core-service/service"
)

type UserDptControllerImpl struct {
	UserDptService service.UserDptService
}

type UserDptController interface {
	NewUserDptRouter(app *fiber.App)
}

func NewUserDptController(userDptService service.UserDptService) UserDptController {
	return &UserDptControllerImpl{
		UserDptService: userDptService,
	}
}

func (controller *UserDptControllerImpl) NewUserDptRouter(app *fiber.App) {
	userDpt := app.Group("/userdpt")
	userDpt.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
			Code:    fiber.StatusOK,
			Status:  true,
			Message: "ok",
		})
	})

	userDpt.Post("/", controller.Create)
	userDpt.Get("/:userDptid", controller.FindById)
	userDpt.Get("/", controller.FindAll)
}

func (controller *UserDptControllerImpl) Create(ctx *fiber.Ctx) error {
	var users []map[string]interface{}
	file, _ := ctx.FormFile("file")
	open, err := file.Open()
	if err != nil {
		panic(exception.NewError(fiber.StatusBadRequest, "error read file"))
	}
	byteValue, _ := ioutil.ReadAll(open)

	json.Unmarshal(byteValue, &users)

	controller.UserDptService.CreateMany(users)

	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    nil,
	})
}

func (controller *UserDptControllerImpl) FindById(ctx *fiber.Ctx) error {
	userDptId := ctx.Params("userDptid")

	userDptResponse := controller.UserDptService.FindById(userDptId)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userDptResponse,
	})
}

func (controller *UserDptControllerImpl) FindAll(ctx *fiber.Ctx) error {
	page := ctx.Query("page")
	limit := ctx.Query("limit")
	pollStationId := ctx.Query("pollstationid")
	villageId := ctx.Query("villageid")
	searchName := ctx.Query("name")
	userDptResponse := controller.UserDptService.FindAll(page, limit, pollStationId, villageId, searchName)
	return ctx.Status(fiber.StatusOK).JSON(web.WebResponse{
		Code:    fiber.StatusOK,
		Status:  true,
		Message: "success",
		Data:    userDptResponse,
	})
}
