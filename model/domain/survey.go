package domain

import "time"

type Survey struct {
	ID              string      `json:"id" bson:"_id"`
	SurveyName      string      `json:"survey_name" bson:"survey_name"`
	Status          int         `json:"status" bson:"status"`
	TotalRespondent int         `json:"total_respondent" bson:"total_respondent"`
	CreatedAt       time.Time   `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time   `json:"updated_at" bson:"updated_at"`
	Questions       []Questions `json:"questions" bson:"questions"`
}

type SurveyWithResponseId struct {
	ID              string      `json:"id" bson:"_id"`
	SurveyName      string      `json:"survey_name" bson:"survey_name"`
	Status          int         `json:"status" bson:"status"`
	TotalRespondent int         `json:"total_respondent" bson:"total_respondent"`
	CreatedAt       time.Time   `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time   `json:"updated_at" bson:"updated_at"`
	ResponseId      string      `json:"response_id" bson:"response_id"`
	Questions       []Questions `json:"questions" bson:"questions"`
}
